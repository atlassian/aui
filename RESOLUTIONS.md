# NPM package resolution overrides

## Check vulnerabilities locally

`SNYK_TOKEN=XXX npx --yes snyk test --yarn-workspaces --dev --exclude=p2 --strict-out-of-sync=false --json-file-output=snyk-output-yarn.json`

You can obtain SNYK_TOKEN by going to `go/snyk` > profile > account settings > API Token

## Unpatchable

https://security.snyk.io/vuln/SNYK-JS-INFLIGHT-6095116 introduced in multiple paths

`inflight` is no longer maintained and there is no fixed version. This dependency usually comes from `glob` which removes `inflight` in version 11, but it can't be easily upgraded because of api changes.

https://security.snyk.io/vuln/SNYK-JS-LODASHTEMPLATE-1088054 in lodash.template@2.4.1 introduced by gulp-webserver@0.9.1 > gulp-util@2.2.20 > lodash.template@2.4.1

We patch it manually `gulp-util` to only export color/ansi which doesn't depend on lodash.template
