# 9.14.0 [Unreleased]
* [Documentation](https://aui.atlassian.com/aui/9.14/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.14.0)

# 9.13.2 [Unreleased]
* [Documentation](https://aui.atlassian.com/aui/9.13/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.13.2)

# 9.13.1
* [Documentation](https://aui.atlassian.com/aui/9.13/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.13.1)

## Fixed
* `com.atlassian.auiplugin:aui-design-tokens-themes` is now correctly declared as dependency of `design-tokens-api` and `design-tokens-api-full` (DCA11Y-1587)

# 9.13.0
* [Documentation](https://aui.atlassian.com/aui/9.13/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.13.0)

## Changed
* Changed App header bottom padding from 10px to 8px to match Atlaskit header [Atlaskit Atlassian Navigation header](https://atlassian.design/components/atlassian-navigation/examples) (DCA11Y-1213)
* Changed root boundary to 'document' instead of the frame to address datepicker alignment issue in an iframe (AUI-5497)
* (internal) The a11y tests now use a11y-runner via npx avoiding the need to install it otherwise,
  cutting vulnerability chains and other pipeline stage execution times at the same time (DCA11Y-1338)
* Remove focus trap from the flag component(COM-1088)
* (internal) Builds are now using Bamboo instead of Bitbucket Pipelines (DCA11Y-1356)
* (internal) The integration tests are now using LambdaTest instead of BrowserStack (DCA11Y-1426)
* (internal) The theme css resources now have WRM dependencies on the Look and Feel web resource provided by the LookAndFeel plugin (DCA11Y-1521)
* P2 plugin now builds with Platform 7 (DCA11Y-1560)
* update `@atlaskit/tokens` to 3.3.1 (DCA11Y-1588)

## Fixed
* Fix a flag focus order which caused the accessibility issue (SSDF-778)
* Select2: `.select2-container-multi` was missing AUI overrides when combined with `.select2-container-disabled` (DCA11Y-1354)
* hover state for input fields is consistent and as close to Atlaskit as possible (DCA11Y-1401)
* removed unnecessary table elements roles - grid, columnheader, row (A11Y-307)

## Added
* experimental Look and Feel support: set colors in app header based on `--atl-theme-primary-color`
  (DCA11Y-1446)
* second iteration of Look and feel support: rename `--atl-theme-primary-color` to
  `--atl-theme-header-bg-color`; provide other input colors (DCA11Y-1495)

# 9.12.7 [Unreleased]
* [Documentation](https://aui.atlassian.com/aui/9.12/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.12.7)

## Changed
* (internal) Builds are now using Bamboo instead of Bitbucket Pipelines (DCA11Y-1356)
* (internal) The integration tests are now using LambdaTest instead of BrowserStack (DCA11Y-1426)
* P2 plugin now builds with Platform 7 (DCA11Y-1560)

## Fixed
* Select2: `.select2-container-multi` was missing AUI overrides when combined with `.select2-container-disabled` (DCA11Y-1354)
* hover state for input fields is consistent and as close to Atlaskit as possible (DCA11Y-1401)

# 9.12.6
* [Documentation](https://aui.atlassian.com/aui/9.12/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.12.6)

## Added
* App header - To make icon button rounded background on hover use `aui-round-button` class

## Changed
* Docs - Change the background for all component examples to use ADS-like checkerboard to increase clarity

## Fixed
* App header - Quick search - Fixed the background color by matching it with `--ds-background-input` token

# 9.12.5
* [Documentation](https://aui.atlassian.com/aui/9.12/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.12.5)

## Changed
* Form fields' border width is now 1px instead of 2px, because the new color is
  less subtle, so a 2px-width was drawing too much attention to itself. The
  focused state of controls (header quick search, text, password, textarea,
  select, multi-select, Select2) is now using box-shadow, consistent with the
  rest of components, instead of changing border color. (DCA11Y-1322)

# 9.12.4
* [Documentation](https://aui.atlassian.com/aui/9.12/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.12.4)

## Changed
* (internal) The a11y tests now use a11y-runner via npx avoiding the need to install it otherwise,
  cutting vulnerability chains and other pipeline stage execution times at the same time (DCA11Y-1338)
* Remove focus trap from the flag component(COM-1088)

# 9.12.3
* [Documentation](https://aui.atlassian.com/aui/9.12/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.12.2)

## Fixed
* Address a package publishing issue that caused public versions to not install (DCA11Y-1314)

# 9.12.2
* [Documentation](https://aui.atlassian.com/aui/9.12/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.12.2)

## Changed
* Changed App header bottom padding from 10px to 8px to match Atlaskit header [Atlaskit Atlassian Navigation header](https://atlassian.design/components/atlassian-navigation/examples) (DCA11Y-1213)
* Changed root boundary to 'document' instead of the frame to address datepicker alignment issue in an iframe (AUI-5497)

## Fixed
* Fix a flag focus order which caused the accessibility issue (SSDF-778)

# 9.12.1
* [Documentation](https://aui.atlassian.com/aui/9.12/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.12.1)

## Added
* Flag now can take ariaLabel and ariaDescription; default label is based on type (AUI-5495)

# 9.12.0
* [Documentation](https://aui.atlassian.com/aui/9.12/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.12.0)

## Changed
* The font-size of `html` (thus `rem`) and it's direct children elements to provide better forward compatibility with Atlaskit, see the upgrade guide for more details. (AUI-5494)
* Math divisions in LESS files are wrapped in parantheses to comply with the default value of the
  [`math` option](https://lesscss.org/usage/#less-options-math) in LESS v4 (AUI-5488)
* (internal) The builds in Bitbucket Pipelines are now using the `sfp-base-agent` image, shared with the builds in Bamboo (DCA11Y-956)
* (internal) The builds in Bitbucket Pipelines now use Node 22 (DCA11Y-1029)
* (internal) Changed the bundle size reporting during Bitbucket Pipelines builds to be more
  developer-friendly (DCA11Y-750)
* Upgrade to LESS v4 (AUI-5493)
* fix interacting with Tab in a dialog that has a group of radio buttons: jump out of the current group; when
  land on the checked radio of a group; or when none is checked, on the first or last depending on tabbing
  direction (DCA11Y-1110)

## Fixed
* Date picker initialized with input type as text retains its initial value (AUI-5484)

# 9.11.8 [Unreleased]
* [Documentation](https://aui.atlassian.com/aui/9.11/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.11.8)

## Changed
* (internal) The a11y tests now use a11y-runner via npx avoiding the need to install it otherwise,
  cutting vulnerability chains and other pipeline stage execution times at the same time (DCA11Y-1338)
* (internal) Builds are now using Bamboo instead of Bitbucket Pipelines (DCA11Y-1356)
* (internal) The integration tests are now using LambdaTest instead of BrowserStack (DCA11Y-1426)

# 9.11.7
* [Documentation](https://aui.atlassian.com/aui/9.11/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.11.7)

## Changed
* Changed App header bottom padding from 10px to 8px to match Atlaskit header [Atlaskit Atlassian Navigation header](https://atlassian.design/components/atlassian-navigation/examples) (DCA11Y-1213)

## Fixed
* Fix a flag focus order which caused the accessibility issue (SSDF-778)
* Address a package publishing issue that caused public versions to not install (DCA11Y-1314)

# 9.11.6
* [Documentation](https://aui.atlassian.com/aui/9.11/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.11.6)

## Added
* Flag now can take ariaLabel and ariaDescription; default label is based on type (AUI-5495)

# 9.11.5
* [Documentation](https://aui.atlassian.com/aui/9.11/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.11.5)

## Changed
* (internal) The builds in Bitbucket Pipelines now use Node 22 (DCA11Y-1029)
* (internal) Changed the bundle size reporting during Bitbucket Pipelines builds to be more
  developer-friendly (DCA11Y-750)
* Upgrade to LESS v4 (AUI-5493)

## Fixed
* fix interacting with Tab in a dialog that has a group of radio buttons: jump out of the current group; when
  land on the checked radio of a group; or when none is checked, on the first or last depending on tabbing
  direction (DCA11Y-1110)

# 9.11.4
* [Documentation](https://aui.atlassian.com/aui/9.11/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.11.4)

## Fixed
* (internal) fixed problems with `yarn` having problems installing the 9.11.3 version (DCA11Y-1094)

# 9.11.3
* [Documentation](https://aui.atlassian.com/aui/9.11/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.11.3)

## Changed
* (internal) The builds in Bitbucket Pipelines are now using the `sfp-base-agent` image, shared with the
  builds in Bamboo (DCA11Y-956)
* Math divisions in LESS files are wrapped in parantheses to comply with the default value of the
  [`math` option](https://lesscss.org/usage/#less-options-math) in LESS v4 (AUI-5488)

## Fixed
* Select2: can't always find its `dropdownLayer` when closing (DCA11Y-1063)

# 9.11.2
* [Documentation](https://aui.atlassian.com/aui/9.11/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.11.2)

## Fixed
* A button inside a dropdown now once again fills its width on Firefox, due to a `width: -moz-available`
  value that had been lost for a while (DCA11Y-996)
* inline-dialog is compatible with jquery 3. The user is expected to pass jquery object along with selector property.

## Changed
* App header now has 10px bottom padding (instead of 7px) to match Atlaskit header (DCA11Y-747)

# 9.11.1
* [Documentation](https://aui.atlassian.com/aui/9.11/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.11.1)

## Changed
* Updated `jQuery.TableSorter` to 2.17.8 + prevent empty `headerTemplate` (DCA11Y-957)

## Added

## Fixed
* Updated `jQuery.TableSorter` to 2.17.8 + prevent empty `headerTemplate` (DCA11Y-957)
* Patched `tablesorter` to avoid replacement of literal `{icon}` in the content of the
  header with non-empty `headerTemplate` (DCA11Y-957)

## Breaking
* Sortable tables and `tablesorter` only allow a limited set of characters to prevent an inadvertent XSS if consumers pass UGC (DCA11Y-957)

# 9.11.0
* [Documentation](https://aui.atlassian.com/aui/9.11/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.11.0)

## Changed
* used jQuery `.prop()` instead of `.attr()` to fetch or modify boolean properties
* used jQuery `.prop(propertyName, false)` instead of `.removeAttr(propertyName)` to set boolean property to `false`
* add focus trap to `flag` component (COM-53)
* add `role="alert"` and `role="alertdialog"` to flag component (COM-53)
* make it possible to remove not only the last item from focus manager (COM-53)
* tooltip A11Y: make tooltip container invisible also for assistive software when it's not shown (DCA11Y-781)
* updated @atlaskit/tokens from 1.33.0 to 1.43.0
* updated backbone from 1.5.0 to 1.6.0
* updated dompurify from 2.4.9 to 2.5.0

## Added
* tooltip now monitors trigger removal to also remove itself (DCA11Y-744)
  * due to performance reasons, we can't track if trigger parents were removed

# 9.10.6 [Unreleased]
* [Documentation](https://aui.atlassian.com/aui/9.10/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.10.6)

## Fixed
* Date picker initialized with input type as text retains its initial value (AUI-5484)

## Changed
* (internal) The builds in Bitbucket Pipelines now use Node 22 (DCA11Y-1029)
* (internal) Changed the bundle size reporting during Bitbucket Pipelines builds to be more
  developer-friendly (DCA11Y-750)
* Changed App header bottom padding from 10px to 8px to match Atlaskit header [Atlaskit Atlassian Navigation header](https://atlassian.design/components/atlassian-navigation/examples) (DCA11Y-1213)
* (internal) Builds are now using Bamboo instead of Bitbucket Pipelines (DCA11Y-1356)
* (internal) The integration tests are now using LambdaTest instead of BrowserStack (DCA11Y-1426)

# 9.10.5
* [Documentation](https://aui.atlassian.com/aui/9.10/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.10.5)

## Changed
* (internal) The builds in Bitbucket Pipelines are now using the `sfp-base-agent` image, shared with the
  builds in Bamboo (DCA11Y-956)

## Fixed
* Select2: can't always find its `dropdownLayer` when closing (DCA11Y-1063)

# 9.10.4
* [Documentation](https://aui.atlassian.com/aui/9.10/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.10.4)

## Fixed
* inline-dialog is compatible with jquery 3. The user is expected to pass jquery object along with selector property.

## Changed
* App header now has 10px bottom padding (instead of 7px) to match Atlaskit header (DCA11Y-747)

# 9.10.3
* [Documentation](https://aui.atlassian.com/aui/9.10/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.10.3)

## Fixed
* Updated `jQuery.TableSorter` to 2.17.8 + prevent empty `headerTemplate` (DCA11Y-957)
* Patched `tablesorter` to avoid replacement of literal `{icon}` in the content of the
  header with non-empty `headerTemplate` (DCA11Y-957)

## Breaking
* Sortable tables and `tablesorter` only allow a limited set of characters to prevent an inadvertent XSS if consumers pass UGC (DCA11Y-957)

# 9.10.2
* [Documentation](https://aui.atlassian.com/aui/9.10/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.10.2)

## Changed
* Updated `jQuery.TableSorter` to 2.17.8 + prevent empty `headerTemplate` (DCA11Y-957)
* Updated `dompurify` to 2.5.0

# 9.10.1
* [Documentation](https://aui.atlassian.com/aui/9.10/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.10.1)

## Added
* tooltip now monitors trigger removal to also remove itself (DCA11Y-744)
  * due to performance reasons, we can't track if trigger parents were removed

## Fixed
* The CSS artifact with tokens now includes only the light and dark themes, not
  additional themes like spacing or overrides that are provided by the Atlaskit
  tokens library. This fixes the color of the input border
  (`--ds-border-input`) when design tokens are included via the CSS
  artifact. (DCA11Y-907)
* correctly applying layering z-index to select2 dropdown so that it stays above nested modals (AUI-5482)
* visual accessibility improvements
    * improved visual difference of disabled checkbox (DCA11Y-920)
    * improved focus outline contrast on checkbox and radio button (DCA11Y-837)
    * improved contrast of checkbox border (DCA11Y-837)
    * improved contrast of label on hover of close button (DCA11Y-837)

# 9.10.0
* [Documentation](https://aui.atlassian.com/aui/9.10/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.10.0)

## Changed
* update Refapp to 6.2.8 (DCA11Y-636)
* deprecated AUI Message `fade-out` and `removeOnHide` (DCA11Y-581)
* updated Backbone (`1.4.1` -> `1.5.0`)
* updated @atlaskit/tokens from 1.20.1 to 1.33.0
  * most notably, this update includes minor adjustments to our color palette
    and design tokens. These changes make certain colors more distinguishable,
    improves the visual appearance of warning and yellow accent backgrounds,
    and reduces saturation for subtle backgrounds in dark mode.
  * refer to the full changelog:
    https://atlassian.design/components/tokens/changelog#1201

## Added
* improved a11y of AUI Message component (DCA11Y-581)
  * introduced extra a11y labels for default Message types
  * added `a11yTypeLabel` option to apply labels to custom message types
  * made AUI Message to be announced as `note`
* added new function `getTopOpenLayer` in LayerManager (DCA11Y-688)

## Fixed
* add missing dependency on WRM I18n web-resource (AUI-5471)
* Fixed navigation with Enter in Dropdown (DCA11Y-691, AUI-5469)
* Add missing dependency on WRM I18n web-resource (AUI-5471)
* Returned styles for Message usage in Header (DCA11Y-739)
  * Styles are deprecated and will be removed later, it's recommended to use Banner instead

# 9.9.7 [Unreleased]
* [Documentation](https://aui.atlassian.com/aui/9.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.9.7)

## Changed
* (internal) The builds in Bitbucket Pipelines now use Node 22 (DCA11Y-1029)
* (internal) Changed the bundle size reporting during Bitbucket Pipelines builds to be more
  developer-friendly (DCA11Y-750)
* Changed App header bottom padding from 10px to 8px to match Atlaskit header [Atlaskit Atlassian Navigation header](https://atlassian.design/components/atlassian-navigation/examples) (DCA11Y-1213)
* (internal) Builds are now using Bamboo instead of Bitbucket Pipelines (DCA11Y-1356)
* (internal) The integration tests are now using LambdaTest instead of BrowserStack (DCA11Y-1426)

# 9.9.6
* [Documentation](https://aui.atlassian.com/aui/9.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.9.6)

## Changed
* Changed App header padding from 0 to 7px and 10 px for the top and bottom respectively to match Atlaskit header (DCA11Y-747)
* (internal) The builds in Bitbucket Pipelines are now using the `sfp-base-agent` image, shared with the
  builds in Bamboo (DCA11Y-956)

## Fixed
* Select2: can't always find its `dropdownLayer` when closing (DCA11Y-1063)

# 9.9.5
* [Documentation](https://aui.atlassian.com/aui/9.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.9.5)

## Changed
* Updated `jQuery.TableSorter` to 2.17.8 + prevent empty `headerTemplate` (DCA11Y-957)

## Fixed
* Updated `jQuery.TableSorter` to 2.17.8 + prevent empty `headerTemplate` (DCA11Y-957)
* Patched `tablesorter` to avoid replacement of literal `{icon}` in the content of the
  header with non-empty `headerTemplate` (DCA11Y-957)

## Breaking
* Sortable tables and `tablesorter` only allow a limited set of characters to prevent an inadvertent XSS if consumers pass UGC (DCA11Y-957)

# 9.9.4
* [Documentation](https://aui.atlassian.com/aui/9.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.9.4)

## Added
* tooltip now monitors trigger removal to also remove itself (DCA11Y-744)
  * due to performance reasons, we can't track if trigger parents were removed

## Fixed
* The CSS artifact with tokens now includes only the light and dark themes, not
  additional themes like spacing or overrides that are provided by the Atlaskit
  tokens library. This fixes the color of the input border
  (`--ds-border-input`) when design tokens are included via the CSS
  artifact. (DCA11Y-907)
* correctly applying layering z-index to select2 dropdown so that it stays above nested modals (AUI-5482)
* visual accessibility improvements
  * improved visual difference of disabled checkbox (DCA11Y-920)
  * improved focus outline contrast on checkbox and radio button (DCA11Y-837)
  * improved contrast of checkbox border (DCA11Y-837)
  * improved contrast of label on hover of close button (DCA11Y-837)

# 9.9.3
* [Documentation](https://aui.atlassian.com/aui/9.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.9.3)

## Fixed
* make design tokens testing theme enable correctly by moving it to `<html>`
  level. Make sure it works only when design tokens are active (DCA11Y-766)

## Added
* build tokens as CSS file to use in an iframe (DCA11Y-755)

# 9.9.2
* [Documentation](https://aui.atlassian.com/aui/9.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.9.2)

## Fixed
* When Dropdown2 item opens a Dialog2, focus was set outside the Dialog's focus
  trap (AUI-5474)
  * Additionally, bring back logic fixing AUI-4283 (pressing Enter in a
    Dropdown2 item sometimes didn't navigate), which was erroneously removed in
    9.6.0.
* Add missing dependency on WRM I18n web-resource (AUI-5471)
* Returned styles for Message usage in Header (DCA11Y-739)
  * Styles are deprecated and will be removed later, it's recommended to use Banner instead

## Changed
* Refactor to use flexbox and relative positioning for page header avatars in the sidebar (AUI-5477)
* Apply header styles to all H1 and H2 in the sidebar header (AUI-5477)

# 9.9.1
* [Documentation](https://aui.atlassian.com/aui/9.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.9.1)

## Fixed
* fix sidebar's layering issues when it's in `fly-out` state (DCA11Y-690)
  Note: this fix was inspired by BitBucket's and Confluence's overrides made in their stylesheets,
  so updating to this version should allow to remove them from the products.
* Fixed navigation with Enter in Dropdown (DCA11Y-691, AUI-5469)
* add missing dependency on WRM I18n web-resource (AUI-5471)

## Added
* added new function `getTopOpenLayer` in LayerManager (DCA11Y-688)
- added new function `getTopOpenLayer` in LayerManager (DCA11Y-688)

# 9.9.0
* [Documentation](https://aui.atlassian.com/aui/9.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.9.0)

## Added

* Buttons: two new visual states ("danger" and "warning") available (DCA11Y-556)
* New web-resource `com.atlassian.auiplugin:aui-design-tokens-themes` to preload Design Token themes (DCA11Y-610)
* New NPM resource `aui-prototyping-design-tokens-themes.js` to preload Design Token themes (DCA11Y-610)
* Introduce minimal web-resource (DCA11Y-622)
  * New web-resource `aui.page.design-tokens-api-full` to provide full version of DT API to consumer
  * New NPM resource `aui-prototyping-design-tokens-api-full.js` to provide full version of DT API to consumer
  * Side-effect that inits DT lib with `colorMode: "auto"` if `data-color-mode-auto` is present on page during execution
    * It will happen both for `aui.page.design-tokens-api-full` and `aui.page.design-tokens-api`
* Customise design token `themeImportMap` (DCA11Y-640)
  * `original` theme added, but not set in `theme` attribute yet
  * `com.atlassian.auiplugin:aui-design-tokens-themes` now loads themes as split chunks
* `ciTools` and pipeline step for creating upmerge PRs automatically (DCA11Y-542)
* New web-resource `com.atlassian.auiplugin:design-tokens-themes` to preload Design Token themes (DCA11Y-610)
* Customise design token `themeImportMap` (DCA11Y-640)
  * `original` theme added, but not set in `theme` attribute yet
  * `com.atlassian.auiplugin:design-tokens-themes` now loads themes as split chunks

## Changed

* Removed `.aui-theme-design-tokens`. Now Design Tokens AUI theme will automatically be used when `setGlobalTheme()` is called. (DCA11Y-610)
* Now web-resource `aui.page.design-tokens-api` exposes only `setGlobalTheme()` function, which is minimal API needed to use DT (DCA11Y-622)
  * The same for entry `aui-prototyping-design-tokens-api.js` in NPM package
* Design tokens compatibility themes for displaying components that use obsolete color definitions (DCA11Y-621):
  * in the P2 build:
    - definitions exposed under new `aui-design-tokens-compatibility-themes` web resource
    - base themes are moved into `aui-design-tokens-base-themes`
    - `design-tokens-themes` now includes both base and compatibility themes
  * in the NPM build:
    - definitions exposed under new `aui-prototyping-design-tokens-compatibility.css` output
    - base themes are moved into `aui-prototyping-design-tokens-base-themes.js`
    - there is no common entry point that includes both
* Updated `@atlaskit/tokens` to v. 1.20.1 (DCA11Y-627)

## Fixed

* Form Validation and Notification: fix rendering errors found in soy templates (DCA11Y-600)
* Fixed Expander a11y issues (DCA11Y-579)
* Fixed "Cannot read properties of undefined" in buttons.soy (DCA11Y-663)
* Design token theme file duplicates are removed from build result (DCA11Y-640)
* React is removed from build result (DCA11Y-464)
* AUI Documentation (Icon Component): Updated clipboard box to copy A11Y-compliant markup (DCA11Y-642)

# 9.8.3 [Unreleased]
* [Documentation](https://aui.atlassian.com/aui/9.8/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.8.3)

## Changed
* Updated `jQuery.TableSorter` to 2.17.8 + prevent empty `headerTemplate` (DCA11Y-957)
* (internal) The builds in Bitbucket Pipelines are now using the `sfp-base-agent` image, shared with the
  builds in Bamboo (DCA11Y-956)
* (internal) The builds in Bitbucket Pipelines now use Node 22 (DCA11Y-1029)
* (internal) Changed the bundle size reporting during Bitbucket Pipelines builds to be more
  developer-friendly (DCA11Y-750)
* (internal) Builds are now using Bamboo instead of Bitbucket Pipelines (DCA11Y-1356)
* (internal) The integration tests are now using LambdaTest instead of BrowserStack (DCA11Y-1426)

## Fixed
* Updated `jQuery.TableSorter` to 2.17.8 + prevent empty `headerTemplate` (DCA11Y-957)
* Patched `tablesorter` to avoid replacement of literal `{icon}` in the content of the
  header with non-empty `headerTemplate` (DCA11Y-957)

## Breaking
* Sortable tables and `tablesorter` only allow a limited set of characters to prevent an inadvertent XSS if consumers pass UGC (DCA11Y-957)

# 9.8.2
* [Documentation](https://aui.atlassian.com/aui/9.8/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.8.2)

## Fixed
* Fixed navigation with Enter in Dropdown (DCA11Y-691, AUI-5469)
* add missing dependency on WRM I18n web-resource (AUI-5471)
* Add missing dependency on WRM I18n web-resource (AUI-5471)
* Returned styles for Message usage in Header (DCA11Y-739)
  * Styles are deprecated and will be removed later, it's recommended to use Banner instead

# 9.8.1
* [Documentation](https://aui.atlassian.com/aui/9.8/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.8.1)

## Changed

* Updated `@atlaskit/tokens` to v. 1.20.1 (DCA11Y-627)

## Fixed
* AUI Documentation (Icon Component): Updated clipboard box to copy A11Y-compliant markup (DCA11Y-642)

## Added
* `ciTools` and pipeline step for creating upmerge PRs automatically (DCA11Y-542)

# 9.8.0
* [Documentation](https://aui.atlassian.com/aui/9.8/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.8.0)

## Added

* New Banner types (DCA11Y-559)
* Docs: Avatar a11y guidelines (DCA11Y-491)
* Avatar Group dropdown closes when clicked outside the dropdown (DCA11Y-500)
* Avatar Group dropdown closes when 'Escape' button is clicked (DCA11Y-500)
* Extended Flag API: Added new parameter for duration (DCA11Y-605)
* Exposed ThemeMutationObserver through AUI API (DCA11Y-615)

## Changed

* Docs: Avatar structure of the page (DCA11Y-491)
* Docs: updated Banner documentation (DCA11Y-559)
* Updated Banner docs with a11y guidelines (DCA11Y-554)
* Deprecated: Banners with hidden attribute will no longer be removed during subsequent banner function invocations. (DCA11Y-554)
* Default duration for flag display set to 8 seconds (DCA11Y-605)
* Minor updates to the AUI components for the design tokens theme (DCA11Y-570):
  * Added border to the Header
  * Updated shadows to match AtlasKit
  * Updated colors for Button, Dropdown, Flag, Select2, Tabs

## Fixed

* Avatar A11Y issues (DCA11Y-491)
* Fixed Banner a11y issues (DCA11Y-554)
* Fixed Lozenge text color in the design tokens theme (DCA11Y-552)
* Fixed Flag component for optimal display on small screens (AUI-5459)
* Fixed interaction of design tokens theme and product's Original theme (DCA11Y-550)
* Fixed design token previews on the 'AUI colors' docs page (DCA11Y-550)

# 9.7.5 [Unreleased]
* [Documentation](https://aui.atlassian.com/aui/9.7/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.7.5)

## Changed
* Updated `jQuery.TableSorter` to 2.17.8 + prevent empty `headerTemplate` (DCA11Y-957)
* (internal) The builds in Bitbucket Pipelines are now using the `sfp-base-agent` image, shared with the
  builds in Bamboo (DCA11Y-956)
* (internal) The builds in Bitbucket Pipelines now use Node 22 (DCA11Y-1029)
* (internal) Changed the bundle size reporting during Bitbucket Pipelines builds to be more
  developer-friendly (DCA11Y-750)
* (internal) Builds are now using Bamboo instead of Bitbucket Pipelines (DCA11Y-1356)
* (internal) The integration tests are now using LambdaTest instead of BrowserStack (DCA11Y-1426)

## Fixed
* Updated `jQuery.TableSorter` to 2.17.8 + prevent empty `headerTemplate` (DCA11Y-957)
* Patched `tablesorter` to avoid replacement of literal `{icon}` in the content of the
  header with non-empty `headerTemplate` (DCA11Y-957)

## Breaking
* Sortable tables and `tablesorter` only allow a limited set of characters to prevent an inadvertent XSS if consumers pass UGC (DCA11Y-957)

# 9.7.4
* [Documentation](https://aui.atlassian.com/aui/9.7/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.7.4)

## Fixed
* Fixed navigation with Enter in Dropdown (DCA11Y-691, AUI-5469)
* add missing dependency on WRM I18n web-resource (AUI-5471)

# 9.7.3
* [Documentation](https://aui.atlassian.com/aui/9.7/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.7.3)

## Added
* `ciTools` and pipeline step for creating upmerge PRs automatically (DCA11Y-542)

## Fixed
* AUI Documentation (Icon Component): Updated clipboard box to copy A11Y-compliant markup (DCA11Y-642)

# 9.7.2
* [Documentation](https://aui.atlassian.com/aui/9.7/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.7.2)

## Added

* Docs: Avatar a11y guidelines (DCA11Y-491)
* Avatar Group dropdown closes when clicked outside the dropdown (DCA11Y-500)
* Avatar Group dropdown closes when 'Escape' button is clicked (DCA11Y-500)

## Fixed

* Fixed `marked` version resolution in docs to 4.0.12 (BSP-5251)
* Avatar A11Y issues (DCA11Y-491)
* Avatar Group fix disappearing avatar icons inside dropdown (DCA11Y-499)
* Fixed custom select2 error handlers not working (AUI-5464)

## Changed

* Docs: Avatar structure of the page (DCA11Y-491)

# 9.7.1
* [Documentation](https://aui.atlassian.com/aui/9.7/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.7.1)

## Added

* Added documentation for AUI colors (DCA11Y-458)

## Fixed

* Fixed Tooltip a11y by adding ESC key event handler (DCA11Y-488)
* Fixed the docs site so the design tokens theme is loaded correctly (DCA11Y-484)
* Fixed vulnerability of `tough-cookie` updated to 4.1.3 (BSP-5186)
* Form Validation and Notification: more accessibility improvements (DCA11Y-489)

# 9.7.0
* [Documentation](https://aui.atlassian.com/aui/9.7/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.7.0)

## Added

* Added a new option for select2 single- and multi-select for providing custom label text for search labels (AUI-5463)
* Added a design tokens theme - a new approach to implement light/dark mode (DCA11Y-356)

## Changed

* Radio buttons: visual/a11y improvements in radio buttons used in Flatapp (DCA11Y-400)
* Radio buttons: update forms docs with radio a11y guidelines (DCA11Y-400)

## Fixed

* Fixed `marked` version resolution in docs to 4.0.12 (BSP-5251)
* Fixed select2 single select search field is not announced by SRs (AUI-5461)
* Fixed select2 single select button label is not announced by SRs (AUI-5462)
* Toggle button: accessibility improvements in flatapp (DCA11Y-419)
* Tooltip: accessibility improvements in flatapp (DCA11Y-451)
* Multi-select: deprecated in favor of Select2 (DCA11Y-447)
* Form Validation and Notification: accessibility improvements (DCA11Y-455)


# 9.6.4 [Unreleased]
* [Documentation](https://aui.atlassian.com/aui/9.6/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.6.4)

## Changed
* Updated `jQuery.TableSorter` to 2.17.8 + prevent empty `headerTemplate` (DCA11Y-957)
* (internal) The builds in Bitbucket Pipelines are now using the `sfp-base-agent` image, shared with the
  builds in Bamboo (DCA11Y-956)
* (internal) The builds in Bitbucket Pipelines now use Node 22 (DCA11Y-1029)
* (internal) Changed the bundle size reporting during Bitbucket Pipelines builds to be more
  developer-friendly (DCA11Y-750)
* (internal) Builds are now using Bamboo instead of Bitbucket Pipelines (DCA11Y-1356)
* (internal) The integration tests are now using LambdaTest instead of BrowserStack (DCA11Y-1426)

## Fixed
* Updated `jQuery.TableSorter` to 2.17.8 + prevent empty `headerTemplate` (DCA11Y-957)
* Patched `tablesorter` to avoid replacement of literal `{icon}` in the content of the
  header with non-empty `headerTemplate` (DCA11Y-957)

## Breaking
* Sortable tables and `tablesorter` only allow a limited set of characters to prevent an inadvertent XSS if consumers pass UGC (DCA11Y-957)

# 9.6.3
* [Documentation](https://aui.atlassian.com/aui/9.6/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.6.3)

## Added
* `ciTools` and pipeline step for creating upmerge PRs automatically (DCA11Y-542)

## Fixed
* AUI Documentation (Icon Component): Updated clipboard box to copy A11Y-compliant markup (DCA11Y-642)
* Fixed navigation with Enter in Dropdown (DCA11Y-691, AUI-5469)
* add missing dependency on WRM I18n web-resource (AUI-5471)

# 9.6.2
* [Documentation](https://aui.atlassian.com/aui/9.6/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.6.2)

## Fixed
* Fixed `marked` version resolution in docs to 4.0.12 (BSP-5251)
* Fixed custom select2 error handlers not working (AUI-5464)

# 9.6.1
* [Documentation](https://aui.atlassian.com/aui/9.6/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.6.1)

## Added
* Added a new option for select2 single- and multi-select for providing custom label text for search labels (AUI-5463)

## Changed
* Radio buttons: visual/a11y improvements in radio buttons used in Flatapp (DCA11Y-400)
* Radio buttons: update forms docs with radio a11y guidelines (DCA11Y-400)

## Fixed
* Updated `glob-parent` to 6.0.2 (BSP-4944)
* Updated `hosted-git-info` to 4.0.0 (BSP-4997)
* Updated `follow-redirects` to 1.15.0 (BSP-4998)
* Updated `minimist` to 0.2.4 (BSP-5000)
* Updated `json5` to 2.2.3 (BSP-4956)
* Updated `decode-uri-component` to 0.2.2 (BSP-5002)
* Updated `Uglify-js` to 3.14.3 (BSP-5003)
* Updated `simple-git` to 3.18.0 (BSP-5009)
* Updated `nth-check` to 2.1.1 (BSP-4908)
* Updated `debug` to 4.3.4 (BSP-5013)
* Updated `ms` to 2.1.3 (BSP-5014)
* Updated `qs` to 6.11.1 (BSP-4949)
* Updated `ramda` to 0.29.0 (BSP-4991)
* Updated `marked` to 4.0.12 (BSP-5010)
* Updated `i` to 0.3.7 (BSP-5039)
* Updated `istanbul-reports` to 3.1.5 (BSP-5038)
* Updated `terser` to 5.16.5 (BSP-5036)
* Updated `@sideway/formula` to 3.0.1 (BSP-5029)
* Updated `engine.io` to 6.4.2 (BSP-5022)
* Updated `ua-parser-js` to 6.4.2 (BSP-5024)
* Updated `commons-codec:commons-codec` to 1.15 (BSP-5031)
* FileUpload: accessibility improvements in flatapp (DCA11Y-424)
* Textarea: accessibility improvements in flatapp (DCA11Y-401)
* Updated `mocha` to 10.1.0 (BSP-4999)
* Updated `codeceptjs` to 3.4.1 (BSP-4999)
* Fixed select2 single select search field is not announced by SRs (AUI-5461)
* Fixed select2 single select button label is not announced by SRs (AUI-5462)
* Fix vulnerability of `request` (BSP-5099)
* Fix vulnerability of `tough-cookie` updated to 4.1.3 (BSP-5186)

# 9.6.0
* [Documentation](https://aui.atlassian.com/aui/9.6/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.6.0)

## Fixed
* Updated jquery-ui to 1.13.2 that fixes [CVE-2022-31160](https://www.cve.org/CVERecord?id=CVE-2022-31160) (AUI-5456)
* Fixed responsive layout related CSS selector (for aui-dialog2) (AUI-5455)
* Fix the filtering of options with the same labels, but different values/ids in Single Select view (AUI-5454)
* Updated webpack to 5.76.2 that fixes vulnerability (BSP-4740)
* Inline Dialog: Fix the event cleaning in the inline dialog (DCA11Y-343)

## Changed
* Buttons: a11y improvements in examples and soy template (DCA11Y-188)
* Docs: buttons a11y guidelines (DCA11Y-188)
* Single Select: a11y improvements (DCA11Y-191)
* Docs: Single Select a11y guidelines (DCA11Y-191)
* Dropdown2: a11y improvements (DCA11Y-260)
* Docs: Dropdown2 a11y guidelines (DCA11Y-260)
* Dialog2: a11y improvements (DCA11Y-263)
  * aria-hidden not added anymore to body children when Dialog2 is opened
  * autofocus attribute should be used in Dialog2 now to define custom focused element instead of old data-aui-focus-selector attribute
  * deprecated data-aui-focus-selector attribute on Dialog2
  * initial focus is changed for Dialog2
      * first autofocus element in Dialog is now focused
      * if autofocus not defined, focus is set on first focusable element in Dialog
      * if not focusable elements, focus is set on Dialog element
  * added describedby attribute to Dialog2 soy template to allow custom aria-describedby on dialog element
  * aria-modal="true" is now added to Dialog2 soy template
* updates to Yarn Dependencies (AUI-5457)
  * @popperjs/core (2.4.4 -> 2.11.6)
  * autoprefixer (10.4.7 -> 10.4.14)
  * classnames (2.3.1 -> 2.3.2)
  * cssnano (5.1.12 -> 5.1.15)
  * dompurify (2.4.0 -> 2.4.5)
  * jquery-ui (1.13.1 -> 1.13.2)
  * postcss (8.4.14 -> 8.4.21)
  * underscore (1.13.4 -> 1.13.6)
* updates to Yarn Dev Dependencies
  * jquery-migrate (3.4.0 -> 3.4.1)
  * metalsmith (2.5.0 -> 2.5.1)
* updates to Yarn Resolutions
  * marked (4.0.17 -> 4.0.19)
  * open (8.4.0 -> 8.4.2)
  * plist (3.0.5 -> 3.0.6)
  * tar (6.1.11 -> 6.1.13)
* updates to Maven dependencies
  * com.atlassian.plugins:jquery (3.5.0 -> 3.5.1)
  * com.google.code.gson:gson (2.9.0 -> 2.9.1)
  * com.atlassian.platform:third-party (6.0.7 -> 6.0.26)
  * com.atlassian.platform:platform (6.0.7 -> 6.0.26)
  * com.atlassian.pom:public-pom (6.2.2 -> 6.2.5)
* update to NodeJS version reflected in .nvmrc file (14.19.3 -> 14.21.3)
* Checkbox: accessibility improvements in flatapp and docs (DCA11Y-262)
* Inline Dialog: accessibility improvements in flatapp and docs (DCA11Y-338)
* Icon: accessibility improvements (DCA11Y-264)
* Updated browserslist to 4.21.5 (BSP-4929)
* Updated nanoid to 3.2.0 (BSP-4932)


# 9.5.5 [Unreleased]
* [Documentation](https://aui.atlassian.com/aui/9.5/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.5.5)

## Changed
* Updated `jQuery.TableSorter` to 2.17.8 + prevent empty `headerTemplate` (DCA11Y-957)
* (internal) The builds in Bitbucket Pipelines are now using the `sfp-base-agent` image, shared with the
  builds in Bamboo (DCA11Y-956)
* (internal) The builds in Bitbucket Pipelines now use Node 22 (DCA11Y-1029)
* (internal) Changed the bundle size reporting during Bitbucket Pipelines builds to be more
  developer-friendly (DCA11Y-750)
* (internal) Builds are now using Bamboo instead of Bitbucket Pipelines (DCA11Y-1356)
* (internal) The integration tests are now using LambdaTest instead of BrowserStack (DCA11Y-1426)

## Fixed
* Updated `jQuery.TableSorter` to 2.17.8 + prevent empty `headerTemplate` (DCA11Y-957)
* Patched `tablesorter` to avoid replacement of literal `{icon}` in the content of the
  header with non-empty `headerTemplate` (DCA11Y-957)

## Breaking
* Sortable tables and `tablesorter` only allow a limited set of characters to prevent an inadvertent XSS if consumers pass UGC (DCA11Y-957)

# 9.5.4
* [Documentation](https://aui.atlassian.com/aui/9.5/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.5.4)

## Added
* `ciTools` and pipeline step for creating upmerge PRs automatically (DCA11Y-542)

## Fixed
* add missing dependency on WRM I18n web-resource (AUI-5471)

# 9.5.3
* [Documentation](https://aui.atlassian.com/aui/9.5/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.5.3)

## Fixed
* Fixed `marked` version resolution in docs to 4.0.12 (BSP-5251)
* Updated browserslist to 4.21.5 (BSP-4929)
* Updated nanoid to 3.2.0 (BSP-4932)
* Updated `glob-parent` to 6.0.2 (BSP-4944)
* Updated `hosted-git-info` to 4.0.0 (BSP-4997)
* Updated `follow-redirects` to 1.15.0 (BSP-4998)
* Updated `minimist` to 0.2.4 (BSP-5000)
* Updated `json5` to 2.2.3 (BSP-4956)
* Updated `decode-uri-component` to 0.2.2 (BSP-5002)
* Updated `Uglify-js` to 3.14.3 (BSP-5003)
* Updated `nth-check` to 2.1.1 (BSP-4908)
* Updated `debug` to 4.3.4 (BSP-5013)
* Updated `ms` to 2.1.3 (BSP-5014)
* Updated `simple-git` to 3.18.0 (BSP-5009)
* Updated `qs` to 6.11.1 (BSP-4949)
* Updated `ramda` to 0.29.0 (BSP-4991)
* Updated `marked` to 4.0.12 (BSP-5010)
* Updated `i` to 0.3.7 (BSP-5039)
* Updated `istanbul-reports` to 3.1.5 (BSP-5038)
* Updated `terser` to 5.16.5 (BSP-5036)
* Updated `@sideway/formula` to 3.0.1 (BSP-5029)
* Updated `engine.io` to 6.4.2 (BSP-5022)
* Updated `ua-parser-js` to 6.4.2 (BSP-5024)
* Updated `commons-codec:commons-codec` to 1.15 (BSP-5031)
* Updated `mocha` to 10.1.0 (BSP-4999)
* Updated `codeceptjs` to 3.4.1 (BSP-4999)
* Fix vulnerability of `request` (BSP-5099)
* Fix vulnerability of `tough-cookie` updated to 4.1.3 (BSP-5186)
* Updated `nth-check` to 2.0.1 (BSP-4908)
* Updated `jquery-ui` to 1.13.2 (BSP-4910)

# 9.5.2
* [Documentation](https://aui.atlassian.com/aui/9.5/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.5.2)

## Fixed
* Updated jquery-ui to 1.13.2 that fixes [CVE-2022-31160](https://www.cve.org/CVERecord?id=CVE-2022-31160) (AUI-5456)
* Fixed responsive layout related CSS selector (for aui-dialog2) (AUI-5455)
* Fix the filtering of options with the same labels, but different values/ids in Single Select view (AUI-5454)
* Updated webpack to 5.76.2 that fixes vulnerability (BSP-4740)

# 9.5.1
* [Documentation](https://aui.atlassian.com/aui/9.5/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.5.1)

## Changed
* No new changes. Version 9.5.0 was released by mistake; this is the first
  usable version in 9.5.x.

# 9.5.0
* [Documentation](https://aui.atlassian.com/aui/9.5/)
* [Jira issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.5.0)

## Added
* `aui-item-link` supports `extra-classes` attribute;
  CSS classes provided in the attribute are reflected in the underlying anchor element (MNSTR-6095)

## Changed
* within the responsive app header component: custom CSS classes from the original dropdown trigger are now
  copied to the responsive trigger created inside the "More" dropdown (MNSTR-6095)

## Fixed
* Asynchronous initialisation of AUI header (AUI-5432). See AUI header documentation for details.
* Accessibility improvement for avatar (AUI-5318)
* Makes chevron glyph consistent across dropdowns (AUI-5417)
* Support blanket showing when there are non-HTML elements (e.g. SVG nodes) in the body of the `document` (AUI-5429)
* Prevent AUI overriding the `AJS.I18n` after WRM has set it, for compatibility with the two phase JS I18n transform (AUI-5431)
* Vulnerability in jquery-ui used by aui (VULN-627502, VULN-627503, VULN-627504)
* Vulnerability of XSS fix in jquery-form component (VULN-539557)
* Updated css-what version to 5.0.1 (BSP-3269)
* nth-check npm version upgraded to 2.0.1 (BSP-3270)
* Updated ajv from 5.5.2 to 6.12.3 (BSP-3266)
* Updated ansi-regex version to 5.0.1 (BSP-3268)
* Update lodash version to 4.17.21 (BSP-3635)
* Updated the open version to 8.4.0 (BSP-3634)
* Updated json-schema to 0.4.0 and wait-on to 6.0.1 (BSP-3716)
* Updated marked to 4.0.12 (BSP-3740)
* Updated qs to 6.5.2 or higher (BSP-3666)
* Updated plist to 3.0.4 (BSP-3675)
* Updated commons-io to 2.7 (BSP-3767)
* Updated engine-io to 6.1.3 (BSP-3719)
* Excluded dom4j dependency (BSP-3722)
* Upgrade async to 3.2.2 (BSP-3801)
* Upgrade atlassian-spring-scanner to 2.2.5 (BSP-3814)
* Updated the gson version to 2.8.9 (BSP-3718)
* Fix the dropdown selector is scrolling instead of anchoring issue (BSP-4032)
* Fix the dropdown position change and not able to see all items (BSP-3808)
* Fix the tooltip container caching issue (AUI-5444)
* Updated karma version to 6.4.1 (BSP-4590)

## Added

* Avatar group component (AUI-4676)
* Avatar component and avatar badged component (AUI-4484)
* Added `lazyLoadImage` attribute to `avatar.soy` template (MNSTR-5545)
* Added auiSelectField Soy template as a type of `aui.forms.field` in `form.soy` (MNSTR-5602)
* Added `enable` and `disable` options to `tooltip()` (AUI-5428)


# 9.4.14 [Unreleased]
* [Documentation](https://aui.atlassian.com/aui/9.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.4.14)

## Changed
* (internal) The builds in Bitbucket Pipelines are now using the `sfp-base-agent` image, shared with the
  builds in Bamboo (DCA11Y-956)
* (internal) The builds in Bitbucket Pipelines now use Node 22 (DCA11Y-1029)
* (internal) Changed the bundle size reporting during Bitbucket Pipelines builds to be more
  developer-friendly (DCA11Y-750)
* (internal) Builds are now using Bamboo instead of Bitbucket Pipelines (DCA11Y-1356)
* (internal) The integration tests are now using LambdaTest instead of BrowserStack (DCA11Y-1426)

# 9.4.13
* [Documentation](https://aui.atlassian.com/aui/9.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.4.13)

## Added
* `ciTools` and pipeline step for creating upmerge PRs automatically (DCA11Y-542)

## Changed
* Updated `jQuery.TableSorter` to 2.17.8 + prevent empty `headerTemplate` (DCA11Y-957)

## Fixed
* Updated `jQuery.TableSorter` to 2.17.8 + prevent empty `headerTemplate` (DCA11Y-957)
* Patched `tablesorter` to avoid replacement of literal `{icon}` in the content of the
  header with non-empty `headerTemplate` (DCA11Y-957)

## Breaking
* Sortable tables and `tablesorter` only allow a limited set of characters to prevent an inadvertent XSS if consumers pass UGC (DCA11Y-957)

# 9.4.12
* [Documentation](https://aui.atlassian.com/aui/9.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.4.12)

## Fixed
* Updated browserslist to 4.21.5 (BSP-4929)
* Updated nanoid to 3.2.0 (BSP-4932)
* Updated `glob-parent` to 6.0.2 (BSP-4944)
* Updated `hosted-git-info` to 4.0.0 (BSP-4997)
* Updated `follow-redirects` to 1.15.0 (BSP-4998)
* Updated `minimist` to 0.2.4 (BSP-5000)
* Updated `json5` to 2.2.3 (BSP-4956)
* Updated `minimatch` to 3.0.5 (BSP-5001)
* Updated `decode-uri-component` to 0.2.2 (BSP-5002)
* Updated `Uglify-js` to 3.14.3 (BSP-5003)
* Updated `nth-check` to 2.0.1 (BSP-4908)
* Updated `debug` to 2.6.9 (BSP-5013)
* Updated `ms` to 2.1.3 (BSP-5014)
* Updated `qs` to 6.11.1 (BSP-4949)
* Updated `ramda` to 0.29.0 (BSP-4991)
* Updated `marked` to 4.0.12 (BSP-5010)
* Updated `i` to 0.3.7 (BSP-5039)
* Updated `istanbul-reports` to 3.1.5 (BSP-5038)
* Updated `terser` to 5.16.5 (BSP-5036)
* Updated `@sideway/formula` to 3.0.1 (BSP-5029)
* Updated `engine.io` to 6.4.2 (BSP-5022)
* Updated `ua-parser-js` to 6.4.2 (BSP-5024)
* Updated `commons-codec:commons-codec` to 1.15 (BSP-5031)
* Updated `mocha` to 10.1.0 (BSP-4999)
* Fix vulnerability of `request` (BSP-5099)
* Fix vulnerability of `tough-cookie` updated to 4.1.3 (BSP-5186)
* Updated `jquery-ui` to 1.13.2 (BSP-4910)

# 9.4.11
* [Documentation](https://aui.atlassian.com/aui/9.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.4.11)

## Fixed
* Updated webpack to 5.76.2 that fixes vulnerability (BSP-4740)

# 9.4.10
* [Documentation](https://aui.atlassian.com/aui/9.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.4.10)

## Fixed
* Updated jquery-ui to 1.13.2 that fixes [CVE-2022-31160](https://www.cve.org/CVERecord?id=CVE-2022-31160) (AUI-5456)
* Fixed responsive layout related CSS selector (for aui-dialog2) (AUI-5455)
* Fix the filtering of options with the same labels, but different values/ids in Single Select view (AUI-5454)


# 9.4.9
* [Documentation](https://aui.atlassian.com/aui/9.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.4.9)

## Fixed
* Fixed race condition in Checkbox and Radio components (AUI-5445)
* Fix tooltip width issue (AUI-5430)

## Changed
* Made passing an empty alt to the avatar template possible (AUI-5448)
* Added ability to has simplified aria-label text from title attribute (example: Edit (Type 'e') => Edit Type 'e')
* When opening a dropdown, the focus is now set on the first/last item instead of the dropdown container (AUI-5452)


# 9.4.8

* [Documentation](https://aui.atlassian.com/aui/9.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.4.8)

## Fixed
* Fix package minification issue

# 9.4.7

* [Documentation](https://aui.atlassian.com/aui/9.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.4.7)

## Fixed
* Fixed AUI failing parcel builds (AUI-5439)

# 9.4.6
* [Documentation](https://aui.atlassian.com/aui/9.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.4.6)

# 9.4.5

* [Documentation](https://aui.atlassian.com/aui/9.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.4.5)

## Fixed
* Excluded dom4j dependency (BSP-3722)
* Upgrade async to 3.2.2 (BSP-3801)
* Upgrade atlassian-spring-scanner to 2.2.5 (BSP-3814)
* Updated the gson version to 2.8.9 (BSP-3718)
* Fix the dropdown selector is scrolling instead of anchoring issue (BSP-4032)
* Fix the dropdown position change and not able to see all items (BSP-3808)
* Fix the placeholder contrast ratio issue (AUI-5442)

## Changed
* Updated aui-placeholder color to use ak-color-N300 to meet WCAG for input background contrast color (AUI-5442)
* Updated ak-color-N30 color to increase contrast ratio between aui-form-field-hover-bg-color and aui-placeholder (AUI-5442)
* Added --aui-appheader-quicksearch-placeholder-text-color to meet WCAG for app header's quick search (AUI-5442)

# 9.4.4

* [Documentation](https://aui.atlassian.com/aui/9.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.4.4)

## Fixed
* Asynchronous initialisation of AUI header (AUI-5432). See AUI header documentation for details.
* Vulnerability in jquery-ui used by aui (VULN-627502, VULN-627503, VULN-627504)
* Vulnerability of XSS fix in jquery-form component (VULN-539557)
* Updated css-what version to 5.0.1 (BSP-3269)
* nth-check npm version upgraded to 2.0.1 (BSP-3270)
* Updated ajv from 5.5.2 to 6.12.3 (BSP-3266)
* Updated ansi-regex version to 5.0.1 (BSP-3268)
* Update lodash version to 4.17.21 (BSP-3635)
* Update open version to 8.4.0 (BSP-3634)
* Updated unset-value to 2.0.1 (BSP-3668)
* Updated json-schema to 0.4.0 and wait-on to 6.0.1 (BSP-3716)
* Updated trim-newlines to 3.0.1 or 4.0.1 (BSP-3670)
* Updated marked to 4.0.12 (BSP-3740)
* Updated qs to 6.5.2 or higher (BSP-3666)
* Updated plist to 3.0.4 (BSP-3675)
* Updated normalize-url to 4.5.1 (BSP-3753)
* Updated commons-io to 2.7 (BSP-3767)
* Reply CSS classes from the original app header dropdown trigger to the new responsive trigger within the "More" dropdown (MNSTR-6095) (it's an opt-in behaviour in this bugfix AUI version)
* Updated axios to 0.26.1 (BSP-3717)
* Updated engine-io to 6.1.3 (BSP-3719)

# 9.4.3

* [Documentation](https://aui.atlassian.com/aui/9.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.4.3)

* Re-run release

# 9.4.2 - botched release

* [Documentation](https://aui.atlassian.com/aui/9.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.4.2)

## Fixed
* Vulnerability in jquery-ui used by aui (VULN-627502, VULN-627503, VULN-627504)
* Prevent AUI overriding the `AJS.I18n` after WRM has set it, for compatibility with the two phase JS I18n transform (AUI-5431)

# 9.4.1

* [Documentation](https://aui.atlassian.com/aui/9.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.4.1)

## Fixed

* Date picker sets proper value when underlying input has `type=date` (AUI-5380)
* Makes chevron glyph consistent across dropdowns (AUI-5417)

## Added

* Removing the message from DOM after being visually hidden (AUI-5103)
* Added auiSelectField Soy template as a type of `aui.forms.field` in `form.soy` (MNSTR-5602)
* Added `enable` and `disable` options to `tooltip()` (AUI-5428)

# 9.4.0

* [Documentation](https://aui.atlassian.com/aui/9.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.4.0)

## Added

* Closing of any layered element can be prevented (AUI-5273)

## Changed

* Updated build dependencies
    * Compiled using webpack 5 (AUI-5331)
    * Updated autoprefixer 9.8.6 -> 10.2.5
    * Updated cssnano 4.10.0 -> 5.0.2
    * Updated postcss 7.0.25 -> 8.2.15
* Forms with `form.aui` CSS styles are now using `static` positioning instead of `relative`. This fixes an issue with
  inline-dialogs rendering incorrectly when placed inside forms that are inside dialogs (AUI-5353)
* `z-index` values revised on several components.
    * See the `Removed` section for components where explicit `z-index` values were removed.
    * AUI sidebar `z-index` decreased to `1` down from `3`. (AUI-5330)
    * AUI Tooltip `z-index` decreased to `5000` down from `9999`.
    * AUI Select2 `z-index` decreased to `3001` down from `9999`.

## Removed

* Explicit `z-index` values were removed from the following components:
    * AUI application header (was `4`). (AUI-5330)
    * AUI Inline Dialog (was `100`, but effectively `3000` when opened by layer manager).
    * Closeable AUI messages (was `4000`).


# 9.3.23 [Unreleased]
* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.23)

## Changed
* (internal) Builds are now using Bamboo instead of Bitbucket Pipelines (DCA11Y-1356)
* (internal) The integration tests are now using LambdaTest instead of BrowserStack (DCA11Y-1426)

# 9.3.22
* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.22)

## Changed
* (internal) The builds in Bitbucket Pipelines are now using the `sfp-base-agent` image, shared with the
  builds in Bamboo (DCA11Y-956)
* (internal) The builds in Bitbucket Pipelines now use Node 22 (DCA11Y-1029)
* (internal) Changed the bundle size reporting during Bitbucket Pipelines builds to be more
  developer-friendly (DCA11Y-750)

# 9.3.21
* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.21)

## Added
* `ciTools` and pipeline step for creating upmerge PRs automatically (DCA11Y-542)

## Changed
* Updated `jQuery.TableSorter` to 2.17.8 + prevent empty `headerTemplate` (DCA11Y-957)

## Fixed
* Updated `jQuery.TableSorter` to 2.17.8 + prevent empty `headerTemplate` (DCA11Y-957)
* Patched `tablesorter` to avoid replacement of literal `{icon}` in the content of the
  header with non-empty `headerTemplate` (DCA11Y-957)

## Breaking
* Sortable tables and `tablesorter` only allow a limited set of characters to prevent an inadvertent XSS if consumers pass UGC (DCA11Y-957)

# 9.3.20
* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.20)

## Changed
* Updated `browserslist` to 4.21.5 (BSP-4929)
* Updated `nanoid` to 3.2.0 (BSP-4932)
* Updated `glob-parent` to 6.0.2 (BSP-4944)
* Updated `json5` to 2.2.3 (BSP-4956)
* Updated `hosted-git-info` to 4.0.0 (BSP-4997)
* Updated `follow-redirects` to 1.15.0 (BSP-4998)
* Updated `minimist` to 0.2.4 (BSP-5000)
* Updated `minimatch` to 3.0.5 (BSP-5001)
* Updated `decode-uri-component` to 0.2.2 (BSP-5002)
* Updated `Uglify-js` to 3.14.3 (BSP-5003)
* Updated `nth-check` to 2.0.1 (BSP-4908)
* Updated `debug` to 2.6.9 (BSP-5013)
* Updated `ms` to 2.1.3 (BSP-5014)
* Updated `qs` to 6.11.1 (BSP-4949)
* Updated `ramda` to 0.29.0 (BSP-4991)
* Updated `marked` to 4.0.12 (BSP-5010)
* Updated `i` to 0.3.7 (BSP-5039)
* Updated `istanbul-reports` to 3.1.5 (BSP-5038)
* Updated `terser` to 5.16.5 (BSP-5036)
* Updated `@sideway/formula` to 3.0.1 (BSP-5029)
* Updated `engine.io` to 6.4.2 (BSP-5022)
* Updated `ua-parser-js` to 6.4.2 (BSP-5024)
* Updated `commons-codec:commons-codec` to 1.15 (BSP-5031)
* Updated `mocha` to 10.1.0 (BSP-4999)
* Fix vulnerability of `request` (BSP-5099)
* Fix vulnerability of `tough-cookie` updated to 4.1.3 (BSP-5186)
* Updated `jquery-ui` to 1.13.2 (BSP-4910)

# 9.3.19

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.19)

## Fixed
* Fix the filtering of options with the same labels, but different values/ids in Single Select view (AUI-5454)

## Changed
* Updated `less` to 3.13.1 (DCA11Y-305)

# 9.3.18

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.18)

## Fixed
* Updated jquery-ui to 1.13.2 that fixes [CVE-2022-31160](https://www.cve.org/CVERecord?id=CVE-2022-31160) (AUI-5456)

# 9.3.17

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.17)

## Fixed
* Fixed responsive layout related CSS selector (for aui-dialog2) (AUI-5455)

## Changed
* Made passing an empty alt to the avatar template possible (AUI-5448)
* Added ability to has simplified aria-label text from title attribute (example: Edit (Type 'e') => Edit Type 'e')
* When opening a dropdown, the focus is now set on the first/last item instead of the dropdown container (AUI-5452)


# 9.3.16

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.16)

## Fixed
* Fixed race condition in Checkbox and Radio components (AUI-5445)
* Fix tooltip width issue (AUI-5430)


# 9.3.15

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.15)

## Fixed
* Fix package minification issue

# 9.3.14

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.14)

## Fixed
* Fix the dropdown selector is scrolling instead of anchoring issue (BSP-4032)
* Fix the dropdown position change and not able to see all items (BSP-3808)
* Fix the placeholder contrast ratio issue (AUI-5442)

## Changed
* Updated aui-placeholder color to use ak-color-N300 to meet WCAG for input background contrast color (AUI-5442)
* Updated ak-color-N30 color to increase contrast ratio between aui-form-field-hover-bg-color and aui-placeholder (AUI-5442)
* Added --aui-appheader-quicksearch-placeholder-text-color to meet WCAG for app header's quick search (AUI-5442)
* Added a new option to the Tooltip component for disabling the aria-describedby-related logic on trigger elements (AUI-5447)
* Added DOMpurify library to sanitize HTML in tooltip.js against XSS vulnerabilites which appeared in this component (RAID-3189)

# 9.3.13

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.13)

## Fixed
* Excluded dom4j dependency (BSP-3722)
* Upgrade async to 3.2.2 (BSP-3801)
* Upgrade atlassian-spring-scanner to 2.2.5 (BSP-3814)
* Updated the gson version to 2.8.9 (BSP-3718)
* Fix the dropdown selector is scrolling instead of anchoring issue (BSP-4032)

## Changed
* Updated aui-focus color to use ak-color-B200 to meet accessibility guidelines for focus ring contrast colour. Remember to review any overides of --aui-focus (AUI-5438)

# 9.3.12

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.12)

## Fixed
* Reply CSS classes from the original app header dropdown trigger to the new responsive trigger within the "More" dropdown (MNSTR-6095)

  (it's an opt-in behaviour in this bugfix AUI version)

# 9.3.11

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.11)

## Fixed
* Fix the empty tooltip shown for empty title (AUI-5434)
* Modify the tooltip component to avoid makes a redundant DOM update and title function call if passed (AUI-5433)
* Updated json-schema to 0.4.0 and wait-on to 6.0.1 (BSP-3716)
* Updated trim-newlines to 3.0.1 or 4.0.1 (BSP-3670)
* Updated marked to 4.0.12 (BSP-3740)
* Updated tar version to 6.1.10 (BSP-3741)
* Updated qs to 6.5.2 or higher (BSP-3666)
* Updated commons-io to 2.7 (BSP-3767)
* Updated axios to 0.26.1 (BSP-3717)
* Updated engine-io to 6.1.3 (BSP-3719)

# 9.3.10 - failed release

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.10)

## Fixed
* Fix the empty tooltip shown for empty title (AUI-5434)
* Modify the tooltip component to avoid makes a redundant DOM update and title function call if passed (AUI-5433)

# 9.3.9

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.9)

## Fixed
* Asynchronous initialisation of AUI header (AUI-5432). See AUI header documentation for details.
* Vulnerability in jquery-ui used by aui (VULN-627502, VULN-627503, VULN-627504)
* Vulnerability of XSS fix in jquery-form component (VULN-539557)
* Updated css-what version to 5.0.1 (BSP-3269)
* nth-check npm version upgraded to 2.0.1 (BSP-3270)
* Updated ajv from 5.5.2 to 6.12.3 (BSP-3266)
* Updated ansi-regex version to 5.0.1 (BSP-3268)
* Update lodash version to 4.17.21 (BSP-3635)
* Updated unset-value to 2.0.1 (BSP-3668)

# 9.3.8 - failed release

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.8)

## Fixed
* Asynchronous initialisation of AUI header (AUI-5432). See AUI header documentation for details.
* Vulnerability in jquery-ui used by aui (VULN-627502, VULN-627503, VULN-627504)
* Vulnerability of XSS fix in jquery-form component (VULN-539557)
* Updated css-what version to 5.0.1 (BSP-3269)
* nth-check npm version upgraded to 2.0.1 (BSP-3270)
* Updated ajv from 5.5.2 to 6.12.3 (BSP-3266)
* Updated ansi-regex version to 5.0.1 (BSP-3268)

# 9.3.7

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.7)

* Re-run release

# 9.3.6 - failed release

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.6)

## Fixed
* Vulnerability in jquery-ui used by aui (VULN-627502, VULN-627503, VULN-627504)
* Prevent AUI overriding the `AJS.I18n` after WRM has set it, for compatibility with the two phase JS I18n transform (AUI-5431)

# 9.3.5

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.5)

## Fixed

* Date picker sets proper value when underlying input has `type=date` (AUI-5380)
* Date picker support for `minDate` and `maxDate` options (AUI-5376)
* CalendarWidget is properly exposed under `window.AJS` in `aui.prototyping.js` and `aui.prototyping.nodeps.js` (AUI-5377)
* The `.tooltip()` won't throw an error when it cannot find the trigger in the document (AUI-5401)
* Added `enable` and `disable` options to `tooltip()` (AUI-5428)
* Makes chevron glyph consistent across dropdowns (AUI-5417)

## Deprecated

* The alignment arrows on layered components (`aui-inline-dialog2`, `aui-dropdown2`) are going to be removed in AUI 10.0.0 (AUI-5170)
  * The undocumented `aui-dropdown2-tailed` CSS class prints deprecation warning

## Added

* Added auiSelectField Soy template as a type of `aui.forms.field` in `form.soy` (MNSTR-5602)

# 9.3.4

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.4)

## Changed

* Updated underscore 1.10.2 -> 1.13.1. See https://underscorejs.org/#changelog for full list of changes in underscore.

# 9.3.3

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.3)

## Fixed

* AUI icons in layers nested in application header will not be set to 24px in size. (AUI-5221, AUI-5368)

### Atlassian-Plugin

* Fixed translations in `AJS.whenIType` keyboard shortcuts module. (AUI-5370)

# 9.3.2

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.2)

## Changed

* Icons in `.aui-header-before` section reduced in size from 24px to 16px (AUI-5368)
* Tooltip timeout reduced from 500ms to 300ms (AUI-5369).

## Fixed

* Badges have a border again when placed on sidebar (AUI-5346)
* Avatars have the correct size in layered elements on the header (AUI-5348)
* Radio and checkbox form fields style are applied to direct label only (AUI-5343)
* `.assistive` properties are declared with `!important` rule (AUI-5343)
* Content in `.aui-header-before` section receives correct text color (AUI-5368)
* Tooltips no longer add scrollbars to pages after first appearance on the page (AUI-5366)

# 9.3.1

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.1)

## Changed

* All layered elements - Dropdown, Inline Dialog, Modal Dialog, Flag - explicitly set a number of inheritable CSS
  properties in order to prevent undesirable cascades: `color`, `font-variant`, `font-style`, `font-weight`,
  `letter-spacing`, `line-height`, `text-align`, and `white-space`.

## Fixed

* Text inside Inline dialogs nested in AUI application header have correct body text color (AUI-5337).
* Links inside Inline dialogs nested in AUI application header have correct link color (AUI-5337).
* Dropdowns and Inline Dialogs nested in AUI table headers have correct font weight (AUI-5339).
* Dropdowns and Inline Dialogs nested in AUI toolbars wrap text correctly (AUI-5344).
* Global browser focus ring styles are no longer included by default (AUI-5325)
* Tooltip styles now match ADG (AUI-5350)
* Selec2 clear icon shows correctly (AUI-5342).
* Select2 has the correct ADG dropdown styles (AUI-5345)

## Deprecated

### Atlassian-Plugin

* The `jQuery().tipsy()` function and styles are available via a `com.atlassian.auiplugin:jquery-tipsy` web-resource.
  This will be removed in a future major version.

# 9.3.0

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.0)

## Changed

* Swap jQuery.tipsy with Popper.js to handle AUI tooltips (AUI-5253)
* Updated underscore 1.10.2 -> 1.12.0

## Removed

* The `jQuery().tipsy()` function is no longer provided by AUI.


# 9.2.16 [Unreleased]
* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.16)

## Changed
* (internal) The builds in Bitbucket Pipelines are now using the `sfp-base-agent` image, shared with the
  builds in Bamboo (DCA11Y-956)
* (internal) The builds in Bitbucket Pipelines now use Node 22 (DCA11Y-1029)
* (internal) Changed the bundle size reporting during Bitbucket Pipelines builds to be more
  developer-friendly (DCA11Y-750)
* (internal) Builds are now using Bamboo instead of Bitbucket Pipelines (DCA11Y-1356)
* (internal) The integration tests are now using LambdaTest instead of BrowserStack (DCA11Y-1426)

# 9.2.15
* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.15)

## Fixed
* Built with Firefox 44 for Confluence using Selenium 2

# 9.2.14
* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.14)

## Changed
* `ciTools` are now used from the external package `@atlassian/aui-ci-tools`
  rather than bundled in `packages/ciTools` because they're *about* AUI rather
  than *a part of* AUI. (DCA11Y-751)
* Updated `jQuery.TableSorter` to 2.17.8 + prevent empty `headerTemplate` (DCA11Y-957)

## Fixed
* Updated `jQuery.TableSorter` to 2.17.8 + prevent empty `headerTemplate` (DCA11Y-957)
* Patched `tablesorter` to avoid replacement of literal `{icon}` in the content of the
  header with non-empty `headerTemplate` (DCA11Y-957)

## Breaking
* Sortable tables and `tablesorter` only allow a limited set of characters to prevent an inadvertent XSS if consumers pass UGC (DCA11Y-957)

# 9.2.13
* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.13)

## Added
* `ciTools` and pipeline step for creating upmerge PRs automatically (DCA11Y-542)

# 9.2.12
* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.12)

## Changed
* Updated `browserslist` to 4.21.5 (BSP-4929)
* Updated `nanoid` to 3.2.0 (BSP-4932)
* Updated `glob-parent` to 6.0.2 (BSP-4944)
* Updated `hosted-git-info` to 4.0.0 (BSP-4997)
* Updated `follow-redirects` to 1.15.0 (BSP-4998)
* Updated `minimist` to 0.2.4 (BSP-5000)
* Updated `json5` to 2.2.3 (BSP-4956)
* Updated `minimatch` to 3.0.5 (BSP-5001)
* Updated `decode-uri-component` to 0.2.2 (BSP-5002)
* Updated `Uglify-js` to 3.14.3 (BSP-5003)
* Updated `nth-check` to 2.0.1 (BSP-4908)
* Updated `jquery-ui` to 1.13.2 (BSP-4910)
* Updated `debug` to 2.6.9 (BSP-5013)
* Updated `ms` to 2.1.3 (BSP-5014)
* Updated `qs` to 6.11.1 (BSP-4949)
* Updated `ramda` to 0.29.0 (BSP-4991)
* Updated `marked` to 4.0.12 (BSP-5010)
* Updated `i` to 0.3.7 (BSP-5039)
* Updated `istanbul-reports` to 3.1.5 (BSP-5038)
* Updated `terser` to 5.16.5 (BSP-5036)
* Updated `@sideway/formula` to 3.0.1 (BSP-5029)
* Updated `engine.io` to 6.4.2 (BSP-5022)
* Updated `ua-parser-js` to 6.4.2 (BSP-5024)
* Updated `commons-codec:commons-codec` to 1.15 (BSP-5031)
* Updated `mocha` to 10.1.0 (BSP-4999)
* Fix vulnerability of `request` (BSP-5099)
* Fix vulnerability of `tough-cookie` updated to 4.1.3 (BSP-5186)

# 9.2.11
* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.11)

## Changed
* Updated `less` to 3.13.1 (DCA11Y-305)

# 9.2.10

* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.10)

## Fixed
* Fix the dropdown selector is scrolling instead of anchoring issue (BSP-4032)
* Fix the dropdown position change and not able to see all items (BSP-3808)
* Fix the placeholder contrast ratio issue (AUI-5442)
* Fix labelling of the Sidebar expand/collapse button (AUI-5447)
* Fixed race condition in Checkbox and Radio components (AUI-5445)
* Fix the filtering of options with the same labels, but different values/ids in Single Select view (AUI-5454)

## Changed
* Updated aui-placeholder color to use ak-color-N300 to meet WCAG for input background contrast color (AUI-5442)
* Updated ak-color-N30 color to increase contrast ratio between aui-form-field-hover-bg-color and aui-placeholder (AUI-5442)
* Added --aui-appheader-quicksearch-placeholder-text-color to meet WCAG for app header's quick search (AUI-5442)
* Made passing an empty alt to the avatar template possible (AUI-5448)
* Added ability to has simplified aria-label text from title attribute (example: Edit (Type 'e') => Edit Type 'e')
* When opening a dropdown, the focus is now set on the first/last item instead of the dropdown container (AUI-5452)

# 9.2.9

* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.9)

## Fixed
* Excluded dom4j dependency (BSP-3722)
* Upgrade async to 3.2.2 (BSP-3801)
* Upgrade atlassian-spring-scanner to 2.2.5 (BSP-3814)
* Updated the gson version to 2.8.9 (BSP-3718)

## Changed
* Updated aui-focus color to use ak-color-B200 to meet accessibility guidelines for focus ring contrast colour. Remember to review any overides of --aui-focus (AUI-5438)

# 9.2.8

* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.8)

## Fixed
* Updated unset-value to 2.0.1 (BSP-3668)
* Updated tar version to 6.1.10 (BSP-3741)
* Updated json-schema to 0.4.0 and wait-on to 6.0.1 (BSP-3716)
* Updated trim-newlines to 3.0.1 or 4.0.1 (BSP-3670)
* Updated marked to 4.0.12 (BSP-3740)
* Updated qs to 6.5.2 or higher (BSP-3666)
* Updated plist to 3.0.4 (BSP-3675)
* Updated commons-io to 2.7 (BSP-3767)

# 9.2.7

* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.7)

## Fixed
* Asynchronous initialisation of AUI header (AUI-5432). See AUI header documentation for details.
* Vulnerability in jquery-ui used by aui (VULN-627502, VULN-627503, VULN-627504)
* Vulnerability of XSS fix in jquery-form component (VULN-539557)
* Updated css-what version to 5.0.1 (BSP-3269)
* nth-check npm version upgraded to 2.0.1 (BSP-3270)
* Updated ajv from 5.5.2 to 6.12.3 (BSP-3266)
* Updated ansi-regex version to 5.0.1 (BSP-3268)
* Updated unset-value to 2.0.1 (BSP-3668)

# 9.2.6

* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.6)

* Re-run release

# 9.2.5 - failed release

* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.5)

## Fixed
* Vulnerability in jquery-ui used by aui (VULN-627502, VULN-627503, VULN-627504)
* Prevent AUI overriding the `AJS.I18n` after WRM has set it, for compatibility with the two phase JS I18n transform (AUI-5431)

# 9.2.4

* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.4)

## Fixed

* Makes chevron glyph consistent across dropdowns (AUI-5417)

## Added

* Added auiSelectField Soy template as a type of `aui.forms.field` in `form.soy` (MNSTR-5602)

# 9.2.3

* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.3)

## Fixed

* Date picker sets proper value when underlying input has `type=date` (AUI-5380)

## Added

* Added `lazyLoadImage` attibute to `avatar.soy` template (MNSTR-5545)

# 9.2.2

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.2)

## Changed

* Updated underscore 1.10.2 -> 1.13.1. See https://underscorejs.org/#changelog for full list of changes in underscore.

## Fixed

### Atlassian-Plugin

* Fixed translations in `AJS.whenIType` keyboard shortcuts module. (AUI-5370)

# 9.2.1

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.1)

## Changed

* Built using Node v12
* Production dependency versions are pinned, so that they do not change between builds
    * jQuery Tipsy version bumped 1.3.1 -> 1.3.3

## Fixed

* RestfulTable autoFocus on CreateRow event not turning off (AUI-5048)
* AUI Toggle component rendering in IE11 (AUI-5074)
* Aligned underline for link button that uses text and icon (AUI-5306)
* Fixed XSS vulnerability in Tabs component (AUI-5080)
* Tabbing order in AUI dialog2 component now takes into account iframes (AUI-5290)

# 9.2.0

* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.0)

## Added

* Sortable Table now accepts options via its JavaScript API. See the documentation for details.
* Badges have three new variants: `.aui-badge-added`, `.aui-badge-removed`, and `.aui-badge-important`.

## Changed

* Bumped build and minifier dependencies.
* The navigation items CSS pattern now accepts arbitrary elements as children, not just `<span>` or `<a>` elements.

## Fixed

* Date picker's background colour is correct in light mode.
* Select2's background color is set correctly.
* Chevrons do not repeat on standard `<select>` elements.

## Removed

* The `Navigation#hideMoreItems` method has been removed -- it has not worked for years and nobody seemed to mind ;)

### Atlassian-Plugin

# 9.1.11

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.11)

## Fixed
* Upgrade async to 3.2.2 (BSP-3801)
* Upgrade atlassian-spring-scanner to 2.2.5 (BSP-3814)
* Updated the gson version to 2.8.9 (BSP-3718)
* Fix the dropdown selector is scrolling instead of anchoring issue (BSP-4032)
* Fix the dropdown position change and not able to see all items (BSP-3808)
* Removed IE7 styling causing build failures(BSP-4420)
* Updated karma version to 6.4.1 (BSP-4590)

# 9.1.10

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.10)

## Fixed
* Vulnerability in jquery-ui used by aui (VULN-627502, VULN-627503, VULN-627504)
* Vulnerability of XSS fix in jquery-form component (VULN-539557)
* Updated css-what version to 5.0.1 (BSP-3269)
* nth-check npm version upgraded to 2.0.1 (BSP-3270)
* Updated ajv from 5.5.2 to 6.12.3 (BSP-3266)
* Updated ssri version to 6.0.2 (BSP-3505)
* Updated ansi-regex version to 5.0.1 (BSP-3268)
* Update lodash version to 4.17.21 (BSP-3635)
* Update open version to 8.4.0 (BSP-3634)
* Updated unset-value to 2.0.1 (BSP-3668)
* Updated tar version to 6.1.10 (BSP-3741)
* Updated json-schema to 0.4.0 and wait-on to 6.0.1 (BSP-3716)
* Updated trim-newlines to 3.0.1 or 4.0.1 (BSP-3670)
* Updated marked to 4.0.12 (BSP-3740)
* Updated qs to 6.5.2 or higher (BSP-3666)
* Updated plist to 3.0.4 (BSP-3675)
* Updated commons-io to 2.7 (BSP-3767)
* Updated axios to 0.26.1 (BSP-3717)
* Updated engine-io to 6.1.3 (BSP-3719)
* Excluded dom4j dependency (BSP-3722)

# 9.1.9

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.9)

## Fixed

* Date picker sets proper value when underlying input has `type=date` (AUI-5380)
* Makes chevron glyph consistent across dropdowns (AUI-5417)

# 9.1.8

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.8)

## Changed

* Updated underscore 1.10.2 -> 1.13.1. See https://underscorejs.org/#changelog for full list of changes in underscore.

## Fixed

* Fixed translations in `AJS.whenIType` keyboard shortcuts module. (AUI-5370)

# 9.1.7

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.7)

## Changed

* Built using Node v12
* Updated build dependencies
    * Updated autoprefixer 6.7.7 -> 6.8.9
    * Updated cssnano 3.10.0 -> 4.1.10
    * Updated postcss 5 -> 7.0.35
    * Replaced uglifyjs-webpack-plugin with terser-webpack-plugin 4.2.3
* Production dependency versions are pinned, so that they do not change between builds
    * jQuery Tipsy version bumped 1.3.1 -> 1.3.3

# 9.1.6

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.6)

## Changed

* Compiled with babel v7.13

## Fixed

* RestfulTable autoFocus on CreateRow event not turning off (AUI-5048)
* AUI Toggle component rendering in IE11 (AUI-5074)
* Aligned underline for link button that uses text and icon (AUI-5306)
* Fixed XSS vulnerability in Tabs component (AUI-5080)

# 9.1.5

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.5)

## Fixed

* Tabbing order in AUI dialog2 component now takes into account iframes (AUI-5290)

# 9.1.4

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.4)

## Fixed

* Missing accessibility features in sidebar.soy template (AUI-5244)
* Application header styles for logo match other header items (AUI-5276)

# 9.1.3

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.3)

## Fixed

* Fixed XSS vulnerability in Dropdown2.

# 9.1.2

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.2)

## Fixed

* Form fields elements do not render double-sized focus-ring in browsers that natively support :focus-visible
* Expander component behaves properly when trying to toggle multiple times

## Changed

### Documentation

* AUI Documentation now properly describes multiple ways to consume AUI

### Single Select

* aui-option uses encodeURI for img-src escaping, allowing for usage of URLs with additional parameters and/or special
  characters

# 9.1.1

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.1)

## Fixed

* Trigger component no longer causes DOM exception when there are cross-origin iframes in the document

# 9.1.0

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.0)

## Fixed

### Dropdown

* Dropdown aligns properly when rendered in legacy Edge.

## Changed

* AUI's version of `Popper.js` has been bumped to v2.4.4, up from v1.16.1.
* layered components expose data-popper-placement attribute instead of x-placement as their internal API. x-placement
  attribute has been DEPRECATED and is kept for backwards compatibility only.

# 9.0.7

* [Documentation](https://aui.atlassian.com/aui/9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.0.7)

## Fixed

* Date picker sets proper value when underlying input has `type=date` (AUI-5380)

# 9.0.6

* [Documentation](https://aui.atlassian.com/aui/9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.0.6)

## Changed

* Updated underscore 1.10.2 -> 1.13.1. See https://underscorejs.org/#changelog for full list of changes in underscore.

## Fixed

### Atlassian-Plugin

* Fixed translations in `AJS.whenIType` keyboard shortcuts module. (AUI-5370)

# 9.0.5

* [Documentation](https://aui.atlassian.com/aui/9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.0.5)

## Changed

* Built using Node v12
* Updated build dependencies
    * Updated autoprefixer 6.7.7 -> 6.8.9
    * Updated cssnano 3.10.0 -> 4.1.10
    * Updated postcss 5 -> 7.0.35
    * Replaced uglifyjs-webpack-plugin with terser-webpack-plugin 4.2.3
* Production dependency versions are pinned, so that they do not change between builds
    * jQuery Tipsy version bumped 1.3.1 -> 1.3.3

# 9.0.4

* [Documentation](https://aui.atlassian.com/aui/9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.0.4)

This version contains all changes from up to 7.9.11 that were skipped in 7.10.1

## Changed

* Compiled with babel v7.13

## Fixed

* RestfulTable autoFocus on CreateRow event not turning off (AUI-5048)
* AUI Toggle component rendering in IE11 (AUI-5074)

## Security

* XSS vulnerability in dropdown component (AUI-5275)
* All XSS vulnerabilities fixed up to 7.9.11

# 9.0.3

* [Documentation](https://aui.atlassian.com/aui/9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.0.3)

## Fixed

### Documentation

* Contrast on elements in FlatApp has been corrected

### Select component

* Chevrons in select fields were adjusted for dark mode

### Layered components

* All layered components (i.e.: Dropdown) position correctly if loaded inside iFrame.

# 9.0.2

* [Documentation](https://aui.atlassian.com/aui/9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.0.2)

## Changed

### Date Picker component

* AUI date picker widgets can be destroyed and re-created

## Fixed

### Dropdown component

* Buttons in Dropdown items span the Dropdown's width properly

# 9.0.1

* [Documentation](https://aui.atlassian.com/aui/9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.0.1)

## Changed

### Toggle button component

* Documentation now correctly states that label is accepted as attribute only.
* It used to state that the component may accept label as attribute and property. It was never true.

### Documentation

* Dark mode toggle is now available across all our demo and test apps.

## Fixed

### Flag component

* Flag uses legacy-browsers friendly way of looping trough NodeList

### Toggle button component

* Toggle buttons should retain their icon size in more places.

# 9.0.0

* [Documentation](https://aui.atlassian.com/aui/9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.0.0)

## Highlights

* AUI no longer supports IE 11.
* AUI now has dark mode support!
* AUI's layered components have been audited and updated to improve their accessibility.
* AUI's patterns now include skip-links and landmarks for better screen reader and keyboard navigation support - they
  are applayed on docs website as well.
* All patterns planned to be removed in 9.0.0 will be removed in 10.0.0 instead.

## Added

* Dark mode for all AUI components and patterns. It can be enabled by adding an `aui-theme-dark` CSS class to `<body>`.
* A new skip link pattern has been added and documented.
* A new `.aui-close-button` pattern has been added. All elements that can be closed now use this pattern.
* All focusable elements have an explicit `:focus` and `:focus-visible` style by default.
* Form fields auto-filled by the browser have a new custom style.
* All layered elements may specify a dom container that will be used as appending point upon render.
    * Read the AUI 9 upgrade documentation for details on how to make best use of this property.

## Changed

* AUI's version of `Underscore` has been bumped to v1.10.2, up from v1.9.2.
* All AUI components should meet WCAG 2.0 AA colour contrast rates between text and background colours when used with
  AUI's page layout patterns.
* Layered components are considered closed by default. They are visible once an `open` attribute is added to them.
* Layered components reset their font-size to `1rem` and `text-align` to `left`.
* Layered components, except Dialog2, are no longer appended to the body.
* Animations for layered components are based on the presence or absence of an `open` attribute. Changing the `hidden`
  attribute is immediate and does not animate.
* Callbacks registered using a Dialog2 instance's `on` and `off` methods will only trigger when that specific dialog
  fires the event, as opposed to nested components.
* The `.aui-page-header`, `.aui-group`, `.aui-buttons`, and other positional patterns now use CSS flexbox for layout.
* AUI's datepicker no longer displays the placeholder by default. The config API was extended to set the placeholder
  explicitly.

### Avatar component

* Avatar sizes are now controlled by the `--aui-avatar-size` CSS property.

### Button component

* Button colours are now controlled by the `--aui-btn-bg`, `--aui-btn-border`, and `--aui-btn-text` CSS properties.
    * Read the AUI button documentation for details on how to make best use of these properties.

### Dropdown component

* ARIA attributes are no longer given discrete styles. CSS classes or basic HTML elements should be used instead.
    * Replaced selection of `[role=menuitem]` with `a` and `button`.
    * Replaced selection of `[role=menuitemcheckbox]` and `[role=checkbox]` with `.aui-dropdown2-checkbox`.
    * Replaced selection of `[role=menuitemradio]` and `[role=radio]` with `.aui-dropdown2-radio`.

### Icon component

* Icons are now `display:flex`. Icon glyphs are centered within the icon's display box using `margin:auto`.
* Icon sizes are now set by the `--aui-icon-size` CSS property, initially set within `.aui-icon`.
* Icon glyphs are now set by the `--aui-ig` CSS property, initially set within `.aui-icon`.

### Message component

* Message dimensions are now set by `--aui-message-padding` and `--aui-message-gutter`, initially set on `.aui-message`.
* Message dimensions are now re-used and selectively overridden by the Flag and Banner patterns.

### Page CSS reset

* Base font-size is defined on `html` instead of `body`. This means `rem` units will reflect AUI's default font size.
* CSS normalization rules needed for IE9, IE10 and IE11 have been removed.
* The hidden attribute is now declared with `display: none !important `.

### Page layout

* Page layout has been updated to simplify the placement of sidebar, <main>, and other content regions.
* Page layout and application header patterns now document skip links that should be added to a page, such that
  keyboard-only users can quickly jump to specific page regions and important actions.
* If the page includes a sidebar, the `#content` element should have only 2 child elements:
    * `<section class="aui-sidebar" aria-label="sidebar">` - houses the sidebar and its content.
    * `<section aria-label="page">` - houses all other page content.
    * See the updated page layout documentation, or the upgrade guide, for more details.

### Sidebar

* We changed `a` element from extend button to actual `button` element.
* We now use iconfont `aui-iconfont-chevron-double-left` instead of custom icon for the extend button.
* Sidebar width is now set by the `--aui-sidebar-width` CSS property, initially set within `.aui-page-sidebar`.

## Fixed

* Any dropdown trigger element that references a non-existent element will no longer throw an error on click or hover.
* Any dropdown item element with the `aui-dropdown2-interactive` class will *always* prevent default for any event
  triggered on it.
    * This is in line with the intent and behaviour described in the documentation.
    * Previously, events would be passed through if the item was either checked or disabled.
* Opening a dropdown via the down arrow no longer causes the document scroll position to jump.
* Layer positioning accounts for triggers inside iframe containers and should position themselves correctly.
* Date picker's year suffix will no longer render as the literal string "null" by default.
* Layered element's shadows should be visible in Edge.

### Accessibility

* All invalid usage of `aria-hidden` has been removed.
    * Read the upgrade guide to determine whether you need to make similar changes to your markup or usages of AUI
      components.
* All close buttons in all AUI components are part of the tab order and have a consistent label of "Close".
* Modal dialog accessibility is improved:
    * Modal dialogs are given keyboard focus and announced to screen readers when opened.
    * The documentation now outlines how to give a modal dialog an appropriate accessible label.
* Inline dialog accessibility is improved for toggle behaviour:
    * The contents of an open inline dialog can now be navigated to using only the keyboard.
    * Inline dialog element is now focused after opening.
    * The documentation now outlines how to give an inline dialog an appropriate accessible label. Note that the hover
      behaviour variant of inline dialog is inaccessible and is now deprecated.
* Application header menu accessibility is improved:
    * The pattern's `<nav>` element must now be labelled as "site".
    * The site logo is now a `<span>` instead of an `<h1>`. The `<h1>` is reserved for the page's main content heading.
* Dropdown component accessibility is improved:
    * Incorrect use of `role=application` and `role=presentation` have been removed.
    * The styles for hover and focus are visually distinct, making it easier for people using a keyboard and mouse
      simultaneously to tell what item has keyboard vs. mouse focus.
    * The first focusable item of a dropdown is focussed on opening the dropdown. This eases access for NVDA and JAWS
      users.
    * Dropdown group headings will be announced as focus moves in to the group. The headings are no longer navigable by
      screen reader's virtual cursors.
* Message pattern accessibility is improved:
    * Link and button text colours are now set to the message's standard text colour and are given an underline, to
      ensure colour contrast ratios are met.
    * Focus indicators are set to the message text colour, to ensure contrast ratios are met.

## Deprecated

* Inline dialog 2 hover functionality is deprecated and will be removed in the future.
* populateParameters function and params object is deprecated and will be removed in AUI 10.0.0.
* AUI's legacy dropdown1 component has been extracted to its own `@atlassian/aui-legacy-dropdown` package.
* AUI's legacy toolbar1 component has been extracted to its own `@atlassian/aui-legacy-toolbar` package.

## Removed

* All Atlassian brand logos have been removed. You can access them from
  the [@atlassian/brand-logos](https://www.npmjs.com/package/@atlassian/brand-logos) package.
* The `.aui-legacy-focus` class support has been removed. All focusable elements should have an appropriate focus ring
  applied.
* Message icons - we removed following classes
    * .aui-message .icon-close
    * .aui-icon-close
    * .aui-message .icon-close-inverted,
    * .aui-message.error .icon-close,
    * .aui-icon-close-inverted The `.aui-close-button` pattern should be used instead.
* jQuery ajaxSettings configuration was removed. Please specify it globaly for product or add it per request where
  needed.

# Previous versions

- [8.x.x](./changelogs/8.x.x.md)
- [7.x.x](./changelogs/7.x.x.md)
- [6.x.x](./changelogs/6.x.x.md)
- [5.x.x](./changelogs/5.x.x.md)
- [4.x.x](./changelogs/4.x.x.md)
