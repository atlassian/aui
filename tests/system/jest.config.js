module.exports = {
    reporters: [
        'default',
        [
            'jest-junit',
            {
                suiteName: 'AUI system-level tests',
                outputFile: 'test-reports/aui-system-test-suite.xml',
            },
        ],
    ],
};
