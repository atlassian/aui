const path = require('path');

function getVerdaccioConfig(sandboxDir) {
    const storageDir = path.resolve(sandboxDir, 'v', 'storage');
    const htpasswdFile = path.resolve(sandboxDir, 'v', 'htpasswd');

    const uplink = 'https://registry.npmjs.org/';

    const anonymous = '$anonymous';

    return {
        storage: storageDir,
        auth: {
            htpasswd: {
                file: htpasswdFile,
            },
        },
        uplinks: {
            npmjs: {
                url: uplink,
            },
        },
        self_path: './',
        packages: {
            '@*/*': {
                access: anonymous,
                publish: anonymous,
                proxy: 'npmjs',
            },
            '**': {
                access: anonymous,
                publish: anonymous,
                proxy: 'npmjs',
            },
        },
        log: {
            type: 'stdout',
            format: 'pretty',
            level: 'fatal',
        },
    };
}

module.exports = {
    getVerdaccioConfig,
};
