const { existsSync } = require('fs');
const { mkdir, rm, writeFile } = require('fs').promises;
const readline = require('readline/promises');
const { stdin: input, stdout: output } = require('process');
const { tmpdir } = require('os');
const path = require('path');
const { runServer } = require('verdaccio');
const { getVerdaccioConfig } = require('./verdaccio-config');

const sandboxDir = path.resolve(tmpdir(), 'aui-system-tests-local-npm');
const serverPort = 4873;

async function main() {
    const config = getVerdaccioConfig(sandboxDir);

    // Manage previous storage directory
    if (existsSync(config.storage)) {
        console.log(`The directory ${config.storage} exists.`);

        const rl = readline.createInterface({ input, output });
        const answer = await rl.question(
            'Do you want to use it? (y/n - delete it and create a fresh one): '
        );
        rl.close();

        if (answer.toLowerCase() === 'n') {
            await rm(config.storage, { recursive: true });
            console.log('Previous storage directory deleted.');
        } else {
            console.log('Using existing storage directory.');
        }
    }

    // Ensure the storage directory exists
    await mkdir(config.storage, { recursive: true });

    console.log(`Starting NPM registry in ${config.storage} at ${serverPort} ...`);
    const serverInstance = await runServer(config);
    serverInstance.listen(serverPort);
    const address = serverInstance.address();
    console.log(
        `Verdaccio server running at http://localhost:${address.port}/ (${address.address}, ${address.family}, ${address.port})`
    );

    await writeFile(
        path.resolve(sandboxDir, '.npmrc'),
        [
            `registry=http://localhost:${serverPort}/`,
            `@atlassian:registry=http://localhost:${serverPort}/`,
            `//localhost:${serverPort}/:_authToken="Ensure npm publish will carry the auth token as required"`,
        ].join('\n')
    );
    console.log('NPM registry configuration file created - to publish some package:');
    console.log(`    cp .npmrc .npmrc.bak; cp ${sandboxDir}/.npmrc .`);
    console.log('    npm publish --workspaces=false');
    console.log('    rm .npmrc; mv .npmrc.bak .npmrc');
    console.log('to install it for testing, in another shell, in a distant directory...:');
    console.log(`    cp ${sandboxDir}/.npmrc .`);
    console.log('    yarn init && yarn add @atlassian/aui@9.12.0-SNAPSHOT');
}

main();
