const { copyFileSync, renameSync, readFileSync, writeFileSync, existsSync } = require('fs');

function modifyPackageJson(packageJsonFilepath, modifyOperation) {
    let testPackageJsonContents = JSON.parse(readFileSync(packageJsonFilepath, 'utf8'));
    modifyOperation(testPackageJsonContents);
    writeFileSync(packageJsonFilepath, JSON.stringify(testPackageJsonContents, null, 2));

    return testPackageJsonContents;
}

function backupFile(filepath) {
    if (!existsSync(filepath + '.bak')) {
        copyFileSync(filepath, filepath + '.bak');
    }
}

function restoreFile(filepath) {
    if (!existsSync(filepath + '.bak')) {
        throw new Error(`Unable to restore ${filepath}. Backup file does not exist`);
    }

    renameSync(filepath + '.bak', filepath);
}

module.exports = {
    backupFile,
    modifyPackageJson,
    restoreFile,
};
