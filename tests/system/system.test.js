const { exec } = require('child_process');
const { existsSync } = require('fs');
const { mkdir, mkdtemp, writeFile } = require('fs').promises;
const { copy } = require('fs-extra');
const { afterAll, beforeAll, describe, expect, it } = require('@jest/globals');
const { tmpdir } = require('os');
const path = require('path');
const rimraf = require('rimraf');
const { promisify } = require('util');
const { runServer } = require('verdaccio');
const { getVerdaccioConfig } = require('./tools/verdaccio-config');
const { backupFile, modifyPackageJson, restoreFile } = require('./tools/utils');

const execAsync = promisify(exec);

const shouldSkip = process.env.SKIP_AUI_SYSTEM_TESTS === 'true';

(shouldSkip ? describe.skip : describe)('aui system', () => {
    const npmrcFilename = '.npmrc';
    const packageJsonFilename = 'package.json';
    let sandboxDir = undefined;
    let serverInstance = undefined;
    const serverPort = 4873;
    let packagePublishedVersion = undefined;

    beforeAll(
        async () => {
            const packageDir = path.resolve(__dirname, '..', '..', 'packages', 'core');
            const sourcePackageJsonFilepath = path.resolve(packageDir, packageJsonFilename);
            if (!existsSync(sourcePackageJsonFilepath)) {
                throw new Error(
                    `Unable to find ${sourcePackageJsonFilepath}. CWD=${process.cwd()}`
                );
            }

            await createSandbox();

            const npmrcFilepath = path.resolve(packageDir, npmrcFilename);
            backupFile(npmrcFilepath);
            backupFile(sourcePackageJsonFilepath);

            try {
                let testPackageJsonContents = modifyPackageJson(
                    sourcePackageJsonFilepath,
                    (contents) => {
                        if (contents.publishConfig) {
                            contents.publishConfig.registry = `http://localhost:${serverPort}/`;
                        }
                        contents.version = `${contents.version}-test`;
                    }
                );
                if (testPackageJsonContents.name !== '@atlassian/aui') {
                    throw new Error(`Invalid package (${testPackageJsonContents.name})!`);
                }

                await copy(
                    path.resolve(sandboxDir, npmrcFilename),
                    path.resolve(packageDir, npmrcFilename)
                );

                console.log('Starting NPM registry...');
                const config = getVerdaccioConfig(sandboxDir);
                await mkdir(config.storage, { recursive: true });
                serverInstance = await runServer(config);
                serverInstance.listen(serverPort);
                const address = serverInstance.address();
                console.log(
                    `Verdaccio server running at ${address.address}, ${address.family}, ${address.port}`
                );

                const { stderr, stdout } = await execAsync(
                    'npm publish --workspaces=false',
                    getSandboxExecOptions(packageDir)
                );
                packagePublishedVersion = testPackageJsonContents.version;
                console.log(
                    ['Package published:', `stdout:${stdout}`, `stderr:${stderr}`].join('\n')
                );
            } finally {
                restoreFile(npmrcFilepath);
                restoreFile(sourcePackageJsonFilepath);
            }
        },
        1 * 60 * 1000
    );

    afterAll(() => {
        if (process.env.CONTINUE_RUNNING_VERDACCIO === 'true') {
            console.log(
                `Keeping Verdaccio server running...\nTo use the .npmrc configured for it use:\nexport HOME=${sandboxDir}`
            );
            return;
        }

        return stopServer();
    });

    it.each(['test-install-standalone'])(
        'should install with npm',
        async (testName) => {
            await executeInstallTest(
                testName,
                'npm',
                /added \d+ packages in .*/,
                'package-lock.json'
            );
        },
        5 * 60 * 1000
    ); // The test usually run in a bit over a minute but this may need to be extended on mobile etc

    it.each(['test-install-standalone'])(
        'should install with yarn',
        async (testName) => {
            await executeInstallTest(testName, 'yarn', /success Saved lockfile/, 'yarn.lock');
        },
        5 * 60 * 1000
    ); // The test usually run in a bit over a minute but this may need to be extended on mobile etc

    async function executeInstallTest(testName, managerName, expectedOutput, lockfileName) {
        const testDir = path.resolve(__dirname, testName);
        const testPackageJsonFilepath = path.resolve(testDir, packageJsonFilename);

        backupFile(testPackageJsonFilepath);

        try {
            modifyPackageJson(testPackageJsonFilepath, (packageJsonContents) => {
                packageJsonContents.dependencies['@atlassian/aui'] = packagePublishedVersion;
            });

            const { stderr, stdout } = await execAsync(
                `${managerName} install`,
                getSandboxExecOptions(testDir)
            );
            console.log(`AUI installed.\nstdout:${stdout}\nstderr:\n${stderr}`);
            expect(stdout).toEqual(expect.stringMatching(expectedOutput));
        } catch (error) {
            expect(`Failed to install AUI with ${managerName}:${error}`).toBeFalsy();
        } finally {
            restoreFile(testPackageJsonFilepath);
            await rimraf.rimraf(path.resolve(testDir, 'node_modules'));
            await rimraf.rimraf(path.resolve(testDir, lockfileName));
        }
    }

    function stopServer() {
        expect(sandboxDir).toBeDefined();

        return new Promise((resolve, reject) => {
            if (serverInstance && serverInstance.close) {
                serverInstance.close(() => {
                    removeSandbox();
                    serverInstance = undefined;
                    resolve('Verdaccio server has been stopped.');
                });
            } else {
                reject('Server instance is not running or lacks a close method.');
            }
        });
    }

    function getSandboxExecOptions(cwd) {
        expect(sandboxDir).toBeDefined();

        const environment = Object.assign({}, process.env);
        environment.HOME = sandboxDir;
        Object.keys(environment).forEach((key) => {
            if (key.startsWith('npm_') || key.startsWith('PAC_')) {
                delete environment[key];
            }
        });

        return {
            stdio: 'inherit',
            cwd: cwd,
            env: environment,
        };
    }

    async function createSandbox() {
        expect(sandboxDir).toBeUndefined();
        sandboxDir = await mkdtemp(path.resolve(tmpdir(), 'aui-system-tests-'));
        console.log(`Creating sandbox: ${sandboxDir} ...`);

        await rimraf.rimraf(sandboxDir);
        await mkdir(sandboxDir, { recursive: true });
        await writeFile(
            path.resolve(sandboxDir, npmrcFilename),
            [
                `registry=http://localhost:${serverPort}/`,
                `@atlassian:registry=http://localhost:${serverPort}/`,
                `//localhost:${serverPort}/:_authToken="Ensure npm publish will carry the auth token as required"`,
            ].join('\n')
        );
    }

    function removeSandbox() {
        expect(sandboxDir).toBeDefined();

        console.log(`Removing sandbox: ${sandboxDir} ...`);
        rimraf.sync(sandboxDir);
    }
});
