import '@atlassian/aui/src/js/aui/tooltip';
import { hover, fixtures, afterMutations } from '../helpers/browser';
import { tooltipThemes } from './fixtures/tooltip';
import { defaultThemeKey } from './fixtures/common';
import $ from '@atlassian/aui/src/js/aui/jquery';

const theme = window.__karma__.config.auitheme || defaultThemeKey;
const a11yState = tooltipThemes[theme];
const TOOLTIP_TIMEOUT = 301;

describe(`aui/tooltip with theme=${theme}`, function () {
    let $trigger;

    beforeEach(() => {
        const { trigger } = fixtures(
            {
                trigger: '<div title="The tooltip" style="margin: 200px">Element<div>',
            },
            true
        );
        $trigger = $(trigger);
        $trigger.tooltip({});
    });

    afterEach(() => {
        $trigger.tooltip('destroy');
    });

    it(`hidden tooltip state should have ${a11yState.hiddenTooltipIssuesNumber} issues`, function (done) {
        afterMutations(() => {
            expect(document.body).to.be.exactNumberOfAccessibilityIssues(
                a11yState.hiddenTooltipIssuesNumber,
                done,
                this.test
            );
        }, TOOLTIP_TIMEOUT);
    });

    it(`hidden tooltip state should have [${a11yState.hiddenTooltipIssues.join()}] issues`, function (done) {
        afterMutations(() => {
            expect(document.body).to.be.exactAccessibilityIssues(
                a11yState.hiddenTooltipIssues,
                done,
                this.test
            );
        }, TOOLTIP_TIMEOUT);
    });

    it(`shown tooltip state should have ${a11yState.shownTooltipIssuesNumber} issues`, function (done) {
        hover($trigger);

        afterMutations(() => {
            expect(document.body).to.be.exactNumberOfAccessibilityIssues(
                a11yState.shownTooltipIssuesNumber,
                done,
                this.test
            );
        }, TOOLTIP_TIMEOUT);
    });

    it(`shown tooltip state should have [${a11yState.shownTooltipIssues.join()}] issues`, function (done) {
        hover($trigger);

        afterMutations(() => {
            expect(document.body).to.be.exactAccessibilityIssues(
                a11yState.shownTooltipIssues,
                done,
                this.test
            );
        }, TOOLTIP_TIMEOUT);
    });
});
