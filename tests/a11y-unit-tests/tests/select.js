import skate from '@atlassian/aui/src/js/aui/internal/skate';
import { blur, fixtures, focus, fakeTypingOut } from '../helpers/browser';
import { selectThemes } from './fixtures/select';
import { defaultThemeKey } from './fixtures/common';

function createAndSkate(html, target) {
    const constructed = fixtures(
        {
            singleSelect: html,
            styles: '<style>[hidden] { display: none !important; } .active { background-color: yellow; } </style>',
        },
        true,
        target
    );

    return skate.init(constructed.singleSelect);
}

function getInputFor(singleSelect) {
    return singleSelect._input;
}

const theme = window.__karma__.config.auitheme || defaultThemeKey;
const a11yState = selectThemes[theme];

describe(`aui/select with theme=${theme}.`, function () {
    let singleSelect;
    let input;

    beforeEach(function () {
        const wrapper = createAndSkate(`
                <form class="aui" style="background: white;">
                    <aui-label for="test-id">Test select</aui-label>
                    <aui-select id="test-id" name="foo">
                        <aui-option value="option1">Option 1</aui-option>
                        <aui-option value="option2">Option 2</aui-option>
                        <aui-option value="option2">Option 3</aui-option>
                    </aui-select>
                </form>
            `);
        singleSelect = wrapper.querySelector('aui-select');
        input = getInputFor(singleSelect);
    });

    it(`initialized select should have ${a11yState.initiatedStateIssuesNumber} issues`, function (done) {
        expect(document.body).to.have.exactNumberOfAccessibilityIssues(
            a11yState.initiatedStateIssuesNumber,
            done,
            this.test
        );
    });

    it(`initialized select should have [${a11yState.initiatedStateIssues.join()}] issues`, function (done) {
        expect(document.body).to.have.exactAccessibilityIssues(
            a11yState.initiatedStateIssues,
            done,
            this.test
        );
    });

    it(`opened select should have ${a11yState.openedStateIssuesNumber} issues`, function (done) {
        focus(singleSelect);

        expect(document.body).to.have.exactNumberOfAccessibilityIssues(
            a11yState.openedStateIssuesNumber,
            done,
            this.test
        );
    });

    it(`opened select should have [${a11yState.openedStateIssues.join()}] issues`, function (done) {
        focus(singleSelect);

        expect(document.body).to.have.exactAccessibilityIssues(
            a11yState.openedStateIssues,
            done,
            this.test
        );
    });

    it(`closed select should have ${a11yState.closedStateIssuesNumber} issues`, function (done) {
        focus(singleSelect);
        blur(singleSelect);

        expect(document.body).to.have.exactNumberOfAccessibilityIssues(
            a11yState.closedStateIssuesNumber,
            done,
            this.test
        );
    });

    it(`closed select should have [${a11yState.closedStateIssues.join()}] issues`, function (done) {
        focus(singleSelect);
        blur(singleSelect);

        expect(document.body).to.have.exactAccessibilityIssues(
            a11yState.closedStateIssues,
            done,
            this.test
        );
    });

    it(`select with empty suggestion list should have ${a11yState.withEmptySuggestionIssuesNumber} issues`, function (done) {
        focus(input);
        fakeTypingOut('Test');

        expect(document.body).to.have.exactNumberOfAccessibilityIssues(
            a11yState.withEmptySuggestionIssuesNumber,
            done,
            this.test
        );
    });

    it(`select with empty suggestion list should have [${a11yState.withEmptySuggestionIssues.join()}] issues`, function (done) {
        focus(input);
        fakeTypingOut('Test');

        expect(document.body).to.have.exactAccessibilityIssues(
            a11yState.withEmptySuggestionIssues,
            done,
            this.test
        );
    });

    it(`select with filtered suggestion list should have ${a11yState.withFilteredSuggestionIssuesNumber} issues`, function (done) {
        focus(input);
        fakeTypingOut('Option 1');

        expect(document.body).to.have.exactNumberOfAccessibilityIssues(
            a11yState.withFilteredSuggestionIssuesNumber,
            done,
            this.test
        );
    });

    it(`select with filtered suggestion list should have [${a11yState.withFilteredSuggestionIssues.join()}] issues`, function (done) {
        focus(input);
        fakeTypingOut('Option 1');

        expect(document.body).to.have.exactAccessibilityIssues(
            a11yState.withFilteredSuggestionIssues,
            done,
            this.test
        );
    });
});
