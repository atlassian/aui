import { afterMutations, fixtures, focus } from '../helpers/browser';
import { textFieldThemes } from './fixtures/text-field';
import { defaultThemeKey } from './fixtures/common';

const theme = window.__karma__.config.auitheme || defaultThemeKey;
const a11yState = textFieldThemes[theme];

describe(`aui/text-field with theme=${theme}`, function () {
    let form;
    let textfield;

    beforeEach(() => {
        form = fixtures(
            {
                form: `
        <form class="aui">
            <fieldset>
                <div class="field-group">
                    <label for="text-filed-1">First name<span class="aui-icon icon-required"> required</span></label>
                    <input class="text" type="text" id="text-filed-1" name="name" value="Test" aria-describedby="text-filed-1"/>
                    <div id="description" class="description">Default width input</div>
                </div>
            </fieldset>
        </form>
        `,
            },
            true
        ).form;

        textfield = form.querySelector('.text');
    });

    it(`default text field state should have ${a11yState.defaultTextFieldIssuesNumber} issues`, function (done) {
        expect(document.body).to.be.exactNumberOfAccessibilityIssues(
            a11yState.defaultTextFieldIssuesNumber,
            done,
            this.test
        );
    });

    it(`default text field state should have [${a11yState.defaultTextFieldIssues.join()}] issues`, function (done) {
        expect(document.body).to.be.exactAccessibilityIssues(
            a11yState.defaultTextFieldIssues,
            done,
            this.test
        );
    });

    it(`focused text field state should have ${a11yState.focusedTextFieldIssuesNumber} issues`, function (done) {
        focus(textfield);

        afterMutations(() => {
            expect(document.body).to.be.exactNumberOfAccessibilityIssues(
                a11yState.focusedTextFieldIssuesNumber,
                done,
                this.test
            );
        }, 200);
    });

    it(`focused text field state should have [${a11yState.focusedTextFieldIssues.join()}] issues`, function (done) {
        focus(textfield);

        afterMutations(() => {
            expect(document.body).to.be.exactAccessibilityIssues(
                a11yState.focusedTextFieldIssues,
                done,
                this.test
            );
        }, 200);
    });

    it(`disabled text field state should have ${a11yState.disabledTextFieldIssuesNumber} issues`, function (done) {
        textfield.setAttribute('disabled', '');

        afterMutations(() => {
            expect(document.body).to.be.exactNumberOfAccessibilityIssues(
                a11yState.disabledTextFieldIssuesNumber,
                done,
                this.test
            );
        }, 100);
    });

    it(`disabled text field state should have [${a11yState.disabledTextFieldIssues.join()}] issues`, function (done) {
        textfield.setAttribute('disabled', '');

        afterMutations(() => {
            expect(document.body).to.be.exactAccessibilityIssues(
                a11yState.disabledTextFieldIssues,
                done,
                this.test
            );
        }, 100);
    });
});
