import { addHoverClass, afterMutations, fixtures } from '../helpers/browser';
import { labelThemes } from './fixtures/label';
import { defaultThemeKey } from './fixtures/common';

const theme = window.__karma__.config.auitheme || defaultThemeKey;
const a11yState = labelThemes[theme];

describe(`aui/label with theme=${theme}`, function () {
    let label;
    let closeButton;

    beforeEach(() => {
        label = fixtures(
            {
                label: `
            <span id="closable-label" class="aui-label aui-label-closeable aui-label-split">
                <a class="aui-label-split-main" href="http://localhost:8080">link text</a>
                <span class="aui-label-split-close">
                    <span id="close-btn" tabindex="0" class="aui-icon aui-icon-close" title="title"></span>
                </span>
            </span>`,
            },
            true
        ).label;
        closeButton = label.querySelector('#close-btn');
    });

    it(`default state should have ${a11yState.defaultLabelIssuesNumber} issues`, function (done) {
        afterMutations(() => {
            expect(document.body).to.be.exactNumberOfAccessibilityIssues(
                a11yState.defaultLabelIssuesNumber,
                done,
                this.test
            );
            expect(document.body).to.be.exactAccessibilityIssues(
                a11yState.defaultLabelIssues,
                done,
                this.test
            );
        }, 200);
    });

    it(`hover close icon should have ${a11yState.hoverCloseLabelIssuesNumber} issues`, function (done) {
        const originalBg = getLabelBg();
        addHoverClass(closeButton);

        afterMutations(() => {
            // assert hover visual changes were applied
            expect(getLabelBg()).to.not.equal(originalBg);
            expect(document.body).to.be.exactNumberOfAccessibilityIssues(
                a11yState.hoverCloseLabelIssuesNumber,
                done,
                this.test
            );
            expect(document.body).to.be.exactAccessibilityIssues(
                a11yState.hoverCloseLabelIssues,
                done,
                this.test
            );
        }, 200);
    });

    const getLabelBg = () => {
        return window.getComputedStyle(label, null).getPropertyValue('background-color');
    };
});
