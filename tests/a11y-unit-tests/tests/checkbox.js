import { afterMutations, fixtures, click, focus } from '../helpers/browser';
import { checkboxThemes } from './fixtures/checkbox';
import { defaultThemeKey } from './fixtures/common';

const theme = window.__karma__.config.auitheme || defaultThemeKey;
const a11yState = checkboxThemes[theme];

describe(`aui/checkbox with theme=${theme}`, function () {
    let firstOption;

    beforeEach(() => {
        const form = fixtures(
            {
                form: `
        <form class="aui" style="background: #fff;">
            <fieldset class="group">
                <div class="checkbox">
                    <input type="checkbox" name="checkbox1" id="checkbox1">
                    <label for="checkbox1">Simple checkbox</label>
                </div>
            </fieldset>
        </form>`,
            },
            true
        ).form;

        firstOption = form.querySelector('#checkbox1');
    });

    it(`unchecked checkbox state should have ${a11yState.uncheckedCheckboxIssuesNumber} issues`, function (done) {
        afterMutations(() => {
            expect(document.body).to.be.exactNumberOfAccessibilityIssues(
                a11yState.uncheckedCheckboxIssuesNumber,
                done,
                this.test
            );
        }, 200);
    });

    it(`unchecked checkbox state should have [${a11yState.uncheckedCheckboxIssues.join()}] issues`, function (done) {
        afterMutations(() => {
            expect(document.body).to.be.exactAccessibilityIssues(
                a11yState.uncheckedCheckboxIssues,
                done,
                this.test
            );
        }, 200);
    });

    it(`checked checkbox state should have ${a11yState.checkedCheckboxIssuesNumber} issues`, function (done) {
        click(firstOption);

        afterMutations(() => {
            expect(document.body).to.be.exactNumberOfAccessibilityIssues(
                a11yState.checkedCheckboxIssuesNumber,
                done,
                this.test
            );
        }, 200);
    });

    it(`checked checkbox state should have [${a11yState.checkedCheckboxIssues.join()}] issues`, function (done) {
        click(firstOption);

        afterMutations(() => {
            expect(document.body).to.be.exactAccessibilityIssues(
                a11yState.checkedCheckboxIssues,
                done,
                this.test
            );
        }, 200);
    });

    it(`disabled checkbox state should have ${a11yState.disabledCheckboxIssuesNumber} issues`, function (done) {
        firstOption.setAttribute('disabled', '');

        afterMutations(() => {
            expect(document.body).to.be.exactNumberOfAccessibilityIssues(
                a11yState.disabledCheckboxIssuesNumber,
                done,
                this.test
            );
        }, 200);
    });

    it(`disabled checkbox state should have [${a11yState.disabledCheckboxIssues.join()}] issues`, function (done) {
        firstOption.setAttribute('disabled', '');

        afterMutations(() => {
            expect(document.body).to.be.exactAccessibilityIssues(
                a11yState.disabledCheckboxIssues,
                done,
                this.test
            );
        }, 200);
    });

    it(`focused checkbox state should have ${a11yState.focusedCheckboxIssuesNumber} issues`, function (done) {
        focus(firstOption);

        afterMutations(() => {
            expect(document.body).to.be.exactNumberOfAccessibilityIssues(
                a11yState.focusedCheckboxIssuesNumber,
                done,
                this.test
            );
        }, 200);
    });

    it(`focused checkbox state should have [${a11yState.focusedCheckboxIssues.join()}] issues`, function (done) {
        focus(firstOption);

        afterMutations(() => {
            expect(document.body).to.be.exactAccessibilityIssues(
                a11yState.focusedCheckboxIssues,
                done,
                this.test
            );
        }, 200);
    });
});
