import { afterMutations, fixtures } from '../helpers/browser';
import { iconThemes } from './fixtures/icon';
import { defaultThemeKey } from './fixtures/common';
import aui from '@atlassian/aui-soy/entry/aui-soy';

const theme = window.__karma__.config.auitheme || defaultThemeKey;
const a11yState = iconThemes[theme];

describe(`aui/textarea with theme=${theme}`, function () {
    beforeEach(() => {
        fixtures({
            smallIcon: aui.icons.icon({
                useIconFont: true,
                icon: 'arrow-up',
                accessibilityText: 'Small Icon',
            }),
            smallIconW: aui.icons.icon({
                useIconFont: true,
                icon: 'arrow-up',
            }),
            largeIcon: aui.icons.icon({
                useIconFont: true,
                icon: 'arrow-up',
                accessibilityText: 'Large Icon',
                size: 'large',
            }),
            largeIconM: aui.icons.icon({
                useIconFont: true,
                icon: 'arrow-up',
                size: 'large',
            }),
        });
    });

    it(`icon should have ${a11yState.defaultIconIssuesNumber} issues`, function (done) {
        afterMutations(() => {
            expect(document.body).to.be.exactNumberOfAccessibilityIssues(
                a11yState.defaultIconIssuesNumber,
                done,
                this.test
            );
        }, 100);
    });

    it(`default textarea should have [${a11yState.defaultIconIssues.join()}] issues`, function (done) {
        afterMutations(() => {
            expect(document.body).to.be.exactAccessibilityIssues(
                a11yState.defaultIconIssues,
                done,
                this.test
            );
        }, 100);
    });
});
