import { dialogThemes } from './fixtures/dialog2';
import { defaultThemeKey } from './fixtures/common';

const theme = window.__karma__.config.auitheme || defaultThemeKey;
const a11yState = dialogThemes[theme];

describe(`aui/dialog2 with theme=${theme}`, function () {
    let dialog;

    beforeEach(() => {
        dialog = createContentEl();
    });

    it(`initial dialog state should have ${a11yState.initialDialogIssuesNumber} issues`, function (done) {
        expect(document.body).to.be.exactNumberOfAccessibilityIssues(
            a11yState.initialDialogIssuesNumber,
            done,
            this.test
        );
    });

    it(`initial dialog state should have [${a11yState.initialDialogIssues.join()}] issues`, function (done) {
        expect(document.body).to.be.exactAccessibilityIssues(
            a11yState.initialDialogIssues,
            done,
            this.test
        );
    });

    it(`opened dialog state should have ${a11yState.openedDialogIssuesNumber} issues`, function (done) {
        dialog.show();

        expect(document.body).to.be.exactNumberOfAccessibilityIssues(
            a11yState.openedDialogIssuesNumber,
            done,
            this.test
        );
    });

    it(`opened dialog state should have [${a11yState.openedDialogIssues.join()}] issues`, function (done) {
        dialog.show();

        expect(document.body).to.be.exactAccessibilityIssues(
            a11yState.openedDialogIssues,
            done,
            this.test
        );
    });

    it(`hidden dialog state should have ${a11yState.hiddenDialogIssuesNumber} issues`, function (done) {
        dialog.show();
        dialog.hide();

        expect(document.body).to.be.exactNumberOfAccessibilityIssues(
            a11yState.hiddenDialogIssuesNumber,
            done,
            this.test
        );
    });

    it(`hidden dialog state should have [${a11yState.hiddenDialogIssues.join()}] issues`, function (done) {
        dialog.show();
        dialog.hide();

        expect(document.body).to.be.exactAccessibilityIssues(
            a11yState.hiddenDialogIssues,
            done,
            this.test
        );
    });

    it(`removed dialog state should have ${a11yState.removedDialogIssuesNumber} issues`, function (done) {
        dialog.show();
        dialog.remove();

        expect(document.body).to.be.exactNumberOfAccessibilityIssues(
            a11yState.removedDialogIssuesNumber,
            done,
            this.test
        );
    });

    it(`removed dialog state should have ${a11yState.removedDialogIssues.join()} issues`, function (done) {
        dialog.show();
        dialog.remove();

        expect(document.body).to.be.exactAccessibilityIssues(
            a11yState.removedDialogIssues,
            done,
            this.test
        );
    });

    function createContentEl() {
        const html = `
            <section
                id="demo-dialog"
                class="aui-dialog2 aui-dialog2-small aui-layer"
                role="dialog"
                tabindex="-1"
                aria-modal="true"
                aria-labelledby="dialog-show-button--heading"
                aria-describedby="dialog-show-button--description"
                hidden
            >
                <header class="aui-dialog2-header">
                    <h1 class="aui-dialog2-header-main" id="dialog-show-button--heading">Captain...</h1>
                </header>
                <div class="aui-dialog2-content" id="dialog-show-button--description">
                    <p>We've detected debris of some sort in a loose orbit.</p>
                    <p>I suggest we beam a section aboard for analysis...</p>
                </div>
                <footer class="aui-dialog2-footer">
                    <div class="aui-dialog2-footer-actions">
                        <button id="dialog-submit-button" class="aui-button aui-button-primary">Make it so</button>
                    </div>
                </footer>
            </section>`;

        const fixture = document.getElementById('test-fixture');
        fixture.innerHTML = html;
        return AJS.dialog2('#demo-dialog');
    }
});
