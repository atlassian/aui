import '@atlassian/aui/src/js/aui/inline-dialog2';
import '@atlassian/aui/src/js/aui/trigger';
import { click, fixtures, afterMutations } from '../helpers/browser';
import skate from '@atlassian/aui/src/js/aui/internal/skate';
import { inlineDialogThemes } from './fixtures/inline-dialog';
import { defaultThemeKey } from './fixtures/common';

const theme = window.__karma__.config.auitheme || defaultThemeKey;
const a11yState = inlineDialogThemes[theme];

describe(`aui/inline-dialog-integration with theme=${theme}.`, function () {
    let trigger;
    let inlineDialog;

    beforeEach(() => {
        const fixturesEl = fixtures({
            trigger:
                '<button class="aui-button" data-aui-trigger aria-controls="my-inline-dialog">Show</button>',
            inlineDialog:
                '<aui-inline-dialog alignment="bottom center" id="my-inline-dialog" aria-labelledby="dialog-header" aria-describedby="dialog-content">' +
                '<h3 id="dialog-header">Header</h3>' +
                '<div id="dialog-content">Inline dialog Content</div>' +
                '</aui-inline-dialog>',
        });

        trigger = fixturesEl.trigger;
        inlineDialog = fixturesEl.inlineDialog;

        skate.init(trigger);
        skate.init(inlineDialog);
    });

    it(`initiated state should have ${a11yState.initiatedStateIssuesNumber} issues`, function (done) {
        expect(inlineDialog.open).to.equal(false);
        expect(document.body).to.be.exactNumberOfAccessibilityIssues(
            a11yState.initiatedStateIssuesNumber,
            done,
            this.test
        );
    });

    it(`initiated state should have [${a11yState.initiatedStateIssues.join()}] issues`, function (done) {
        expect(inlineDialog.open).to.equal(false);
        expect(document.body).to.be.exactAccessibilityIssues(
            a11yState.initiatedStateIssues,
            done,
            this.test
        );
    });

    it(`opened state should have ${a11yState.openedStateIssuesNumber} issues`, function (done) {
        const testData = this.test;

        click(trigger);

        afterMutations(() => {
            expect(inlineDialog.open).to.equal(true);
            expect(document.body).to.be.exactNumberOfAccessibilityIssues(
                a11yState.openedStateIssuesNumber,
                done,
                testData
            );
        }, 100);
    });

    it(`opened state should have [${a11yState.openedStateIssues.join()}] issues`, function (done) {
        const testData = this.test;

        click(trigger);

        afterMutations(() => {
            expect(inlineDialog.open).to.equal(true);
            expect(document.body).to.be.exactAccessibilityIssues(
                a11yState.openedStateIssues,
                done,
                testData
            );
        }, 100);
    });

    it(`closed state should have ${a11yState.closedStateIssuesNumber} issues`, function (done) {
        const testData = this.test;

        click(trigger);
        click(document);

        afterMutations(() => {
            expect(inlineDialog.open).to.equal(false);
            expect(document.body).to.be.exactNumberOfAccessibilityIssues(
                a11yState.closedStateIssuesNumber,
                done,
                testData
            );
        }, 100);
    });

    it(`closed state should have [${a11yState.closedStateIssues.join()}] issues`, function (done) {
        const testData = this.test;

        click(trigger);
        click(document);

        afterMutations(() => {
            expect(inlineDialog.open).to.equal(false);
            expect(document.body).to.be.exactAccessibilityIssues(
                a11yState.closedStateIssues,
                done,
                testData
            );
        }, 100);
    });
});
