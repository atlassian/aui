import { afterMutations, fixtures, focus } from '../helpers/browser';
import { textareaThemes } from './fixtures/textarea';
import { defaultThemeKey } from './fixtures/common';

const theme = window.__karma__.config.auitheme || defaultThemeKey;
const a11yState = textareaThemes[theme];

describe(`aui/textarea with theme=${theme}`, function () {
    let form;
    let textarea;

    beforeEach(() => {
        form = fixtures(
            {
                form: `
        <form class="aui">
            <fieldset>
                <legend><span>Leave a comment</span></legend>
                <div class="field-group">
                    <label for="textarea-id">Comment</label>
                    <textarea class="textarea" name="comment" id="textarea-id" placeholder="Your comment here..."></textarea>
                </div>
            </fieldset>
        </form>
        `,
            },
            true
        ).form;

        textarea = form.querySelector('.textarea');
    });

    it(`default textarea should have ${a11yState.defaultTextareaIssuesNumber} issues`, function (done) {
        expect(form).to.be.exactNumberOfAccessibilityIssues(
            a11yState.defaultTextareaIssuesNumber,
            done,
            this.test
        );
    });

    it(`default textarea should have [${a11yState.defaultTextareaIssues.join()}] issues`, function (done) {
        expect(form).to.be.exactAccessibilityIssues(
            a11yState.defaultTextareaIssues,
            done,
            this.test
        );
    });

    it(`focused textarea should have ${a11yState.focusedTextareaIssuesNumber} issues`, function (done) {
        focus(textarea);

        afterMutations(() => {
            expect(form).to.be.exactNumberOfAccessibilityIssues(
                a11yState.focusedTextareaIssuesNumber,
                done,
                this.test
            );
        }, 200);
    });

    it(`focused textarea should have [${a11yState.focusedTextareaIssues.join()}] issues`, function (done) {
        focus(textarea);

        afterMutations(() => {
            expect(form).to.be.exactAccessibilityIssues(
                a11yState.focusedTextareaIssues,
                done,
                this.test
            );
        }, 200);
    });

    it(`disabled textarea should have ${a11yState.disabledTextareaIssuesNumber} issues`, function (done) {
        textarea.setAttribute('disabled', '');

        afterMutations(() => {
            expect(form).to.be.exactNumberOfAccessibilityIssues(
                a11yState.disabledTextareaIssuesNumber,
                done,
                this.test
            );
        }, 100);
    });

    it(`disabled textarea should have [${a11yState.disabledTextareaIssues.join()}] issues`, function (done) {
        textarea.setAttribute('disabled', '');

        afterMutations(() => {
            expect(form).to.be.exactAccessibilityIssues(
                a11yState.disabledTextareaIssues,
                done,
                this.test
            );
        }, 100);
    });
});
