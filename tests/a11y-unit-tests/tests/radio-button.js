import { afterMutations, fixtures, click, focus } from '../helpers/browser';
import { radioButtonThemes } from './fixtures/radio-button';
import { defaultThemeKey } from './fixtures/common';

const theme = window.__karma__.config.auitheme || defaultThemeKey;
const a11yState = radioButtonThemes[theme];

describe(`aui/radio-button with theme=${theme}`, function () {
    let firstOption;

    beforeEach(() => {
        const form = fixtures(
            {
                form: `
        <form class="aui" style="background: #fff;">

           <fieldset class="group">
                <div class="radio">
                    <input
                        class="radio"
                        type="radio"
                        name="radio-set"
                        id="radio-button-1"
                    >
                    <label for="radio-button-1">Radio Button 1</label>
                </div>
            </fieldset>
        </form>`,
            },
            true
        ).form;

        firstOption = form.querySelector('#radio-button-1');
    });

    it(`non-selected radio button state should have ${a11yState.nonSelectedRadioButtonIssuesNumber} issues`, function (done) {
        afterMutations(() => {
            expect(document.body).to.be.exactNumberOfAccessibilityIssues(
                a11yState.nonSelectedRadioButtonIssuesNumber,
                done,
                this.test
            );
        }, 200);
    });

    it(`non-selected radio button state should have [${a11yState.nonSelectedRadioButtonIssues.join()}] issues`, function (done) {
        afterMutations(() => {
            expect(document.body).to.be.exactAccessibilityIssues(
                a11yState.nonSelectedRadioButtonIssues,
                done,
                this.test
            );
        }, 200);
    });

    it(`selected radio button state should have ${a11yState.selectedRadioButtonIssuesNumber} issues`, function (done) {
        click(firstOption);

        afterMutations(() => {
            expect(document.body).to.be.exactNumberOfAccessibilityIssues(
                a11yState.selectedRadioButtonIssuesNumber,
                done,
                this.test
            );
        }, 200);
    });

    it(`selected radio button state should have [${a11yState.selectedRadioButtonIssues.join()}] issues`, function (done) {
        click(firstOption);

        afterMutations(() => {
            expect(document.body).to.be.exactAccessibilityIssues(
                a11yState.selectedRadioButtonIssues,
                done,
                this.test
            );
        }, 200);
    });

    it(`disabled radio button state should have ${a11yState.disabledRadioButtonIssuesNumber} issues`, function (done) {
        firstOption.setAttribute('disabled', '');

        afterMutations(() => {
            expect(document.body).to.be.exactNumberOfAccessibilityIssues(
                a11yState.disabledRadioButtonIssuesNumber,
                done,
                this.test
            );
        }, 200);
    });

    it(`disabled radio button state should have [${a11yState.disabledRadioButtonIssues.join()}] issues`, function (done) {
        firstOption.setAttribute('disabled', '');

        afterMutations(() => {
            expect(document.body).to.be.exactAccessibilityIssues(
                a11yState.disabledRadioButtonIssues,
                done,
                this.test
            );
        }, 200);
    });

    it(`focused radio button state should have ${a11yState.focusedRadioButtonIssuesNumber} issues`, function (done) {
        focus(firstOption);

        afterMutations(() => {
            expect(document.body).to.be.exactNumberOfAccessibilityIssues(
                a11yState.focusedRadioButtonIssuesNumber,
                done,
                this.test
            );
        }, 200);
    });

    it(`focused radio button state should have [${a11yState.focusedRadioButtonIssues.join()}] issues`, function (done) {
        focus(firstOption);

        afterMutations(() => {
            expect(document.body).to.be.exactAccessibilityIssues(
                a11yState.focusedRadioButtonIssues,
                done,
                this.test
            );
        }, 200);
    });
});
