import { fixtures, focus } from '../helpers/browser';
import { fileUploadThemes } from './fixtures/file-upload';
import { defaultThemeKey } from './fixtures/common';

const theme = window.__karma__.config.auitheme || defaultThemeKey;
const a11yState = fileUploadThemes[theme];

describe(`aui/file-upload with theme=${theme}.`, function () {
    let fileUpload;

    beforeEach(() => {
        const form = fixtures({
            form: `
            <form class="aui">
                <fieldset>
                    <div class="field-group">
                        <label for="ffi1">Fancy File Input</label>
                        <label class="ffi" data-ffi-button-text="Browse">
                            <input type="file" id="ffi1" name="ffi1" aria-label="Fancy File Input">
                        </label>
                    </div>
                </fieldset>
            </form>`,
        }).form;

        fileUpload = form.querySelector('.ffi input[type="file"]');
    });

    it(`default state should have ${a11yState.defaultFileUploadIssuesNumber} issues`, function (done) {
        AJS.$(fileUpload).fancyFileInput();

        expect(document.body).to.be.exactNumberOfAccessibilityIssues(
            a11yState.defaultFileUploadIssuesNumber,
            done,
            this.test
        );
    });

    it(`default state should have ${a11yState.defaultFileUploadIssues.join()} issues`, function (done) {
        AJS.$(fileUpload).fancyFileInput();

        expect(document.body).to.be.exactAccessibilityIssues(
            a11yState.defaultFileUploadIssues,
            done,
            this.test
        );
    });

    it(`focus state should have ${a11yState.focusFileUploadIssuesNumber} issues`, function (done) {
        AJS.$(fileUpload).fancyFileInput();
        focus(fileUpload);

        expect(document.body).to.be.exactNumberOfAccessibilityIssues(
            a11yState.focusFileUploadIssuesNumber,
            done,
            this.test
        );
    });

    it(`focus state should have ${a11yState.focusFileUploadIssues.join()} issues`, function (done) {
        AJS.$(fileUpload).fancyFileInput();
        focus(fileUpload);

        expect(document.body).to.be.exactAccessibilityIssues(
            a11yState.focusFileUploadIssues,
            done,
            this.test
        );
    });

    it(`disabled state should have ${a11yState.disabledFileUploadIssuesNumber} issues`, function (done) {
        fileUpload.setAttribute('disabled', '');
        AJS.$(fileUpload).fancyFileInput();

        expect(document.body).to.be.exactNumberOfAccessibilityIssues(
            a11yState.disabledFileUploadIssuesNumber,
            done,
            this.test
        );
    });

    it(`disabled state should have ${a11yState.disabledFileUploadIssues.join()} issues`, function (done) {
        fileUpload.setAttribute('disabled', '');
        AJS.$(fileUpload).fancyFileInput();

        expect(document.body).to.be.exactAccessibilityIssues(
            a11yState.disabledFileUploadIssues,
            done,
            this.test
        );
    });
});
