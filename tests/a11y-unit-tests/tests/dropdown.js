import '@atlassian/aui/src/js/aui/dropdown2';
import { click, hover } from '../helpers/browser';
import WebComponentDropdown from '@atlassian/aui-integration-test-suite/unit/aui/dropdown2/dropdown2-test-webcomponent-helper';
import { dropdownThemes } from './fixtures/dropdown';
import { defaultThemeKey } from './fixtures/common';

const theme = window.__karma__.config.auitheme || defaultThemeKey;
const a11yState = dropdownThemes[theme];

describe(`aui/dropdown2 with theme=${theme}`, function () {
    let firstSubmenu;
    let $trigger;

    beforeEach(() => {
        firstSubmenu = WebComponentDropdown();
        firstSubmenu.addTrigger();
        $trigger = firstSubmenu.$trigger;
    });

    it(`simple dropdown should have ${a11yState.simpleDropdownIssuesNumber} issues`, function (done) {
        firstSubmenu.initialise();

        openMenu($trigger);

        expect(document.body).to.be.exactNumberOfAccessibilityIssues(
            a11yState.simpleDropdownIssuesNumber,
            done,
            this.test
        );
    });

    it(`simple dropdown should have [${a11yState.simpleDropdownIssues.join()}] issues`, function (done) {
        firstSubmenu.initialise();

        openMenu($trigger);

        expect(document.body).to.be.exactAccessibilityIssues(
            a11yState.simpleDropdownIssues,
            done,
            this.test
        );
    });

    it(`dropdown with groups should have ${a11yState.withGroupsIssuesNumber} issues`, function (done) {
        firstSubmenu.addPlainSection();
        firstSubmenu.addInteractiveSection();
        firstSubmenu.initialise();

        openMenu($trigger);

        expect(document.body).to.be.exactNumberOfAccessibilityIssues(
            a11yState.withGroupsIssuesNumber,
            done,
            this.test
        );
    });

    it(`dropdown with groups should have [${a11yState.withGroupsIssues.join()}] issues`, function (done) {
        firstSubmenu.addPlainSection();
        firstSubmenu.addInteractiveSection();
        firstSubmenu.initialise();

        openMenu($trigger);

        expect(document.body).to.be.exactAccessibilityIssues(
            a11yState.withGroupsIssues,
            done,
            this.test
        );
    });

    it(`dropdown with submenu should have ${a11yState.withSubmenuIssuesNumber} issues`, function (done) {
        const secondSubmenu = WebComponentDropdown();

        secondSubmenu.addPlainSection();
        firstSubmenu.addSubmenuSection(secondSubmenu);

        firstSubmenu.initialise();
        secondSubmenu.initialise();

        const $trigger2 = firstSubmenu.getItem(2);

        openMenu($trigger);
        openSubMenu($trigger2);

        expect(document.body).to.be.exactNumberOfAccessibilityIssues(
            a11yState.withSubmenuIssuesNumber,
            done,
            this.test
        );
    });

    it(`dropdown with submenu should have [${a11yState.withSubmenuIssues.join()}] issues`, function (done) {
        const secondSubmenu = WebComponentDropdown();

        secondSubmenu.addPlainSection();
        firstSubmenu.addSubmenuSection(secondSubmenu);

        firstSubmenu.initialise();
        secondSubmenu.initialise();

        const $trigger2 = firstSubmenu.getItem(2);

        openMenu($trigger);
        openSubMenu($trigger2);

        expect(document.body).to.be.exactAccessibilityIssues(
            a11yState.withSubmenuIssues,
            done,
            this.test
        );
    });

    it(`dropdown with checkboxes should have ${a11yState.withCheckboxesIssuesNumber} issues`, function (done) {
        firstSubmenu.addPlainSection();
        firstSubmenu.addCheckboxSection();
        firstSubmenu.initialise();

        openMenu($trigger);

        expect(document.body).to.be.exactNumberOfAccessibilityIssues(
            a11yState.withCheckboxesIssuesNumber,
            done,
            this.test
        );
    });

    it(`dropdown with checkboxes should have [${a11yState.withCheckboxesIssues.join()}] issues`, function (done) {
        firstSubmenu.addPlainSection();
        firstSubmenu.addCheckboxSection();
        firstSubmenu.initialise();

        openMenu($trigger);

        expect(document.body).to.be.exactAccessibilityIssues(
            a11yState.withCheckboxesIssues,
            done,
            this.test
        );
    });

    it(`dropdown with radio buttons should have ${a11yState.withRadioButtonsIssuesNumber} issues`, function (done) {
        firstSubmenu.addPlainSection();
        firstSubmenu.addRadioSection();
        firstSubmenu.initialise();

        openMenu($trigger);

        expect(document.body).to.be.exactNumberOfAccessibilityIssues(
            a11yState.withRadioButtonsIssuesNumber,
            done,
            this.test
        );
    });

    it(`dropdown with radio buttons should have [${a11yState.withRadioButtonsIssues.join()}] issues`, function (done) {
        firstSubmenu.addPlainSection();
        firstSubmenu.addRadioSection();
        firstSubmenu.initialise();

        openMenu($trigger);

        expect(document.body).to.be.exactAccessibilityIssues(
            a11yState.withRadioButtonsIssues,
            done,
            this.test
        );
    });

    function openMenu($trigger) {
        hover($trigger);
        click($trigger);
    }

    function openSubMenu($trigger) {
        hover($trigger);
    }
});
