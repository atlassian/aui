import { darkModeKey, defaultThemeKey } from './common';

// The result are expected to be the same bot both themes.
const basicA11yState = {
    defaultFileUploadIssues: [],
    defaultFileUploadIssuesNumber: 0,
    focusFileUploadIssues: [],
    focusFileUploadIssuesNumber: 0,
    disabledFileUploadIssues: [],
    disabledFileUploadIssuesNumber: 0,
};

const darkA11yState = basicA11yState;
const lightA11yState = basicA11yState;

const fileUploadThemes = {
    [darkModeKey]: darkA11yState,
    [defaultThemeKey]: lightA11yState,
};

export { fileUploadThemes };
