import { darkModeKey, defaultThemeKey } from './common';

const basicA11yState = {
    initialDialogIssuesNumber: 0,
    initialDialogIssues: [],
    openedDialogIssuesNumber: 0,
    openedDialogIssues: [],
    hiddenDialogIssuesNumber: 0,
    hiddenDialogIssues: [],
    removedDialogIssuesNumber: 0,
    removedDialogIssues: [],
};

const darkA11yState = basicA11yState;
const lightA11yState = basicA11yState;

const dialogThemes = {
    [darkModeKey]: darkA11yState,
    [defaultThemeKey]: lightA11yState,
};

export { dialogThemes };
