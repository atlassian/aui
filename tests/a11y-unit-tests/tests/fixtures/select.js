import { darkModeKey, defaultThemeKey } from './common';

const axeCoreColorContrastIssue = ['color-contrast'];
const atlassianColorContrastIssues = ['border-contrast-ratio', 'background-contrast-ratio'];
const allColorContrastIssues = [...axeCoreColorContrastIssue, ...atlassianColorContrastIssues];

const darkA11yState = {
    initiatedStateIssuesNumber: allColorContrastIssues.length,
    initiatedStateIssues: allColorContrastIssues,
    openedStateIssuesNumber: allColorContrastIssues.length,
    openedStateIssues: allColorContrastIssues,
    closedStateIssuesNumber: allColorContrastIssues.length,
    closedStateIssues: allColorContrastIssues,
    withEmptySuggestionIssuesNumber: allColorContrastIssues.length,
    withEmptySuggestionIssues: allColorContrastIssues,
    withFilteredSuggestionIssuesNumber: allColorContrastIssues.length,
    withFilteredSuggestionIssues: allColorContrastIssues,
};

const lightA11yState = {
    initiatedStateIssuesNumber: atlassianColorContrastIssues.length,
    initiatedStateIssues: atlassianColorContrastIssues,
    openedStateIssuesNumber: atlassianColorContrastIssues.length,
    openedStateIssues: atlassianColorContrastIssues,
    closedStateIssuesNumber: atlassianColorContrastIssues.length,
    closedStateIssues: atlassianColorContrastIssues,
    withEmptySuggestionIssuesNumber: atlassianColorContrastIssues.length,
    withEmptySuggestionIssues: atlassianColorContrastIssues,
    withFilteredSuggestionIssuesNumber: atlassianColorContrastIssues.length,
    withFilteredSuggestionIssues: atlassianColorContrastIssues,
};

const selectThemes = {
    [darkModeKey]: darkA11yState,
    [defaultThemeKey]: lightA11yState,
};

export { selectThemes };
