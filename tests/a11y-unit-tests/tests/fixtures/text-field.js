import { darkModeKey, defaultThemeKey } from './common';

const basicA11yState = {
    defaultTextFieldIssuesNumber: 2,
    defaultTextFieldIssues: ['border-contrast-ratio', 'background-contrast-ratio'],
    disabledTextFieldIssuesNumber: 2,
    disabledTextFieldIssues: ['border-contrast-ratio', 'background-contrast-ratio'],
    focusedTextFieldIssuesNumber: 0,
    focusedTextFieldIssues: [],
};

const darkA11yState = basicA11yState;
const lightA11yState = basicA11yState;

const textFieldThemes = {
    [darkModeKey]: darkA11yState,
    [defaultThemeKey]: lightA11yState,
};

export { textFieldThemes };
