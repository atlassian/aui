import { darkModeKey, defaultThemeKey } from './common';

const basicA11yState = {
    hiddenTooltipIssuesNumber: 0,
    hiddenTooltipIssues: [],
    shownTooltipIssuesNumber: 0,
    shownTooltipIssues: [],
};

const darkA11yState = basicA11yState;
const lightA11yState = basicA11yState;

const tooltipThemes = {
    [darkModeKey]: darkA11yState,
    [defaultThemeKey]: lightA11yState,
};

export { tooltipThemes };
