import { darkModeKey, defaultThemeKey } from './common';

const basicA11yState = {
    defaultIconIssuesNumber: 0,
    defaultIconIssues: [],
};

const darkA11yState = basicA11yState;
const lightA11yState = basicA11yState;

const iconThemes = {
    [darkModeKey]: darkA11yState,
    [defaultThemeKey]: lightA11yState,
};

export { iconThemes };
