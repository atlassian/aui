import { darkModeKey, defaultThemeKey } from './common';

const noIssues = [];
const axeCoreColorContrastIssue = ['color-contrast'];
const atlassianColorContrastIssues = ['border-contrast-ratio', 'background-contrast-ratio'];

const darkFixtures = [
    {
        type: 'Standard',
        markup: '<button class="aui-button">Standard button</button>',
        a11yState: {
            defaultStateIssues: atlassianColorContrastIssues,
            defaultStateIssuesNumber: atlassianColorContrastIssues.length,
            disabledStateIssues: atlassianColorContrastIssues,
            disabledStateIssuesNumber: atlassianColorContrastIssues.length,
            focusedStateIssues: atlassianColorContrastIssues,
            focusedStateIssuesNumber: atlassianColorContrastIssues.length,
            activeStateIssues: atlassianColorContrastIssues,
            activeStateIssuesNumber: atlassianColorContrastIssues.length,
            pressedStateIssues: axeCoreColorContrastIssue,
            pressedStateIssuesNumber: axeCoreColorContrastIssue.length,
            hoveredStateIssues: atlassianColorContrastIssues,
            hoveredStateIssuesNumber: atlassianColorContrastIssues.length,
        },
    },
    {
        type: 'Primary',
        markup: '<button class="aui-button aui-button-primary">Primary button</button>',
        a11yState: {
            defaultStateIssues: noIssues,
            defaultStateIssuesNumber: noIssues.length,
            disabledStateIssues: atlassianColorContrastIssues,
            disabledStateIssuesNumber: atlassianColorContrastIssues.length,
            focusedStateIssues: noIssues,
            focusedStateIssuesNumber: noIssues.length,
            activeStateIssues: noIssues,
            activeStateIssuesNumber: noIssues.length,
            pressedStateIssues: axeCoreColorContrastIssue,
            pressedStateIssuesNumber: axeCoreColorContrastIssue.length,
            hoveredStateIssues: noIssues,
            hoveredStateIssuesNumber: noIssues.length,
        },
    },
    {
        type: 'Link',
        markup: '<button class="aui-button aui-button-link">Link button</button>',
        a11yState: {
            defaultStateIssues: noIssues,
            defaultStateIssuesNumber: noIssues.length,
            disabledStateIssues: noIssues,
            disabledStateIssuesNumber: noIssues.length,
            focusedStateIssues: noIssues,
            focusedStateIssuesNumber: noIssues.length,
            activeStateIssues: noIssues,
            activeStateIssuesNumber: noIssues.length,
            pressedStateIssues: axeCoreColorContrastIssue,
            pressedStateIssuesNumber: axeCoreColorContrastIssue.length,
            hoveredStateIssues: noIssues,
            hoveredStateIssuesNumber: noIssues.length,
        },
    },
    {
        type: 'Subtle',
        markup: '<button class="aui-button aui-button-subtle">Subtle button</button>',
        a11yState: {
            defaultStateIssues: atlassianColorContrastIssues,
            defaultStateIssuesNumber: atlassianColorContrastIssues.length,
            disabledStateIssues: atlassianColorContrastIssues,
            disabledStateIssuesNumber: atlassianColorContrastIssues.length,
            focusedStateIssues: atlassianColorContrastIssues,
            focusedStateIssuesNumber: atlassianColorContrastIssues.length,
            activeStateIssues: atlassianColorContrastIssues,
            activeStateIssuesNumber: atlassianColorContrastIssues.length,
            pressedStateIssues: axeCoreColorContrastIssue,
            pressedStateIssuesNumber: axeCoreColorContrastIssue.length,
            hoveredStateIssues: atlassianColorContrastIssues,
            hoveredStateIssuesNumber: atlassianColorContrastIssues.length,
        },
    },
];

const lightFixtures = [
    {
        type: 'Standard',
        markup: '<button class="aui-button">Standard button</button>',
        a11yState: {
            defaultStateIssues: atlassianColorContrastIssues,
            defaultStateIssuesNumber: atlassianColorContrastIssues.length,
            disabledStateIssues: atlassianColorContrastIssues,
            disabledStateIssuesNumber: atlassianColorContrastIssues.length,
            focusedStateIssues: atlassianColorContrastIssues,
            focusedStateIssuesNumber: atlassianColorContrastIssues.length,
            activeStateIssues: atlassianColorContrastIssues,
            activeStateIssuesNumber: atlassianColorContrastIssues.length,
            pressedStateIssues: noIssues,
            pressedStateIssuesNumber: noIssues.length,
            hoveredStateIssues: atlassianColorContrastIssues,
            hoveredStateIssuesNumber: atlassianColorContrastIssues.length,
        },
    },
    {
        type: 'Primary',
        markup: '<button class="aui-button aui-button-primary">Primary button</button>',
        a11yState: {
            defaultStateIssues: noIssues,
            defaultStateIssuesNumber: noIssues.length,
            disabledStateIssues: atlassianColorContrastIssues,
            disabledStateIssuesNumber: atlassianColorContrastIssues.length,
            focusedStateIssues: noIssues,
            focusedStateIssuesNumber: noIssues.length,
            activeStateIssues: noIssues,
            activeStateIssuesNumber: noIssues.length,
            pressedStateIssues: noIssues,
            pressedStateIssuesNumber: noIssues.length,
            hoveredStateIssues: noIssues,
            hoveredStateIssuesNumber: noIssues.length,
        },
    },
    {
        type: 'Link',
        markup: '<button class="aui-button aui-button-link">Link button</button>',
        a11yState: {
            defaultStateIssues: noIssues,
            defaultStateIssuesNumber: noIssues.length,
            disabledStateIssues: noIssues,
            disabledStateIssuesNumber: noIssues.length,
            focusedStateIssues: noIssues,
            focusedStateIssuesNumber: noIssues.length,
            activeStateIssues: noIssues,
            activeStateIssuesNumber: noIssues.length,
            pressedStateIssues: noIssues,
            pressedStateIssuesNumber: noIssues.length,
            hoveredStateIssues: noIssues,
            hoveredStateIssuesNumber: noIssues.length,
        },
    },
    {
        type: 'Subtle',
        markup: '<button class="aui-button aui-button-subtle">Subtle button</button>',
        a11yState: {
            defaultStateIssues: atlassianColorContrastIssues,
            defaultStateIssuesNumber: atlassianColorContrastIssues.length,
            disabledStateIssues: atlassianColorContrastIssues,
            disabledStateIssuesNumber: atlassianColorContrastIssues.length,
            focusedStateIssues: atlassianColorContrastIssues,
            focusedStateIssuesNumber: atlassianColorContrastIssues.length,
            activeStateIssues: atlassianColorContrastIssues,
            activeStateIssuesNumber: atlassianColorContrastIssues.length,
            pressedStateIssues: noIssues,
            pressedStateIssuesNumber: noIssues.length,
            hoveredStateIssues: atlassianColorContrastIssues,
            hoveredStateIssuesNumber: atlassianColorContrastIssues.length,
        },
    },
];

const buttonThemes = {
    [darkModeKey]: darkFixtures,
    [defaultThemeKey]: lightFixtures,
};

export { buttonThemes };
