import { darkModeKey, defaultThemeKey } from './common';

// The result are expected to be the same bot both themes.
const basicA11yState = {
    simpleDropdownIssuesNumber: 0,
    simpleDropdownIssues: [],
    withGroupsIssuesNumber: 0,
    withGroupsIssues: [],
    withSubmenuIssuesNumber: 0,
    withSubmenuIssues: [],
    withCheckboxesIssuesNumber: 0,
    withCheckboxesIssues: [],
    withRadioButtonsIssuesNumber: 0,
    withRadioButtonsIssues: [],
};

const darkA11yState = basicA11yState;
const lightA11yState = basicA11yState;

const dropdownThemes = {
    [darkModeKey]: darkA11yState,
    [defaultThemeKey]: lightA11yState,
};

export { dropdownThemes };
