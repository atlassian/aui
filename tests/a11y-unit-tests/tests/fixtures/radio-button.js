import { darkModeKey, defaultThemeKey } from './common';

const lightA11yState = {
    nonSelectedRadioButtonIssuesNumber: 0,
    nonSelectedRadioButtonIssues: [],
    selectedRadioButtonIssuesNumber: 0,
    selectedRadioButtonIssues: [],
    disabledRadioButtonIssuesNumber: 0,
    disabledRadioButtonIssues: [],
    focusedRadioButtonIssuesNumber: 0,
    focusedRadioButtonIssues: [],
};

const darkA11yState = {
    nonSelectedRadioButtonIssuesNumber: 1,
    nonSelectedRadioButtonIssues: ['color-contrast'],
    selectedRadioButtonIssuesNumber: 1,
    selectedRadioButtonIssues: ['color-contrast'],
    disabledRadioButtonIssuesNumber: 0,
    disabledRadioButtonIssues: [],
    focusedRadioButtonIssuesNumber: 2,
    focusedRadioButtonIssues: ['color-contrast', 'outline-contrast-ratio'],
};

const radioButtonThemes = {
    [darkModeKey]: darkA11yState,
    [defaultThemeKey]: lightA11yState,
};

export { radioButtonThemes };
