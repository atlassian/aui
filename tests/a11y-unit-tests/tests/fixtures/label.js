import { darkModeKey, defaultThemeKey } from './common';

const lightA11yState = {
    defaultLabelIssuesNumber: 0,
    defaultLabelIssues: [],
    hoverCloseLabelIssuesNumber: 0,
    hoverCloseLabelIssues: [],
};

const darkA11yState = {
    defaultLabelIssuesNumber: 0,
    defaultLabelIssues: [],
    hoverCloseLabelIssuesNumber: 0,
    hoverCloseLabelIssues: [],
};

const labelThemes = {
    [darkModeKey]: darkA11yState,
    [defaultThemeKey]: lightA11yState,
};

export { labelThemes };
