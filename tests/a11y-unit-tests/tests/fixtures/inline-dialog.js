import { darkModeKey, defaultThemeKey } from './common';

const axeCoreColorContrastIssue = ['color-contrast'];
const atlassianColorContrastIssues = ['border-contrast-ratio', 'background-contrast-ratio'];
const allColorContrastIssues = [...axeCoreColorContrastIssue, ...atlassianColorContrastIssues];

const darkA11yState = {
    initiatedStateIssuesNumber: atlassianColorContrastIssues.length,
    initiatedStateIssues: atlassianColorContrastIssues,
    openedStateIssuesNumber: atlassianColorContrastIssues.length,
    openedStateIssues: atlassianColorContrastIssues,
    closedStateIssuesNumber: allColorContrastIssues.length,
    closedStateIssues: allColorContrastIssues,
};

const lightA11yState = {
    initiatedStateIssuesNumber: atlassianColorContrastIssues.length,
    initiatedStateIssues: atlassianColorContrastIssues,
    openedStateIssuesNumber: atlassianColorContrastIssues.length,
    openedStateIssues: atlassianColorContrastIssues,
    closedStateIssuesNumber: atlassianColorContrastIssues.length,
    closedStateIssues: atlassianColorContrastIssues,
};

const inlineDialogThemes = {
    [darkModeKey]: darkA11yState,
    [defaultThemeKey]: lightA11yState,
};

export { inlineDialogThemes };
