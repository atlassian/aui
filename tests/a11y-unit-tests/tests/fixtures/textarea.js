import { darkModeKey, defaultThemeKey } from './common';

const basicA11yState = {
    defaultTextareaIssuesNumber: 2,
    defaultTextareaIssues: ['border-contrast-ratio', 'background-contrast-ratio'],
    disabledTextareaIssuesNumber: 2,
    disabledTextareaIssues: ['border-contrast-ratio', 'background-contrast-ratio'],
    focusedTextareaIssuesNumber: 0,
    focusedTextareaIssues: [],
};

const darkA11yState = basicA11yState;
const lightA11yState = basicA11yState;

const textareaThemes = {
    [darkModeKey]: darkA11yState,
    [defaultThemeKey]: lightA11yState,
};

export { textareaThemes };
