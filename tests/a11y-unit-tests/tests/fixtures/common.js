const defaultThemeKey = 'default';
const darkModeKey = 'aui-theme-dark';

export { darkModeKey, defaultThemeKey };
