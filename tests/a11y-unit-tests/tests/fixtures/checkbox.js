import { darkModeKey, defaultThemeKey } from './common';

const lightA11yState = {
    uncheckedCheckboxIssuesNumber: 0,
    uncheckedCheckboxIssues: [],
    checkedCheckboxIssuesNumber: 0,
    checkedCheckboxIssues: [],
    disabledCheckboxIssuesNumber: 0,
    disabledCheckboxIssues: [],
    focusedCheckboxIssuesNumber: 0,
    focusedCheckboxIssues: [],
};

const darkA11yState = {
    uncheckedCheckboxIssuesNumber: 1,
    uncheckedCheckboxIssues: ['color-contrast'],
    checkedCheckboxIssuesNumber: 1,
    checkedCheckboxIssues: ['color-contrast'],
    disabledCheckboxIssuesNumber: 0,
    disabledCheckboxIssues: [],
    focusedCheckboxIssuesNumber: 2,
    focusedCheckboxIssues: ['color-contrast', 'outline-contrast-ratio'],
};

const checkboxThemes = {
    [darkModeKey]: darkA11yState,
    [defaultThemeKey]: lightA11yState,
};

export { checkboxThemes };
