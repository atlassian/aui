import '@atlassian/aui/src/js/aui/button';
import { mousedown, fixtures, focus, hover } from '../helpers/browser';
import { buttonThemes } from './fixtures/button';
import { defaultThemeKey } from './fixtures/common';

const theme = window.__karma__.config.auitheme || defaultThemeKey;
const buttonFixtures = buttonThemes[theme];

describe(`aui/button with theme=${theme}.`, function () {
    buttonFixtures.forEach(({ type, markup, a11yState }) => {
        describe(`${type} button`, function () {
            let button;
            beforeEach(() => {
                button = fixtures({ button: markup }, true).button;
            });

            it(`default state should have ${a11yState.defaultStateIssuesNumber} issues`, function (done) {
                expect(document.body).to.have.exactNumberOfAccessibilityIssues(
                    a11yState.defaultStateIssuesNumber,
                    done,
                    this.test
                );
            });

            it(`default state should have [${a11yState.defaultStateIssues.join()}] issues`, function (done) {
                expect(document.body).to.have.exactAccessibilityIssues(
                    a11yState.defaultStateIssues,
                    done,
                    this.test
                );
            });

            it(`disabled state should have ${a11yState.disabledStateIssuesNumber} issues`, function (done) {
                button.setAttribute('disabled', '');

                expect(document.body).to.have.exactNumberOfAccessibilityIssues(
                    a11yState.disabledStateIssuesNumber,
                    done,
                    this.test
                );
            });

            it(`disabled state should have [${a11yState.disabledStateIssues.join()}] issues`, function (done) {
                button.setAttribute('disabled', '');

                expect(document.body).to.have.exactAccessibilityIssues(
                    a11yState.disabledStateIssues,
                    done,
                    this.test
                );
            });

            it(`focused state should have ${a11yState.focusedStateIssuesNumber} issues`, function (done) {
                focus(button);

                expect(document.body).to.have.exactNumberOfAccessibilityIssues(
                    a11yState.focusedStateIssuesNumber,
                    done,
                    this.test
                );
            });

            it(`focused state should have [${a11yState.focusedStateIssues.join()}] issues`, function (done) {
                focus(button);

                expect(document.body).to.have.exactAccessibilityIssues(
                    a11yState.focusedStateIssues,
                    done,
                    this.test
                );
            });

            it(`active state should have ${a11yState.activeStateIssuesNumber} issues`, function (done) {
                mousedown(button);

                expect(document.body).to.have.exactNumberOfAccessibilityIssues(
                    a11yState.activeStateIssuesNumber,
                    done,
                    this.test
                );
            });

            it(`active state should have [${a11yState.activeStateIssues.join()}] issues`, function (done) {
                mousedown(button);

                expect(document.body).to.have.exactAccessibilityIssues(
                    a11yState.activeStateIssues,
                    done,
                    this.test
                );
            });

            it(`pressed state should have ${a11yState.pressedStateIssuesNumber} issues`, function (done) {
                button.setAttribute('aria-pressed', true);

                expect(document.body).to.have.exactNumberOfAccessibilityIssues(
                    a11yState.pressedStateIssuesNumber,
                    done,
                    this.test
                );
            });

            it(`pressed state should have [${a11yState.pressedStateIssues.join()}] issues`, function (done) {
                button.setAttribute('aria-pressed', true);

                expect(document.body).to.have.exactAccessibilityIssues(
                    a11yState.pressedStateIssues,
                    done,
                    this.test
                );
            });

            it(`hovered state should have ${a11yState.hoveredStateIssuesNumber} issues`, function (done) {
                hover(button);

                expect(document.body).to.have.exactNumberOfAccessibilityIssues(
                    a11yState.hoveredStateIssuesNumber,
                    done,
                    this.test
                );
            });

            it(`hovered state should have [${a11yState.hoveredStateIssues.join()}] issues`, function (done) {
                hover(button);

                expect(document.body).to.have.exactAccessibilityIssues(
                    a11yState.hoveredStateIssues,
                    done,
                    this.test
                );
            });
        });
    });
});
