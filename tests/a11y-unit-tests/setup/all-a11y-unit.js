// List of the a11y tests
import '../tests/button';
import '../tests/dropdown';
import '../tests/select';
import '../tests/inline-dialog';
import '../tests/textarea';
import '../tests/file-upload';
import '../tests/dialog2';
import '../tests/checkbox';
import '../tests/radio-button';
import '../tests/text-field';
import '../tests/icon';
import '../tests/tooltip';
import '../tests/label';
