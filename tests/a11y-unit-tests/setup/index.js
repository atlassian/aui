// A11y Setup
import '@atlassian/a11y-unit-tests/dist/chai-extension';
import './bootstrap';

// Get all of AUI (mainly for the CSS)
import '@atlassian/aui/entry/aui.batch.prototyping';
import '@atlassian/aui/entry/styles/aui.page.dark-mode';

// Run the a11y unit tests
import './all-a11y-unit';
