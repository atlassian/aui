import '../helpers/mock-require';
import { afterMutations, removeLayers } from '../helpers/browser';
import { DELAY } from '@atlassian/aui/src/js/aui/internal/elements';
import $ from 'jquery';

let fixtureElement;

/**
 * Sets the el's innerHTML to '' and executes the next callback after any
 * DOM mutation handlers (e.g., skate's detached callbacks) have had a
 * chance to run.
 */
function clearContents(el, next) {
    afterMutations(() => {
        el.innerHTML = '';
        next && afterMutations(next);
    });
}

before(() => {
    // NOTE: It will work only if the theme's'styles are added to the bundle in setup file and inited assigment _theThemeMode before bootstrap.
    const themeClass = window.__karma__.config.auitheme;

    if (themeClass) {
        document.body.classList.toggle(themeClass, true);
    }

    window.onbeforeunload = () => console.trace('Uh oh! Tried unloading the page :(');
});

beforeEach(function (done) {
    // Most tests do not assume animation is enabled. Let the few that actually care enable themselves.
    $.fx.off = true;

    // Cleanup markup before each test.
    $(document.body).empty();

    fixtureElement = document.getElementById('test-fixture');
    if (!fixtureElement) {
        fixtureElement = document.createElement('div');
        fixtureElement.classList.add('aui-page-panel');
        fixtureElement.style.padding = '10px';
        fixtureElement.id = 'test-fixture';
        document.body.appendChild(fixtureElement);
    }

    // reset the URL hash so tests can make assertions based on it
    window.location.hash = '';
    // need to wait a bit to avoid capturing an initial hashchange event
    afterMutations(done);
});

afterEach((done) => {
    // Check that nothing queued behaviours in jQuery
    if (!$.fx.off || $.timers.length) {
        console.warn("The previous test probably queued animations and didn't finish with them!");
        // Empty the fx queue.
        $.fx.off = true;
        $.fx.tick();
        if ($.timers.length) {
            console.warn(
                'Something other than animations queued a behaviour in jQuery that never happened!',
                $.timers
            );
            $.timers.length = 0;
        }
    }

    if (setTimeout.clock) {
        setTimeout.clock.restore();
    }

    // unbind some test-specific handlers that might've not been cleaned up
    $(window).off('.aui-test-suite');
    $(document).off('.aui-test-suite');

    clearContents(fixtureElement, () => {
        $('body, html').css('overflow', '');
        removeLayers();
        afterMutations(done, DELAY + 1);
    });
});
