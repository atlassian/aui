const webpackParts = require('@atlassian/aui-webpack-config/webpack.parts');
const { libraryExternals } = require('@atlassian/aui-webpack-config/webpack.skeleton');
const { merge } = require('webpack-merge');
const path = require('path');

module.exports = function (config) {
    const {
        BITBUCKET_BUILD_NUMBER,
        BITBUCKET_REPO_SLUG,
        BROWSERSTACK_USER,
        BROWSERSTACK_USERNAME,
        BROWSERSTACK_ACCESS_KEY,
        BROWSERSTACK_ACCESSKEY,
        BROWSERSTACK_KEY,
        CI_BUILD_NUMBER,
    } = process.env;

    const reporters = ['a11y', 'progress', 'junit', 'BrowserStack'];
    const plugins = [
        require('@atlassian/a11y-unit-tests/dist/karma-a11y-reporter'),
        'karma-webpack',
        'karma-mocha',
        'karma-chai',
        'karma-browserstack-launcher',
        'karma-chrome-launcher',
        'karma-coverage',
        'karma-firefox-launcher',
        'karma-less-preprocessor',
        'karma-safari-launcher',
        'karma-spec-reporter',
        'karma-coverage-istanbul-reporter',
        'karma-junit-reporter',
        'karma-sinon-chai',
    ];

    config.watch || reporters.push('coverage-istanbul');

    const jsTranspileStrategy = config.watch
        ? webpackParts.transpileJs
        : webpackParts.transpileJsWithCodeCoverage;

    const webpackConfig = merge([
        webpackParts.setAuiVersion(),
        webpackParts.loadSoy(),
        webpackParts.resolveSoyDeps(),
        webpackParts.inlineCss(),
        jsTranspileStrategy({
            exclude: /(tether|soy-template-plugin-js|node_modules|mocks)/,
        }),
        {
            mode: 'development',
            devtool: false,
            externals: [libraryExternals.jqueryExternal],
            module: {
                rules: [
                    {
                        test: /\.js$/i,
                        exclude: /(tether|soy-template-plugin-js|node_modules|mocks)/,
                        resolve: {
                            alias: {
                                wrmI18n$: path.resolve(__dirname, 'mocks/i18n.js'),
                            },
                        },
                    },
                    {
                        test: /\.(png|jpg|gif|svg|ttf|eot|woff|woff2)$/,
                        use: 'ignore-loader',
                    },
                    {
                        test: /\.js$/i,
                        exclude: /(tether|soy-template-plugin-js|node_modules|mocks)/,
                        resolve: {
                            alias: {
                                wrmI18n$: path.resolve(__dirname, '../integration/mocks/i18n.js'),
                            },
                        },
                    },
                ],
            },
        },
    ]);

    const a11yTestPattern = './setup/index.js';
    const bootstrapPattern = './setup/*.js';

    config.set({
        browserStack: {
            username: BROWSERSTACK_USERNAME || BROWSERSTACK_USER,
            accessKey: BROWSERSTACK_ACCESS_KEY || BROWSERSTACK_ACCESSKEY || BROWSERSTACK_KEY,
            project: BITBUCKET_REPO_SLUG || 'aui',
            build: BITBUCKET_BUILD_NUMBER || CI_BUILD_NUMBER || Date.now(),
            startTunnel: true,
            forceLocal: true,
        },
        hostname: '127.0.0.1',
        autoWatch: config.watch,
        singleRun: !config.watch,
        plugins,
        frameworks: ['mocha', 'chai', 'sinon-chai', 'webpack'],
        browsers: config.watch ? ['Chrome_dev'] : ['Chrome_Headless_1024x768'],
        browserDisconnectTolerance: 3,
        browserDisconnectTimeout: 10000,
        client: {
            auitheme: config.auitheme,
            captureConsole: true,
            useIframe: true,
            chai: {
                includeStack: true,
            },
            mocha: {
                timeout: 12345,
                slow: 1,
                ui: 'bdd',
            },
        },
        customLaunchers: {
            Chrome_dev: {
                base: 'Chrome',
                flags: [
                    '--window-size=1624,768',
                    '--disable-translate',
                    '--disable-extensions',
                    '--no-sandbox',
                    '--auto-open-devtools-for-tabs',
                ],
            },
            Chrome_Headless_1024x768: {
                base: 'ChromeHeadless',
                flags: [
                    '--window-size=1024,768',
                    '--disable-translate',
                    '--disable-extensions',
                    '--no-sandbox',
                ],
            },
        },
        reporters,
        coverageIstanbulReporter: {
            'reports': ['text-summary', 'lcov'],
            'projectRoot': '../../',
            'fixWebpackSourcePaths': true,
            'combineBrowserReports': true,
            'skipFilesWithNoCoverage': true,
            'report-config': {
                lcov: { projectRoot: '../../' },
            },
        },
        junitReporter: {
            outputDir: 'test-reports',
            suite: 'AUI',
        },
        preprocessors: {
            [a11yTestPattern]: 'webpack',
            [bootstrapPattern]: 'webpack',
        },
        webpack: webpackConfig,
        files: [
            require.resolve('jquery'),
            require.resolve('jquery-migrate'),
            {
                pattern: a11yTestPattern,
                watched: false,
            },
        ],
    });
};
