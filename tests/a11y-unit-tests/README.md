# AUI Accessibility unit test suite

### What it is

The main idea of this test suite is to check isolated components and eliminate issues associated with other components.
These tests are simpler, faster and more reliable that a11y-runner, but still have a different purpose.
The whole description of the idea is on Confluence page: [Integration of the axe-core into AUI unit tests](https://hello.atlassian.net/wiki/spaces/~701215be2d62792c44802b4c1b15e4b8d532f/pages/2441870783/Integration+of+the+axe-core+into+AUI+unit+tests):

### How to run a11y unit tests

In order to run a11y check we need to:

1. Install dependencies by running next command: `yarn`
2. Run test suite from `a11y-unit-tests` folder:
    1. `yarn test/a11y` - runs tests in headless mode
    2. or `yarn test/a11y/watch` - runs tests in chrome

### How to run a11y unit tests with non-default AUI theme

Currently, there are two available themes: default and dark themes.
In order to run dark (or any other) theme you need to pass additional parameter `--auitheme=${value}`.
The `${value}` is the class that will be added in the `body` class list.

For example, the class that we need to add to the `body` for the dark theme is `aui-theme-dark`:

1. `yarn test/a11y --auitheme=aui-theme-dark` - runs tests in headless mode with a dark theme.
2. or `yarn test/a11y/watch --auitheme=aui-theme-dark` - runs tests in chrome with a dark theme.
3. Also, there are two predefined scripts in `package.json`: `yarn test/a11y/dark` and `yarn test/a11y/watch/dark`

So, in case we add a new theme we can run it the same way, but we need provide different value to `--auitheme=`

### How to add test

**_NOTE: Since the a11y tests are async we have to:_**

- **_use regular functions `function(done) {...}` instead of arrow functions_** - we need access to the `this` context.
- **_provide callback function `done` to assertion method `accessible`_**
- **_and provide information about current test by passing `this.test` as an additional parameter_**
  <br>

The process of test adding is as simple as adding a regular unit test, but using different set of assertions.

1. To add a new test into existing component's tests it would be enough to insert next into appropriate `describe` section:

```js
it('${some-new-state} is accessible', function (done) {
    const componentEl = $('.comopent-selector');

    expect(componentEl).to.be.accessible(done, this.test);
});
```

2. To add new tests for a new component we need:

- Create a new `.js` file for component in the `./tests` folder with content similar to next:

```js
// Imports needed for tests
import '@atlassian/aui/src/js/aui/${component}'; // component itself
import {} from '../helpers/browser'; // If need some browser helpers like click, pressKey, etc.

// Main section for the whole component
describe('aui/newComponent', function () {
    beforeEach(() => {
        /** ... */
    });
    afterEach(() => {
        /** ... */
    });

    describe(`component state 1`, () => {
        it('sub case for state 1 is accessible', function (done) {
            const componentEl = $('.comopent-selector');
            expect(componentEl).to.be.accessible(done, this.test);
        });
        //...
    });

    describe(`component state 2`, () => {
        it('sub case for state 2 is accessible', function (done) {
            const componentEl = $('.comopent-selector');
            expect(componentEl).to.be.accessible(done, this.test);
        });
        //...
    });
    //...
});
```

- Add import of the new test file into list of the all tests `./setup/all-a11y-unit.js`
- In case the component supports different themes we need to test it with all supported themes

```js
// From here you can import available theme's keys, in case it is needed
import {} from './fixtures/common';

// window.__karma__.config.auitheme - contains provided AUI theme
// class/key from yarn command.
const theme = window.__karma__.config.auitheme || defaultThemeKey;

// Usualy, we describe the expected results in a separate file,
// and there we prepare expected results for each theme as a map.
// { [darkThemeKey]: {stateName: stateResult...}, [defaultThemeKey]: {...}};
const a11yState = dropdownThemes[theme];
```

## Accessibility checks - karma plugin & chai assertion method

### What it is

The a11y unit tests works based on several a11y extensions:

- custom chai assertion methods that injects axe-core into `document`, run accessibility check and send results via sockets on the next karma plugin.
- custom karma a11y plugin that collects data that custom assertions send, then a11y plugin transforms results and saves report into `a11y-issues-viewer/a11y-issues.json`.

### How to use chai assertions

In order to use custom chai assertion methods:

1. We have to use regular function instead of arrow function
2. Since the axe-core do check asynchronously we need to provide callback on completion `done`.
3. To collect data about test such as name, describe block, and etc. we need to provide as a second param test context `this.test`.

About what assertions we have and how use them, you can read [here](https://hello.atlassian.net/wiki/spaces/A11YDC/pages/2624017109/Work+with+a11y+tests)

For example,

```js
it('component state is accessible', function (done) {
    // done is a callback function for async tests in chai
    const componentEl = $('.comopent-selector');
    expect(componentEl).to.be.accessible(done, this.test);
});
```

## Accessibility issues viewer

### What it is

This is a simple js application to view a11y check results.
How to use app is shown here: [How to use a11y results viewer.](https://www.loom.com/login?redirect_after=https%3A%2F%2Fwww.loom.com%2Fshare%2Fb029a78785954a5c939e9f66d503b5cf%3FfromJoinRequest%3Dtrue&custom_title=Sign%20up%20or%20Sign%20in%20to%20view&fromJoinRequest=true)

### How to run

Since this is a simple js app, there is no need to build.
You can do next:

```sh
$ npm i -g http-viewer  # once only
$ http-viewer -p 7010 a11y-issues-viewer
```

Then open http://localhost:7010.
