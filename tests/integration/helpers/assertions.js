import jQuery from 'jquery';

export const expectBooleanAttribute = {
    isAbsent: function (el, attr) {
        expect(el.hasAttribute(attr)).to.equal(false, `isAbsent: hasAttribute ${attr}`);
        expect(el.getAttribute(attr)).to.equal(null, `isAbsent: getAttribute ${attr}`);
    },

    isPresentAndEmpty: function (el, attr) {
        expect(el.hasAttribute(attr)).to.equal(true, `isPresentAndEmpty: hasAttribute ${attr}`);
        expect(el.getAttribute(attr)).to.equal('', `isPresentAndEmpty: getAttribute ${attr}`);
    },
};

function expectEventPrevention(e, expected, message) {
    const jqueryEvent = e.isDefaultPrevented ? e : null;
    const realEvent = jqueryEvent ? jqueryEvent.originalEvent : e;

    if (realEvent && jqueryEvent) {
        const dp1 = realEvent.defaultPrevented;
        const dp2 = jqueryEvent.isDefaultPrevented();
        if (dp1 !== dp2) {
            console.warn(
                'jquery event and native event disagree about event prevention',
                realEvent,
                jqueryEvent
            );
        }
        expect(dp1).to.equal(expected, `event was ${message}`);
    } else if (realEvent) {
        expect(realEvent.defaultPrevented).to.equal(expected, `native event was ${message}`);
    } else {
        expect(jqueryEvent.isDefaultPrevented()).to.equal(expected, `jquery event was ${message}`);
    }
}

export const expectEvent = {
    isPrevented: function (e) {
        expectEventPrevention(e, true, 'not prevented as expected');
    },

    isNotPrevented: function (e) {
        expectEventPrevention(e, false, 'prevented unexpectedly');
    },
};

function isFocussed(el) {
    const $element = jQuery(el);
    return document.activeElement === $element[0] || $element.is(':focus');
}

export const expectFocus = {
    is(el, message = false) {
        expect(isFocussed(el), message || `${el} should be the active element, but was not`).to.be
            .true;
    },
    isNot(el, message = false) {
        expect(isFocussed(el), message || `${el} should not be the active element, but was`).to.be
            .false;
    },
};

export default {
    expectBooleanAttribute,
    expectEvent,
    expectFocus,
};
