import _ from 'underscore';
import $ from '@atlassian/aui/src/js/aui/jquery';
import LayerManager from '@atlassian/aui/src/js/aui/layer-manager';
import Layer, { EVENT_PREFIX } from '@atlassian/aui/src/js/aui/layer';
import { createLayer } from './layer-manager-global';

describe('aui/layer-manager', function () {
    beforeEach(function () {
        LayerManager.global = new LayerManager();
    });

    it('globals', function () {
        expect(AJS.LayerManager.toString()).to.equal(LayerManager.toString());
    });

    it('push() accepts jQuery elements', function () {
        const $layer1 = createLayer();
        LayerManager.global.push($layer1);

        expect(LayerManager.global.indexOf($layer1)).to.equal(0);
    });

    it('push() accepts HTML elements', function () {
        const $layer1 = createLayer();
        LayerManager.global.push($layer1[0]);

        expect(LayerManager.global.indexOf($layer1)).to.equal(0);
    });

    it('indexOf() should return the correct index', function () {
        const $layer1 = createLayer(true);
        const $layer2 = createLayer(true);
        const $nonExistentLayer = createLayer();

        LayerManager.global.push($layer1);
        LayerManager.global.push($layer2);

        expect(LayerManager.global.indexOf($nonExistentLayer)).to.equal(-1);
        expect(LayerManager.global.indexOf($layer1)).to.equal(0);
        expect(LayerManager.global.indexOf($layer2)).to.equal(1);
    });

    it('indexOf() accepts HTML elements', function () {
        const $layer1 = createLayer(true);
        const $layer2 = createLayer();

        LayerManager.global.push($layer1);
        LayerManager.global.push($layer2[0]);

        expect(LayerManager.global.indexOf($layer1[0])).to.equal(0);
        expect(LayerManager.global.indexOf($layer2)).to.equal(1);
    });

    it('item() returns a jQuery element', function () {
        const $layer1 = createLayer();
        LayerManager.global.push($layer1[0]);

        expect(LayerManager.global.item(0) instanceof $).to.be.true;
    });

    it('item() should return the correct element', function () {
        const $layer1 = createLayer(true);
        const $layer2 = createLayer();

        LayerManager.global.push($layer1);
        LayerManager.global.push($layer2);

        expect(LayerManager.global.item(0)[0]).to.equal($layer1[0]);
        expect(LayerManager.global.item(1)[0]).to.equal($layer2[0]);
    });

    it('item() returns same jQuery object if jQuery pushed', function () {
        const $layer1 = createLayer(true);
        const $layer2 = createLayer();

        LayerManager.global.push($layer1);
        LayerManager.global.push($layer2[0]);

        expect(LayerManager.global.item(0)).to.equal($layer1);
        expect(LayerManager.global.item(1)).to.not.equal($layer2);
        // NOTE AUI-2604 - We may not actually care about the jQuery object equivalence.
    });

    describe('layering -', function () {
        let $blanketedLayerOne;
        let $normalLayerOne;
        let $normalLayerTwo;
        let $normalLayerThree;

        beforeEach(function () {
            $blanketedLayerOne = createLayer(true);
            $normalLayerOne = createLayer();
            $normalLayerTwo = createLayer();
            $normalLayerThree = createLayer();
        });

        // AUI-2590
        it('push() will implicitly remove other layers at the current level in the stack', function () {
            LayerManager.global.push($normalLayerOne);
            LayerManager.global.push($normalLayerTwo);

            expect(LayerManager.global.indexOf($normalLayerOne)).to.equal(-1);
            expect(LayerManager.global.indexOf($normalLayerTwo)).to.equal(0);

            LayerManager.global.push($normalLayerThree);

            expect(LayerManager.global.indexOf($normalLayerOne)).to.equal(-1);
            expect(LayerManager.global.indexOf($normalLayerTwo)).to.equal(-1);
            expect(LayerManager.global.indexOf($normalLayerThree)).to.equal(0);
        });

        it('push() will stack layers that have a parent-child relationship', function () {
            $normalLayerOne.attr('id', 'one').html('<span aria-controls="two"></span>');
            $normalLayerTwo.attr('id', 'two').html('<span aria-controls="three"></span>');
            $normalLayerThree.attr('id', 'three');

            LayerManager.global.push($normalLayerOne);
            LayerManager.global.push($normalLayerTwo);

            expect(LayerManager.global.indexOf($normalLayerOne)).to.equal(0);
            expect(LayerManager.global.indexOf($normalLayerTwo)).to.equal(1);

            LayerManager.global.push($normalLayerThree);

            expect(LayerManager.global.indexOf($normalLayerOne)).to.equal(0);
            expect(LayerManager.global.indexOf($normalLayerTwo)).to.equal(1);
            expect(LayerManager.global.indexOf($normalLayerThree)).to.equal(2);
        });

        it('push() will start new layer level on top of blanketed layer', function () {
            LayerManager.global.push($blanketedLayerOne);
            LayerManager.global.push($normalLayerOne);

            expect(LayerManager.global.indexOf($blanketedLayerOne)).to.equal(0);
            expect(LayerManager.global.indexOf($normalLayerOne)).to.equal(1);
        });

        //AUI-4518
        it('does not mutate layer stack when hiding all layers', function () {
            const $layer1 = createLayer(true);
            const $layer2 = createLayer(true);
            const $layer3 = createLayer(true);
            LayerManager.global.push($layer1);
            LayerManager.global.push($layer2);
            LayerManager.global.push($layer3);

            LayerManager.global.hideAll();

            expect(LayerManager.global.indexOf($layer1)).to.equal(0);
            expect(LayerManager.global.indexOf($layer2)).to.equal(1);
            expect(LayerManager.global.indexOf($layer3)).to.equal(2);
        });
    });

    describe('layer removal -', function () {
        let $layerOne;
        let $layerTwo;
        let $layerThree;
        let $layerFour;
        let $layerFive;
        let $layerSix;
        let $nonExistentLayer;

        beforeEach(function () {
            $layerOne = createLayer();
            $layerTwo = createLayer();
            $layerThree = createLayer();
            parentChild($layerOne, $layerTwo);
            parentChild($layerTwo, $layerThree);

            $layerFour = createLayer(true);
            parentChild($layerThree, $layerFour);

            $layerFive = createLayer();
            $layerSix = createLayer();
            parentChild($layerFive, $layerSix);

            $nonExistentLayer = createLayer();

            LayerManager.global.push($layerOne);
            LayerManager.global.push($layerTwo);
            LayerManager.global.push($layerThree);
            LayerManager.global.push($layerFour);
            LayerManager.global.push($layerFive);
            LayerManager.global.push($layerSix);

            expect(LayerManager.global.indexOf($layerSix)).to.equal(5);
        });

        function parentChild(parentLayer, childLayer) {
            const id = _.uniqueId();
            childLayer.attr('id', id);
            parentLayer.html(`<span aria-controls="${id}"></span>`);
        }

        it('popUntil() returns top-most layer after popping all non-modals', function () {
            const result = LayerManager.global.popUntil($layerFive);

            expect(LayerManager.global.indexOf($layerSix)).to.equal(-1);
            expect(LayerManager.global.indexOf($layerFive)).to.equal(-1);
            expect(LayerManager.global.indexOf($layerFour)).to.equal(3);
            expect(result[0]).to.equal($layerFive[0]);
        });

        it('popUntil() returns null if argument is not a layer', function () {
            const result = LayerManager.global.popUntil($nonExistentLayer);

            expect(result).to.be.null;
        });

        it('popUntil() returns null if layer was not on stack', function () {
            LayerManager.global.popUntil($layerFive);
            const result = LayerManager.global.popUntil($layerFive);

            expect(result).to.be.null;
        });

        it('popUntil() accepts HTML elements', function () {
            const result = LayerManager.global.popUntil($layerFive[0]);

            expect(result[0]).to.equal($layerFive[0]);
        });

        it('popUntil(el, false) ignores blanketed state', function () {
            const result = LayerManager.global.popUntil($layerTwo);

            expect(LayerManager.global.indexOf($layerFour)).to.equal(
                -1,
                'blanketed option should not keep layer four on the stack'
            );
            expect(result[0]).to.equal($layerTwo[0], 'return of popUntil should be top-most layer');
        });

        it('popUntil(el, true) ignores blanketed state', function () {
            const result = LayerManager.global.popUntil($layerTwo, true);

            expect(LayerManager.global.indexOf($layerFour)).to.equal(
                -1,
                'blanketed option should not keep layer four on the stack'
            );
            expect(result[0]).to.equal($layerTwo[0], 'return of popUntil should be top-most layer');
        });

        it('popUntil(el, false) ignores programmatic prevention', function () {
            $layerSix.on(EVENT_PREFIX + 'beforeHide', (e) => e.preventDefault());
            const result = LayerManager.global.popUntil($layerTwo, false);

            expect(LayerManager.global.indexOf($layerSix)).to.equal(
                -1,
                'programmatic prevention should not keep layer six on the stack when events are not fired'
            );
            expect(result[0]).to.equal($layerTwo[0], 'return of popUntil should be top-most layer');
        });

        it('popUntil(el, true) respects programmatic prevention', function () {
            $layerSix.on(EVENT_PREFIX + 'beforeHide', (e) => e.preventDefault());
            const result = LayerManager.global.popUntil($layerTwo, true);

            expect(LayerManager.global.indexOf($layerSix)).to.equal(
                5,
                'programmatic prevention should keep layer six on the stack when events are fired'
            );
            expect(result[0]).to.equal($layerSix[0], 'return of popUntil should be top-most layer');
        });
    });

    describe('getTopLayer() tests -', function () {
        it('should return null if no layers', function () {
            expect(LayerManager.global.getTopLayer()).to.be.null;
        });

        it('should return layer when there is one layer', function () {
            const layer1 = Layer(createLayer());
            layer1._showLayer();
            LayerManager.global.push(layer1.$el);
            expect(LayerManager.global.getTopLayer()).to.equal(layer1.$el);
        });

        it('should return layer when there is one hidden layer', function () {
            const layer1 = Layer(createLayer());
            layer1._showLayer();
            LayerManager.global.push(layer1.$el);
            layer1._hideLayer();
            expect(LayerManager.global.getTopLayer()).to.equal(layer1.$el);
        });

        it('should return top layer when there are multiple layers', function () {
            const layer1 = Layer(createLayer());
            const layer2 = Layer(createLayer());
            layer1._showLayer();
            layer2._showLayer();
            LayerManager.global.push(layer1.$el);
            LayerManager.global.push(layer2.$el);
            expect(LayerManager.global.getTopLayer()).to.equal(layer2.$el);
        });

        it('should return top layer when there are multiple layers and top layer is hidden', function () {
            const layer1 = Layer(createLayer());
            const layer2 = Layer(createLayer());
            layer1._showLayer();
            layer2._showLayer();
            LayerManager.global.push(layer1.$el);
            LayerManager.global.push(layer2.$el);
            layer2._hideLayer();
            expect(LayerManager.global.getTopLayer()).to.equal(layer2.$el);
        });

        it('should return null when layer was created and removed', function () {
            const layer1 = Layer(createLayer());
            layer1.show(); // Pushes layer into LayerManager as well
            layer1.remove();
            expect(LayerManager.global.getTopLayer()).to.be.null;
        });
    });

    describe('getTopOpenLayer() tests -', function () {
        it('should return null if no layers', function () {
            expect(LayerManager.global.getTopOpenLayer()).to.be.null;
        });

        it('should return layer when there is one open layer', function () {
            const layer1 = Layer(createLayer());
            layer1._showLayer();
            LayerManager.global.push(layer1.$el);
            expect(LayerManager.global.getTopLayer()).to.equal(layer1.$el);
        });

        it('should return null when there is one hidden layer', function () {
            const layer1 = Layer(createLayer());
            layer1._showLayer();
            LayerManager.global.push(layer1.$el);
            layer1._hideLayer();
            expect(LayerManager.global.getTopLayer()).to.equal(layer1.$el);
        });

        it('should return top layer when there are multiple layers', function () {
            const layer1 = Layer(createLayer());
            const layer2 = Layer(createLayer());
            layer1._showLayer();
            layer2._showLayer();
            LayerManager.global.push(layer1.$el);
            LayerManager.global.push(layer2.$el);
            expect(LayerManager.global.getTopOpenLayer()).to.equal(layer2.$el);
        });

        it('should return bottom layer when there are two layers and only bottom layer is open', function () {
            const layer1 = Layer(createLayer(false, true, false));
            const layer2 = Layer(createLayer());
            layer1._showLayer();
            layer2._showLayer();
            LayerManager.global.push(layer1.$el);
            LayerManager.global.push(layer2.$el);
            layer2._hideLayer();
            expect(LayerManager.global.getTopOpenLayer()).to.equal(layer1.$el);
        });

        it('should return null when all layers are hidden', function () {
            const layer1 = Layer(createLayer());
            const layer2 = Layer(createLayer());
            layer1._showLayer();
            layer2._showLayer();
            LayerManager.global.push(layer1.$el);
            LayerManager.global.push(layer2.$el);
            layer1._hideLayer();
            layer2._hideLayer();
            expect(LayerManager.global.getTopOpenLayer()).to.be.null;
        });

        it('should return null when layer was created and removed', function () {
            const layer1 = Layer(createLayer());
            layer1.show(); // Pushes layer into LayerManager as well
            layer1.remove();
            expect(LayerManager.global.getTopLayer()).to.be.null;
        });
    });

    describe('blanket tests -', function () {
        function getVisibleBlankets() {
            const $blankets = $('.aui-blanket');
            const blanketIsVisible = $('.aui-blanket').css('visibility') !== 'hidden';
            return $blankets.length && blanketIsVisible ? $blankets : [];
        }
        ('');
        it('Creating a blanketed layer makes a blanket appear', function () {
            LayerManager.global.push(createLayer(true));
            expect(getVisibleBlankets().length).to.be.above(0);
        });

        it('Creating a normal layer makes no blanket appear', function () {
            LayerManager.global.push(createLayer());
            expect(getVisibleBlankets().length).to.equal(0);
        });

        it('Creating and then popping a blanketed layer results in no blankets', function () {
            LayerManager.global.push(createLayer(true));
            LayerManager.global.popTopIfNonPersistent();
            expect(getVisibleBlankets().length).to.equal(0);
        });
    });
});
