import { createFormsComponentBody } from '@atlassian/aui/src/js/aui/forms/create-forms-component-body';

describe('aui/forms/create-form-component-body', function () {
    let component;

    function createParent() {
        const parent = document.createElement('div');
        parent.classList.add('checkbox');
        return parent;
    }

    function createChildren(parent) {
        const checkbox = document.createElement('input');
        checkbox.id = 'checkBoxOne';
        checkbox.type = 'checkbox';
        checkbox.name = 'checkBoxOne';
        parent.appendChild(checkbox);

        const label = document.createElement('label');
        label.htmlFor = 'checkBoxOne';
        label.innerText = 'Async checkbox test';
        parent.appendChild(label);
    }

    beforeEach(function () {
        component = createParent();
        document.body.appendChild(component);
    });

    afterEach(function () {
        component.remove();
    });

    it('should attach to element and generate glyph (sync)', function () {
        const body = createFormsComponentBody('checkbox');
        createChildren(component);
        body.attached(component);
        expect(!!component.querySelector('.aui-form-glyph')).to.be.equal(true);

        body.detached(component);
    });

    it('should attach to element and generate glyph (async)', function (done) {
        const body = createFormsComponentBody('checkbox');
        body.attached(component);
        createChildren(component);
        setTimeout(() => {
            expect(!!component.querySelector('.aui-form-glyph')).to.be.equal(true);
            done();
        }, 0);

        body.detached(component);
    });

    it('should detach from element', function () {
        const body = createFormsComponentBody('checkbox');
        body.attached(component);
        body.detached(component);
        createChildren(component);
        expect(!!component.querySelector('.aui-form-glyph')).to.be.equal(false);
    });
});
