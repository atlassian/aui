describe('aui/forms/custom-radio', function () {
    let component;

    function createComponent() {
        const parent = document.createElement('div');
        parent.classList.add('radio');

        const radio = document.createElement('input');
        radio.id = 'radioOne';
        radio.type = 'radio';
        radio.name = 'radioOne';
        parent.appendChild(radio);

        const label = document.createElement('label');
        label.htmlFor = 'radioOne';
        label.innerText = 'Async radio test';
        parent.appendChild(label);

        return parent;
    }

    beforeEach(function () {
        component = createComponent();
        document.body.appendChild(component);
    });

    afterEach(function () {
        component.remove();
    });

    it('should attach to element', function (done) {
        setTimeout(() => {
            expect(!!component.querySelector('.aui-form-glyph')).to.be.equal(true);
            done();
        }, 0);
    });
});
