describe('aui/forms/custom-checkbox', function () {
    let component;

    function createComponent() {
        const parent = document.createElement('div');
        parent.classList.add('checkbox');

        const checkbox = document.createElement('input');
        checkbox.id = 'checkBoxOne';
        checkbox.type = 'checkbox';
        checkbox.name = 'checkBoxOne';
        parent.appendChild(checkbox);

        const label = document.createElement('label');
        label.htmlFor = 'checkBoxOne';
        label.innerText = 'Async checkbox test';
        parent.appendChild(label);

        return parent;
    }

    beforeEach(function () {
        component = createComponent();
        document.body.appendChild(component);
    });

    afterEach(function () {
        component.remove();
    });

    it('should attach to element', function (done) {
        setTimeout(() => {
            expect(!!component.querySelector('.aui-form-glyph')).to.be.equal(true);
            done();
        }, 0);
    });
});
