import banner from '@atlassian/aui/src/js/aui/banner';

describe('aui/banner', function () {
    it('should throw an error if a #header element is not present', function () {
        expect(() => banner({ body: 'test' })).to.throw(/header/);
    });

    describe('API', function () {
        let header;

        beforeEach(function () {
            header = document.createElement('div');
            header.id = 'header';
            header.setAttribute('role', 'banner');
            header.innerHTML = '<p>test</p><nav class="aui-header"></nav>';
            document.getElementById('test-fixture').appendChild(header);
        });

        it('should be available via global AJS.banner function', function () {
            expect(AJS.banner.toString()).to.equal(banner.toString());
        });

        it('should be available as an AMD module', function (done) {
            amdRequire(['aui/banner'], function (amdModule) {
                expect(amdModule).to.equal(banner);
                done();
            });
        });

        it('should return a DOM element', function () {
            const bannerElement = banner({ body: 'test banner' });
            expect(bannerElement).to.be.an.instanceof(HTMLElement);
        });

        it('should be prepended to the header', function () {
            const bannerElement = banner({ body: 'test banner' });
            expect(header.children.length).to.equal(3);
            expect(header.children[0]).to.equal(bannerElement);
        });

        it('should have a body that accepts html', function () {
            const bannerElement = banner({
                body: 'with an <strong>important <a href="#">hyperlink</a></strong>!',
            });
            expect(bannerElement.textContent, 'with an important hyperlink!');
            expect(bannerElement.querySelectorAll('strong').length).to.equal(1);
            expect(bannerElement.querySelectorAll('a').length).to.equal(1);
        });

        it("should have an 'alert role'", function () {
            const bannerElement = banner({ body: 'test banner' });
            expect(bannerElement.getAttribute('role')).to.equal('alert');
        });
    });

    describe('security', function () {
        let alerter;
        beforeEach(function () {
            alerter = sinon.stub(window, 'alert');
            document.getElementById('test-fixture').innerHTML =
                '<div id="header" role="banner"><nav class="aui-header"></nav></div>';
        });
        afterEach(function () {
            alerter.restore();
        });

        it('should not execute scripts', function () {
            banner({ body: 'this script should not execute <script>alert("whoops")</script>' });
            expect(alerter.callCount).to.equal(0);
        });
    });
});
