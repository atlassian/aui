import _ from 'underscore';
import $ from '@atlassian/aui/src/js/aui/jquery';
import Backbone from 'backbone';
import ProgressiveDataSet from '@atlassian/aui/src/js/aui/progressive-data-set';
import ResultsList from '@atlassian/aui/src/js/aui/results-list';

describe('aui/results-list', function () {
    it('globals', function () {
        expect(AJS.ResultsList.toString()).to.equal(ResultsList.toString());
    });
});

describe('aui/results-list', function () {
    var ds;
    var resultview;

    beforeEach(function () {
        ds = new ProgressiveDataSet();
        resultview = new ResultsList({ source: ds });
    });

    it('#size', function () {
        ds.trigger('respond', { results: [new Backbone.Model()] });
        expect(resultview.size()).to.equal(1);

        ds.trigger('respond', { results: [] });
        expect(resultview.size()).to.equal(0);
    });

    it('clicking a result item triggers a "select" event', function () {
        var model = new Backbone.Model({ id: 'foo' });
        var selectEventSpy = sinon.spy();

        resultview.bind('selected', selectEventSpy);
        ds.trigger('respond', { query: 'foo', results: [model] });

        expect(selectEventSpy).to.have.not.been.called;

        resultview.$('li').first().trigger('click');

        expect(selectEventSpy).to.have.been.calledOnce;
        expect(selectEventSpy).to.have.been.calledWith(model);
    });
});

describe('aui/results-list', function () {
    var ds;
    var resultview;

    beforeEach(function () {
        ds = _.extend({}, Backbone.Events);
        resultview = new ResultsList({ source: ds });

        $('#test-fixture').append(resultview.$el);
    });

    it('can be hidden programmatically', function () {
        expect(resultview.$el.is(':visible')).to.be.true;

        resultview.hide();
        expect(resultview.$el.is(':visible')).to.be.false;
    });

    it('can be shown programmatically', function () {
        resultview.hide();
        resultview.show();
        expect(resultview.$el.is(':visible')).to.be.true;
    });

    it('is not visible if the query has not changed since it was hidden', function () {
        var response = { query: 'foo', results: new Backbone.Model() };
        ds.trigger('respond', response);
        resultview.hide();
        ds.trigger('respond', response);
        expect(resultview.$el.is(':visible')).to.be.false;
    });

    it('is visible if the query has changed since it was last hidden', function () {
        ds.trigger('respond', { query: 'foo', results: new Backbone.Model() });
        resultview.hide();
        ds.trigger('respond', { query: 'bar', results: new Backbone.Model() });
        expect(resultview.$el.is(':visible')).to.be.true;
    });
});

describe('aui/results-list (render)', function () {
    var ds;
    var resultview;

    beforeEach(function () {
        ds = new ProgressiveDataSet();
        resultview = new ResultsList({ source: ds });
        resultview.render = sinon.spy(resultview, 'render');
    });

    afterEach(function () {
        resultview.render.restore();
    });

    it('is called when #show is called', function () {
        resultview.show();

        expect(resultview.render).to.have.been.calledOnce;
    });

    it('is called when the source is queried', function () {
        ds.query('foo');

        expect(resultview.render).to.have.been.calledOnce;
    });

    it('is called even with an empty query for the source', function () {
        ds.query('');

        expect(resultview.render).to.have.been.calledOnce;
    });

    it('is not called if the query has not changed since it was hidden', function () {
        var response = { query: 'foo', results: new Backbone.Model() };
        ds.trigger('respond', response);
        resultview.hide();
        ds.trigger('respond', response);
        expect(resultview.render).to.have.been.calledOnce;
    });

    it('is called if the query changes since it was last hidden', function () {
        ds.trigger('respond', { query: 'foo', results: new Backbone.Model() });
        resultview.hide();
        ds.trigger('respond', { query: 'bar', results: new Backbone.Model() });
        expect(resultview.render).to.have.been.calledTwice;
    });
});

describe('aui/results-list (render callback)', function () {
    var realRender;
    var ds;

    beforeEach(function () {
        realRender = sinon.stub(ResultsList.prototype, 'render');
        ds = new ProgressiveDataSet();
    });

    afterEach(function () {
        realRender.restore();
    });

    it('fires events before and after it is called', function () {
        var view = new ResultsList({ source: ds });
        var before = sinon.spy();
        var after = sinon.spy();

        view.render = sinon.spy(view, 'render');
        view.on('rendering', before);
        view.on('rendered', after);
        view.render();

        expect(before).to.have.been.called;
        expect(before).to.have.been.calledAfter(view.render);
        expect(before).to.have.been.calledBefore(realRender);

        expect(after).to.have.been.called;
        expect(after).to.have.been.calledAfter(view.render);
        expect(after).to.have.been.calledAfter(realRender);
    });

    it('fires events even if ResultsList is extended', function () {
        var realRender = sinon.stub();
        var AnotherList = ResultsList.extend({ render: realRender });
        var view = new AnotherList({ source: ds });
        var before = sinon.spy();
        var after = sinon.spy();

        view.render = sinon.spy(view, 'render');
        view.on('rendering', before);
        view.on('rendered', after);
        view.render();

        expect(before).to.have.been.called;
        expect(before).to.have.been.calledAfter(view.render);
        expect(before).to.have.been.calledBefore(realRender);

        expect(after).to.have.been.called;
        expect(after).to.have.been.calledAfter(view.render);
        expect(after).to.have.been.calledAfter(realRender);
    });
});
