import '@atlassian/aui/src/js/aui/tooltip';
import $ from '@atlassian/aui/src/js/aui/jquery';
import { hover, mockPopper } from '../../helpers/all';

const TOOLTIP_SELECTOR = '#aui-tooltip';
const TOOLTIP_TIMEOUT = 301;

export function removeTooltipContainer() {
    $(TOOLTIP_SELECTOR).remove();
}

export function getTooltipContent() {
    return document.querySelector('.aui-tooltip-content');
}

describe('aui/tooltip', () => {
    let ctx = document.body;
    let $tooltipTrigger;
    let popperMock;

    beforeEach(() => {
        popperMock = mockPopper();
    });

    afterEach(() => {
        $tooltipTrigger.tooltip('destroy');
        $tooltipTrigger.remove();
        popperMock.restore();
    });

    /**
     * @returns {boolean}
     */
    function isTooltipVisible() {
        const tooltipContainer = getTooltipContainer();

        return !!tooltipContainer.attr('style') && !!tooltipContainer.attr('data-popper-placement');
    }

    function getTooltipContainer() {
        return $(ctx).find(TOOLTIP_SELECTOR);
    }

    function createTooltip(tooltipOptions) {
        $tooltipTrigger = $('<div title="The tooltip" style="margin: 200px">Element<div>');
        $(ctx).find('#test-fixture').append($tooltipTrigger);
        $tooltipTrigger.tooltip(tooltipOptions);
    }

    function createLongTooltip(tooltipOptions) {
        $tooltipTrigger = $(
            '<div title="The tooltip with loooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong text" style="margin: 200px">Element<div>'
        );
        $(ctx).find('#test-fixture').append($tooltipTrigger);
        $tooltipTrigger.tooltip(tooltipOptions);
    }

    function createEmptyTitleTooltip(tooltipOptions) {
        $tooltipTrigger = $('<div style="margin: 200px">Element<div>');
        $(ctx).find('#test-fixture').append($tooltipTrigger);
        $tooltipTrigger.tooltip(tooltipOptions);
    }

    function focusOnTooltipTrigger() {
        hover($tooltipTrigger);
        popperMock.tick(TOOLTIP_TIMEOUT);
    }

    function removeTooltipTrigger() {
        $tooltipTrigger.remove();
    }

    function processCallStack() {
        const clock = sinon.useFakeTimers();
        const promise = new Promise((resolve) => setTimeout(resolve, 0));
        clock.tick(0);
        return promise;
    }

    it('should create tooltip container in given context', () => {
        createTooltip();

        focusOnTooltipTrigger();

        expect(getTooltipContainer()[0]).to.exist;
    });

    it('should be destroyed with the "destroy" option', () => {
        createTooltip();
        focusOnTooltipTrigger();

        $tooltipTrigger.tooltip('destroy');

        expect(isTooltipVisible()).to.equal(false);
    });

    it('should throw error when command name does not exist', () => {
        createTooltip();
        focusOnTooltipTrigger();

        expect(() => $tooltipTrigger.tooltip('non-existing-cmd-name')).to.throw(
            'Method non-existing-cmd-name does not exist on tooltip.'
        );
    });

    it('should show tooltip with "hide" option', () => {
        createTooltip();
        focusOnTooltipTrigger();

        $tooltipTrigger.tooltip('hide');

        expect(isTooltipVisible()).to.equal(false);
    });

    it('should not show tooltip when title is empty', () => {
        createEmptyTitleTooltip();
        focusOnTooltipTrigger();

        $tooltipTrigger.tooltip('show');
        expect(isTooltipVisible()).to.equal(false);
    });

    it('should not show tooltip with "disable" option', () => {
        createTooltip();
        focusOnTooltipTrigger();
        expect(isTooltipVisible()).to.equal(true);

        $tooltipTrigger.tooltip('disable');
        expect(isTooltipVisible()).to.equal(false);

        $tooltipTrigger.tooltip('enable');
        focusOnTooltipTrigger();
        expect(isTooltipVisible()).to.equal(true);
    });

    it('should be visible', () => {
        createTooltip();

        focusOnTooltipTrigger();

        expect(isTooltipVisible()).to.equal(true);
    });

    it('should have restricted width', () => {
        createLongTooltip({});

        focusOnTooltipTrigger();

        expect(getTooltipContainer().width() <= 200).to.equal(true);
    });

    it('should change max-width', () => {
        createLongTooltip({ maxWidth: 150 });

        focusOnTooltipTrigger();

        expect(getTooltipContainer().width() <= 150).to.equal(true);
    });

    it('should recreate tooltip container after it was removed', () => {
        $('.aui-tooltip').remove();

        expect(getTooltipContainer()[0]).to.not.exist;

        createTooltip();

        focusOnTooltipTrigger();

        expect(getTooltipContainer()[0]).to.exist;
        expect(isTooltipVisible()).to.equal(true);
    });

    it('should be destroyed when trigger is removed', async () => {
        createTooltip();
        focusOnTooltipTrigger();
        expect(isTooltipVisible()).to.be.true;

        removeTooltipTrigger();
        await processCallStack();
        expect(isTooltipVisible()).to.be.false;
    });

    describe('should contain', () => {
        [
            { gravity: 'n', expectedPopperPlacement: 'bottom' },
            { gravity: 'ne', expectedPopperPlacement: 'bottom-end' },
            { gravity: 'e', expectedPopperPlacement: 'left' },
            { gravity: 'se', expectedPopperPlacement: 'top-end' },
            { gravity: 's', expectedPopperPlacement: 'top' },
            { gravity: 'sw', expectedPopperPlacement: 'top-start' },
            { gravity: 'w', expectedPopperPlacement: 'right' },
            { gravity: 'nw', expectedPopperPlacement: 'bottom-start' },
        ].forEach(({ gravity, expectedPopperPlacement }) => {
            it(`popper placement "${expectedPopperPlacement}" for gravity "${gravity}"`, () => {
                createTooltip({ gravity });

                focusOnTooltipTrigger();

                expect(getTooltipContainer().attr('data-popper-placement')).to.equal(
                    expectedPopperPlacement
                );
            });
        });
    });

    describe('live tooltip', () => {
        function createLiveTooltip(container) {
            $tooltipTrigger = $(
                '<div class="tooltip-me" title="Tooltip on dynamic element">I was added dynamically!</div>'
            );
            $(container).tooltip({
                live: '.tooltip-me',
            });
            $(container).append($tooltipTrigger);
        }

        it('should be visible', () => {
            createLiveTooltip($(ctx).find('#test-fixture'));

            focusOnTooltipTrigger();

            expect(isTooltipVisible()).to.equal(true);
        });

        it('should be destroyed', () => {
            createLiveTooltip($(ctx).find('#test-fixture'));
            focusOnTooltipTrigger();

            $tooltipTrigger.tooltip('destroy');

            expect(isTooltipVisible()).to.equal(false);
        });

        it('should be destroyed when target container is removed', async () => {
            const container = document.createElement('span');
            ctx.querySelector('#test-fixture').appendChild(container);
            const $container = $(container);
            createLiveTooltip($container);

            focusOnTooltipTrigger();
            await processCallStack();
            expect(isTooltipVisible()).to.be.true;

            removeTooltipTrigger();
            await processCallStack();
            expect(isTooltipVisible()).to.be.false;
        });
    });
});

describe('aui/tooltip when the trigger is not in the document', () => {
    let ctx = document.body;
    let popperMock;
    beforeEach(() => {
        popperMock = mockPopper();
    });

    afterEach(() => {
        popperMock.restore();
    });

    it('should not throw errors when there is no valid tooltip trigger', () => {
        expect(() => {
            $(ctx).find('.non-existent-element').tooltip(true);
        }).not.to.throw();
    });
});
