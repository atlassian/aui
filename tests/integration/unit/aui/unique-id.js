import $ from '@atlassian/aui/src/js/aui/jquery';
import addId from '@atlassian/aui/src/js/aui/internal/add-id';
import uniqueId from '@atlassian/aui/src/js/aui/unique-id';

describe('aui/unique-id', function () {
    it('globals', function () {
        expect(AJS.id.toString()).to.equal(uniqueId.toString());
    });

    function expectStringToStartWith(s, prefix) {
        expect(s.indexOf(prefix)).to.equal(0);
    }

    describe('API', function () {
        it('default ID', function () {
            expectStringToStartWith(uniqueId(), 'aui-uid-');
        });

        it('ID using a given prefix', function () {
            expectStringToStartWith(uniqueId('foo'), 'foo');
        });

        it('default ID and add to element', function () {
            var $el = $('<div></div>').appendTo('#test-fixture');
            addId($el);
            expectStringToStartWith($el.attr('id'), 'aui-uid-');
        });

        it('prefixed ID and add to element', function () {
            var $el = $('<div></div>').appendTo('#test-fixture');
            addId($el, 'foo');
            expectStringToStartWith($el.attr('id'), 'foo');
        });

        it('multiple IDs and add them to elements', function () {
            $(
                '<div class="idmultiple"></div><div class="idmultiple"></div><div class="idmultiple"></div>'
            ).appendTo('#test-fixture');
            addId($('.idmultiple'));
            var ids = [
                $('.idmultiple:eq(0)').attr('id'),
                $('.idmultiple:eq(1)').attr('id'),
                $('.idmultiple:eq(2)').attr('id'),
                $('.idmultiple:eq(3)').attr('id'),
            ];

            for (var i = 0; i < 4; i++) {
                for (var j = i; j < 4; j++) {
                    if (i === j) {
                        continue;
                    }

                    expect(ids[i]).to.not.equal(ids[j]);
                }
            }
        });

        it('ID must return a string', function () {
            expect(uniqueId()).to.be.a('string');
        });
    });
});
