import $ from '@atlassian/aui/src/js/aui/jquery';
import '@atlassian/aui/src/js/aui/expander';
import { click, afterMutations } from '../../helpers/all';

describe('aui/expander', function () {
    let expander;
    let trigger;
    let clock;
    let textBlob = `
What happens when you add a shiny new browser
 to a stack of already-disagreeing citizens? You’ll inevitably find some bugs.
 This is the story of how we found a rendering quirk and how the Atlassian frontend team found and refined the fix.
 The Atlassian User Interface (AUI) library has just finished an IE10 sprint to get our library prepped and
 ready for the newest member of the browser family.
 While IE10 seems generally quite good, we found a couple of problems due to IE10 dropping conditional comments;
 plus some undesirable behaviours.
 `;

    beforeEach(function () {
        clock = sinon.useFakeTimers();
    });

    afterEach(function () {
        clock.restore();
    });

    function initialiseExpanded() {
        $('#test-fixture').html(`
<div id="test-expander" class="aui-expander-content expanded">${textBlob}</div>
<button id="test-expander-trigger" data-replace-text="Read more" class="aui-expander-trigger" aria-controls="test-expander">Read less</button>
`);
        expander = $('#test-expander').get(0);
        trigger = $('#test-expander-trigger').get(0);

        return [expander, trigger];
    }

    function initialiseCollapsed() {
        $('#test-fixture').html(`
<div id="test-expander" class="aui-expander-content" hidden>${textBlob}</div>
<button id="test-expander-trigger" data-replace-text="Read less" class="aui-expander-trigger" aria-controls="test-expander">Read more</button>
`);
        expander = $('#test-expander').get(0);
        trigger = $('#test-expander-trigger').get(0);

        return [expander, trigger];
    }

    function initialiseReplaceSelector() {
        $('#test-fixture').html(`
<div id="test-expander" class="aui-expander-content">${textBlob}</div>
<button id="test-expander-trigger" data-replace-text="Show less" data-replace-selector=".test-trigger-button" class="aui-expander-trigger" aria-controls="test-expander" aria-expanded="false">
    <span class="test-trigger-button aui-button aui-button-link" aria-busy="false">Show more</span>
</button>
`);
        expander = $('#test-expander').get(0);
        trigger = $('#test-expander-trigger').get(0);

        return [expander, trigger];
    }

    function initialiseRevealText() {
        $('#test-fixture').html(`
<div id="container">
    <div id="test-expander" class="aui-expander-content" aria-expanded="false" style="min-height: 1em">
        ${textBlob}
        <button id="test-expander-trigger" data-replace-text="Show less" class="aui-expander-trigger aui-expander-reveal-text" aria-controls="test-expander" aria-expanded="false">
            Show more
        </button>
    </div>
</div>
`);
        expander = $('#test-expander').get(0);
        trigger = $('#test-expander-trigger').get(0);

        return [expander, trigger];
    }

    function getExpanderProperties(expander, trigger) {
        return {
            trigger: {
                rawText: $(trigger).text(),
                text: $(trigger).text().trim(),
            },
            expander: {
                height: expander.offsetHeight,
                isExpanded: expander.classList.contains('expanded'),
                isHidden: expander.hasAttribute('hidden'),
            },
        };
    }

    it('Expander test: Defaults to closed initially', function (done) {
        const [expander, trigger] = initialiseCollapsed();
        const properties = getExpanderProperties(expander, trigger);
        expect(properties.expander.isExpanded).to.be.false;
        expect(properties.expander.height).to.equal(0);
        done();
    });

    it('Expander test: Toggle changes height of expander', function (done) {
        const [expander, trigger] = initialiseCollapsed();
        const properties = getExpanderProperties(expander, trigger);
        click(trigger);
        afterMutations(() => {
            const newProperties = getExpanderProperties(expander, trigger);
            expect(properties.expander.height).to.not.equal(newProperties.expander.height);
            done();
        });
    });

    it('Expander test: Toggling twice returns to initial state', function (done) {
        const [expander, trigger] = initialiseCollapsed();
        const properties = getExpanderProperties(expander, trigger);
        click(trigger);
        click(trigger);
        afterMutations(() => {
            const newProperties = getExpanderProperties(expander, trigger);
            expect(properties.height).to.equal(newProperties.height);
            done();
        });
    });

    function assertCollapsed(expander, trigger) {
        const properties = getExpanderProperties(expander, trigger);
        expect(properties.trigger.text).to.equal('Read more');
        expect(properties.expander.height).to.equal(0);
        expect(properties.expander.isExpanded, 'Should not be expanded').to.be.false;
        expect(properties.expander.isHidden, 'Should not be hidden').to.be.true;
    }

    function assertOpen(expander, trigger) {
        const properties = getExpanderProperties(expander, trigger);
        expect(properties.trigger.text).to.equal('Read less');
        expect(properties.expander.height).to.not.equal(0);
        expect(properties.expander.isExpanded, 'Should be expanded').to.be.true;
        expect(properties.expander.isHidden, 'Should be hidden').to.be.false;
    }

    it('Expander test: Expanding and closing performs as expected', function (done) {
        const [expander, trigger] = initialiseCollapsed();
        assertCollapsed(expander, trigger);
        click(trigger);
        assertOpen(expander, trigger);
        click(trigger);
        assertCollapsed(expander, trigger);
        click(trigger);
        assertOpen(expander, trigger);
        done();
    });

    it('Expander test: Test initialisation as expanded', function (done) {
        const [expander, trigger] = initialiseExpanded();
        const properties = getExpanderProperties(expander, trigger);
        expect(properties.expander.isExpanded).to.be.true;
        expect(properties.expander.height).to.be.at.least(0);
        expect(properties.expander.isHidden).to.be.false;
        done();
    });

    it('Expander test: Test collapsing after initialisation as expanded', function (done) {
        const [expander, trigger] = initialiseExpanded();
        click(trigger);

        const properties = getExpanderProperties(expander, trigger);
        expect(properties.trigger.text).to.equal('Read more');
        expect(properties.expander.isExpanded).to.be.false;
        expect(properties.expander.height).to.equal(0);
        expect(properties.expander.isHidden).to.be.true;
        done();
    });

    it('Expander test: Trigger replace selector modifies right element', function (done) {
        const [, trigger] = initialiseReplaceSelector();
        const triggerButton = trigger.querySelector('.test-trigger-button');
        expect(triggerButton.textContent).to.equal('Show more');
        click(trigger);
        expect(triggerButton.textContent).to.equal('Show less');
        click(trigger);
        expect(triggerButton.textContent).to.equal('Show more');
        done();
    });

    it('Expander test: Test expected behaviour of AUI ADG reveal text pattern', function (done) {
        const [expander, trigger] = initialiseRevealText();
        let properties = getExpanderProperties(expander, trigger);
        expect(properties.trigger.text).to.equal('Show more');
        expect($(trigger).css('position')).to.equal('absolute');
        expect(properties.expander.height).to.be.above(0);
        expect(properties.expander.isExpanded).to.be.false;
        click(trigger);
        properties = getExpanderProperties(expander, trigger);
        expect(properties.trigger.text).to.equal('Show less');
        expect($(trigger).css('position')).to.equal('relative');
        expect(properties.expander.isExpanded).to.be.true;
        done();
    });
});
