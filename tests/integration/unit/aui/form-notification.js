import $ from '@atlassian/aui/src/js/aui/jquery';
import skate from '@atlassian/aui/src/js/aui/internal/skate';
import '@atlassian/aui/src/js/aui/form-notification'; // auto-initialises the components
import { afterMutations, click, focus } from '../../helpers/all';

describe('aui/form-notification', function () {
    describe('Form notification Unit Tests -', function () {
        var clock;

        function createInput(attributes) {
            var $input = $('<input type="text">');
            attributes = $.extend({}, attributes, { 'data-aui-notification-field': '' });
            $.each(attributes, function (key, value) {
                $input.attr(key, value);
            });
            $('#test-fixture').append($input);
            skate.init($input);
            return $input;
        }

        function countDescriptionsOnPage() {
            return $('.description').length;
        }

        function countErrorsOnPage() {
            return $('.error').length;
        }

        function setupLinkEnvironment() {
            clock.tick(100);

            return createInput({
                'data-aui-notification-info':
                    '["Some text here followed by a link <a href="#link-to-google">click here</a>"]',
            });
        }

        beforeEach(function () {
            clock = sinon.useFakeTimers();
        });

        afterEach(function () {
            $('.tipsy').remove();
            clock.restore();
        });

        it('global', function () {
            expect(AJS.formNotification).to.equal(undefined);
        });

        it('AMD module', function () {
            amdRequire(['aui/form-notification'], function (amdModule) {
                expect(amdModule).to.equal(undefined);
            });
        });

        it('Fields are rendered with info notifications', function () {
            createInput({
                'data-aui-notification-info': 'Test info message',
            });

            expect(countDescriptionsOnPage()).to.equal(1);
        });

        it('Field notification messages stack correctly', function () {
            createInput({
                'data-aui-notification-info': '["Test JSON message 1","Test JSON message 2"]',
            });
            expect($('.description')[0].innerHTML.indexOf('<li>')).to.not.equal(-1);
        });

        it('Field notification links are followed', function (done) {
            var $input = setupLinkEnvironment();
            focus($input[0]);
            expect(countDescriptionsOnPage()).to.equal(1);

            click($('a', '#test-fixture'));
            afterMutations(() => {
                expect(window.location.hash).to.equal('#link-to-google');
                done();
            });
        });

        it('Notification states can be changed after a field is created', function (done) {
            var $input = createInput({
                'data-aui-notification-info': 'Test info message',
            });
            skate.init($input[0]);

            focus($input[0]);
            $input.attr('data-aui-notification-error', 'Test error message');

            afterMutations(function () {
                expect(countErrorsOnPage()).to.equal(1);
                done();
            });
        });
        // AUI-5076
        it('should remove errors when the error attribute is removed', function (done) {
            var $input = createInput({
                'data-aui-notification-error': 'Test',
            });
            expect(countErrorsOnPage()).to.equal(1);

            $input.removeAttr('data-aui-notification-error');
            clock.tick(100);

            afterMutations(function () {
                expect(countErrorsOnPage()).to.equal(0);
                done();
            });
        });

        it('should add errors after the attribute is added back', function (done) {
            var $input = createInput({
                'data-aui-notification-error': 'Test',
            });
            expect(countErrorsOnPage()).to.equal(1);

            $input.removeAttr('data-aui-notification-error');
            clock.tick(100);

            afterMutations(function () {
                expect(countErrorsOnPage()).to.equal(0);

                $input.attr('data-aui-notification-error', 'Test');
                clock.tick(100);
                afterMutations(function () {
                    expect(countErrorsOnPage()).to.equal(1);

                    $input.removeAttr('data-aui-notification-error');
                    clock.tick(100);

                    afterMutations(function () {
                        expect(countErrorsOnPage()).to.equal(0);
                        done();
                    });
                });
            });
        });
    });
});
