import $ from '@atlassian/aui/src/js/aui/jquery';
import keyCode from '@atlassian/aui/src/js/aui/key-code';
import messageFunctions from '@atlassian/aui/src/js/aui/messages';
import { pressKey } from '../../helpers/all';

describe('aui/messages', function () {
    let messagebar;
    let clock;

    beforeEach(function () {
        $('#test-fixture').html('<div id="aui-message-bar"></div>');
        messagebar = $('#aui-message-bar');
        clock = sinon.useFakeTimers();
        createMessageWithID('close-message-test');
    });

    afterEach(function () {
        clock.restore();
        $('.aui-message').remove();
    });

    function createMessageWithID(testid) {
        createMessageWithIDAndSetCloseable(testid, true);
    }

    function createMessageWithIDAndSetCloseable(testid, closeable, removeOnHide = false) {
        messageFunctions.info({
            id: testid,
            title: 'Title',
            body: `This message was created by messagesSetup() with id ${testid}`,
            closeable: closeable,
            removeOnHide: removeOnHide,
        });
    }

    function checkNoID(target) {
        return {
            found: target.find('.aui-message')[0].getAttribute('id'),
            expected: null,
        };
    }

    it('globals', function () {
        expect(AJS.messages.toString()).to.equal(messageFunctions.toString());
    });

    it('Messages API', function () {
        expect(messageFunctions).to.be.an('object');
        expect(messageFunctions.setup).to.be.a('function');
        expect(messageFunctions.makeCloseable).to.be.a('function');
        expect(messageFunctions.createMessage).to.be.a('function');

        expect(messageFunctions.info).to.be.a('function');
        expect(messageFunctions.hint).to.be.a('function');
        expect(messageFunctions.generic).to.be.a('function');
        expect(messageFunctions.success).to.be.a('function');
        expect(messageFunctions.confirmation).to.be.a('function');
        expect(messageFunctions.change).to.be.a('function');
        expect(messageFunctions.error).to.be.a('function');
        expect(messageFunctions.warning).to.be.a('function');
    });

    it('Messages ID test: bad ID', function () {
        $('.aui-message').remove();
        createMessageWithID("#t.e.st-m### e s s a '''\"\"g e-id-full-of-dodgy-crap");
        var checkedNoID = checkNoID(messagebar);
        expect(checkedNoID.found).to.equal(checkedNoID.expected);
    });

    it('Messages ID test: no ID', function () {
        $('.aui-message').remove();
        createMessageWithID();
        var checkedNoID = checkNoID(messagebar);
        expect(checkedNoID.found).to.equal(checkedNoID.expected);
    });

    it('Messages ID test: good ID', function () {
        expect($('#close-message-test').length).to.equal(1);
    });

    it('Should render no title when it is falsy', function () {
        $('.aui-message').remove();
        messageFunctions.error({
            title: '',
            body: 'Cannot perform this operation',
            closeable: false,
        });

        expect(messagebar.find('.aui-message-error').length).to.equal(1);
        expect(messagebar.find('p.title').length).to.equal(0);
    });

    it('Should render no title when it is empty', function () {
        $('.aui-message').remove();
        messageFunctions.error({
            title: '      ',
            body: 'Cannot perform this operation',
            closeable: false,
        });

        expect(messagebar.find('.aui-message-error').length).to.equal(1);
        expect(messagebar.find('p.title').length).to.equal(0);
    });

    it('can output HTML in the body', function () {
        let messageElement = messageFunctions.info({ body: '<b class="oh-yay">groovy!</b>' });
        expect($(messageElement).find('.oh-yay').length).to.equal(1);
    });

    it('Closeable messages get a close button', function () {
        expect($('#close-message-test').find('.aui-close-button').length).to.equal(1);
    });

    it('Closing a message triggers the document aui-close-message event', function () {
        expect($('#close-message-test').length).to.equal(1);

        var closeMessageHandler = sinon.spy();
        $(document).on('aui-message-close', closeMessageHandler);

        $('#close-message-test .aui-close-button').click();
        clock.tick(100);

        expect($('#close-message-test').length).to.equal(0);
        expect(closeMessageHandler).to.have.been.calledOnce;
    });

    it('Calling makeCloseable on a closeable message will not generate multiple close buttons', function () {
        messageFunctions.makeCloseable('#close-message-test');

        expect($('#close-message-test').find('.aui-close-button').length).to.equal(1);
    });

    it('Calling makeCloseable on a closeable message will not bind multiple handlers', function () {
        messageFunctions.makeCloseable('#close-message-test');

        const closeMessageHandler = sinon.spy();
        $(document).on('aui-message-close', closeMessageHandler);

        $('#close-message-test .aui-close-button').click();
        clock.tick(100);

        expect(closeMessageHandler.callCount).to.equal(1);
    });

    it('Calling makeCloseable on a closeable message will not unbind dev added handlers', function () {
        const customClickHandler = sinon.spy();
        $('#close-message-test .aui-close-button').on('click', customClickHandler);

        messageFunctions.makeCloseable('#close-message-test');

        $('#close-message-test .aui-close-button').click();
        clock.tick(100);

        expect(customClickHandler.callCount).to.equal(1);
    });

    it('Pressing SPACE when focused on aui-close-button will close a message box', function () {
        expect($('#close-message-test').length).to.equal(1);

        var closeMessageHandler = sinon.spy();
        $(document).on('aui-message-close', closeMessageHandler);

        $('#close-message-test .aui-close-button')[0].focus();
        pressKey(keyCode.SPACE);
        clock.tick(100);

        expect($('#close-message-test').length).to.equal(0);
        expect(closeMessageHandler).to.have.been.calledOnce;
    });

    it('Pressing ENTER when focused on aui-close-button will close a message box', function () {
        expect($('#close-message-test').length).to.equal(1);

        var closeMessageHandler = sinon.spy();
        $(document).on('aui-message-close', closeMessageHandler);

        $('#close-message-test .aui-close-button')[0].focus();
        pressKey(keyCode.ENTER);
        clock.tick(100);

        expect($('#close-message-test').length).to.equal(0);
        expect(closeMessageHandler).to.have.been.calledOnce;
    });

    it('Pressing SPACE when NOT focused on aui-close-button will not close message', function () {
        var testLink = $('<a href="http://www.google.com/" id="test-link">Click Me</a>');
        $('#close-message-test .title').append(testLink);
        expect($('#close-message-test').length).to.equal(1);

        var closeMessageHandler = sinon.spy();
        $(document).on('aui-message-close', closeMessageHandler);

        testLink.focus();
        pressKey(keyCode.SPACE);
        clock.tick(100);

        expect($('#close-message-test').length).to.equal(1);
        expect(closeMessageHandler).to.have.not.been.calledOnce;
    });

    it('Calling makeCloseable() on a non-closeable message will convert it to a closeable message', function () {
        $('.aui-message').remove();
        createMessageWithIDAndSetCloseable('close-message-test', false);
        expect($('#close-message-test').length).to.equal(1);
        expect($('#close-message-test.closeable').length, 0, 'No closeable message present');
        messageFunctions.makeCloseable('#close-message-test');

        expect($('.closeable').length, 1, 'Message is now closeable');

        var closeMessageHandler = sinon.spy();
        $(document).on('aui-message-close', closeMessageHandler);

        $('#close-message-test .aui-close-button').click();
        clock.tick(100);

        expect($('#close-message-test').length).to.equal(0);
        expect(closeMessageHandler).to.have.been.calledOnce;
    });

    it('Messages setup() should enable closeable functionality on messages existing in the DOM', function () {
        $(
            '<div id="test-closeable-message" class="aui-message closeable"><p>Message text</p></div>'
        ).appendTo('#test-fixture');
        messageFunctions.setup();

        expect(
            $('#test-closeable-message .aui-close-button').length,
            1,
            'Close icon added to message'
        );

        var closeMessageHandler = sinon.spy();
        $(document).on('aui-message-close', closeMessageHandler);

        $('#test-closeable-message .aui-close-button').click();
        clock.tick(100);

        expect($('#test-closeable-message').length).to.equal(0);
        expect(closeMessageHandler).to.have.been.calledOnce;
    });

    it('Messages setup() should enable fadeout functionality on messages existing in the DOM', function () {
        $(
            '<div id="test-fadeout-on-message" class="aui-message fadeout"><p>Message text</p></div>'
        ).appendTo('#test-fixture');
        expect($('#test-fadeout-on-message').is(':visible')).to.equal(true);

        messageFunctions.setup();
        // Default fadeout delay is 5 seconds + .5 second fadeout duration.
        clock.tick(10000);
        expect($('#test-fadeout-on-message').is(':visible')).to.equal(false);
    });

    it('Should insert message before specified context when value of insert option is "before" ', function () {
        messagebar.empty();
        messageFunctions.info({
            id: 10001,
            title: 'Title',
            body: 'This message should be added before messagebar with id 10001',
            closeable: true,
            insert: 'before',
        });

        expect(messagebar.find('.aui-message-info').length).to.equal(0);
        expect(messagebar.prev('.aui-message-info').length).to.equal(1);
    });

    it('Should insert message after specified context when value of insert option is "after" ', function () {
        messagebar.empty();
        messageFunctions.info({
            id: 10001,
            title: 'Title',
            body: 'This message should be added after messagebar with id 10001',
            closeable: true,
            insert: 'after',
        });

        expect(messagebar.find('.aui-message-info').length).to.equal(0);
        expect(messagebar.next('.aui-message-info').length).to.equal(1);
    });

    describe('Remove message from DOM with aui-remove-on-hide', () => {
        const selector = '#test-remove-data';

        beforeEach(() => {
            createMessageWithIDAndSetCloseable('test-remove-data', false, true);

            expect($(selector).is(':visible')).to.equal(true);
            expect($(selector).length).to.equal(1);
        });

        it('Message should be remove from the DOM when it is on hide - after the fadeout code', () => {
            clock.tick(10000);

            expect($(selector).length).to.equal(0);
        });

        it('Message should be invisible on the page but exist in the DOM', () => {
            $(selector).removeClass('aui-remove-on-hide');
            clock.tick(10000);

            expect($(selector).is(':visible')).to.equal(false);
            expect($(selector).length).to.equal(1);
        });
    });

    describe('security', function () {
        let alerter;
        beforeEach(function () {
            alerter = sinon.stub(window, 'alert');
        });
        afterEach(function () {
            alerter.restore();
        });

        it('does not execute scripts', function () {
            const $message = messageFunctions.info({
                body: 'this script should not execute <script>alert("whoops")</script>',
            });
            $message.appendTo('#test-fixture');
            expect(alerter.callCount).to.equal(0);
        });
    });
});
