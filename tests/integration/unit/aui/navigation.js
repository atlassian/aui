import $ from '@atlassian/aui/src/js/aui/jquery';
import navigation from '@atlassian/aui/src/js/aui/navigation';
import skate from '@atlassian/aui/src/js/aui/internal/skate';
import { I18n } from '@atlassian/aui/src/js/aui/i18n';
import { fixtures } from '../../helpers/all';

describe('aui/navigation', function () {
    let navWithSelected;
    let navNoneSelected;

    beforeEach(function () {
        const els = fixtures({
            navItems1: `
            <ul class="aui-nav">
                <li aria-expanded="false">
                    <a href="#" class="aui-nav-subtree-toggle"><span class="aui-icon aui-icon-small aui-iconfont-expanded"></span></a>
                    <a href="#" class="aui-nav-item"><span class="aui-icon aui-icon-small aui-iconfont-time"></span><span class="aui-nav-item-label">ITEM</span></a>
                    <ul class="aui-nav" title="one">
                        <li class="aui-nav-selected"><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                    </ul>
                </li>
            </ul>`,
            navItems2: `
            <ul class="aui-nav">
                <li aria-expanded="false">
                    <a href="#" class="aui-nav-subtree-toggle"><span class="aui-icon aui-icon-small aui-iconfont-expanded"></span></a>
                    <a href="#" class="aui-nav-item"><span class="aui-icon aui-icon-small aui-iconfont-time"></span><span class="aui-nav-item-label">ITEM</span></a>
                    <ul class="aui-nav" title="one">
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                        <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>
                    </ul>
                </li>
            </ul>
        `,
        });
        skate.init([els.navItems1, els.navItems2]);
        navWithSelected = els.navItems1;
        navNoneSelected = els.navItems2;
    });

    it('globals', function () {
        expect(AJS.navigation.toString()).to.equal(navigation.toString());
    });

    // Expander tests
    describe('Expander', function () {
        it('auto-expands so that a selected item is visible', function () {
            var expander = navWithSelected.querySelector('li[aria-expanded]');
            expect(expander.getAttribute('aria-expanded')).to.equal('true');
        });

        it('does not auto-expand navs with no selected items', function () {
            let expander = navNoneSelected.querySelector('li[aria-expanded]');
            expect(expander.getAttribute('aria-expanded')).to.equal('false');
        });

        it('can be toggled', function () {
            var toggle = navWithSelected.querySelector('.aui-nav-subtree-toggle');
            var expander = navWithSelected.querySelector('li[aria-expanded]');

            toggle.click();
            expect(expander.getAttribute('aria-expanded')).to.equal('false');
            toggle.click();
            expect(expander.getAttribute('aria-expanded')).to.equal('true');
        });

        it('is translated', function () {
            var toggle = navWithSelected.querySelector('.aui-nav-subtree-toggle');
            var expander = navWithSelected.querySelector('li[aria-expanded]');

            expect(toggle.innerText).to.equal(I18n.getText('aui.words.collapse'));
            expect(expander.getAttribute('aria-expanded')).to.equal('true');
            toggle.click();
            expect(toggle.innerText).to.equal(I18n.getText('aui.words.expand'));
            expect(expander.getAttribute('aria-expanded')).to.equal('false');
            toggle.click();
            expect(toggle.innerText).to.equal(I18n.getText('aui.words.collapse'));
            expect(expander.getAttribute('aria-expanded')).to.equal('true');
        });
    });

    // Make sure aui-nav-child-selected class is added
    it('Child Selected class', function () {
        let selectedEl = navWithSelected.querySelector('.aui-nav-selected');
        expect(
            $(selectedEl).closest('li:not(.aui-nav-selected)').hasClass('aui-nav-child-selected')
        ).to.be.true;
    });
});
