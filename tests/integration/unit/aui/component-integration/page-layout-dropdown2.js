import { fixtures, click, mockPopper } from '../../../helpers/all';
import '@atlassian/aui/src/js/aui/select2';
import '@atlassian/aui/src/js/aui/inline-dialog2';
import aui from '@atlassian/aui-soy/entry/aui-soy';
import skate from '@atlassian/aui/src/js/aui/internal/skate';

describe('Page Layout with navigation and dropdown2 integration', function () {
    describe('dropdown in page layout - problematic for legacy Edge 18', function () {
        const menuMarkup = `<ul class="aui-nav aui-nav-vertical">
                <li><a href="#">Summary</a></li>
                <li><a href="#">Issues {call .dummyBadge}{param content: '5' /}{/call}</a></li>
                <li class="aui-nav-selected"><a href="#">Road Map</a></li>
                <li><a href="#">Change Log</a></li>
                <li><a href="#">Popular Issues</a></li>
                <li><a href="#">Versions</a></li>
                <li><a href="#">Components</a></li>
                <li><a href="#">Labels</a></li>
            </ul>`;

        const lorem =
            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi non quis exercitationem culpa nesciunt nihil aut nostrum explicabo reprehenderit optio amet ab temporibus asperiores quasi cupiditate. Voluptatum ducimus voluptates voluptas?';

        function createAndSkate(o, target) {
            let constructed = fixtures(o, false, target);
            skate.init(Object.keys(constructed).map((key) => constructed[key]));
            return constructed;
        }

        const generateDropdown2 = function (dropdownId, extraAttributes) {
            let options = {
                id: `${dropdownId}-trigger`,
                tagName: 'button',
                extraClasses: 'aui-button aui-dropdown2-trigger-arrowless',
                showIcon: false,
                text: 'Show menu',
                extraAttributes: extraAttributes,
                menu: {
                    id: `${dropdownId}-dropdown`,
                    content: aui.dropdown2.itemGroup({
                        items: [
                            { text: 'Foo', href: '#foo' },
                            { text: 'Bar', href: '#bar' },
                        ],
                    }),
                },
            };

            return {
                trigger: aui.dropdown2.trigger(options),
                dropdown: aui.dropdown2.contents(options.menu),
            };
        };

        let dropdownLeft;
        let dropdownRight;

        let popper;
        let clock;
        beforeEach(function () {
            popper = mockPopper((clock = sinon.useFakeTimers()));

            fixtures({
                page: aui.page.pagePanel({
                    content:
                        aui.page.pagePanelNav({
                            content: menuMarkup,
                        }) +
                        aui.page.pagePanelContent({
                            content: `<p>${lorem}</p><div id="dropdown-content"></div><p>${lorem}</p>`,
                        }),
                }),
            });

            let dropdownContainer = document.getElementById('dropdown-content');
            dropdownLeft = createAndSkate(generateDropdown2('d1'), dropdownContainer);
            dropdownRight = createAndSkate(
                generateDropdown2('d2', { style: 'float: right;' }),
                dropdownContainer
            );
        });

        afterEach(function () {
            popper.restore();
            clock.restore();
        });

        it('Opens a dropdown that is anchored to the left hand side of the trigger', function () {
            click(dropdownLeft.trigger);
            popper.tick();

            let dropdownLeftPos = dropdownLeft.dropdown.getBoundingClientRect().left;
            let triggerLeftPos = dropdownLeft.trigger.getBoundingClientRect().left;
            expect(dropdownLeftPos).to.be.within(triggerLeftPos - 1, triggerLeftPos + 1);
        });

        it('Opens a dropdown that is anchored to the right hand side of the trigger', function () {
            click(dropdownRight.trigger);
            popper.tick();

            let dropdownRightPos = dropdownRight.dropdown.getBoundingClientRect().right;
            let triggerRightPos = dropdownRight.trigger.getBoundingClientRect().right;
            expect(dropdownRightPos).to.be.within(triggerRightPos - 1, triggerRightPos + 1);
        });
    });
});
