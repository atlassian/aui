import $ from '@atlassian/aui/src/js/aui/jquery';
import skate from '@atlassian/aui/src/js/aui/internal/skate';
import { click, hover, pressKey } from '../../../helpers/all';
import keyCode from '@atlassian/aui/src/js/aui/key-code';
import {
    expectDialogOpenAfterTimeout,
    generateInlineDialog2,
    sendMouseEnterMessage,
} from './integration-helpers';
import '@atlassian/aui/src/js/aui/inline-dialog2';
import '@atlassian/aui/src/js/aui/select';

describe('inline dialog2 with single select integration', function () {
    let inlineDialog;
    const getSelectSuggestion = (n) => $('.aui-select-suggestion').get(n);

    beforeEach(function () {
        const singleSelectMarkup = `<form class="aui">
                <aui-select
            id="sync-product-single-select"
            name="product"
            placeholder="Select a product"
                >
                <aui-option>HipChat</aui-option>
                <aui-option>JIRA</aui-option>
                <aui-option>Confluence</aui-option>
                <aui-option>Stash</aui-option>
                <aui-option>FeCru</aui-option>
                </aui-select>
                </form>`;

        const inlineDialogId = 'inline-dialog2-visible';
        const fixtures = generateInlineDialog2(inlineDialogId, singleSelectMarkup);

        skate.init(fixtures.trigger);
        skate.init(fixtures.inlineDialog);
        inlineDialog = fixtures.inlineDialog;
        //aui select becomes initialized as a child of inline dialog
    });

    it('stays open when hovering on options', function (done) {
        sendMouseEnterMessage(inlineDialog);

        $('#sync-product-single-select button').click();
        hover(getSelectSuggestion(3));

        expectDialogOpenAfterTimeout(inlineDialog, done);
    });

    it('stays open after selecting an option', function (done) {
        sendMouseEnterMessage(inlineDialog);

        $('#sync-product-single-select button').click();
        click(getSelectSuggestion(3));
        pressKey(keyCode.ENTER);

        expectDialogOpenAfterTimeout(inlineDialog, done);
    });
});
