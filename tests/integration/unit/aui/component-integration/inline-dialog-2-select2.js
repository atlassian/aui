import $ from '@atlassian/aui/src/js/aui/jquery';
import skate from '@atlassian/aui/src/js/aui/internal/skate';
import { hover, pressKey } from '../../../helpers/all';
import keyCode from '@atlassian/aui/src/js/aui/key-code';
import '@atlassian/aui/src/js/aui/select2';
import '@atlassian/aui/src/js/aui/inline-dialog2';
import {
    expectDialogOpenAfterTimeout,
    generateInlineDialog2,
    sendMouseEnterMessage,
} from './integration-helpers';

describe('inline dialog 2 with select2 integration', function () {
    const SELECT2_ID = 'select2-example';
    const getSelect2 = () => $(`#${SELECT2_ID}`);
    const getSelect2Result = (n) => $('.select2-result').get(n);
    let inlineDialog;

    beforeEach(function () {
        const select2Markup = `<form class="aui">
                                        <select id="${SELECT2_ID}" multiple="">
                                            <option value="CONF">Confluence</option>
                                            <option value="JIRA">JIRA</option>
                                            <option value="BAM">Bamboo</option>
                                            <option value="JAG">JIRA Agile</option>
                                            <option value="CAP">JIRA Capture</option>
                                            <option value="AUI">AUI</option>
                                        </select>
                                    </form>`;

        const inlineDialogId = 'inline-dialog2-visible';
        const fixtures = generateInlineDialog2(inlineDialogId, select2Markup);
        skate.init(fixtures.trigger);
        skate.init(fixtures.inlineDialog);
        inlineDialog = fixtures.inlineDialog;
    });

    it('stays open when hovering on select2 options', function (done) {
        getSelect2().auiSelect2();
        sendMouseEnterMessage(inlineDialog);
        getSelect2().select2('open');
        hover(getSelect2Result(3));

        expectDialogOpenAfterTimeout(inlineDialog, done);
    });

    it('stays open after selecting an option', function (done) {
        getSelect2().auiSelect2();
        sendMouseEnterMessage(inlineDialog);
        getSelect2().select2('open');

        hover(getSelect2Result(3));
        pressKey(keyCode.ENTER);

        expectDialogOpenAfterTimeout(inlineDialog, done);
    });
});
