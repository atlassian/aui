import { afterMutations, fixtures } from '../../../helpers/all';
import '@atlassian/aui/src/js/aui/select2';
import '@atlassian/aui/src/js/aui/inline-dialog2';
import aui from '@atlassian/aui-soy/entry/aui-soy';
import dialog2Widget from '@atlassian/aui/src/js/aui/dialog2';

describe('modal dialog 2 with dropdown2 integration', function () {
    describe('nested modal dialogs', function () {
        let outerDialog;
        let innerDialog;

        beforeEach(function () {
            fixtures({
                dialogs: aui.dialog.dialog2({
                    id: 'outer-dialog',
                    content:
                        'The outer world' +
                        aui.dialog.dialog2({
                            id: 'inner-dialog',
                            content: 'The inner world',
                        }),
                }),
            });
            outerDialog = dialog2Widget(document.getElementById('outer-dialog'));
            innerDialog = dialog2Widget(document.getElementById('inner-dialog'));
        });

        it('fires a close event when closed', function () {
            const spy = sinon.spy();
            dialog2Widget('#outer-dialog').on('hide', spy);
            outerDialog.show();
            outerDialog.hide();
            expect(spy.callCount).to.equal(1);
        });

        it('only fires a close for itself, not nested layers', function () {
            const outerSpy = sinon.spy();
            const innerSpy = sinon.spy();
            dialog2Widget('#outer-dialog').on('hide', outerSpy);
            dialog2Widget('#inner-dialog').on('hide', innerSpy);

            outerDialog.show();
            innerDialog.show();
            innerDialog.hide();
            expect(innerSpy.callCount).to.equal(1);
            expect(outerSpy.callCount).to.equal(0);
        });
    });

    describe('nested dropdowns', function () {
        let outerDialog;
        let dd0;
        let dd1;
        let dd2;

        beforeEach(function (done) {
            fixtures({
                dialogs: aui.dialog.dialog2({
                    id: 'outer-dialog',
                    content: `<p>The outer world</p>
                            <button data-testid="open-dd-level-0" aria-controls="dd-level-0" class="aui-dropdown2-trigger">Open a dropdown</button>
                            <aui-dropdown-menu id="dd-level-0">
                                <aui-item-link for="dd-level-1">
                                    Opens an inner dropdown
                                    <aui-dropdown-menu id="dd-level-1">
                                        <aui-item-link for="dd-level-2">
                                            Opens another dropdown
                                            <aui-dropdown-menu id="dd-level-2">
                                                <aui-item-link disabled>(Does nothing)</aui-item-link>
                                                <aui-item-link>Close this dropdown?</aui-item-link>
                                                <aui-item-link>Close level 1 dropdown</aui-item-link>
                                                <aui-item-link>Close level 0 dropdown</aui-item-link>
                                                <aui-item-link>Close inner modal</aui-item-link>
                                                <aui-item-link>Close outer modal</aui-item-link>
                                            </aui-dropdown-menu>
                                        </aui-item-link>
                                    </aui-dropdown-menu>
                                </aui-item-link>
                            </aui-dropdown-menu>`,
                }),
            });
            outerDialog = dialog2Widget(document.getElementById('outer-dialog'));
            dd0 = document.getElementById('dd-level-0');
            dd1 = document.getElementById('dd-level-1');
            dd2 = document.getElementById('dd-level-2');
            afterMutations(done);
        });

        describe('with one dropdown open', function () {
            beforeEach(function () {
                outerDialog.show();
                dd0.show();
            });

            it('closing the dropdown does not trigger a hide on the dialog', function () {
                const dialogSpy = sinon.spy();
                outerDialog.on('hide', dialogSpy);
                dd0.hide();
                expect(dialogSpy.callCount).to.equal(0);
            });
        });

        describe('with all dropdowns open', function () {
            beforeEach(function () {
                outerDialog.show();
                dd0.show();
                dd1.show();
                dd2.show();
            });

            it('closing the deepest dropdown does not trigger a hide on the dialog', function () {
                const dialogSpy = sinon.spy();
                outerDialog.on('hide', dialogSpy);
                dd2.hide();
                expect(dialogSpy.callCount).to.equal(0);
            });
        });
    });
});
