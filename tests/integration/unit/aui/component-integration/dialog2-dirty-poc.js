import $ from '@atlassian/aui/src/js/aui/jquery';
import aui from '@atlassian/aui-soy/entry/aui-soy';
import skate from '@atlassian/aui/src/js/aui/internal/skate';
import generateUniqueId from '@atlassian/aui/src/js/aui/unique-id';
import dialog2Widget from '@atlassian/aui/src/js/aui/dialog2';
import keyCode from '@atlassian/aui/src/js/aui/key-code';
import { afterMutations, click, fixtures, pressKey } from '../../../helpers/all';

describe('modal dialog 2 dirty form proof-of-concept', () => {
    let form;
    let dialog2;
    let select;
    let hideCb;

    beforeEach((done) => {
        const formId = generateUniqueId('dirty-form');
        const dialog2Id = generateUniqueId('dirty-dialog');
        const selectId = generateUniqueId('dirty-select');

        const built = fixtures({
            modalDialog: aui.dialog.dialog2({
                id: dialog2Id,
                content: `
                <p>A potentially dirty form</p>
                <form id="${formId}" class="aui">
                    <select id="${selectId}" multiple="">
                        <option value="CONF">Confluence</option>
                        <option value="JIRA">JIRA</option>
                        <option value="BAM" selected>Bamboo</option>
                        <option value="JAG">JIRA Agile</option>
                        <option value="CAP">JIRA Capture</option>
                        <option value="AUI">AUI</option>
                    </select>
                </form>`,
            }),
        });

        skate.init(Object.values(built));
        dialog2 = dialog2Widget(document.getElementById(dialog2Id));
        select = document.getElementById(selectId);
        form = document.getElementById(formId);

        // note: this is not a robust dirty form implementation!
        //  we are only demonstrating it's possible to build, not how to build a good one.
        $(form).on('change', () => $(form).data('dirty', true));

        // prevent dialog from closing if form is dirty.
        hideCb = sinon.spy((e) => $(form).data('dirty') && e.preventDefault());
        dialog2.on('beforeHide', hideCb);

        afterMutations(done);
    });

    describe('when dialog is open', () => {
        beforeEach(() => dialog2.show());

        it('can be closed with escape key', () => {
            pressKey(keyCode.ESCAPE, document.body);
            expect(dialog2.isVisible()).to.equal(
                false,
                'dialog should close when form is not dirty'
            );
            expect(hideCb.callCount).to.equal(1, 'hide callback should be invoked');
        });

        it('can be closed when clicking on the blanket', () => {
            click(document.querySelector('.aui-blanket'));
            expect(dialog2.isVisible()).to.equal(
                false,
                'dialog should close when form is not dirty'
            );
            expect(hideCb.callCount).to.equal(1, 'hide callback should be invoked');
        });

        describe('and form is dirty', () => {
            beforeEach(() => $(select).val('JIRA').trigger('change'));

            it('cannot be closed with escape key', () => {
                pressKey(keyCode.ESCAPE, document.body);
                expect(dialog2.isVisible()).to.equal(
                    true,
                    'dialog should stay open when form is dirty'
                );
                expect(hideCb.callCount).to.equal(1, 'hide callback should be invoked');
            });

            it('can be closed when clicking on the blanket', () => {
                click(document.querySelector('.aui-blanket'));
                expect(dialog2.isVisible()).to.equal(
                    true,
                    'dialog should stay open when form is dirty'
                );
                expect(hideCb.callCount).to.equal(1, 'hide callback should be invoked');
            });
        });
    });
});
