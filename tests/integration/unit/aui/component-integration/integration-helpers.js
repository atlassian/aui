import $ from '@atlassian/aui/src/js/aui/jquery';
import { fixtures } from '../../../helpers/all';
import aui from '@atlassian/aui-soy/entry/aui-soy';

export function sendMouseEnterMessage(dialog) {
    dialog.message($.Event('mouseenter'));
}

export function generateInlineDialog2(inlineDialogId, dropdownMarkup, respondsTo = 'hover') {
    return fixtures({
        trigger:
            '<a data-aui-trigger aria-controls="' +
            inlineDialogId +
            '" href="#' +
            inlineDialogId +
            '">Show</a>',
        inlineDialog: aui.inlineDialog2.inlineDialog2({
            id: inlineDialogId,
            content: dropdownMarkup,
            respondsTo,
        }),
    });
}

export function expectDialogOpenAfterTimeout(inlineDialog, done) {
    setTimeout(function () {
        expect(inlineDialog.open).to.equal(true);
        done();
    }, 100);
}
