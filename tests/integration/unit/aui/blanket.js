import * as blanket from '@atlassian/aui/src/js/aui/blanket';
import $ from '@atlassian/aui/src/js/aui/jquery';

function getBlanketZIndex() {
    return blanket.dim.$dim.css('z-index');
}

describe('aui/blanket', function () {
    var clock;

    beforeEach(function () {
        clock = sinon.useFakeTimers();
        //clear body styles
        for (let i = document.body.style.length; i--; ) {
            const nameString = document.body.style[i];
            document.body.style.removeProperty(nameString);
        }
    });

    afterEach(function () {
        clock.restore();
    });

    it('globals', function () {
        expect(AJS.dim.toString()).to.equal(blanket.dim.toString());
        expect(AJS.undim.toString()).to.equal(blanket.undim.toString());
    });

    it('dim() will update the z-index accordingly', function () {
        blanket.dim(false, 5);
        expect(getBlanketZIndex()).to.equal('5');
        blanket.dim(false, 10);
        expect(getBlanketZIndex()).to.equal('10');
    });

    it('dim() will not created multiple blankets on duplicate calls', function () {
        blanket.dim();
        blanket.dim();
        expect($('.aui-blanket').length).to.equal(1);
    });

    it('dim() and undim() will hide all blankets that were previously visible', function () {
        blanket.dim();
        blanket.undim();

        var noBlanket = !blanket.dim.$dim;
        var invisibleBlanket = blanket.dim.$dim.attr('aria-hidden') === 'true';
        expect(noBlanket || invisibleBlanket).to.be.true;
    });

    it('dim() will place aria-hidden="true" on the blanket element it creates', function () {
        const page = document.createElement('div');
        document.body.appendChild(page);

        const cover = blanket.dim().get(0);
        expect(cover.getAttribute('aria-hidden')).to.equal('true');

        document.body.removeChild(page);
    });

    it('unDim() will not change aria-hidden from an element that is a direct descendant of body', function () {
        const page = document.createElement('div');
        document.body.appendChild(page);
        const attrVal = page.getAttribute('aria-hidden');

        blanket.dim();
        blanket.undim();

        expect(page.getAttribute('aria-hidden')).to.equal(attrVal);

        document.body.removeChild(page);
    });

    it('unDim() does not remove aria-hidden from an element that already had aria-hidden when dim was called', function () {
        var divWithAriaHiddenFalse = document.createElement('div');
        var divWithAriaHiddenTrue = document.createElement('div');
        divWithAriaHiddenFalse.setAttribute('aria-hidden', 'false');
        divWithAriaHiddenTrue.setAttribute('aria-hidden', 'true');
        document.body.appendChild(divWithAriaHiddenFalse);
        document.body.appendChild(divWithAriaHiddenTrue);
        blanket.dim();
        blanket.undim();
        expect(divWithAriaHiddenFalse.getAttribute('aria-hidden')).to.equal('false');
        expect(divWithAriaHiddenTrue.getAttribute('aria-hidden')).to.equal('true');
    });

    it('should ensure that the blanket is not displayed after hidden', function () {
        blanket.dim();
        blanket.undim();

        clock.tick(1000);
        expect(blanket.dim.$dim.css('visibility')).to.equal('hidden');
    });

    it('Calling dim() twice restores correctly', function () {
        var divAddedBeforeDim = document.createElement('div');
        var divAddedAfterDim = document.createElement('div');
        document.body.appendChild(divAddedBeforeDim);
        blanket.dim();
        document.body.appendChild(divAddedAfterDim);
        blanket.dim();
        blanket.undim();
        expect(divAddedBeforeDim.getAttribute('aria-hidden')).to.equal(null);
        expect(divAddedAfterDim.getAttribute('aria-hidden')).to.equal(null);
        expect($('body').css('overflow')).to.eql('visible');
    });

    describe('overflow', function () {
        describe('on <body>', function () {
            it('is reverted to visible on undim if it was originally visible', function () {
                $('body').css({ overflow: '' });
                blanket.dim();
                blanket.undim();
                expect($('body').css('overflow')).to.equal('visible');
            });

            it('is set to hidden on undim if it was originally hidden', function () {
                $('body').css({ overflow: 'hidden' });
                blanket.dim();
                blanket.undim();
                expect($('body').css('overflow')).to.equal('hidden');
            });

            it('does not override overflow-x on undim', function () {
                $('body').css({ 'overflow-x': 'hidden' });

                const originalOverflow = document.body.style.overflow;
                const computedOverflow = window.getComputedStyle(document.body).overflow;

                blanket.dim();
                blanket.undim();

                expect($('body').css('overflow-x')).to.equal('hidden');
                expect(document.body.style.overflow).to.equal(originalOverflow);
                expect(window.getComputedStyle(document.body).overflow).to.equal(computedOverflow);
            });

            it('does not override overflow-y on undim', function () {
                $('body').css({ 'overflow-y': 'hidden' });

                const originalOverflow = document.body.style.overflow;
                const computedOverflow = window.getComputedStyle(document.body).overflow;

                blanket.dim();
                blanket.undim();

                expect($('body').css('overflow-y')).to.equal('hidden');
                expect(document.body.style.overflow).to.equal(originalOverflow);
                expect(window.getComputedStyle(document.body).overflow).to.equal(computedOverflow);
            });
        });

        describe('on <html>', function () {
            it('is reverted to visible on undim if it was originally visible', function () {
                //have to query the dom for the default in this case because ie7 does not adhere to the spec
                var overrideDefault = $('html').css('overflow');
                blanket.dim();
                blanket.undim();
                expect($('html').css('overflow')).to.equal(overrideDefault);
            });

            it('is set to hidden on undim if it was originally hidden', function () {
                $('html').css({ overflow: 'hidden' });
                blanket.dim();
                blanket.undim();
                expect($('html').css('overflow')).to.equal('hidden');
            });

            it('does not override overflow-x on undim', function () {
                $('html').css({ 'overflow-x': 'hidden' });

                const originalOverflow = document.documentElement.style.overflow;
                const computedOverflow = window.getComputedStyle(document.documentElement).overflow;

                blanket.dim();
                blanket.undim();

                expect($('html').css('overflow-x')).to.equal('hidden');
                expect(document.documentElement.style.overflow).to.equal(originalOverflow);
                expect(window.getComputedStyle(document.documentElement).overflow).to.equal(
                    computedOverflow
                );
            });

            it('does not override overflow-y on undim', function () {
                $('html').css({ 'overflow-y': 'hidden' });

                const originalOverflow = document.documentElement.style.overflow;
                const computedOverflow = window.getComputedStyle(document.documentElement).overflow;

                blanket.dim();
                blanket.undim();

                expect($('html').css('overflow-y')).to.equal('hidden');
                expect(document.documentElement.style.overflow).to.equal(originalOverflow);
                expect(window.getComputedStyle(document.documentElement).overflow).to.equal(
                    computedOverflow
                );
            });
        });
    });
});
