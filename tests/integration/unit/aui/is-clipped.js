import $ from '@atlassian/aui/src/js/aui/jquery';
import isClipped from '@atlassian/aui/src/js/aui/is-clipped';

describe('aui/is-clipped', function () {
    it('globals', function () {
        expect(AJS.isClipped.toString()).to.equal(isClipped.toString());
    });

    describe('API', function () {
        beforeEach(function () {
            const $el = $('#test-fixture');
            $el.html(`
                <p id="shouldBeClipped">Long Long Long Long Long Long Long Long Long Long Long Long Long Long Long</p>
                <p id="shouldNotBeClipped">Short</p>
            `);
            $el.find('p').css({
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
                overflow: 'hidden',
            });
            $el.find('#shouldBeClipped').css({
                width: '50px',
            });
        });

        it('long paragraph', function () {
            expect(isClipped($('#shouldBeClipped'))).to.be.true;
        });

        it('short paragraph', function () {
            expect(isClipped($('#shouldNotBeClipped'))).to.be.false;
        });
    });
});
