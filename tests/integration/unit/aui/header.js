import '@atlassian/aui/src/js/aui/dropdown2';
import responsiveHeader from '@atlassian/aui/src/js/aui/header';
import '@atlassian/aui/src/js/aui/header-async';
import $ from '@atlassian/aui/src/js/aui/jquery';
import { afterMutations, click, fixtures, focus, pressKey } from '../../helpers/all';
import skate from '@atlassian/aui/src/js/aui/internal/skate';
import keyCode from '@atlassian/aui/src/js/aui/key-code';
import { I18n } from '@atlassian/aui/src/js/aui/i18n';
import { mockPopper } from '../../helpers/all';

const $window = $(window);
let clock;
let popper;

const MORE_DROPDOWN_TRIGGER_SELECTOR = '#aui-responsive-header-dropdown-0-trigger';
const MORE_DROPDOWN_SELECTOR = '#aui-responsive-header-dropdown-0';

function expectMoreTriggerAndDropdownToNotExist() {
    const moreDropdownTriggerEl = document.querySelector(MORE_DROPDOWN_TRIGGER_SELECTOR);
    const moreDropdownEl = document.querySelector(MORE_DROPDOWN_SELECTOR);
    expect(moreDropdownTriggerEl).to.not.exist;
    expect(moreDropdownEl).to.not.exist;
}

// AUI-5432 running test suite for aui/header two times:
// - 1st run: w/o aui-header-end element, i.e. testing backward-compatibility
// - 2nd run: with aui-header-end element, i.e. testing a fixed async. init. of AUI header
[{ shouldUseAsyncInit: false }, { shouldUseAsyncInit: true }].forEach(({ shouldUseAsyncInit }) => {
    describe(`aui/header (shouldUseAsyncInit: ${shouldUseAsyncInit})`, function () {
        let $headerContainer;

        function resizeWindow(width, next) {
            $headerContainer.width(width);
            $window.trigger('resize');
            clock.tick(200);
            if (next) {
                afterMutations(next);
            }
        }

        function createHeader(content, asyncInit = shouldUseAsyncInit) {
            content = content || {};
            const container = $headerContainer.get(0);
            const header = fixtures(
                {
                    header: `<nav id="test-header" class="aui-header aui-dropdown2-trigger-group" role="navigation" data-aui-responsive="true">
                    ${content.before || ''}
                    <div class="aui-header-primary" aria-label="${I18n.getText('aui.header.home.link')}}">
                        <span id="logo" class="aui-header-logo">
                            <a href="/">
                                ${content.header || ''}
                            </a>
                        </span>
                        ${content.primary || ''}
                        <content select=".aui-header-content"></content>
                    </div>
                    ${content.secondary || ''}
                    ${content.after || ''}
                    ${asyncInit ? '<aui-header-end></aui-header-end>' : ''}
                </nav>`,
                },
                true,
                container
            ).header;

            // Initialise any dropdown triggers inside the header
            Array.prototype.forEach.call(
                header.querySelectorAll('.aui-dropdown2-trigger'),
                function (el) {
                    skate.init(el);
                }
            );

            return header;
        }

        beforeEach(function () {
            $headerContainer = $('#test-fixture');
            $headerContainer.css('width', 'auto');
        });

        afterEach(function () {
            $window.off('resize'); // todo: get a reference to the Header instance and call destroy instead.
            $headerContainer.css('width', false);
        });

        describe('Responsive header - ', function () {
            // Since the nav item <li>'s may be detatched from the DOM, save a reference to them.
            let $navItems;

            beforeEach(function (done) {
                clock = sinon.useFakeTimers();
                createHeader({
                    primary: `
                    <ul class="aui-nav aui-header-content">
                        <li id="item-1" class="test-header-item"><a class="aui-nav-link" href="#item-1-href">Some long text (1)</a></li>
                        <li id="item-2" class="test-header-item"><a class="aui-nav-link" href="#item-2-href">Some long text (2)</a></li>
                        <li id="item-3" class="test-header-item"><a class="aui-nav-link" href="#item-3-href">Some long text (3)</a></li>
                        <li id="item-4" class="test-header-item">
                            <a class="aui-dropdown2-trigger" href="#test-menu" aria-controls="test-menu" aria-haspopup="true" >Some long text (4)</a>
                            <div id="test-menu" class="aui-dropdown2">
                                <ul>
                                    <li><a href="#">Google</a></li>
                                </ul>
                            </div>
                        </li>
                        <li id="item-5" class="test-header-item">
                            <a class="aui-dropdown2-trigger aui-should-not-copy-this-one jira-dropdown2-ajax" href="#test.menu:.[],=with.special-chars" aria-controls="test.menu:.[],=with.special-chars" aria-haspopup="true" data-aui-extra-classes>Some long text (5)</a>
                            <div id="test.menu:.[],=with.special-chars" class="aui-dropdown2">
                                <ul>
                                    <li><a href="#">Google</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>`,
                });

                responsiveHeader.setup();
                $navItems = $('#test-header .aui-nav > li');
                // afterMutations to allow the mutation observer to run <aui-header>'s attached callback.
                afterMutations(done);
            });

            afterEach(function (done) {
                clock.restore();
                afterMutations(done);
            });

            function getMoreDropdownItemEl($navItem) {
                const $navItemTrigger = $navItem.children('a');
                const $moreDropdown = $(MORE_DROPDOWN_SELECTOR);
                if ($navItemTrigger.hasClass('aui-dropdown2-trigger')) {
                    const dropdownId = $navItemTrigger.attr('aria-controls');
                    return $moreDropdown.find(`aui-item-link[for="${dropdownId}"]`)[0];
                } else {
                    return $moreDropdown.find(
                        `aui-item-link[href="${$navItemTrigger.attr('href')}"]`
                    )[0];
                }
            }

            function expectItemIsInHeader($navItem) {
                expect(document.getElementById($navItem.attr('id'))).to.equal($navItem[0]);
                expect(getMoreDropdownItemEl($navItem)).to.not.be.ok;
            }

            function expectItemIsInResponsiveDropdown($navItem) {
                const $navItemTrigger = $navItem.children('a');
                const moreDropdownItemEl = getMoreDropdownItemEl($navItem);

                expect(document.getElementById($navItem.attr('id'))).to.not.be.ok;
                expect(moreDropdownItemEl.textContent).to.equal($navItemTrigger.text());
                expect(moreDropdownItemEl.getAttribute('href')).to.equal(
                    $navItemTrigger.attr('href')
                );
                if ($navItemTrigger.hasClass('aui-dropdown2-trigger')) {
                    expect(moreDropdownItemEl.getAttribute('for')).to.equal(
                        $navItemTrigger.attr('aria-controls')
                    );
                }
            }

            function expectTriggerClassesInResponsiveDropdown($navItem, ...classes) {
                const moreDropdownItemEl =
                    getMoreDropdownItemEl($navItem).querySelector(':scope > a');
                classes.forEach((cssClass) => {
                    expect(
                        moreDropdownItemEl.classList.contains(cssClass),
                        `expected ${cssClass} CSS class to be present`
                    ).is.true;
                });
            }

            function expectTriggerClassesNotPresentInResponsiveDropdown($navItem, ...classes) {
                const moreDropdownItemEl =
                    getMoreDropdownItemEl($navItem).querySelector(':scope > a');
                classes.forEach((cssClass) => {
                    expect(
                        moreDropdownItemEl.classList.contains(cssClass),
                        `expected ${cssClass} CSS class to NOT be present`
                    ).is.false;
                });
            }

            function $getMoreTrigger() {
                return $(
                    '#test-header .aui-header-primary .aui-nav > li:last .aui-dropdown2-trigger:not([aria-controls=test-menu])'
                );
            }

            describe('applies aui-dropdown2-in-header class', function () {
                let trigger;
                let dropdown;

                beforeEach(function () {
                    trigger = document.querySelector('a[aria-controls="test-menu"]');
                    dropdown = document.getElementById('test-menu');
                });

                function expectHeaderClass(dropdown, isExpected, summary) {
                    expect(dropdown.classList.contains('aui-dropdown2-in-header')).to.equal(
                        isExpected,
                        summary
                    );
                }

                it('to dropdown triggered via click', function () {
                    expectHeaderClass(
                        dropdown,
                        false,
                        'Dropdown should not have class before triggered'
                    );
                    click(trigger);
                    expectHeaderClass(
                        dropdown,
                        true,
                        'Dropdown should have class when triggered by click'
                    );
                });

                it('to dropdown triggered via keyboard', function () {
                    expectHeaderClass(
                        dropdown,
                        false,
                        'Dropdown should not have class before triggered'
                    );
                    focus(trigger);
                    pressKey(keyCode.SPACE);
                    expectHeaderClass(
                        dropdown,
                        true,
                        'Dropdown should have class when triggered by keyboard'
                    );
                });
            });

            describe('when all items fit and when the container is resized smaller', function () {
                // More menu width is always the same. Unable to grab the width dynamically because the more menu is
                // inserted after the resize not before, hence calling width will get an incorrect value.
                const moreMenuWidth = 100;
                let spy;
                beforeEach(function (done) {
                    const itemWidth = document.querySelector('.test-header-item').offsetWidth;
                    const logo = $('#logo');
                    const padding = logo.offset().left * 2 + logo.outerWidth(true) + moreMenuWidth;
                    spy = sinon.spy();
                    $('.aui-header').on('aui-responsive-menu-item-created', spy);
                    resizeWindow(itemWidth * 2 + padding, done);
                });

                afterEach(function () {
                    $('.aui-header').off('aui-responsive-menu-item-created');
                });

                it('the trigger for the responsive menu should be visible', function () {
                    expect(document.querySelector(MORE_DROPDOWN_TRIGGER_SELECTOR)).to.be.visible;
                });

                it('the custom event is fired for each of the 3 items moving into the More dropdown', function () {
                    expect(spy).to.have.been.calledThrice;
                });

                it('the event should originate from the aui header element itself', function () {
                    expect(spy.args[0][0].target).to.equal($('.aui-header')[0]);
                });

                it('the original and new elements should be available via the originalEvent detail arg', function () {
                    const firstCall = spy.getCall(0);
                    const firstCallEvent = firstCall.args[0].originalEvent;

                    expect(firstCallEvent.detail.originalItem.id).to.equal('item-5');
                    expect(firstCallEvent.detail.newItem.tagName.toLowerCase()).to.equal(
                        'aui-item-link'
                    );
                    expect($(firstCallEvent.detail.newItem).text()).to.equal('Some long text (5)');

                    const secondCall = spy.getCall(1);
                    const secondCallEvent = secondCall.args[0].originalEvent;

                    expect(secondCallEvent.detail.originalItem.id).to.equal('item-4');
                    expect(secondCallEvent.detail.newItem.tagName.toLowerCase()).to.equal(
                        'aui-item-link'
                    );
                    expect($(secondCallEvent.detail.newItem).text()).to.equal('Some long text (4)');

                    const thirdCall = spy.getCall(2);
                    const thirdCallEvent = thirdCall.args[0].originalEvent;

                    expect(thirdCallEvent.detail.originalItem.id).to.equal('item-3');
                    expect(thirdCallEvent.detail.newItem.tagName.toLowerCase()).to.equal(
                        'aui-item-link'
                    );
                    expect($(thirdCallEvent.detail.newItem).text()).to.equal('Some long text (3)');
                });

                it('the header contains items 1 and 2, the responsive menu contains items 3, 4, and 5', function () {
                    expectItemIsInHeader($navItems.filter('#item-1'));
                    expectItemIsInHeader($navItems.filter('#item-2'));
                    expectItemIsInResponsiveDropdown($navItems.filter('#item-3'));
                    expectItemIsInResponsiveDropdown($navItems.filter('#item-4'));
                    expectItemIsInResponsiveDropdown($navItems.filter('#item-5'));
                });

                it('should copy extra CSS classes from the original trigger to the responsive one', function () {
                    expectTriggerClassesInResponsiveDropdown(
                        $navItems.filter('#item-5'),
                        'jira-dropdown2-ajax'
                    );
                    expectTriggerClassesNotPresentInResponsiveDropdown(
                        $navItems.filter('#item-5'),
                        'aui-should-not-copy-this-one'
                    );

                    expectTriggerClassesNotPresentInResponsiveDropdown(
                        $navItems.filter('#item-4'),
                        'jira-dropdown2-ajax'
                    );
                });
            });

            describe('when only some items fit and when the container is resized larger', function () {
                beforeEach(function (done) {
                    const itemWidth = document.querySelector('.test-header-item').offsetWidth;
                    resizeWindow(itemWidth * 3, function () {
                        //couldn't find a reliable way that works cross browser (auto and 100% didn't seem to work in phantomJS)
                        //so we just set it to something very large
                        resizeWindow('9999', done);
                    });
                });

                it('the responsive menu trigger should not be visible', function () {
                    expect(document.querySelector(`${MORE_DROPDOWN_SELECTOR} > a`)).to.not.be
                        .visible;
                });

                it('the header contains items 1, 2, 3, and 4', function () {
                    expectItemIsInHeader($navItems.filter('#item-1'));
                    expectItemIsInHeader($navItems.filter('#item-2'));
                    expectItemIsInHeader($navItems.filter('#item-3'));
                    expectItemIsInHeader($navItems.filter('#item-4'));
                });
            });

            describe('Submenus -', function () {
                function expectDropdownOpenedSideways($trigger) {
                    const $dropdown = $(document.getElementById($trigger.attr('aria-controls')));
                    expect($trigger.hasClass('aui-dropdown2-sub-trigger')).to.equal(
                        true,
                        'trigger'
                    );
                    expect($dropdown.hasClass('aui-dropdown2-sub-menu')).to.equal(true, 'dropdown');
                    expect($dropdown.attr('data-aui-alignment')).to.equal('submenu auto');
                    expect($dropdown.attr('data-popper-placement')).to.equal('right-start');
                }

                function expectDropdownOpenedDownwards($trigger) {
                    const $dropdown = $(document.getElementById($trigger.attr('aria-controls')));
                    expect($trigger.hasClass('aui-dropdown2-sub-trigger')).to.equal(
                        false,
                        'trigger'
                    );
                    expect($dropdown.hasClass('aui-dropdown2-sub-menu')).to.equal(
                        false,
                        'dropdown'
                    );
                    expect($dropdown.attr('data-aui-alignment')).to.equal('bottom auto');
                }

                let itemWidth;
                let $navItemTrigger;
                let $navItemDropdown;
                beforeEach(function () {
                    popper = mockPopper(clock);
                    itemWidth = document.querySelector('.test-header-item').offsetWidth;
                    $navItemTrigger = $navItems.filter('#item-5').find('.aui-dropdown2-trigger');
                    $navItemDropdown = $(
                        document.getElementById($navItemTrigger.attr('aria-controls'))
                    );
                });

                afterEach(function () {
                    popper.restore();
                });

                [
                    {
                        desc: 'regular -',
                        selector: '#item-4',
                    },
                    {
                        desc: 'with special chars in id -',
                        selector: '#item-5',
                    },
                ].forEach(function (item) {
                    describe(item.desc, () => {
                        beforeEach(function () {
                            itemWidth = document.querySelector('.test-header-item').offsetWidth;
                            $navItemTrigger = $navItems
                                .filter(item.selector)
                                .find('.aui-dropdown2-trigger');
                            $navItemDropdown = $(
                                document.getElementById($navItemTrigger.attr('aria-controls'))
                            );
                        });

                        [true, false].forEach(function (openBeforeResizing) {
                            it(`${openBeforeResizing ? 'with' : 'without'} opening before resizing, opens as a submenu then downwards`, function (done) {
                                if (openBeforeResizing) {
                                    click($navItemTrigger);
                                    popper.tick();

                                    expectDropdownOpenedDownwards($navItemTrigger);
                                    click($navItemTrigger);
                                    popper.tick();
                                }

                                resizeWindow(itemWidth * 3, function () {
                                    const $moreItemLinkAnchor = $(
                                        `aui-item-link[for="${$navItemDropdown.attr('id')}"] > a`
                                    );

                                    click($getMoreTrigger());
                                    popper.tick();

                                    click($moreItemLinkAnchor);
                                    popper.tick();

                                    expectDropdownOpenedSideways($moreItemLinkAnchor);
                                    click($getMoreTrigger());
                                    popper.tick();

                                    resizeWindow('9999', function () {
                                        click($navItemTrigger);
                                        popper.tick();

                                        expectDropdownOpenedDownwards($navItemTrigger);
                                        done();
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });

        describe('Responsive header initialisation - ', function () {
            let header;

            beforeEach(function (done) {
                clock = sinon.useFakeTimers();
                header = createHeader({
                    primary: `<ul class="aui-nav aui-header-content">
                        <li id="item-1" class="test-header-item"><a class="aui-nav-link" href="#item-1-href">Some long text (1)</a></li>
                        <li id="item-2" class="test-header-item"><a class="aui-nav-link" href="#item-2-href">Some long text (2)</a></li>
                        <li id="item-3" class="test-header-item"><a class="aui-nav-link" href="#item-3-href">Some long text (3)</a></li>
                        <li id="item-4" class="test-header-item">
                            <a class="aui-dropdown2-trigger" href="#test-menu" aria-controls="test-menu" aria-haspopup="true" >Some long text (4)</a>
                            <div id="test-menu" class="aui-dropdown2">
                                <ul>
                                    <li><a href="#">Google</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>`,
                });

                afterMutations(done);
            });

            afterEach(function () {
                clock.restore();
            });

            it('is the same when initialised with the setup function first and then skated', function (done) {
                responsiveHeader.setup();
                const initial = header.cloneNode(true); // Make a deep clone of the header
                skate.init(header);
                afterMutations(function () {
                    expect(header.isEqualNode(initial)).to.equal(true);
                    done();
                });
            });

            // AUI-5432 - the following test is going to work only with the updated markup
            //          - if the markup is not updated, the init. will happen on DCL as it used to be prev. as well
            if (shouldUseAsyncInit) {
                it('is the same when skated first and then initialised with the setup function', function (done) {
                    skate.init(header);

                    afterMutations(function () {
                        const initial = header.cloneNode(true); // Make a deep clone of the header
                        responsiveHeader.setup();
                        expect(header.isEqualNode(initial)).to.equal(true);
                        done();
                    });
                });
            }
        });

        // AUI-5432 - the following tests are regression tests for the bug fix
        //          - during the page load DOM parsing is streamlined, thus we cannot assume that
        //            a parent is able to see all of its children at the time of processing the parent
        //            in the MO callback
        if (shouldUseAsyncInit) {
            describe('Responsive header async. init. during the page load -', function () {
                let header;

                beforeEach(function (done) {
                    clock = sinon.useFakeTimers();
                    header = createHeader(
                        {
                            primary: `<ul class="aui-nav aui-header-content">
                            <li id="item-1" class="test-header-item"><a class="aui-nav-link" href="#item-1-href">Some long text (1)</a></li>
                            <li id="item-2" class="test-header-item"><a class="aui-nav-link" href="#item-2-href">Some long text (2)</a></li>
                            <li id="item-3" class="test-header-item"><a class="aui-nav-link" href="#item-3-href">Some long text (3)</a></li>
                            <li id="item-4" class="test-header-item">
                                <a class="aui-dropdown2-trigger" href="#test-menu" aria-controls="test-menu" aria-haspopup="true" >Some long text (4)</a>
                                <div id="test-menu" class="aui-dropdown2">
                                    <ul>
                                        <li><a href="#">Google</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>`,
                        },
                        false
                    );

                    afterMutations(done);
                });

                afterEach(function () {
                    clock.restore();
                });

                const isHeaderInitialised = (element) => typeof element._header !== 'undefined';

                it('should wait for the aui-header-end element before initialising the header', function (done) {
                    skate.init(header);

                    afterMutations(function () {
                        expect(isHeaderInitialised(header)).to.equal(false);

                        header.appendChild(document.createElement('aui-header-end'));
                        skate.init(header);

                        afterMutations(function () {
                            expect(isHeaderInitialised(header)).to.equal(true);

                            done();
                        });
                    });
                });
            });
        }

        describe('Responsive header with no nav items -', function () {
            beforeEach(function () {
                clock = sinon.useFakeTimers();
            });

            afterEach(function () {
                clock.restore();
            });

            describe('no <ul class="aui-nav">', function () {
                beforeEach(function (done) {
                    createHeader();
                    responsiveHeader.setup();
                    afterMutations(() => resizeWindow(500, done));
                });

                it('does not init', function () {
                    expectMoreTriggerAndDropdownToNotExist();
                });
            });

            describe('empty <ul class="aui-nav">', function () {
                beforeEach(function (done) {
                    createHeader({
                        primary: '<ul class="aui-nav aui-header-content"></ul>',
                    });
                    responsiveHeader.setup();
                    afterMutations(() => resizeWindow(500, done));
                });

                it('does not init', function () {
                    expectMoreTriggerAndDropdownToNotExist();
                });
            });

            describe('hidden secondary nav (AUI-3550)', function () {
                beforeEach(function (done) {
                    createHeader({
                        primary: '<ul class="aui-nav aui-header-content"></ul>',
                        secondary: `<div class="aui-header-secondary">
                                    <ul class="aui-nav" style="display: none;">
                                        <li><a href="#log-out">Log out</a></li>
                                    </ul>
                                </div>`,
                    });
                    responsiveHeader.setup();
                    afterMutations(() => resizeWindow(500, done));
                });

                it('does not init', function () {
                    expectMoreTriggerAndDropdownToNotExist();
                });
            });
        });
    });
});
