import {
    computeBooleanValue,
    setBooleanAttribute,
    computeEnumValue,
    setEnumAttribute,
} from '@atlassian/aui/src/js/aui/internal/attributes';

const TRUTHY_VALUES = [true, 1, 'true', 'false'];
const FALSY_VALUES = [false, 0, '', null, undefined];

function describeValue(value) {
    return `'${value}': ${typeof value}`;
}

describe('aui/internal/attributes', function () {
    const ATTRIBUTE = 'foo';

    var el;
    var setAttributeSpy;
    var removeAttributeSpy;
    beforeEach(function () {
        el = document.createElement('div');
        setAttributeSpy = sinon.spy(el, 'setAttribute');
        removeAttributeSpy = sinon.spy(el, 'removeAttribute');
    });

    describe('computeBooleanValue', function () {
        it('returns false for null', function () {
            expect(computeBooleanValue(null)).to.equal(false);
        });

        it('returns true for non-null falsy values', function () {
            const nonNullFalsyValues = FALSY_VALUES.filter((value) => value !== null);
            nonNullFalsyValues.forEach((value) => {
                expect(computeBooleanValue(value)).to.equal(true, describeValue(value));
            });
        });
    });

    describe('computeBooleanValue for attr absent', function () {
        it('matches hasAttribute', function () {
            expect(computeBooleanValue(el.getAttribute(ATTRIBUTE))).to.equal(
                el.hasAttribute(ATTRIBUTE)
            );
        });
    });

    describe('computeBooleanValue for attr present', function () {
        const nonNullValues = TRUTHY_VALUES.concat(FALSY_VALUES).filter((value) => value !== null);

        nonNullValues.forEach((value) => {
            beforeEach(function () {
                el.setAttribute(ATTRIBUTE, value);
                setAttributeSpy.resetHistory();
            });

            it(`matches hasAttribute (${describeValue(value)})`, function () {
                expect(computeBooleanValue(el.getAttribute(ATTRIBUTE))).to.equal(
                    el.hasAttribute(ATTRIBUTE)
                );
            });
        });
    });

    function itRemovesTheBooleanAttributeWhenPropertySetToBeFalsy() {
        FALSY_VALUES.forEach((value) => {
            it(`removes the attr for falsy values (${describeValue(value)})`, function () {
                setBooleanAttribute(el, ATTRIBUTE, value);
                expect(el.hasAttribute(ATTRIBUTE)).to.equal(false, 'hasAttribute');
                expect(removeAttributeSpy.callCount).to.equal(1, 'removeAttribute');
            });
        });
    }

    function itSetsTheAttributeToEmptyWhenPropertySetToTruthy() {
        TRUTHY_VALUES.forEach((value) => {
            it(`adds the attr for truthy values (${describeValue(value)})`, function () {
                setBooleanAttribute(el, ATTRIBUTE, value);
                expect(el.getAttribute(ATTRIBUTE)).to.equal('', 'getAttribute');
                expect(setAttributeSpy.callCount).to.equal(1, 'removeAttribute');
            });
        });
    }

    describe('setBooleanAttribute for attr absent', function () {
        itRemovesTheBooleanAttributeWhenPropertySetToBeFalsy();
        itSetsTheAttributeToEmptyWhenPropertySetToTruthy();
    });

    describe('setBooleanAttribute for attr present and empty', function () {
        beforeEach(function () {
            el.setAttribute(ATTRIBUTE, '');
            setAttributeSpy.resetHistory();
        });

        itRemovesTheBooleanAttributeWhenPropertySetToBeFalsy();
        itSetsTheAttributeToEmptyWhenPropertySetToTruthy();
    });

    describe('setBooleanAttribute for attr present and non-empty', function () {
        beforeEach(function () {
            el.setAttribute(ATTRIBUTE, ATTRIBUTE);
            setAttributeSpy.resetHistory();
        });

        itRemovesTheBooleanAttributeWhenPropertySetToBeFalsy();

        TRUTHY_VALUES.forEach((value) => {
            it(`normalizes the attr for truthy values (${describeValue(value)})`, function () {
                setBooleanAttribute(el, ATTRIBUTE, value);
                expect(el.getAttribute(ATTRIBUTE)).to.equal('', 'getAttribute');
            });
        });
    });

    function computeEnumValueMatchesValuesCaseInsensitive(enumOptions) {
        it('matches values case-insensitive', function () {
            enumOptions.values.forEach((value) => {
                var computedValue = computeEnumValue(enumOptions, value.toUpperCase());
                expect(computedValue).to.equal(value, describeValue(value));
            });
        });
    }

    describe('enum without defaults', function () {
        const ENUM_OPTIONS = {
            attribute: ATTRIBUTE,
            values: ['foo', 'bar'],
        };

        computeEnumValueMatchesValuesCaseInsensitive(ENUM_OPTIONS);

        it('computeEnumValue returns null when missing', function () {
            expect(computeEnumValue(ENUM_OPTIONS, null)).to.equal(null);
        });

        it('setEnumAttribute sets the value as-is, even if there is a case-insensitive match', function () {
            ENUM_OPTIONS.values.forEach((value) => {
                const upperCasedValue = value.toUpperCase();
                setEnumAttribute(el, ENUM_OPTIONS, upperCasedValue);
                expect(el.getAttribute(ENUM_OPTIONS.attribute)).to.equal(
                    upperCasedValue,
                    describeValue(upperCasedValue)
                );
            });
        });

        it('setEnumAttribute sets the attribute even if it is aleady exactly the new value', function () {
            el.setAttribute(ENUM_OPTIONS.attribute, 'foo');
            setAttributeSpy.resetHistory();

            setEnumAttribute(el, ENUM_OPTIONS, 'foo');
            expect(el.getAttribute(ENUM_OPTIONS.attribute)).to.equal('foo', 'getAttribute');
            expect(setAttributeSpy.callCount).to.equal(1, 'setAttribute');
        });

        it('setEnumAttribute passes the new value verbatim to setAttribute', function () {
            FALSY_VALUES.forEach((value) => {
                setAttributeSpy.resetHistory();

                setEnumAttribute(el, ENUM_OPTIONS, value);
                expect(setAttributeSpy.callCount).to.equal(
                    1,
                    `setAttribute (${describeValue(value)})`
                );
                expect(setAttributeSpy.calledWithExactly(ENUM_OPTIONS.attribute, value)).to.equal(
                    true,
                    `calledWith (${describeValue(value)}`
                );
            });
        });

        it('setEnumAttribute does not remove the attribute for null', function () {
            el.setAttribute(ENUM_OPTIONS.attribute, 'foo');
            setAttributeSpy.resetHistory();

            setEnumAttribute(el, ENUM_OPTIONS, null);
            expect(removeAttributeSpy.callCount).to.equal(0);
        });
    });

    describe('enum with missing default', function () {
        const ENUM_OPTIONS = {
            attribute: ATTRIBUTE,
            values: ['foo', 'bar'],
            missingDefault: 'missing-default',
        };

        computeEnumValueMatchesValuesCaseInsensitive(ENUM_OPTIONS);

        it('computeEnumValue returns the missing default when missing', function () {
            expect(computeEnumValue(ENUM_OPTIONS, null)).to.equal(ENUM_OPTIONS.missingDefault);
        });

        it('computeEnumValue returns the missing default when invalid', function () {
            expect(computeEnumValue(ENUM_OPTIONS, 'invalid')).to.equal(ENUM_OPTIONS.missingDefault);
        });
    });

    describe('enum with missing and invalid defaults', function () {
        const ENUM_OPTIONS = {
            attribute: ATTRIBUTE,
            values: ['foo', 'bar'],
            missingDefault: 'missing-default',
            invalidDefault: 'invalid-default',
        };

        computeEnumValueMatchesValuesCaseInsensitive(ENUM_OPTIONS);

        it('computeEnumValue returns the missing default when missing', function () {
            expect(computeEnumValue(ENUM_OPTIONS, null)).to.equal(ENUM_OPTIONS.missingDefault);
        });

        it('computeEnumValue returns the invalid default when invalid', function () {
            expect(computeEnumValue(ENUM_OPTIONS, 'invalid')).to.equal(ENUM_OPTIONS.invalidDefault);
        });

        it('computeEnumValue does not match against the missing default', function () {
            const computedValue = computeEnumValue(ENUM_OPTIONS, ENUM_OPTIONS.missingDefault);
            expect(computedValue).to.equal(ENUM_OPTIONS.invalidDefault);
        });
    });
});
