import amdify from '@atlassian/aui/src/js/aui/internal/amdify';

describe('aui/internal/amdify', function () {
    let oldDefine;
    let defines = {};

    beforeEach(function () {
        oldDefine = window.define;
        window.define = function (name, dependencies, definition) {
            defines[name] = {
                dependencies: dependencies,
                definition: definition,
            };
        };
    });

    afterEach(function () {
        window.define = oldDefine;
    });

    it('should specify an empty array as the dependencies', function () {
        amdify('something', {});
        expect(defines.something.dependencies).to.be.an('array');
        expect(defines.something.dependencies.length).to.equal(0);
    });

    it('should register the amd module', function () {
        let mod = {};
        amdify('something', mod);
        expect(defines.something.definition).to.be.a('function');
        expect(defines.something.definition()).to.equal(mod);
    });

    it('should return the module that was passed in', function () {
        let mod = {};
        expect(amdify('something', mod)).to.equal(mod);
    });
});
