import $ from '@atlassian/aui/src/js/aui/jquery';
import CustomEvent from '@atlassian/aui/src/js/aui/polyfills/custom-event';
import Alignment, {
    GPU_ACCELERATION_FLAG,
    ATTR_CONTAINER,
} from '@atlassian/aui/src/js/aui/internal/alignment';
import { fixtures, mockPopper } from '../../../helpers/all';

describe('aui/internal/alignment', function () {
    let $viewport;
    let $canvas;
    let $anchor;
    let popper;
    let alignment;

    function resize() {
        window.dispatchEvent(new CustomEvent('resize'));
    }

    function scrollTo(pixels) {
        $viewport.get(0).scrollTop = pixels;
        $viewport.get(0).dispatchEvent(new CustomEvent('scroll'));
    }

    function createLayer(alignment) {
        return $('<div id="layer-1" class="box"></div>')
            .attr('data-aui-alignment', alignment)
            .appendTo($canvas);
    }

    function createAlignment(el, target, opt) {
        const defaults = {
            flip: false,
            positionFixed: false,
            preventOverflow: false,
        };

        return new Alignment(el, target, { ...defaults, ...opt });
    }

    function addAlignmentContainer() {
        $anchor.attr(`data-aui-${ATTR_CONTAINER}`, '#align_container');
    }

    function removeAlignmentContainer() {
        $anchor.removeAttr(`data-aui-${ATTR_CONTAINER}`);
    }

    function comparePosition($layer, expected) {
        const position = $layer.offset();

        expect(
            position.top === expected.top && position.left === expected.left,
            `${expected.position}: expected "${expected.left}, ${expected.top}" but got "${position.left}, ${position.top}"`
        ).to.equal(true);
    }

    beforeEach(function () {
        const constructed = fixtures({
            viewport: $('<div id="viewport">').css({
                overflow: 'scroll',
                height: 200,
                width: 200,
            }),
            canvas: $('<div id="canvas"></div>').css({
                height: 400,
                width: 400,
                top: 0,
                left: 0,
            }),
            align_container: $('<div id="align_container"></div>').css({
                height: 400,
                width: 200,
                top: 0,
                left: 200,
                position: 'absolute',
            }),
            styles: `<style>
                .dot {
                    position: relative;
                    overflow: hidden;
                    width: 10px;
                    height: 10px;
                    background: rebeccapurple;
                    display: block;
                }
                .box {
                    position: absolute;
                    display: block;
                    background: orange;
                    /* layer needs some height so that layer :visible selector works properly */
                    width: 16px;
                    height: 16px;
                }
            </style>`,
        });
        popper = mockPopper();

        $viewport = $(constructed.viewport);
        $canvas = $(constructed.canvas).appendTo($viewport);
        $(constructed.align_container).appendTo($canvas);
        $anchor = $('<div class="dot">Im the target</div>')
            // throw it in the middle of the canvas
            .css({
                top: 40,
                left: 40,
            })
            .appendTo($canvas);
    });

    afterEach(function () {
        popper.restore();
        if (alignment) {
            alignment.destroy();
        }
        removeAlignmentContainer();
    });

    const expectedOffsets = [
        { position: '', top: 37, left: 50 },
        { position: 'bad position', top: 37, left: 50 },

        { position: 'bottom right', top: 50, left: 34, anchor: { top: 100, left: 100 } },
        { position: 'bottom center', top: 50, left: 37 },
        { position: 'bottom left', top: 50, left: 40 },

        { position: 'top left', top: 24, left: 40 },
        { position: 'top center', top: 24, left: 37 },
        { position: 'top right', top: 24, left: 34 },

        { position: 'right top', top: 40, left: 50 },
        { position: 'right middle', top: 37, left: 50 },
        { position: 'right bottom', top: 34, left: 50 },

        { position: 'left top', top: 40, left: 24 },
        { position: 'left middle', top: 37, left: 24 },
        { position: 'left bottom', top: 34, left: 24 },
    ];

    expectedOffsets.forEach(function (expected) {
        it(`sets proper transform for ${expected.position}`, function () {
            const $layer = createLayer(expected.position);
            popper.tick();
            alignment = createAlignment($layer.get(0), $anchor.get(0));
            popper.tick();

            comparePosition($layer, expected);
        });
    });

    it('does not automatically follow resize and scroll events', function () {
        const offset = expectedOffsets[2];
        const $layer = createLayer(offset.position);
        alignment = createAlignment($layer.get(0), $anchor.get(0));
        popper.tick();

        $anchor.css(offset.anchor);
        resize();
        popper.tick();

        comparePosition($layer, offset);

        scrollTo(10);
        popper.tick();

        comparePosition($layer, offset);
    });

    describe('css positioning', function () {
        const localTestOffsets = [
            { position: 'bottom auto', top: 50, left: 40 },
            { position: 'bottom auto', top: 110, left: 250, anchor: { top: 100, left: 250 } },
            { position: 'bottom auto', top: 110, left: 384, anchor: { top: 100, left: 390 } },
        ];

        it('is using top/left to position layer', function () {
            const offset = expectedOffsets[2];
            const $layer = createLayer(offset.position);
            alignment = createAlignment($layer.get(0), $anchor.get(0));
            popper.tick();

            expect($layer.get(0).style.transform).to.equal('');
            expect($layer.get(0).style.top).to.not.equal('');
            expect($layer.get(0).style.top).to.not.equal('0px');
            expect($layer.get(0).style.left).to.not.equal('');
            expect($layer.get(0).style.left).to.not.equal('0px');
        });

        it('is using transform to position layer when CSS class exists on body', function () {
            const offset = expectedOffsets[2];
            const $layer = createLayer(offset.position);
            document.body.classList.add(GPU_ACCELERATION_FLAG);
            alignment = createAlignment($layer.get(0), $anchor.get(0));
            popper.tick();

            expect($layer.get(0).style.transform).to.not.equal('');
            expect($layer.get(0).style.top).to.equal('0px');
            expect($layer.get(0).style.left).to.equal('auto');
        });

        it('Is left snapped by default', function () {
            const offset = localTestOffsets[0];
            const $layer = createLayer(offset.position);

            addAlignmentContainer();

            alignment = createAlignment($layer.get(0), $anchor.get(0), { preventOverflow: false });
            popper.tick();

            comparePosition($layer, offset);
            expect($layer.attr('data-popper-placement')).to.equal('bottom-start');
        });

        it('Is left snapped even if in right half of the viewport', function () {
            const offset = localTestOffsets[1];
            const $layer = createLayer(offset.position);

            addAlignmentContainer();
            $anchor.css(offset.anchor);

            alignment = createAlignment($layer.get(0), $anchor.get(0), { preventOverflow: false });
            popper.tick();

            comparePosition($layer, offset);
            expect($layer.attr('data-popper-placement')).to.equal('bottom-start');
        });

        it('Is right snapped on the edge of viewport', function () {
            const offset = localTestOffsets[2];
            const $layer = createLayer(offset.position);

            addAlignmentContainer();
            $anchor.css(offset.anchor);

            alignment = createAlignment($layer.get(0), $anchor.get(0), { preventOverflow: false });
            popper.tick();

            comparePosition($layer, offset);
            expect($layer.attr('data-popper-placement')).to.equal('bottom-end');
        });
    });

    describe('', function () {
        const oldPosition = { top: 40, left: 40 };
        const newPosition = { top: 100, left: 100 };
        const expectedOffset = { position: 'right top', left: 110, top: 100 };
        let $layer;

        beforeEach(function () {
            $anchor.css(oldPosition);
            $layer = createLayer(expectedOffset.position);
            alignment = createAlignment($layer.get(0), $anchor.get(0));
            popper.tick();
        });

        describe('#enable', function () {
            it('causes the element to re-position when the window is resized', function () {
                alignment.enable();
                $anchor.css(newPosition);
                resize();
                popper.tick();

                comparePosition($layer, expectedOffset);
            });

            it('causes the element to re-position when its scroll parent is scrolled', function () {
                const offsetY = 27;
                alignment.enable();
                $anchor.css(newPosition);
                scrollTo(offsetY);
                popper.tick();

                comparePosition($layer, {
                    position: 'right top',
                    left: expectedOffset.left,
                    top: expectedOffset.top - offsetY,
                });
            });
        });

        describe('#scheduleUpdate', function () {
            it('re-positions the element next to its target', function () {
                $anchor.css(newPosition);
                alignment.scheduleUpdate();
                popper.tick();

                comparePosition($layer, expectedOffset);
            });
        });
    });

    describe('', function () {
        let $layer;
        let $anchor1;
        let $anchor2;

        beforeEach(function () {
            $anchor1 = $anchor.css({
                top: 20,
                left: 40,
            });
            $anchor2 = $('<div class="dot">Second anchor</div>')
                .css({
                    top: 80,
                    left: 80,
                })
                .appendTo($canvas);
            $layer = createLayer('right top');
        });

        it('has callbacks', function () {
            const created = sinon.spy();
            const updated = sinon.spy();
            const enabled = sinon.spy();
            const disabled = sinon.spy();

            alignment = createAlignment($layer.get(0), $anchor.get(0), {
                onCreate: created,
                onUpdate: updated,
                onEvents: {
                    enabled,
                    disabled,
                },
            });

            // callbacks don't happen until at least one render occurs.
            expect(created.callCount, 'should not be called until a render occurs').to.equal(0);
            expect(updated.callCount, 'should not be called until a render occurs').to.equal(0);
            expect(enabled.callCount, 'should not be called until a render occurs').to.equal(0);
            expect(disabled.callCount, 'should not be called until a render occurs').to.equal(0);

            // trigger popper to render
            popper.tick();
            expect(created.callCount, 'should not be called on first render').to.equal(1);
            expect(updated.callCount, 'should not be called on first update').to.equal(0);
            expect(enabled.callCount, 'enabled should not be called on first render').to.equal(0);
            expect(disabled.callCount, 'disabled should not be called on first render').to.equal(0);

            // schedule and trigger another render
            alignment.scheduleUpdate();
            popper.tick();

            expect(created.callCount, 'should not be called on second render').to.equal(1);
            expect(updated.callCount, 'should be called on second render').to.equal(1);
            expect(enabled.callCount, 'should not be called on second render').to.equal(0);
            expect(disabled.callCount, 'should not be called on second render').to.equal(0);

            // enable eventListenes and schedule and trigger another render
            alignment.enable();
            popper.tick();

            expect(created.callCount, 'should not be called on eventsLister enable').to.equal(1);
            expect(updated.callCount, 'should be called on eventsLister enable').to.equal(2);
            expect(enabled.callCount, 'should be called on eventsLister enable').to.equal(1);
            expect(disabled.callCount, 'should not be called on eventsLister enable').to.equal(0);

            // schedule and trigger another render
            alignment.scheduleUpdate();
            popper.tick();

            expect(created.callCount, 'should not be called on fourth render').to.equal(1);
            expect(updated.callCount, 'should be called on fourth render').to.equal(3);
            expect(enabled.callCount, 'should not be called on fourth render').to.equal(1);
            expect(disabled.callCount, 'should not be called on fourth render').to.equal(0);

            // disable eventListenes and schedule and trigger another render
            alignment.disable();
            popper.tick();
            expect(created.callCount, 'should not be called on eventsLister disable').to.equal(1);
            expect(updated.callCount, 'should be called on eventsLister disable').to.equal(4);
            expect(enabled.callCount, 'should not be called on eventsLister disable').to.equal(1);
            expect(disabled.callCount, 'should be called on eventsLister disable').to.equal(1);

            // schedule and trigger another render
            alignment.scheduleUpdate();
            popper.tick();

            expect(created.callCount, 'should not be called on sixth render').to.equal(1);
            expect(updated.callCount, 'should be called on sixth render').to.equal(5);
            expect(enabled.callCount, 'should not be called on sixth render').to.equal(1);
            expect(disabled.callCount, 'should not be called on sixth render').to.equal(1);
        });

        describe('#changeTarget', function () {
            beforeEach(function () {
                alignment = createAlignment($layer.get(0), $anchor1.get(0));
                popper.tick();
            });

            it('updates the alignment', function () {
                alignment.enable();
                alignment.changeTarget($anchor2.get(0));
                popper.tick();

                comparePosition($layer, { left: 90, top: 90 });
            });

            it('updates the alignment even when resize and scroll events are disabled', function () {
                alignment.disable();
                alignment.changeTarget($anchor2.get(0));
                popper.tick();

                comparePosition($layer, { left: 90, top: 90 });
            });
        });
    });
});
