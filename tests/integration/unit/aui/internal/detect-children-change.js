import { onChildrenChange } from '@atlassian/aui/src/js/aui/internal/detect-children-change';

describe('aui/internal/detect-children-change', function () {
    let el;

    function createParentEl() {
        const parent = document.createElement('div');
        document.body.appendChild(parent);
        return parent;
    }

    function addChild(parent) {
        const child = document.createElement('div');
        parent.appendChild(child);
    }

    beforeEach(function () {
        document.body.innerText = '';
        el = createParentEl();
    });

    it('should call initially', function (done) {
        onChildrenChange(el, () => {
            done();
        });
    });

    it('should detect change', function (done) {
        let callsCount = 0;
        onChildrenChange(el, () => {
            callsCount += 1;
            if (callsCount === 2) {
                done();
            }
        });
        addChild(el);
    });

    it('should not detect change when unsubscribed', function () {
        let callsCount = 0;
        onChildrenChange(el, (unsub) => {
            if (callsCount === 0) {
                unsub();
            }
            callsCount += 1;
        });
        addChild(el);
        expect(callsCount).to.be.equal(1);
    });

    it('should not detect attribute change on parent', function () {
        let callsCount = 0;
        onChildrenChange(el, () => {
            callsCount += 1;
        });
        el.setAttribute('aria-label', 'test');
        expect(callsCount).to.be.equal(1);
    });
});
