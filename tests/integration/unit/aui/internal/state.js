import state from '@atlassian/aui/src/js/aui/internal/state';

describe('aui/internal/state', function () {
    it('state is set on the object', function () {
        var element = {};
        state(element).set('testState', 'testValue');
        expect(element._state.testState).to.equal('testValue');
    });

    it('gets the correct value', function () {
        var element = {
            _state: {
                testState: 'testValue',
            },
        };
        expect(state(element).get('testState')).to.equal('testValue');
    });

    it('returns undefined if _state does not exist', function () {
        var element = {};
        expect(state(element).get(element, 'testState')).to.equal(undefined);
    });

    it('returns undefined if requested state does not exist', function () {
        var element = {
            _state: {},
        };
        expect(state(element).get('testState')).to.equal(undefined);
    });
});
