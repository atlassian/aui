import globalize, { putOnI18nIfAbsent } from '@atlassian/aui/src/js/aui/internal/globalize';
import { expect } from 'chai';

describe('aui/internal/globalize', function () {
    var oldAjs;

    beforeEach(function () {
        oldAjs = window.AJS;
        window.AJS = undefined;
    });

    afterEach(function () {
        window.AJS = oldAjs;
    });

    describe('the AJS namespace', function () {
        it('should be created if it does not exist', function () {
            expect(window.AJS).to.be.undefined;
            globalize('test', true);
            expect(window.AJS).to.not.be.undefined;
        });

        it('should be an object when created', function () {
            globalize('test', true);
            expect(window.AJS).to.be.an('object');
        });

        it('should leave any previously-created object in place', function () {
            var ourAjs = (window.AJS = { foo: 'bar' });
            globalize('test', true);
            expect(window.AJS).to.equal(ourAjs);
        });

        it('should get a new property based on the name and value passed in', function () {
            globalize('test', true);
            expect(window.AJS.test).to.equal(true);
        });
    });

    it('should return the namespaced value that was passed in', function () {
        expect(globalize('test', true)).to.equal(true);
    });

    describe('setting up the I18n object on the AJS namespace', function () {
        it('should mixin anything missing', function () {
            const providedByAui = 'provided by AUI';
            const missingProperty = 'missingProperty';

            putOnI18nIfAbsent('missingProperty', providedByAui);

            expect(window.AJS.I18n[missingProperty]).to.equal(providedByAui);
        });

        it('should not be override what is set by the WRM', function () {
            const providedByTheWrm = 'provided by the WRM';
            const providedByAui = 'provided by AUI';

            const keys = 'keys';
            const getText = 'getText';

            // Pretend the WRM sets up AJS.I18n with the getText method
            window.AJS = {
                I18n: {
                    [getText]: providedByTheWrm,
                },
            };

            putOnI18nIfAbsent(keys, providedByAui);
            putOnI18nIfAbsent(getText, providedByAui);

            expect(window.AJS.I18n[getText]).to.equal(providedByTheWrm);
            expect(window.AJS.I18n[keys]).to.equal(providedByAui);
        });
    });
});
