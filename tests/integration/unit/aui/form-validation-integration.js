import '@atlassian/aui/src/js/aui/select';
import $ from '@atlassian/aui/src/js/aui/jquery';
import keyCode from '@atlassian/aui/src/js/aui/key-code';
import skate from '@atlassian/aui/src/js/aui/internal/skate';
import validator from '@atlassian/aui/src/js/aui/form-validation';
import { fixtures, fakeTypingOut, pressKey } from '../../helpers/all';

describe('aui/form-validation-integration', function () {
    describe('Form validation component integration', function () {
        var clock;

        afterEach(function () {
            $('.tipsy').remove();
        });

        function fieldValidationState(el) {
            return $(el).attr('data-aui-validation-state');
        }

        describe('with single select', function () {
            var singleSelect;

            beforeEach(function () {
                clock = sinon.useFakeTimers();
                singleSelect = createAndSkate(
                    '<aui-select data-aui-validation-field data-aui-validation-required="required">' +
                        '<aui-option>Option 1</aui-option>' +
                        '<aui-option>Option 2</aui-option>' +
                        '<aui-option>Option 3</aui-option>' +
                        '</aui-select>'
                );
            });

            afterEach(function () {
                clock.restore();
            });

            function focusSingleSelectInput() {
                singleSelect._input.focus();
            }

            function createAndSkate(html) {
                return skate.init(fixtures({ html }).html);
            }

            it('validates successfully with the required validator', function () {
                focusSingleSelectInput();
                fakeTypingOut('Option 1');
                pressKey(keyCode.ENTER);
                validator.validate(singleSelect);
                expect(fieldValidationState(singleSelect)).to.equal('valid');
            });

            it('invalidates successfully with the required validator', function () {
                validator.validate(singleSelect);
                expect(fieldValidationState(singleSelect)).to.equal('invalid');
            });
        });
    });
});
