import * as I18nModule from '@atlassian/aui/src/js/aui/i18n';

describe('aui/i18n', function () {
    it('globals', function () {
        expect(AJS.I18n.toString()).to.equal(I18nModule.I18n.toString());
    });

    it('return key test', function () {
        expect(I18nModule.I18n.getText('test.key')).to.equal('test.key');
    });

    describe('returns the value for a defined key', function () {
        beforeEach(function () {
            I18nModule.I18n.keys['test.key'] = 'This is a Value';
            I18nModule.I18n.keys['test.formatting.key'] = 'Formatting {0}, and {1}';
        });

        afterEach(function () {
            delete I18nModule.I18n.keys['test.key'];
            delete I18nModule.I18n.keys['test.formatting.key'];
        });

        it('with no formatting', function () {
            expect(I18nModule.I18n.getText('test.key')).to.equal('This is a Value');
        });

        it('with formatting', function () {
            expect(I18nModule.I18n.getText('test.formatting.key', 'hello', 'world')).to.equal(
                'Formatting hello, and world'
            );
        });
    });

    describe('correctly exports functions', function () {
        it("I18n.getText is present for when something hasn't been transformed", function () {
            expect(I18nModule.I18n.getText).to.be.a('function');
        });

        it('format is exported on the parent of I18n.getText for the single phase JS I18n transformer', function () {
            expect(I18nModule.format).to.be.a('function');
        });
    });
});
