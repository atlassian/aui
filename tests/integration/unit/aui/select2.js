import '@atlassian/aui/src/js/aui/select2';
import $ from '@atlassian/aui/src/js/aui/jquery';

const DROPDOWN_LAYER_KEY = 'aui.select2DropdownLayer';

describe('aui/select2', function () {
    let $fixture;
    let server;
    let clock;
    let logger;
    const TIMEOUT_MS = 100;
    const data = [
        {
            id: 1,
            text: 'one',
        },
        {
            id: 2,
            text: 'two',
        },
    ];
    let ajaxOptions;

    beforeEach(function () {
        $fixture = $('#test-fixture');
        clock = sinon.useFakeTimers();
        server = sinon.fakeServer.create({
            respondImmediately: true,
        });
        logger = sinon.stub(console, 'warn');

        ajaxOptions = {
            ajax: {
                type: 'GET',
                url: '/dummy/search',
                dataType: 'json',
                data: function () {
                    return {};
                },
                results: function (data) {
                    return { results: data };
                },
            },
            formatResult: function (item) {
                return item.name;
            },
            formatSelection: function (item) {
                return item.name;
            },
        };
    });

    afterEach(function () {
        $fixture.html('');
        server.restore();
        console.warn.restore();
    });

    it('exists', function () {
        expect($.fn.auiSelect2).to.be.a('function');
    });

    it('can be created', function () {
        const $select = $('<select />');
        $select.appendTo($fixture);
        $select.auiSelect2();
        expect($select.prev('.aui-select2-container').length).to.be.above(0);
    });

    // Extra spaces in class attribute will cause an <input type="hidden"> with select2 applied to it  to not close in FF
    // try the below example in the sandbox
    it('with appended custom AUI classes', function () {
        const $input = $('<input>').attr('type', 'hidden');
        $input.appendTo(this.$fixture);
        $input.auiSelect2({
            data: data,
        });
        expect(/\s\s+/.test($input.prev().attr('class'))).to.be.false;
    });

    it('includes the search field label into the aria-labelledby value for single select', function () {
        $fixture.html(
            '<div>' +
                '<label for="test-select">Test label</label>' +
                '<input id="test-select" />' +
                '</div>'
        );
        const $input = $fixture.find('#test-select');
        $input.auiSelect2({
            data: data,
        });
        // Regular .focus() call may not trigger the event in Debug mode for some reason
        $input.trigger('select2-focus');
        this.timeout(TIMEOUT_MS);
        const $button = $('.select2-container .select2-focusser');
        const $label = $('.select2-container .select2-choice').next();
        expect($button.attr('aria-labelledby')).to.include($label.attr('id'));
    });

    it('replaces the search label text with the custom one in single select', function () {
        $fixture.html(
            '<div>' +
                '<label for="test-select">Test label</label>' +
                '<input id="test-select" />' +
                '</div>'
        );
        const $input = $fixture.find('#test-select');
        $input.auiSelect2({
            data: data,
            searchLabel: 'Overriden label',
        });
        $input.auiSelect2('open');
        this.timeout(TIMEOUT_MS);
        expect($('.select2-drop .select2-search label').text()).to.equal('Overriden label');
    });

    it('replaces the search label text with the custom one in multi-select', function () {
        $fixture.html(
            '<div>' +
                '<label for="test-select-multi">Test multi label</label>' +
                '<input id="test-select-multi" />' +
                '</div>'
        );
        var $input = $fixture.find('#test-select-multi');
        $input.auiSelect2({
            data: data,
            multiple: true,
            searchLabel: 'Overriden multi label',
        });
        // Regular .focus() call may not trigger the event in Debug mode for some reason
        $input.trigger('select2-focus');
        this.timeout(TIMEOUT_MS);
        expect($('.select2-container .select2-search-field label').text()).to.equal(
            'Overriden multi label'
        );
    });

    it('resets the aria-activedescendant value till the first user interaction inside single select dropdown', function () {
        $fixture.html(
            '<div>' +
                '<label for="test-select">Test label</label>' +
                '<input id="test-select" />' +
                '</div>'
        );
        const $input = $fixture.find('#test-select');
        $input.auiSelect2({
            data: data,
            searchLabel: 'Overriden label',
        });
        $input.auiSelect2('open');
        this.timeout(TIMEOUT_MS);
        const $searchInput = $('.select2-drop .select2-search input');
        expect($searchInput.attr('aria-activedescendant')).to.be.equal('');
    });

    it('runs custom ajax error handler on failure if provided', function () {
        $fixture.html('<div>' + '<input id="test-select-ajax" />' + '</div>');
        const $input = $fixture.find('#test-select-ajax');
        let errorCallback = sinon.spy();

        ajaxOptions.ajax.params = {
            error: errorCallback,
        };

        $input.auiSelect2(ajaxOptions);
        expect(errorCallback).to.not.have.been.called;

        $input.auiSelect2('open');
        clock.tick(TIMEOUT_MS);
        expect(errorCallback).to.have.been.calledOnce;
    });

    it('exposes its Layer instance in `data` after opening the dropdown for the first time', function () {
        $fixture.html(
            '<div>' +
                '<label for="test-select">Test label</label>' +
                '<input id="test-select" />' +
                '</div>'
        );
        const $input = $fixture.find('#test-select');
        $input.auiSelect2({
            data: data,
        });

        expect($input.data(DROPDOWN_LAYER_KEY)).to.be.undefined;
        $input.auiSelect2('open');
        clock.tick(TIMEOUT_MS);
        expect($input.data(DROPDOWN_LAYER_KEY)).not.to.be.undefined;
    });

    it('shows warning in a console if the layer was removed by other code', function () {
        $fixture.html(
            '<div>' +
                '<label for="test-select">Test label</label>' +
                '<input id="test-select" />' +
                '</div>'
        );
        const $input = $fixture.find('#test-select');
        const warningMessagePart =
            'Warning! AUI: `select2-close` event handler could not discover the layer linked with the Select2 dropdown.';
        $input.auiSelect2({
            data: data,
        });

        logger.reset();

        $input.auiSelect2('open');
        clock.tick(TIMEOUT_MS);
        expect($input.data(DROPDOWN_LAYER_KEY)).not.to.be.undefined;

        // intentionally (out of spite!) removing the layer data
        $input.data(DROPDOWN_LAYER_KEY, false);
        expect($input.data(DROPDOWN_LAYER_KEY)).to.be.false;

        // to see if the warning message was logged
        $input.auiSelect2('close');
        clock.tick(TIMEOUT_MS);
        expect(logger.callCount).to.be.greaterThan(0);
        expect(logger.args.some((argSet) => argSet.includes(warningMessagePart))).to.be.true;
    });
});
