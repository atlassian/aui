import $ from '@atlassian/aui/src/js/aui/jquery';
import skate from '@atlassian/aui/src/js/aui/internal/skate';
import '@atlassian/aui/src/js/aui/spin';
import SpinnerEl, { SIZE } from '@atlassian/aui/src/js/aui/spinner';
import { afterMutations } from '../../helpers/all';

describe('aui/spin', () => {
    let $testFixture;
    const SPINNER_SELECTOR = 'aui-spinner';
    const INTERNAL_SVG_SELECTOR = '.aui-spinner > svg';

    const getSVG = (el) => $(el).find(INTERNAL_SVG_SELECTOR).get(0);

    beforeEach(() => {
        $testFixture = $('#test-fixture');
        $testFixture.html(
            '<style>* { animation: none !important; -webkit-animation: none !important; }</style>'
        );
    });

    describe('web component', function () {
        it('creates an internal SVG to represent the spinner', () => {
            let spinner = document.createElement('aui-spinner');
            skate.init(spinner);
            let svg = getSVG(spinner);
            expect(svg).to.be.an.instanceof(Element);
        });

        it('has a default size of "medium"', () => {
            let spinner = document.createElement('aui-spinner');
            skate.init(spinner);
            let svg = getSVG(spinner);
            expect(svg.getAttribute('size')).to.equal(String(SIZE.MEDIUM.px));
        });

        it('can be constructed through its class', () => {
            let spinner = new SpinnerEl();
            expect(spinner).to.be.an.instanceof(Element);
            let svg = getSVG(spinner);
            expect(svg).to.be.an.instanceof(Element);
        });
    });

    describe('jQuery integration (deprecated) - ', () => {
        beforeEach(() => {
            $testFixture.append(`
                <span class="add-here"></span>
                <span class="not-here"></span>
                <span class="add-here"></span>
            `);
        });

        ['spin', 'spinStop'].forEach((method) => {
            it(`${method}() should be tolerant of nonsensical selections`, () => {
                let $els = $([null, undefined, {}]);
                $els[method]();
                expect($(SPINNER_SELECTOR).length).to.equal(0);
            });

            it(`${method}() should support jQuery function chaining`, () => {
                let $els = $testFixture.find('span');
                let result = $els[method]();
                expect(result).to.be.an.instanceof($);
            });
        });

        describe('spin() ', () => {
            it('should add a spinner to each selected element', () => {
                let $els = $testFixture.find('.add-here');
                $els.spin();
                expect($(SPINNER_SELECTOR).length).to.equal(2);
            });

            it('should not add spinners when no elements are selected', () => {
                let $els = $testFixture.find('.nonexistent');
                $els.spin();
                expect($(SPINNER_SELECTOR).length).to.equal(0);
            });

            it('should create a "small" sized spinner by default', (done) => {
                let $els = $testFixture.find('.add-here');
                $els.spin();
                afterMutations(() => {
                    let $svg = $(getSVG(SPINNER_SELECTOR));
                    expect($svg.height()).to.equal(SIZE.SMALL.px);
                    done();
                });
            });

            it('should accept a boolean to start and stop spinning', (done) => {
                let $els = $testFixture.find('.add-here');
                $els.spin(true);
                afterMutations(() => {
                    let $svg = $(getSVG(SPINNER_SELECTOR));
                    expect($svg.height()).to.equal(SIZE.SMALL.px);

                    $els.spin(false);
                    afterMutations(() => {
                        expect($(SPINNER_SELECTOR).length).to.equal(0);
                        done();
                    });
                });
            });

            it('should accept a valid size as a string', (done) => {
                let $els = $testFixture.find('.add-here');
                $els.spin('medium');

                afterMutations(() => {
                    let $svg = $(getSVG(SPINNER_SELECTOR));
                    expect($svg.height()).to.equal(SIZE.MEDIUM.px);
                    done();
                });
            });

            it('should accept a valid size in an object', (done) => {
                let $els = $testFixture.find('.add-here');
                $els.spin({ size: 'LaRGe' });

                afterMutations(() => {
                    let $svg = $(getSVG(SPINNER_SELECTOR));
                    expect($svg.height()).to.equal(SIZE.LARGE.px);
                    done();
                });
            });

            it('should prioritise the string-based size', (done) => {
                let $els = $testFixture.find('.add-here');
                $els.spin('medium', { size: 'large' });

                afterMutations(() => {
                    let $svg = $(getSVG(SPINNER_SELECTOR));
                    expect($svg.height()).to.equal(SIZE.MEDIUM.px);
                    done();
                });
            });

            // todo: does not work, because spinner relies on valid attribute value to set size...
            xit('should use the default size when given invalid sizes', (done) => {
                let $els = $testFixture.find('.add-here');
                $els.spin({ size: '"><script>alert("whoops")</script>' });

                afterMutations(() => {
                    let $svg = $(getSVG(SPINNER_SELECTOR));
                    expect($svg.height()).to.equal(SIZE.MEDIUM.px);
                    done();
                });
            });
        });

        describe('spinStop() ', () => {
            beforeEach(() => {
                $testFixture
                    .append('<span class="already-spinning"></span>')
                    .prepend('<span class="already-spinning"></span>');
                $testFixture.find('.already-spinning').spin();
                expect($(SPINNER_SELECTOR).length).to.equal(2);
            });

            it('should remove a spinner from each selected element', () => {
                let $els = $testFixture.find('.already-spinning');
                $els.spinStop();
                expect($(SPINNER_SELECTOR).length).to.equal(0);
            });
        });

        // This is effectively the case whenever a spinner was added by a means other than calling .spin()
        describe('$.data() returned null', () => {
            let dataFn;
            let $spinner;

            beforeEach(() => {
                $spinner = $('<span id="spinner"></span>').appendTo($testFixture);
                $spinner.spin();
                dataFn = $.fn.data;
                $.fn.data = () => null;
            });

            it('should not hide previous spinner', () => {
                $spinner.spinStop();
                expect($(SPINNER_SELECTOR).length).to.equal(1);
            });

            afterEach(() => {
                $.fn.data = dataFn;
            });
        });

        describe('"spinner" data entry', () => {
            let $spinner;

            beforeEach(() => {
                $spinner = $('<span id="spinner"></span>').appendTo($testFixture);
                $spinner.spin();
            });

            afterEach(() => {
                $spinner.remove();
            });

            it('should be set after spin() is called', () => {
                expect($spinner.data('spinner')).to.not.be.undefined;
            });

            it('should be removed after stop() is called', () => {
                $spinner.spinStop();
                expect($spinner.data('spinner')).to.be.undefined;
            });
        });
    });

    describe('filled position (deprecated) - ', () => {
        const SPINNER_SIZE = SIZE.SMALL;
        const CONTAINER_SIZE = { height: 100, width: 100 };
        const ZERO_CONTAINER_SIZE = { height: 0, width: 0 };
        const PADDING = 5;
        const MARGIN = 5;

        const filledSpinnerHtml = `<aui-spinner filled size="${SPINNER_SIZE.name}"></aui-spinner>`;

        const setupSpinner = ($html) => $html.appendTo($testFixture).find('aui-spinner');

        const absoluteContainer = ({ width, height }) =>
            setupSpinner(
                $(`<div style="width: ${width}px; height: ${height}px; display: inline-block; position: absolute; top: 0; left: 0">
                   ${filledSpinnerHtml}
               </div>`)
            );

        const staticContainer = ({ width, height }) =>
            setupSpinner(
                $(`<div style="width: ${width}px; height: ${height}px">
                   ${filledSpinnerHtml}
               </div>`)
            );

        const embeddedContainer = ({ width, height }, padding = 0, margin = 0) =>
            setupSpinner(
                $(`<div style="width: ${width}px; height: ${height}px; padding: ${padding}px;">
                   <div style="margin: ${margin}px;">
                       ${filledSpinnerHtml}
                   </div>
               </div>`)
            );

        function getExpectedSpinnerPosition(containerSize, padding = 0, margin = 0) {
            return {
                expectedLeft: (containerSize.width - SPINNER_SIZE.px) / 2 + (padding + margin),
                expectedTop: (containerSize.height - SPINNER_SIZE.px) / 2 + (padding + margin),
            };
        }

        it('should set correct spin position for simple div box', (done) => {
            const { expectedLeft, expectedTop } = getExpectedSpinnerPosition(CONTAINER_SIZE);
            const $spinner = absoluteContainer(CONTAINER_SIZE);

            afterMutations(() => {
                const { left, top } = getSVG($spinner).getBoundingClientRect();
                expect(left).to.equal(expectedLeft, 'the left position was incorrect');
                expect(top).to.equal(expectedTop, 'the top position was incorrect');
                done();
            });
        });

        it('should set correct spin position for div with zero size', (done) => {
            const { expectedLeft, expectedTop } = getExpectedSpinnerPosition(ZERO_CONTAINER_SIZE);
            const $spinner = staticContainer(ZERO_CONTAINER_SIZE);

            afterMutations(() => {
                const { left, top } = getSVG($spinner).getBoundingClientRect();
                expect(left).to.equal(expectedLeft, 'the left position was incorrect');
                expect(top).to.equal(expectedTop, 'the top position was incorrect');
                done();
            });
        });

        // simulating spinner behavior in dropdown
        it('should set correct spin position for div with padding inside div with margin', (done) => {
            const { expectedLeft, expectedTop } = getExpectedSpinnerPosition(
                ZERO_CONTAINER_SIZE,
                PADDING,
                MARGIN
            );
            const $spinner = embeddedContainer(ZERO_CONTAINER_SIZE, PADDING, MARGIN);

            afterMutations(() => {
                const { left, top } = getSVG($spinner).getBoundingClientRect();
                expect(left).to.equal(expectedLeft, 'the left position was incorrect');
                expect(top).to.equal(expectedTop, 'the top position was incorrect');
                done();
            });
        });

        // AUI-4819 dom thrashing
        it('should calculate the same position when only the spinner element changes', (done) => {
            const { expectedTop } = getExpectedSpinnerPosition(
                ZERO_CONTAINER_SIZE,
                PADDING,
                MARGIN
            );
            const $spinner = embeddedContainer(ZERO_CONTAINER_SIZE, PADDING, MARGIN);

            afterMutations(() => {
                // trigger a refresh without moving the element.
                $spinner.attr('size', 'SMALL');
                afterMutations(() => {
                    // note: we don't check 'left' because of a bug in our CSS:
                    // 'SMALL' is not matched because values in attribute selectors are case-sensitive.
                    // see https://googlechrome.github.io/samples/css-attribute-case-sensitivity/
                    const { top } = getSVG($spinner).getBoundingClientRect();
                    expect(top).to.equal(expectedTop, 'the top position was incorrect');
                    done();
                });
            });
        });
    });
});
