import RestfulTableEditRow from '@atlassian/aui/src/js/aui/restful-table/edit-row';
import EntryModel from '@atlassian/aui/src/js/aui/restful-table/entry-model';

describe('restuful table edit row', function () {
    describe('exiting edit mode', function () {
        let view;
        let options;

        beforeEach(() => {
            options = {
                model: EntryModel,
            };
        });

        it('should disable editing if not in create row mode', function () {
            options.isCreateRow = false;
            view = new RestfulTableEditRow(options);

            view.trigger(view._event.CANCEL);

            expect(view.disabled).to.equal(true);
        });

        it('should not change disable editing value when in create row mode', function () {
            options.isCreateRow = true;
            view = new RestfulTableEditRow(options);
            const disabledValueBefore = view.disabled;

            view.trigger(view._event.CANCEL);

            expect(view.disabled).to.equal(disabledValueBefore);
        });
    });
});
