import $ from '@atlassian/aui/src/js/aui/jquery';
import aui from '@atlassian/aui-soy/entry/aui-soy';
import dialog2Widget from '@atlassian/aui/src/js/aui/dialog2';

describe('aui/dialog2', function () {
    it('global', function () {
        expect(AJS.dialog2.toString()).to.equal(dialog2Widget.toString());
    });

    it('AMD module', function (done) {
        amdRequire(['aui/dialog2'], function (amdModule) {
            expect(amdModule).to.equal(AJS.dialog2);
            done();
        });
    });

    describe('soy tests -', function () {
        it('Dialog2 creates close button for non-modal', function () {
            var $el = $(
                aui.dialog.dialog2({
                    content: 'hello world',
                })
            );

            expect($el.find('.aui-close-button').get(0)).to.exist;
        });

        it('Dialog2 does not create close button for modal', function () {
            var $el = $(
                aui.dialog.dialog2({
                    content: 'hello world',
                    modal: true,
                })
            );

            expect($el.find('.aui-close-button').get(0)).to.not.exist;
        });
    });

    describe('unit tests -', function () {
        // Creates a mock of a layer object. AJS.layer will return this when passed the given $el
        function createLayerMock($el) {
            var layerInstance = {
                show: function () {},
                hide: function () {},
                remove: function () {},
                isVisible: function () {},
                on: function () {},
                above: function () {},
                below: function () {},
            };
            var mockedLayer = sinon.mock(layerInstance);
            $el.data('_aui-widget-layer', layerInstance);
            return mockedLayer;
        }

        function createContentEl() {
            return $(
                aui.dialog.dialog2({
                    content: 'Hello world',
                })
            ).appendTo('#test-fixture');
        }

        it('Dialog2 creates a dialog with given content', function () {
            var $el = createContentEl();

            var dialog = dialog2Widget($el);

            expect($el[0]).to.equal(dialog.$el[0]);
        });

        it('Dialog2 wraps layer for show, hide, remove', function () {
            expect(0);
            var $el = createContentEl();
            var dialog = dialog2Widget($el);
            var layerMock = createLayerMock(dialog.$el);
            layerMock.expects('show').once();
            layerMock.expects('hide').once();
            layerMock.expects('remove').once();

            dialog.show();
            dialog.hide();
            dialog.remove();

            layerMock.verify();
        });

        it('Dialog2 hide is called on close button click', function () {
            expect(0);
            var $el = createContentEl();

            var dialog = dialog2Widget($el);
            var layerMock = createLayerMock(dialog.$el);
            layerMock.expects('hide').once();
            dialog.show();

            $el.find('.aui-close-button').click();

            layerMock.verify();
        });
    });
});
