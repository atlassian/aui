import $ from '@atlassian/aui/src/js/aui/jquery';
import DatePicker, { CalendarWidget } from '@atlassian/aui/src/js/aui/date-picker';
import { afterMutations, click, fixtures, focus } from '../../helpers/all';

describe('aui/date-picker', function () {
    let $input;
    let datePicker;
    let originalBrowserSupport = DatePicker.prototype.browserSupportsDateField;

    function selectDate(date) {
        const $results = $('.ui-datepicker-calendar td').filter(function () {
            return String(this.textContent || '').trim() === String(date);
        });
        click($results.first());
    }

    beforeEach(function () {
        const field = fixtures({
            field: '<input class="aui-date-picker" id="test-input" type="date">',
        }).field;
        $input = $(field);
    });

    afterEach(function () {
        datePicker && datePicker.destroy();
        DatePicker.prototype.browserSupportsDateField = originalBrowserSupport;
    });

    it('globals', function () {
        expect(AJS.DatePicker.toString()).to.equal(DatePicker.toString());
    });

    it('API', function () {
        expect(DatePicker).to.be.a('function');
        expect(DatePicker.prototype.browserSupportsDateField).to.be.a('boolean');
        expect(DatePicker.prototype.defaultOptions).to.be.an('object');
        expect(DatePicker.prototype.defaultOptions.overrideBrowserDefault).to.be.a('boolean');
        expect(DatePicker.prototype.defaultOptions.firstDay).to.be.a('number');
        expect(DatePicker.prototype.defaultOptions.languageCode).to.be.a('string');
        expect(DatePicker.prototype.localisations).to.be.an('object');
    });

    describe('jQuery helper', function () {
        describe('construction', function () {
            afterEach(function () {
                expect(datePicker).to.be.an('object');
                expect(datePicker.getField).to.be.a('function');
                expect(datePicker.getOptions).to.be.a('function');
                expect(datePicker.reset).to.be.a('function');
            });

            it('instance API when type=date supported in browser', function () {
                DatePicker.prototype.browserSupportsDateField = true;
                datePicker = $input.datePicker();

                expect($input.attr('data-aui-dp-uuid')).to.not.exist;
                expect(datePicker.hide).to.be.undefined;
                expect(datePicker.show).to.be.undefined;
                expect(datePicker.getDate).to.be.undefined;
                expect(datePicker.setDate).to.be.undefined;
                expect(datePicker.destroyPolyfill).to.be.undefined;
            });

            it('instance API when type=date supported and developer overrides', function () {
                DatePicker.prototype.browserSupportsDateField = true;
                datePicker = $input.datePicker({ overrideBrowserDefault: true });

                expect($input.attr('data-aui-dp-uuid')).to.not.be.empty;
                expect(datePicker.reset).to.be.a('function');
                expect(datePicker.hide).to.be.a('function');
                expect(datePicker.show).to.be.a('function');
                expect(datePicker.destroyPolyfill).to.be.a('function');
            });

            it('instance API when type=date NOT supported in browser', function () {
                DatePicker.prototype.browserSupportsDateField = false;
                datePicker = $input.datePicker();

                expect($input.attr('data-aui-dp-uuid')).to.not.be.empty;
                expect(datePicker.hide).to.be.a('function');
                expect(datePicker.show).to.be.a('function');
                expect(datePicker.getDate).to.be.a('function');
                expect(datePicker.setDate).to.be.a('function');
                expect(datePicker.destroyPolyfill).to.be.a('function');
            });
        });

        it('remembers being constructed', function () {
            datePicker = $input.datePicker({ overrideBrowserDefault: true });
            const datePicker2 = $input.datePicker();

            expect(datePicker).to.equal(datePicker2);
        });

        it('updates constructed datepicker options', function () {
            datePicker = $input.datePicker({ overrideBrowserDefault: true, firstDay: 1 });
            $input.datePicker({ overrideBrowserDefault: true, firstDay: 5 });

            expect(datePicker.getOptions().firstDay).to.equal(5);
        });

        it('can be destroyed', function () {
            datePicker = $input.datePicker({ overrideBrowserDefault: true });
            datePicker.destroy();

            expect($input.is('[data-aui-dp-uuid]')).to.be.false;
        });

        it('can be destroyed via command that looks like jQuery-UI API', function () {
            datePicker = $input.datePicker({ overrideBrowserDefault: true });
            $input.datePicker('destroy');

            expect($input.is('[data-aui-dp-uuid]')).to.be.false;
        });

        it('can be re-created', function () {
            datePicker = $input.datePicker({ overrideBrowserDefault: true, firstDay: 1 });
            datePicker.destroy();
            datePicker = $input.datePicker({ overrideBrowserDefault: true, firstDay: 5 });

            expect($input.attr('data-aui-dp-uuid')).to.not.be.empty;
            expect(datePicker.getOptions().firstDay).to.equal(5);
        });
    });

    describe('CalandarWidget', function () {
        let $container;
        let $calendarWidget;
        beforeEach(function () {
            const container = fixtures({
                container: '<div id="test-container" type="date"></div>',
            }).container;
            $container = $(container);
        });

        afterEach(function () {
            if ($calendarWidget && $calendarWidget.destroy) {
                $calendarWidget.destroy();
            }
        });

        it('globals', function () {
            expect(typeof AJS.CalendarWidget).to.equal('function');
            expect(AJS.CalendarWidget.toString()).to.equal(CalendarWidget.toString());
        });

        describe('jQuery helper', function () {
            it('instance API', function () {
                $calendarWidget = $container.calendarWidget();
                expect($calendarWidget).to.be.an('object');
                expect($calendarWidget.reconfigure).to.be.a('function');
                expect($calendarWidget.destroy).to.be.a('function');
            });

            it('renders', function () {
                $calendarWidget = $container.calendarWidget();
                expect($container.html()).to.not.be.empty;
            });

            it('remembers being constructed', function () {
                $calendarWidget = $container.calendarWidget();
                const $calendarWidget2 = $container.calendarWidget();

                expect($calendarWidget).to.equal($calendarWidget2);
            });

            it('respects options', function () {
                $calendarWidget = $container.calendarWidget({ firstDay: 2 });
                expect($calendarWidget.find('th:first-child').text()).to.equal('Tu');
            });

            it('updates constructed calendarWidget options', function () {
                $calendarWidget = $container.calendarWidget({ firstDay: 2 });
                $container.calendarWidget({ firstDay: 5 });

                expect($calendarWidget.find('th:first-child').text()).to.equal('Fr');
            });

            it('respects minDate/maxDate options', function () {
                $calendarWidget = $container.calendarWidget({
                    minDate: '24 Jun 2000',
                    maxDate: '28 Jun 2000',
                    dateFormat: 'd M yy',
                    defaultDate: '26 Jun 2000',
                });

                const $disabledDays = $calendarWidget.find(
                    '.ui-datepicker-unselectable.ui-state-disabled .ui-state-default'
                );
                const disabledDayNumbers = [...$disabledDays].map((el) =>
                    parseInt(el.innerText, 10)
                );
                const expectedDayNumbers = [
                    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
                    23, 29, 30,
                ];
                expect($disabledDays.length).to.equal(25);
                expect(disabledDayNumbers).to.deep.equal(
                    expectedDayNumbers,
                    `Found: ${disabledDayNumbers.toString()} but the expected was: ${expectedDayNumbers.toString()}`
                );
            });

            it('can be destroyed', function () {
                $calendarWidget = $container.calendarWidget();
                $calendarWidget.destroy();
                expect($container.html()).to.be.empty;
            });
        });
    });

    describe('Behaviour', function () {
        beforeEach(function () {
            DatePicker.prototype.browserSupportsDateField = false;
        });

        it('change event fires (with polyfill)', function () {
            const inputEventSpy = sinon.spy();

            //We need to wrap the spy because sinon uses 'this' to access and record properties
            //when called directly as an eventHandler the value of 'this' is the element and
            //in chrome there are some properties on HTMLInputElement that throw exceptions
            //when accessed.
            function runSpy() {
                inputEventSpy();
            }

            $input.on('change', runSpy);
            datePicker = $input.datePicker();
            datePicker.show();
            selectDate('16');

            expect(inputEventSpy).to.have.been.calledOnce;
        });

        it('focus stays on input field after opening dialog', function (done) {
            $input.datePicker();

            focus($input);

            const dialog = $('aui-inline-dialog');

            afterMutations(function () {
                expect(dialog.get(0).open, 'dialog to be open').to.equal(true);
                expect(document.activeElement, 'input to be element in focus').to.equal(
                    $input.get(0)
                );
                done();
            });
        });

        describe('field attributes', function () {
            describe('min', function () {
                beforeEach(function () {
                    $input.val('2000-01-01');
                });

                it('is used on construction', function (done) {
                    $input.attr('min', '2999-01-20');
                    datePicker = $input.datePicker();
                    datePicker.show();
                    afterMutations(function () {
                        selectDate(1);
                        const date = datePicker.getDate();
                        expect(date.getFullYear()).to.equal(2999);
                        expect(date.getDate()).to.equal(20);
                        done();
                    });
                });

                it('is set in configuration', function (done) {
                    $input.removeAttr('min');
                    datePicker = $input.datePicker({
                        minDate: '2999-01-19',
                    });
                    datePicker.show();
                    afterMutations(function () {
                        selectDate(1);
                        const date = datePicker.getDate();
                        expect(date.getFullYear()).to.equal(2999);
                        expect(date.getDate()).to.equal(19);
                        done();
                    });
                });

                it('is set in both: attribute and config', function (done) {
                    $input.attr('min', '2999-01-20');
                    datePicker = $input.datePicker({
                        minDate: '2999-01-19',
                    });
                    datePicker.show();
                    afterMutations(function () {
                        selectDate(1);
                        const date = datePicker.getDate();
                        expect(date.getFullYear()).to.equal(2999);
                        expect(date.getDate()).to.equal(19);
                        done();
                    });
                });

                it('can be changed after construction', function (done) {
                    datePicker = $input.datePicker();
                    datePicker.show();
                    $input.attr('min', '2999-01-20');
                    afterMutations(() => {
                        selectDate(1);
                        const date = datePicker.getDate();
                        expect(date.getFullYear()).to.equal(2999);
                        expect(date.getDate()).to.equal(20);
                        done();
                    });
                });

                it('can be removed', function (done) {
                    $input.attr('min', '2999-01-20');
                    datePicker = $input.datePicker();
                    datePicker.show();
                    $input.removeAttr('min');
                    afterMutations(() => {
                        selectDate(1);
                        const date = datePicker.getDate();
                        expect(date.getFullYear()).to.equal(2999);
                        expect(date.getDate()).to.equal(1);
                        done();
                    });
                });
            });

            describe('max', function () {
                beforeEach(function () {
                    $input.val('2999-01-16');
                });

                it('is used on construction', function (done) {
                    $input.attr('max', '2000-01-07');
                    datePicker = $input.datePicker();
                    datePicker.show();
                    afterMutations(function () {
                        selectDate(31);
                        const date = datePicker.getDate();
                        expect(date.getFullYear()).to.equal(2000);
                        expect(date.getDate()).to.equal(7);
                        done();
                    });
                });

                it('is set in configuration', function (done) {
                    $input.removeAttr('max');
                    datePicker = $input.datePicker({
                        maxDate: '2000-01-06',
                    });
                    datePicker.show();
                    afterMutations(function () {
                        selectDate(8);
                        const date = datePicker.getDate();
                        expect(date.getFullYear()).to.equal(2000);
                        expect(date.getDate()).to.equal(6);
                        done();
                    });
                });

                it('is set in both: attribute and config', function (done) {
                    $input.attr('max', '2000-01-07');
                    datePicker = $input.datePicker({
                        maxDate: '2000-01-06',
                    });
                    datePicker.show();
                    afterMutations(function () {
                        selectDate(8);
                        const date = datePicker.getDate();
                        expect(date.getFullYear()).to.equal(2000);
                        expect(date.getDate()).to.equal(6);
                        done();
                    });
                });

                it('can be changed after construction', function (done) {
                    datePicker = $input.datePicker();
                    datePicker.show();
                    $input.attr('max', '2000-01-07');
                    afterMutations(() => {
                        selectDate(31);
                        const date = datePicker.getDate();
                        expect(date.getFullYear()).to.equal(2000);
                        expect(date.getDate()).to.equal(7);
                        done();
                    });
                });

                it('can be removed', function (done) {
                    $input.attr('max', '2000-01-07');
                    datePicker = $input.datePicker();
                    datePicker.show();
                    $input.removeAttr('max');
                    afterMutations(() => {
                        selectDate(31);
                        const date = datePicker.getDate();
                        expect(date.getFullYear()).to.equal(2000);
                        expect(date.getDate()).to.equal(31);
                        done();
                    });
                });
            });

            describe('placeholder', function () {
                beforeEach(function () {
                    $input.val('');
                });

                it('when missing the input has no placeholder', function (done) {
                    datePicker = $input.datePicker({});
                    afterMutations(function () {
                        expect($input.attr('placeholder')).to.equal(undefined);
                        done();
                    });
                });

                it('when set the input has placeholder', function (done) {
                    datePicker = $input.datePicker({
                        placeholder: 'eg. 2020-01-28',
                    });
                    afterMutations(function () {
                        expect($input.attr('placeholder')).to.equal('eg. 2020-01-28');
                        done();
                    });
                });
            });

            describe('value attribute', () => {
                beforeEach(() => {
                    // Browser should support date field in those tests.
                    DatePicker.prototype.browserSupportsDateField = originalBrowserSupport;
                });

                [
                    ['d M yy', '28 Jun 2021', { day: '28', month: '5', year: '2021' }],
                    ['yy-mm-dd', '2012-01-10', { day: '10', month: '0', year: '2012' }],
                    ['dd/MM/yy', '06/January/2021', { day: '6', month: '0', year: '2021' }],
                ].forEach(([dateFormat, attrValue, expectedDate]) => {
                    it(`should be set in the provided format "${attrValue}" when overrideBrowserDefault: true`, (done) => {
                        const field = fixtures({
                            field: `<input class="aui-date-picker" type="date" value="${attrValue}">`,
                        }).field;
                        $input = $(field);
                        datePicker = $input.datePicker({
                            overrideBrowserDefault: true,
                            dateFormat: dateFormat,
                        });
                        $input.click();

                        afterMutations(() => {
                            expect($input.val()).to.equal(attrValue);

                            const calendarElement = [
                                ...document.querySelectorAll('.ui-datepicker-current-day'),
                            ].slice(-1)[0];
                            expect(calendarElement.getAttribute('data-year')).to.equal(
                                expectedDate.year
                            );
                            expect(calendarElement.getAttribute('data-month')).to.equal(
                                expectedDate.month
                            );
                            expect(calendarElement.querySelector('a').innerText).to.equal(
                                expectedDate.day
                            );

                            done();
                        });
                    });

                    describe('retains custom input value which is set before creation', () => {
                        const val = '2024-03-04';

                        it('when the value is set in HTML', (done) => {
                            const field = {
                                field: `<input class="aui-date-picker" type="text" value="${val}">`,
                            }.field;
                            $input = $(field);
                            datePicker = $input.datePicker({
                                overrideBrowserDefault: true,
                            });
                            afterMutations(() => {
                                expect($input.val()).to.equal(val);
                                done();
                            });
                        });

                        it('when the value is put programmatically', (done) => {
                            const field = {
                                field: '<input class="aui-date-picker" type="text">',
                            }.field;
                            $input = $(field);
                            $input.val(val);
                            datePicker = $input.datePicker({
                                overrideBrowserDefault: true,
                            });
                            afterMutations(() => {
                                expect($input.val()).to.equal(val);
                                done();
                            });
                        });
                    });
                });
            });
        });
    });
});
