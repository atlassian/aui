import * as cookie from '@atlassian/aui/src/js/aui/cookie';

describe('aui/cookie', function () {
    it('globals', function () {
        Object.keys(cookie).forEach((key) => {
            expect(AJS.cookie[key].toString()).to.equal(cookie[key].toString());
            expect(AJS.Cookie[key].toString()).to.equal(cookie[key].toString());
        });
    });
});
