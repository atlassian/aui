import $ from '@atlassian/aui/src/js/aui/jquery';
import FocusManager from '@atlassian/aui/src/js/aui/focus-manager';
import { afterMutations, pressKey } from '../../helpers/all';
import keyCode from '@atlassian/aui/src/js/aui/key-code';

describe('aui/focus-manager', function () {
    var $el;
    var spy;
    var $container;
    var $el1;
    var $el2;
    var spy1;
    var spy2;

    function createSingleInput() {
        $el = $('<input type="text" />').appendTo('#test-fixture');
        spy = sinon.spy();
    }

    function createTwoInputs() {
        $container = $('<div></div>').appendTo('#test-fixture');
        $el1 = $('<input type="text" />').appendTo($container);
        $el2 = $('<input type="text" />').appendTo($container);
        spy1 = sinon.spy();
        spy2 = sinon.spy();
    }

    function createSingleInputAndFocus(done) {
        createSingleInput();
        afterMutations(function () {
            $el.on('focus', spy);
            done();
        }, 50);
    }

    function createTwoInputsAndFocus(done) {
        createTwoInputs();

        afterMutations(function () {
            $el1.on('focus', spy1);
            $el2.on('focus', spy2);
            done();
        }, 50);
    }

    function createSingleInputAndBlur() {
        createSingleInput();
        $el.on('blur', spy);
    }

    function createTwoInputsAndBlur() {
        createTwoInputs();
        $el1.on('blur', spy1);
        $el2.on('blur', spy2);
    }

    afterEach(() => {
        // Since the focus manager is a singleton we have to clean it manually.
        if ($container) {
            const focusManagerInstance = new FocusManager();
            focusManagerInstance.exit($container);
        }
    });

    it('globals', function () {
        expect(AJS.FocusManager.toString()).to.equal(FocusManager.toString());
    });

    it('enter() focuses on the first element only using the default selector', function (done) {
        createTwoInputsAndFocus(function () {
            new FocusManager().enter($container);

            afterMutations(function () {
                expect(spy1).to.have.been.calledOnce;
                expect(spy2).to.have.not.been.called;
                done();
            }, 50);
        });
    });

    it('enter() does not focus if data-aui-focus="false" is provided', function (done) {
        createTwoInputsAndFocus(function () {
            $container.attr('data-aui-focus', 'false');
            new FocusManager().enter($container);

            afterMutations(function () {
                expect(spy1).to.have.not.been.called;
                expect(spy2).to.have.not.been.called;
                done();
            }, 50);
        });
    });

    it('enter() does not focus if data-aui-focus="false" is provided and data-aui-focus-selector is present', function (done) {
        createTwoInputsAndFocus(function () {
            $el2.attr('id', 'sideshow-bob');
            $container.attr('data-aui-focus', 'false');
            $container.attr('data-aui-focus-selector', '#sideshow-bob');

            new FocusManager().enter($container);

            afterMutations(function () {
                expect(spy1).to.have.not.been.called;
                expect(spy2).to.have.not.been.called;
                done();
            }, 50);
        });
    });

    it('enter() focuses on the specified element using a custom selector', function (done) {
        createTwoInputsAndFocus(function () {
            $el2.attr('id', 'sideshow-bob');
            $container.attr('data-aui-focus-selector', '#sideshow-bob');

            new FocusManager().enter($container);

            afterMutations(function () {
                expect(spy1).to.have.not.been.called;
                expect(spy2).to.have.been.calledOnce;
                done();
            }, 50);
        });
    });

    it('enter() focuses on the first element only using a custom selector', function (done) {
        createTwoInputsAndFocus(function () {
            $el2.attr('id', 'sideshow-bob');
            $container.attr('data-aui-focus-selector', ':input');

            new FocusManager().enter($container);

            afterMutations(function () {
                expect(spy1).to.have.been.calledOnce;
                expect(spy2).to.have.not.been.called;
                done();
            }, 50);
        });
    });

    it('enter() selects passed element if it matches the focus selector', function (done) {
        createSingleInputAndFocus(function () {
            new FocusManager().enter($el);

            afterMutations(function () {
                expect(spy).to.have.been.calledOnce;
                done();
            }, 50);
        });
    });

    it('exit() blurs the active element', function (done) {
        createTwoInputsAndBlur();
        $el1.focus();

        new FocusManager().exit($container);

        afterMutations(function () {
            expect(spy1).to.have.been.calledOnce;
            done();
        }, 50);
    });

    it('exit() blurs the active element if the passed element is focussed', function (done) {
        createSingleInputAndBlur();
        $el.focus();

        new FocusManager().exit($el);
        afterMutations(function () {
            expect(spy).to.have.been.calledOnce;
            done();
        }, 50);
    });

    it('exit() does not trigger blur on an element that is not underneath it', function () {
        createTwoInputsAndBlur();
        var $el = $('<input type="text" />').appendTo('#test-fixture');
        $el.focus();

        new FocusManager().exit($container);

        expect(spy1).to.have.not.been.called;
        expect(spy2).to.have.not.been.called;
    });

    it('preserves focus after enter() then exit()', function () {
        createTwoInputsAndBlur();
        var $focusButton = $('<button id="focusButton">Focus button</button>');
        $('#test-fixture').append($focusButton);
        $focusButton.focus();

        new FocusManager().enter($container);
        expect($focusButton.is(document.activeElement)).to.be.false;
        new FocusManager().exit($container);

        expect($focusButton.is(document.activeElement)).to.be.true;
    });

    it('should not have handler & focusTrapStack if the container does not have aui-focus-trap', function () {
        createTwoInputsAndBlur();

        const focusManagerInstance = new FocusManager();
        focusManagerInstance.enter($container);

        expect(focusManagerInstance._handler).to.be.undefined;
        expect(focusManagerInstance._focusTrapStack.length).to.be.equal(0);
    });

    it('should have handler & focusTrapStack if the container has aui-focus-trap="true"', function () {
        createTwoInputsAndBlur();
        $container[0].setAttribute('aui-focus-trap', true);

        const focusManagerInstance = new FocusManager();
        focusManagerInstance.enter($container);

        expect(focusManagerInstance._handler).to.not.be.undefined;
        expect(focusManagerInstance._focusTrapStack.length).to.be.equal(1);
    });

    it('should not have handler & focusTrapStack after the FocusManager exit from container', function () {
        createTwoInputsAndBlur();
        $container[0].setAttribute('aui-focus-trap', true);

        const focusManagerInstance = new FocusManager();
        focusManagerInstance.enter($container);
        focusManagerInstance.exit($container);

        expect(focusManagerInstance._handler).to.be.undefined;
        expect(focusManagerInstance._focusTrapStack.length).to.be.equal(0);
    });

    describe(':aui-focusable', function () {
        it('matches an <input>', function () {
            var html = '<input>';
            var $el = $(html);
            $('#test-fixture').append($el);
            expect($el.is(':aui-focusable')).to.be.true;
        });

        it('matches an <iframe>', function () {
            var html = `
                <iframe
                    srcdoc='
                    <!DOCTYPE html>
                    <html>
                        <head></head><body>Hello <button>Gimme focus</button></body>
                    </html>'
                >
                </iframe>
            `;
            var $el = $(html);
            $('#test-fixture').append($el);
            expect($el.is(':aui-focusable')).to.be.true;
        });

        it('does not match an invisible <input>', function () {
            var html = '<input style="visibility: hidden">';
            var $el = $(html);
            $('#test-fixture').append($el);
            expect($el.is(':aui-focusable')).to.be.false;
        });

        it('matches a visible <input> nested inside an invisible <div>', function () {
            var html =
                '<div style="visibility: hidden"><input id="nested-visible-input" style="visibility: visible"></div>';
            $('#test-fixture').html(html);
            var $el = $('#nested-visible-input');
            expect($el.is(':aui-focusable')).to.be.true;
        });
    });

    describe('radio groups', () => {
        let $form;

        beforeEach(() => {
            const testFixtureId = '#test-fixture';
            $form = $('<form aui-focus-trap="true"></form>').appendTo(testFixtureId);
        });

        function initializeFocusManager() {
            const focusManagerInstance = new FocusManager();
            focusManagerInstance.enter($form);
        }

        function createFieldset() {
            const $fieldset = $('<fieldset></fieldset>').appendTo($form);
            return $fieldset;
        }

        function createRadioWidget(id, name, labelText, $fieldset) {
            const $radio = $(
                `<label><input id="${id}" type="radio" name="${name}" />${labelText}</label>`
            ).appendTo($fieldset);
            return $radio;
        }

        function getRadio($radioWidget) {
            return $radioWidget.find('input').get(0);
        }

        function assertActiveElement(expectedElement, message) {
            assert.equal(document.activeElement, expectedElement, message);
        }

        function createTextInput() {
            const $input = $('<input type="text"></input>').appendTo($form);
            return $input;
        }

        function pressTab() {
            pressKey(keyCode.TAB);
        }

        function pressShiftTab() {
            pressKey(keyCode.TAB, { shift: true });
        }

        function checkRadioOfWidget($radioWidget) {
            const radio = getRadio($radioWidget);
            $(radio).prop('checked', true);
        }

        it('from one of the only two radios in the same group', () => {
            const $fieldset = createFieldset();

            const $radioWidget1 = createRadioWidget('radio1', 'group1', 'radio 1', $fieldset);
            createRadioWidget('radio2', 'group1', 'radio 2', $fieldset);

            initializeFocusManager();

            pressTab();

            assertActiveElement(getRadio($radioWidget1), 'stay on the current radio');

            pressShiftTab();

            assertActiveElement(
                getRadio($radioWidget1),
                'after shift+tab, stay on the current radio'
            );
        });

        it('from one of the two radios in the same group when another focusable thing is present', () => {
            const $fieldset = createFieldset();

            createRadioWidget('radio1', 'group1', 'radio 1', $fieldset);
            const $radioWidget2 = createRadioWidget('radio2', 'group1', 'radio 2', $fieldset);
            const $input = createTextInput();

            initializeFocusManager();

            pressTab();

            assertActiveElement($input.get(0), 'skip over the second radio in the group');

            pressShiftTab();

            assertActiveElement(
                getRadio($radioWidget2),
                'after shift+tab, go the last radio of a group'
            );
        });

        it('from a focusable element to a radio group with no checked radio buttons', () => {
            const $input = createTextInput();
            const $fieldset = createFieldset();

            const $radioWidget1 = createRadioWidget('radio1', 'group1', 'radio 1', $fieldset);
            createRadioWidget('radio2', 'group1', 'radio 2', $fieldset);

            initializeFocusManager();

            pressTab();

            assertActiveElement(getRadio($radioWidget1), 'focus the first radio button');

            pressShiftTab();

            assertActiveElement($input.get(0), 'after shift+tab, focus the input');
        });

        it('from a focusable element to a radio group that has a checked radio button', () => {
            const $input = createTextInput();
            const $fieldset = createFieldset();

            createRadioWidget('radio1', 'group1', 'radio 1', $fieldset);
            const $radioWidget2 = createRadioWidget('radio2', 'group1', 'radio 2', $fieldset);
            checkRadioOfWidget($radioWidget2);

            initializeFocusManager();

            pressTab();

            assertActiveElement(getRadio($radioWidget2), 'focus the checked radio button');

            pressShiftTab();

            assertActiveElement($input.get(0), 'after shift+tab, focus the input');
        });
    });
});
