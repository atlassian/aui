import $ from '@atlassian/aui/src/js/aui/jquery';
import flag from '@atlassian/aui/src/js/aui/flag';
import getFocusManager from '@atlassian/aui/src/js/aui/focus-manager';
import { I18n } from '@atlassian/aui/src/js/aui/i18n';
import { FOCUSABLE_QUERY } from '@atlassian/aui/src/js/aui/internal/a11y/focusable-query';

describe('aui/flag', function () {
    const TEST_DEFAULTS = {
        type: 'info',
        title: 'Title',
        body: 'This message should float over the screen',
    };

    beforeEach(function () {
        $('#test-fixture').append('<div id="aui-flag-container"></div>');
    });

    afterEach(function () {
        Array.from($('.aui-flag')).forEach((item) => {
            getFocusManager.global.exit($(item));
        });

        $('#aui-flag-container').remove();
    });

    function setupFlag({ type, title, body } = TEST_DEFAULTS) {
        return flag({ type, title, body });
    }

    const getFlagInDom = () => $('.aui-flag');
    const closeFlagInDom = () => $('.aui-flag .aui-close-button').click();

    const getTitleText = (flagElement) => $(flagElement).find('p.title').text();
    const getBodyText = (flagElement) => $(flagElement).text();

    const isVisible = (element) => $(element).is('[open]:not([hidden])');

    const hasCloseIcon = (element) => !!$(element).find('.aui-close-button').length;

    it('global', function () {
        expect(AJS.flag.toString()).to.equal(flag.toString());
    });

    it('AMD module', function (done) {
        amdRequire(['aui/flag'], function (amdModule) {
            expect(amdModule).to.equal(AJS.flag);
            done();
        });
    });

    describe('Construction', function () {
        describe('via the JS API', function () {
            it('should return the raw DOM element', function () {
                expect(flag({})).to.be.an.instanceof(Element);
            });
        });
    });

    it('Floating message is present on the screen', function () {
        setupFlag();
        expect(getFlagInDom().length).to.equal(1);
        expect(isVisible(getFlagInDom())).to.equal(true);
    });

    it('Floating messages HTML contain the title and contents somewhere', function () {
        const flagProperties = {
            title: 'look at me!',
            body: 'I have a body!!!1',
        };
        const flag = setupFlag(flagProperties);
        const flagHtml = $(flag).html();
        expect(flagHtml).to.include(flagProperties.title);
        expect(flagHtml).to.include(flagProperties.body);
    });

    it('Messages appear with mostly default options', function () {
        setupFlag({ body: 'This is a message with nearly all options default' });
        expect(getFlagInDom().length).to.equal(1);
    });

    it('Closing message triggers a close event on the message', function () {
        const flagElement = flag({ body: 'Close me' });
        const flagSpy = sinon.spy();
        $(flagElement).on('aui-flag-close', flagSpy);

        closeFlagInDom();
        expect(flagSpy.callCount).to.equal(1);
    });

    it('Flags can be closed via the API method', function () {
        const flag = setupFlag();
        expect(flag.hasAttribute('open')).to.equal(true);
        flag.close();
        expect(flag.hasAttribute('open')).to.equal(false);
    });

    it('Flags can be closed via the DOM', function () {
        setupFlag();
        expect(getFlagInDom()[0].hasAttribute('open')).to.equal(true);
        closeFlagInDom();
        expect(getFlagInDom()[0].hasAttribute('open')).to.equal(false);
    });

    it('Removes hidden flags when new one is added', () => {
        setupFlag();
        closeFlagInDom();
        setupFlag();
        expect(getFlagInDom().length).to.equal(1);
    });

    describe('When a flag is shown', function () {
        var clock;
        var flagElement;

        beforeEach(function () {
            clock = sinon.useFakeTimers();
        });

        afterEach(function () {
            clock.restore();
        });

        generateSelectorsMocks().forEach(([selector, htmlElement]) => {
            it(`should focus on the ${selector} interactive element`, () => {
                const flagElement = flag({ body: `${htmlElement} <button>Test</button>` });

                clock.tick(10000);

                expect(document.activeElement).to.equal(flagElement.querySelector(selector));
            });
        });

        describe('flag with focusable content', () => {
            beforeEach(() => {
                flagElement = flag({ body: ' <button>Test</button>' });
            });

            it('should focus on the latest flag', () => {
                flag({ body: '<input type="text" id="focus-1" /> <button>Test</button>' });
                flag({ body: '<input type="text" id="focus-2"/> <button>Test</button>' });
                flagElement = flag({ body: '<button id="focus-3">Test</button>' });
                clock.tick(10000);

                expect(document.activeElement).to.equal(flagElement.querySelector('#focus-3'));
            });

            it('should call focus manager enter', () => {
                const enterSpy = sinon.spy(getFocusManager.global, 'enter');
                clock.tick(10000);

                expect(enterSpy).to.have.been.calledOnce;
                enterSpy.restore();
            });

            it('should call focus manager exit on close', () => {
                const exitSpy = sinon.spy(getFocusManager.global, 'exit');
                clock.tick(10000);

                closeFlagInDom();

                clock.tick(10000);

                expect(exitSpy).to.have.been.calledOnce;
                exitSpy.restore();
            });
        });

        describe('aria-hidden', () => {
            beforeEach(() => {
                flagElement = flag({ title: null });
                clock.tick(1);
            });

            it('should set aria-hidden to false', () => {
                expect(flagElement.getAttribute('aria-hidden')).to.equal('false');
            });

            it('should set aria-hidden to true when closed', () => {
                flagElement.close();
                clock.tick(1);

                expect(flagElement.getAttribute('aria-hidden')).to.equal('true');
            });
        });

        describe('setting a flag role', () => {
            it('should set role to alert if there are no interactive elements', () => {
                flagElement = flag({ title: null });
                clock.tick(1);

                expect(flagElement.getAttribute('role')).to.equal('alert');
            });

            it('should set role to alertdialog if there are interactive elements', () => {
                flagElement = flag({ title: null, body: '<button>Close dialog</button>' });
                clock.tick(101);

                expect(flagElement.getAttribute('role')).to.equal('alertdialog');
            });
        });

        describe('with the title option', function () {
            it('set to null, it has no title', function () {
                flagElement = flag({ title: null });
                expect(getTitleText(flagElement)).to.equal('');
            });

            it('set to false, it has no title', function () {
                flagElement = flag({ title: false });
                expect(getTitleText(flagElement)).to.equal('');
            });

            it('unset, it has no title', function () {
                flagElement = flag({});
                expect(getTitleText(flagElement)).to.equal('');
            });

            it('is HTML-safe', function () {
                flagElement = flag({ title: '<b class="oh-whoops">groovy!</b>' });
                expect($(flagElement).find('.oh-whoops').length).to.equal(0);
            });
        });

        describe('with the body option', function () {
            it('set to null, it has no body', function () {
                flagElement = flag({ body: null });
                expect(getBodyText(flagElement)).to.equal('');
            });

            it('set to false, it has no body', function () {
                flagElement = flag({ body: false });
                expect(getBodyText(flagElement)).to.equal('');
            });

            it('unset, it has no body', function () {
                flagElement = flag({});
                expect(getBodyText(flagElement)).to.equal('');
            });

            it('outputs HTML', function () {
                flagElement = flag({ body: '<b class="oh-yay">groovy!</b>' });
                expect($(flagElement).find('.oh-yay').length).to.equal(1);
            });

            it('preserves whitespace in body content', function () {
                flagElement = flag({ body: "<p> one flew over \n the <b> cuckoo's nest</b> </p>" });
                expect(getBodyText(flagElement)).to.equal(" one flew over \n the  cuckoo's nest ");
            });
        });

        describe('with the close option', function () {
            describe('set to "manual"', function () {
                beforeEach(function () {
                    flagElement = flag({ close: 'manual' });
                });

                it('the flag should not close after five seconds', function () {
                    clock.tick(10000);
                    expect(isVisible(flagElement)).to.equal(true);
                });

                it('the flag should have a close icon', function () {
                    expect(hasCloseIcon(flagElement)).to.equal(true);
                });
            });

            describe('set to "auto"', function () {
                beforeEach(function () {
                    flagElement = flag({ close: 'auto' });
                });

                it('the flag should close after five seconds', function () {
                    clock.tick(10000);
                    expect(isVisible(flagElement)).to.equal(false);
                });

                it('should not close the flag if it has interactive elements', function () {
                    const flagElementWithButtons = flag({
                        close: 'auto',
                        body: '<button>Click</button>',
                    });
                    clock.tick(10000);
                    expect(isVisible(flagElementWithButtons)).to.equal(true);
                });

                it('the flag should have a close icon', function () {
                    expect(hasCloseIcon(flagElement)).to.equal(true);
                });

                it('should set inert attr on close', function () {
                    expect($(flagElement).is('[inert]')).to.equal(false);

                    clock.tick(10000);

                    expect($(flagElement).is('[inert]')).to.equal(true);
                });

                it('should set display:none attr on close', function () {
                    expect($(flagElement).css('display')).to.equal('block');

                    clock.tick(10000);

                    expect($(flagElement).css('display')).to.equal('none');
                });
            });

            describe('set to "never"', function () {
                beforeEach(function () {
                    flagElement = flag({ close: 'never' });
                });

                it('the flag should not close after five seconds', function () {
                    clock.tick(10000);
                    expect(isVisible(flagElement)).to.equal(true);
                });

                it('the flag should not have a close icon', function () {
                    expect(hasCloseIcon(flagElement)).to.equal(false);
                });
            });

            describe('left as default', function () {
                beforeEach(function () {
                    flagElement = flag({});
                });

                it('the flag should not close after five seconds', function () {
                    clock.tick(10000);
                    expect(isVisible(flagElement)).to.equal(true);
                });

                it('the flag should have a close icon', function () {
                    expect(hasCloseIcon(flagElement)).to.equal(true);
                });
            });
        });
    });

    describe('security', function () {
        let alerter;
        beforeEach(function () {
            alerter = sinon.stub(window, 'alert');
        });
        afterEach(function () {
            alerter.restore();
        });

        it('does not execute scripts', function () {
            flag({ body: 'this script should not execute <script>alert("whoops")</script>' });
            expect(alerter.callCount).to.equal(0);
        });
    });

    describe('aria-label', function () {
        it('aria-label can be set with ariaLabel attribute', function () {
            const ariaLabel = 'test aria label';
            const parameters = {
                ...TEST_DEFAULTS,
                ariaLabel,
            };
            const flagElement = flag(parameters);
            const actualAriaLabel = $(flagElement).attr('aria-label');
            expect(actualAriaLabel).to.equal(ariaLabel);
        });

        describe('default aria-label depends on the type', function () {
            const types = ['info', 'error', 'warning', 'success'];

            types.forEach((type) => {
                it(`should have correct aria-label for type: ${type}`, function () {
                    const parameters = {
                        ...TEST_DEFAULTS,
                        type,
                    };
                    const flagElement = flag(parameters);
                    const actualAriaLabel = $(flagElement).attr('aria-label');
                    expect(actualAriaLabel).to.equal(
                        I18n.getText(`aui.flag.default-aria-label.${type}`)
                    );
                });
            });
        });
    });

    describe('aria-description', function () {
        it('aria-description can be set with ariaDescription attribute', function () {
            const ariaDescription = 'test aria description';
            const parameters = {
                ...TEST_DEFAULTS,
                ariaDescription,
            };
            const flagElement = flag(parameters);
            const actualAriaDescription = $(flagElement).attr('aria-description');
            expect(actualAriaDescription).to.equal(ariaDescription);
        });

        it('shound not render aria-description if it is an empty string', function () {
            const flagElement = flag(TEST_DEFAULTS);
            const actualAriaDescription = $(flagElement).attr('aria-description');
            expect(actualAriaDescription).to.equal(undefined);
        });
    });

    describe('data-aui-focus-*', () => {
        var clock;

        beforeEach(function () {
            clock = sinon.useFakeTimers();
        });

        afterEach(function () {
            clock.restore();
        });

        describe('with focusable content', () => {
            let flagElement;

            beforeEach(() => {
                flagElement = flag({
                    body: '<a href="#">awesome</a><input type="text" /> <button>Test</button>',
                });
            });

            it('should have data-aui-focus', () => {
                clock.tick(10000);
                expect(flagElement.getAttribute('data-aui-focus')).to.equal('true');
            });

            it('should have data-aui-focus-selector', () => {
                clock.tick(10000);
                expect(flagElement.getAttribute('data-aui-focus-selector')).to.equal(
                    FOCUSABLE_QUERY.split(', ')
                        .map((item) => `.aui-message ${item}`)
                        .join(', ')
                );
            });
        });

        describe('without focusable content', () => {
            let flagElement;
            beforeEach(() => {
                flagElement = flag({ body: 'Ignore me' });
            });

            it('should have data-aui-focus', () => {
                clock.tick(10000);
                expect(flagElement.getAttribute('data-aui-focus')).to.equal(null);
            });

            it('should have data-aui-focus-selector', () => {
                clock.tick(10000);
                expect(flagElement.getAttribute('data-aui-focus-selector')).to.equal(null);
            });
        });
    });
});

function generateSelectorsMocks() {
    const selectors = FOCUSABLE_QUERY.split(',').map((selector) => selector.trim());

    const transformedArray = selectors
        .filter((selector) => selector !== 'object')
        .map((selector) => {
            let htmlElement;
            if (selector.startsWith('a[')) {
                htmlElement = '<a href="#"></a>';
            } else if (selector.startsWith('area[')) {
                htmlElement = `<map name="infographic">
                <area
                    shape="poly"
                    coords="129,0,260,95,129,138"
                    href="https://developer.mozilla.org/docs/Web/HTTP"
                    target="_blank"
                    alt="HTTP" />
                    </map><img usemap="#infographic" src="/media/examples/mdn-info.png" alt="MDN infographic" />`;
            } else if (selector.startsWith('input')) {
                htmlElement = '<input type="text"/>';
            } else if (selector === 'object') {
                htmlElement =
                    '<object type="application/pdf" data="/media/examples/In-CC0.pdf" width="250" height="200"></object>';
            } else if (selector === 'embed') {
                htmlElement =
                    '<embed type="video/webm" src="/media/cc0-videos/flower.mp4" width="250" height="200" />';
            } else if (selector.startsWith('[tabindex')) {
                htmlElement = `<div tabindex="${selector.match(/\d+/)[0]}"></div>`;
            } else if (selector === '[contenteditable]') {
                htmlElement = '<div contenteditable="true"></div>';
            } else if (selector.startsWith('audio')) {
                htmlElement = '<audio controls></audio>';
            } else if (selector.startsWith('video')) {
                htmlElement = '<video controls></video>';
            } else if (selector === 'summary') {
                htmlElement = '<details><summary>Object</summary></details>';
            } else {
                const tag = selector.match(/^\w+/)[0];
                htmlElement = `<${tag}></${tag}>`;
            }

            return [selector, htmlElement];
        });

    return transformedArray;
}
