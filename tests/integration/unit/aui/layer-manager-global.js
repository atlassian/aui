import $ from '@atlassian/aui/src/js/aui/jquery';
import layer from '@atlassian/aui/src/js/aui/layer';
import LayerManager from '@atlassian/aui/src/js/aui/layer-manager';
import keyCode from '@atlassian/aui/src/js/aui/key-code';
import { click, pressKey } from '../../helpers/all';

const pressEsc = pressKey.bind(undefined, keyCode.ESCAPE);

export function createLayer(blanketed, modal, persistent) {
    const $el = $('<div hidden class="aui-layer"></div>').appendTo('#test-fixture');

    if (blanketed) {
        $el.attr('data-aui-blanketed', true);
    }

    if (modal) {
        $el.attr('data-aui-modal', true);
    }

    if (persistent) {
        $el.attr('persistent', '');
    }

    return $el;
}

describe('aui/layer-manager-global', function () {
    var dimSpy;
    var undimSpy;
    var layerManagerPopTopSpy;
    var layerManagerPopUntilTopBlanketedSpy;

    function createBlanketedLayer() {
        return createLayer().attr('data-aui-blanketed', 'true');
    }

    beforeEach(function () {
        LayerManager.global = new LayerManager();
        dimSpy = sinon.spy(AJS, 'dim');
        undimSpy = sinon.spy(AJS, 'undim');
        layerManagerPopTopSpy = sinon.stub(LayerManager.global, 'popTopIfNonPersistent');
        layerManagerPopUntilTopBlanketedSpy = sinon.stub(
            LayerManager.global,
            'popUntilTopBlanketed'
        );
    });

    afterEach(function () {
        layerManagerPopTopSpy.restore();
        layerManagerPopUntilTopBlanketedSpy.restore();
        dimSpy.restore();
        undimSpy.restore();
    });

    it('Pressing ESC hides the layer', function () {
        var $el = createLayer();

        LayerManager.global.push($el);
        pressEsc();
        expect(layer($el).isVisible()).to.be.false;
    });

    it('Pressing ESC hides the top layer only', function () {
        var $layer1 = createLayer(true);
        var $layer2 = createLayer();

        LayerManager.global.push($layer1);
        LayerManager.global.push($layer2);

        pressEsc();

        expect(layer($layer1).isVisible()).to.be.true;
        expect(layer($layer2).isVisible()).to.be.false;
    });

    it('Clicking blanket calls popUntilTopBlanketed()', function () {
        var $layer1 = createLayer();
        var $layer2 = createLayer(true);

        LayerManager.global.push($layer1);
        LayerManager.global.push($layer2);

        click('.aui-blanket');

        expect(layerManagerPopUntilTopBlanketedSpy).to.have.been.calledOnce;
    });

    it('Pressing ESC hides the non modal/persistent layers', function () {
        var $modalLayer = createLayer(false, true);
        var $persistentLayer = createLayer(false, false, true);
        var $layer1 = createLayer();

        LayerManager.global.push($modalLayer);
        LayerManager.global.push($persistentLayer);
        LayerManager.global.push($layer1);

        pressEsc();
        pressEsc();

        expect(layer($modalLayer).isVisible()).to.be.true;
        expect(layer($persistentLayer).isVisible()).to.be.true;
        expect(layer($layer1).isVisible()).to.be.false;
    });

    it('Pressing ESC hides the first non modal/persistent layer, but not under the modal blanket', function () {
        var $layer1 = createLayer(true);
        var $layer2 = createLayer(true, true);
        var $persistentLayer = createLayer(false, false, true);
        var $layer3 = createLayer();

        LayerManager.global.push($layer1);
        LayerManager.global.push($layer2);
        LayerManager.global.push($persistentLayer);
        LayerManager.global.push($layer3);

        pressEsc();
        pressEsc();

        expect(layer($layer1).isVisible()).to.be.true;
        expect(layer($layer2).isVisible()).to.be.true;
        expect(layer($layer3).isVisible()).to.be.false;
        expect(layer($persistentLayer).isVisible()).to.be.true;
    });

    it('Pressing ESC hides the first non modal/persistent layers and any layers ontop as a result', function () {
        var $layer1 = createLayer(true);
        var $layer2 = createLayer(true, true);
        var $layer3 = createLayer(true);
        var $modalLayer = createLayer(false, true);
        var $persistentLayer = createLayer(false, false, true);

        LayerManager.global.push($layer1);
        LayerManager.global.push($layer2);
        LayerManager.global.push($layer3);
        LayerManager.global.push($modalLayer);
        LayerManager.global.push($persistentLayer);

        pressEsc();
        pressEsc();

        expect(layer($layer1).isVisible()).to.be.true;
        expect(layer($layer2).isVisible()).to.be.true;
        expect(layer($layer3).isVisible()).to.be.false;
        expect(layer($modalLayer).isVisible()).to.be.false;
        expect(layer($persistentLayer).isVisible()).to.be.false;
    });

    it('Pressing ESC stops at the first blanket, with top modal', function () {
        var $layer1 = createLayer(true, false);
        var $layer2 = createLayer(true, true);
        var $layer3 = createLayer(false, true);

        LayerManager.global.push($layer1);
        LayerManager.global.push($layer2);
        LayerManager.global.push($layer3);

        pressEsc();

        expect(layer($layer1).isVisible()).to.be.true;
        expect(layer($layer2).isVisible()).to.be.true;
        expect(layer($layer3).isVisible()).to.be.true;
    });

    it('Clicking blanket should hide the layer', function () {
        var $el = createBlanketedLayer();

        LayerManager.global.push($el);
        click('.aui-blanket');
        expect(layerManagerPopUntilTopBlanketedSpy).to.have.been.calledOnce;
    });

    it('Clicking anywhere outside of the top layer should close it', function () {
        var $layer = createLayer();

        LayerManager.global.push($layer);
        click(document);
        expect(layer($layer).isVisible()).to.be.false;
    });

    it('Clicking outside all layers should close all layers', function () {
        var $layer1 = createLayer();
        var $layer2 = createLayer();

        LayerManager.global.push($layer1);
        LayerManager.global.push($layer2);

        click(document);

        expect(layer($layer1).isVisible()).to.be.false;
        expect(layer($layer2).isVisible()).to.be.false;
    });

    it('Clicking outside all layers should close all non modal/persistent layers', function () {
        var $layer1 = createLayer();
        var $modalLayer = createLayer(false, true);
        var $layer2 = createLayer();
        var $persistentLayer = createLayer(false, false, true);

        LayerManager.global.push($layer1);
        LayerManager.global.push($modalLayer);
        LayerManager.global.push($layer2);
        LayerManager.global.push($persistentLayer);

        click(document);

        expect(layer($layer1).isVisible()).to.be.false;
        expect(layer($modalLayer).isVisible()).to.be.true;
        expect(layer($layer2).isVisible()).to.be.false;
        expect(layer($persistentLayer).isVisible()).to.be.true;
    });

    describe('aui-close-layers-on-outer-click event', function () {
        afterEach(function () {
            $(document).off('aui-close-layers-on-outer-click.test');
        });

        it('triggers aui-close-layers-on-outer-click event before closing all layers', function () {
            const eventHandler = sinon.spy();

            $(document).on('aui-close-layers-on-outer-click.test', eventHandler);
            click(document);

            expect(eventHandler.calledOnce).to.equal(true);
        });

        it('aui-close-layers-on-outer-click event can prevent closing all layers', function () {
            const $layer = createLayer();
            const eventHandler = sinon.spy(function (e) {
                e.preventDefault();
            });
            $(document).on('aui-close-layers-on-outer-click.test', eventHandler);

            LayerManager.global.push($layer);

            click(document);

            expect(layer($layer).isVisible()).to.be.true;
        });
    });

    it('Clicking a layer should close all layers above it if not modal/persistent', function () {
        var $modalLayer1 = createLayer(false, true);
        var $layer2 = createLayer(true);
        var $modalLayer2 = createLayer(false, true);
        var $layer3 = createLayer();

        LayerManager.global.push($modalLayer1);
        LayerManager.global.push($layer2);
        LayerManager.global.push($modalLayer2);
        LayerManager.global.push($layer3);

        click($layer2[0]);

        expect(layer($modalLayer2).isVisible()).to.be.true;
        expect(layer($layer2).isVisible()).to.be.true;
        expect(layer($modalLayer2).isVisible()).to.be.true;
        expect(layer($layer3).isVisible()).to.be.false;
    });

    describe('Calling popUntil()', () => {
        let beforeHideSpy;
        let hideSpy;
        let $layer1;
        let $layer2;

        beforeEach(() => {
            beforeHideSpy = sinon.spy();
            hideSpy = sinon.spy();
            $layer1 = createLayer(true);
            $layer2 = createLayer();

            LayerManager.global.push($layer1);
            LayerManager.global.push($layer2);

            layer($layer2).on('beforeHide', beforeHideSpy);

            $layer2.get(0).addEventListener('aui-layer-hide', hideSpy);
        });

        it('should trigger "beforeHide" and "hide" events on each layer when "triggerBeforeEvents" is enabled', () => {
            // triggerBeforeEvents - second parameter
            LayerManager.global.popUntil($layer1, true);

            expect(beforeHideSpy).to.have.been.calledOnce;
            expect(hideSpy).to.have.been.calledOnce;
            expect(beforeHideSpy).to.have.been.calledBefore(hideSpy);
            expect(layer($layer1).isVisible()).to.be.false;
            expect(layer($layer2).isVisible()).to.be.false;
        });

        it('should not trigger "beforeHide" and "hide" events on each layer by default', () => {
            LayerManager.global.popUntil($layer1);

            expect(beforeHideSpy).to.have.not.been.called;
            expect(hideSpy).to.have.not.been.called;
            expect(layer($layer1).isVisible()).to.be.false;
            expect(layer($layer2).isVisible()).to.be.false;
        });
    });
});
