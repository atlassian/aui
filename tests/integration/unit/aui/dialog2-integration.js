import $ from '@atlassian/aui/src/js/aui/jquery';
import aui from '@atlassian/aui-soy/entry/aui-soy';
import keyCode from '@atlassian/aui/src/js/aui/key-code';
import dialog2Widget from '@atlassian/aui/src/js/aui/dialog2';
import layerWidget from '@atlassian/aui/src/js/aui/layer';
import { afterMutations, pressKey } from '../../helpers/all';

describe('aui/dialog2-integration', function () {
    function onEvent(dialog, event, fn) {
        dialog.addEventListener(`aui-layer-${event}`, fn);
    }

    function offEvent(dialog, event, fn) {
        dialog.removeEventListener(`aui-layer-${event}`, fn);
    }

    function createDialog(attrs) {
        const $el = createContentEl(attrs);
        return dialog2Widget($el);
    }

    function createContentEl(attrs) {
        return $(
            aui.dialog.dialog2(
                Object.assign(
                    {
                        content: 'Hello world',
                    },
                    attrs
                )
            )
        ).appendTo('#test-fixture');
    }

    function createInputContentEl() {
        return createContentEl({
            headerActionContent: '<button id="my-button">Button</button>',
            content: '<input type="text" id="my-input" />',
            footerActionContent: '<button id="footer-button">Footer button</button>',
        });
    }

    function clickBlanket() {
        // We don't want to include blanket.js - which creates the blanket - in our dependencies,
        // so create a mock blanket element
        var $blanket = $('<div></div>').addClass('aui-blanket').appendTo('#test-fixture');
        $blanket.click();
    }

    it('focuses element with `autofocus` attribute after opening', function (done) {
        const $el = $(
            aui.dialog.dialog2({
                headerActionContent:
                    '<button id="my-button1">Button 1</button><button id="my-button2" autofocus>Button 2</button>',
                content: 'Some content (nothing you can focus)',
            })
        ).appendTo('#test-fixture');
        const button1FocusSpy = sinon.spy();
        const button2FocusSpy = sinon.spy();
        $el.find('#my-button1').focus(button1FocusSpy);
        $el.find('#my-button2').focus(button2FocusSpy);

        const dialog = dialog2Widget($el);

        dialog.on('show', function () {
            afterMutations(function () {
                expect(button1FocusSpy).not.to.have.been.called;
                expect(button2FocusSpy).to.have.been.calledOnce;
                done();
            }, 50);
        });

        dialog.show();
    });

    it('sets proper tabindex upon opening', function (done) {
        var $el = $(
            aui.dialog.dialog2({
                content: 'Some content (nothing you can focus)',
            })
        ).appendTo('#test-fixture');

        var dialog = dialog2Widget($el);

        dialog.on('show', function () {
            afterMutations(function () {
                expect($el.attr('tabindex')).to.equal('-1');
                done();
            }, 50);
        });

        dialog.show();
    });

    it('creates a blanket', function () {
        var $el = createInputContentEl();

        var dialog = dialog2Widget($el);
        dialog.show();

        expect(dialog.$el).to.be.visible;
    });

    it('will not be removed if data-aui-remove-on-hide is not true', function () {
        var $el = createContentEl();
        var dialog = dialog2Widget($el);

        dialog.show();
        dialog.hide();

        expect($el.parent().get(0)).to.exist;
    });

    it('will be removed if data-aui-remove-on-hide is true', function () {
        var $el = createContentEl();
        $el.attr('data-aui-remove-on-hide', 'true');
        var dialog = dialog2Widget($el);

        dialog.show();
        dialog.hide();

        expect($el.parent().get(0)).to.not.exist;
    });

    it('delegates events with "on"', function () {
        var dialog = createDialog();

        var hideSpy = sinon.spy();

        dialog.on('hide', hideSpy);
        dialog.show();
        dialog.hide();

        expect(hideSpy).to.have.been.calledOnce;
    });

    it('responds to native show event', function () {
        var $el = createContentEl();
        var dialog = dialog2Widget($el);

        var nativeShowSpy = sinon.spy();

        onEvent($el[0], 'show', nativeShowSpy);
        dialog.show();
        offEvent($el[0], 'show', nativeShowSpy);

        expect(nativeShowSpy).to.have.been.calledOnce;
    });

    it('responds to native hide event', function () {
        var $el = createContentEl();
        var dialog = dialog2Widget($el);

        var nativeHideSpy = sinon.spy();

        onEvent($el[0], 'hide', nativeHideSpy);
        dialog.show();
        dialog.hide();
        offEvent($el[0], 'hide', nativeHideSpy);

        expect(nativeHideSpy).to.have.been.calledOnce;
    });

    it('instance events can be turned off', function () {
        var dialog = createDialog();

        var instanceEventSpy = sinon.spy();

        dialog.on('hide', instanceEventSpy);
        dialog.off('hide', instanceEventSpy);
        dialog.show();
        dialog.hide();

        expect(instanceEventSpy).to.not.have.been.called;
    });

    it('instance events can be turned off native', function () {
        var $el = createContentEl();
        var dialog = dialog2Widget($el);

        var nativeEventSpy = sinon.spy();

        onEvent($el[0], 'hide', nativeEventSpy);
        offEvent($el[0], 'hide', nativeEventSpy);
        dialog.show();
        dialog.hide();

        expect(nativeEventSpy).to.not.have.been.called;
    });

    it('instance events registered twice are only fired once', function () {
        var dialog = createDialog();

        var spy = sinon.spy();
        dialog.on('show', spy);
        dialog.on('show', spy);
        dialog.show();

        expect(spy.callCount).to.equal(1);
    });

    it('instance events do not conflict with each-other', function () {
        var dialog = createDialog();

        var spyA = sinon.spy();
        var spyB = sinon.spy();

        dialog.on('show', spyA);
        dialog.on('show', spyB);
        dialog.show();

        expect(spyA.callCount).to.equal(1);
        expect(spyB.callCount).to.equal(1);
    });

    it('instance events only unregister themselves', function () {
        var dialog = createDialog();

        var spyA = sinon.spy();
        var spyB = sinon.spy();

        dialog.on('show', spyA);
        dialog.on('show', spyA);
        dialog.on('show', spyB);
        dialog.off('show', spyA);
        dialog.show();

        expect(spyA.callCount).to.equal(0);
        expect(spyB.callCount).to.equal(1);
    });

    it('unregistering an instance event that was never registered should not affect others', function () {
        var dialog = createDialog();

        var spyA = sinon.spy();
        var spyB = sinon.spy();

        dialog.on('show', spyB);
        dialog.off('show', spyA);
        dialog.show();

        expect(spyA.callCount).to.equal(0);
        expect(spyB.callCount).to.equal(1);
    });

    describe('global', function () {
        var $el;
        var dialog;

        beforeEach(function () {
            $el = createContentEl();
            dialog = dialog2Widget($el);
        });

        it('show event', function () {
            var globalShowEventSpy = sinon.spy();

            dialog2Widget.on('show', globalShowEventSpy);
            dialog.show();
            dialog2Widget.off('show', globalShowEventSpy);

            expect(globalShowEventSpy).to.have.been.calledOnce;
            var $passedEl = globalShowEventSpy.args[0][1];
            expect($passedEl[0]).to.equal($el[0]);
        });

        it('show event native', function () {
            var globalNativeShowEventSpy = sinon.spy();

            onEvent(document, 'show', globalNativeShowEventSpy);
            dialog.show();
            offEvent(document, 'show', globalNativeShowEventSpy);

            expect(globalNativeShowEventSpy).to.have.been.calledOnce;
        });

        it('show event not triggered by normal show events', function () {
            var globalShowEventSpy = sinon.spy();

            dialog2Widget.on('show', globalShowEventSpy);
            $el.trigger('show');
            dialog2Widget.off('show', globalShowEventSpy);

            expect(globalShowEventSpy).to.not.have.been.called;
        });

        it('show event not triggered by normal show events on innerElement', function () {
            var $innerEl = $('<span class="inner-component"></span>');
            $el.append($innerEl);
            var globalShowEventSpy = sinon.spy();

            dialog2Widget.on('show', globalShowEventSpy);
            $innerEl.trigger('show');
            dialog2Widget.off('show', globalShowEventSpy);

            expect(globalShowEventSpy).to.not.have.been.called;
        });

        it('hide event', function () {
            var globalHideEventSpy = sinon.spy();

            dialog2Widget.on('hide', globalHideEventSpy);
            dialog.show();
            dialog.hide();
            dialog2Widget.off('hide', globalHideEventSpy);

            expect(globalHideEventSpy).to.have.been.calledOnce;
            var $passedEl = globalHideEventSpy.args[0][1];
            expect($passedEl[0]).to.equal($el[0]);
        });

        it('hide event native', function () {
            var nativeHideEventSpy = sinon.spy();

            onEvent(document, 'hide', nativeHideEventSpy);
            dialog.show();
            dialog.hide();
            offEvent(document, 'hide', nativeHideEventSpy);

            expect(nativeHideEventSpy).to.have.been.called;
        });

        it('events can be turned off', function () {
            var globalEventSpy = sinon.spy();

            dialog2Widget.on('hide', globalEventSpy);
            dialog2Widget.off('hide', globalEventSpy);
            dialog.show();
            dialog.hide();

            expect(globalEventSpy).to.not.have.been.called;
        });

        it('events can be turned off native', function () {
            var nativeEventSpy = sinon.spy();

            onEvent(document, 'hide', nativeEventSpy);
            offEvent(document, 'hide', nativeEventSpy);
            dialog.show();
            dialog.hide();

            expect(nativeEventSpy).to.not.have.been.called;
        });

        it('events registered twice are only fired once', function () {
            var spy = sinon.spy();
            dialog2Widget.on('show', spy);
            dialog2Widget.on('show', spy);
            dialog.show();

            expect(spy.callCount).to.equal(1);
        });

        it('events do not conflict with each-other', function () {
            var spyA = sinon.spy();
            var spyB = sinon.spy();

            dialog2Widget.on('show', spyA);
            dialog2Widget.on('show', spyB);
            dialog.show();

            expect(spyA.callCount).to.equal(1);
            expect(spyB.callCount).to.equal(1);
        });

        it('events only unregister themselves', function () {
            var spyA = sinon.spy();
            var spyB = sinon.spy();

            dialog2Widget.on('show', spyA);
            dialog2Widget.on('show', spyA);
            dialog2Widget.on('show', spyB);
            dialog2Widget.off('show', spyA);
            dialog.show();

            expect(spyA.callCount).to.equal(0);
            expect(spyB.callCount).to.equal(1);
        });

        it('unregistering an event that was never registered should not affect others', function () {
            var spyA = sinon.spy();
            var spyB = sinon.spy();

            dialog2Widget.on('show', spyB);
            dialog2Widget.off('show', spyA);
            dialog.show();

            expect(spyA.callCount).to.equal(0);
            expect(spyB.callCount).to.equal(1);
        });

        it('two global events turned off correctly, same event name', function () {
            var hideEventSpy = sinon.spy();
            var hideEventSpy2 = sinon.spy();

            dialog2Widget.on('hide', hideEventSpy);
            dialog2Widget.on('hide', hideEventSpy2);
            dialog2Widget.off('hide', hideEventSpy);
            dialog.show();
            dialog.hide();
            dialog2Widget.off('hide', hideEventSpy2);

            expect(hideEventSpy).to.not.have.been.called;
            expect(hideEventSpy2).to.have.been.calledOnce;
        });

        it('two global events turned off correctly, same event name native', function () {
            var nativeHideEventSpy = sinon.spy();
            var nativeHideEventSpy2 = sinon.spy();

            onEvent(document, 'hide', nativeHideEventSpy);
            onEvent(document, 'hide', nativeHideEventSpy2);
            offEvent(document, 'hide', nativeHideEventSpy);
            dialog.show();
            dialog.hide();
            offEvent(document, 'hide', nativeHideEventSpy2);

            expect(nativeHideEventSpy).to.not.have.been.called;
            expect(nativeHideEventSpy2).to.have.been.calledOnce;
        });

        it('two global events turned off correctly, different event names', function () {
            var hideEventSpy = sinon.spy();
            var showEventSpy = sinon.spy();

            dialog2Widget.on('hide', hideEventSpy);
            dialog2Widget.on('show', showEventSpy);
            dialog2Widget.off('hide', hideEventSpy);
            dialog2Widget.off('show', showEventSpy);
            dialog.show();
            dialog.hide();

            expect(hideEventSpy).to.have.not.been.called;
            expect(showEventSpy).to.have.not.been.called;
        });

        it('two global events turned off correctly, different event names native', function () {
            var hideEventSpy = sinon.spy();
            var showEventSpy = sinon.spy();

            onEvent(document, 'hide', hideEventSpy);
            onEvent(document, 'show', showEventSpy);
            offEvent(document, 'hide', hideEventSpy);
            offEvent(document, 'show', showEventSpy);
            dialog.show();
            dialog.hide();

            expect(hideEventSpy).to.not.have.been.called;
            expect(showEventSpy).to.not.have.been.called;
        });
    });

    it('global beforeHide event cancels hide', function () {
        var $el = createContentEl();

        var dialog = dialog2Widget($el);
        var beforeHideStub = sinon.stub().returns(false);

        dialog2Widget.on('beforeHide', beforeHideStub);
        dialog.show();
        dialog.hide();
        dialog2Widget.off('beforeHide', beforeHideStub);

        expect(beforeHideStub).to.have.been.called;
        expect(layerWidget($el).isVisible()).to.be.true;
    });

    it('Global hide event cancels hide native', function () {
        var $el = createContentEl();
        var dialog = dialog2Widget($el);

        // Set up spy with predetermined behaviour.
        var prevent = function (e) {
            e.preventDefault();
        };
        var preventDefault = { prevent: function () {} };
        var cancelHideEventSpy = sinon.stub(preventDefault, 'prevent').callsFake(prevent);

        onEvent(document, 'hide', cancelHideEventSpy);
        dialog.show();
        dialog.hide();
        offEvent(document, 'hide', cancelHideEventSpy);

        expect(cancelHideEventSpy).to.have.been.calledOnce;
        expect(layerWidget($el).isVisible()).to.be.true;
    });

    it('global beforeShow event cancels show', function () {
        var $el = createContentEl();

        var dialog = dialog2Widget($el);
        var globalBeforeShowStub = sinon.stub().returns(false);

        dialog2Widget.on('beforeShow', globalBeforeShowStub);
        dialog.hide();
        dialog.show();
        dialog2Widget.off('beforeShow', globalBeforeShowStub);

        expect(globalBeforeShowStub).to.have.been.calledOnce;
        expect(layerWidget($el).isVisible()).to.be.false;
    });

    it('global show event cancels show native', function () {
        var $el = createContentEl();
        var dialog = dialog2Widget($el);

        // Set up spy with predetermined behaviour.
        var prevent = function (e) {
            e.preventDefault();
        };
        var preventDefault = { prevent: function () {} };
        var cancelShowEventSpy = sinon.stub(preventDefault, 'prevent').callsFake(prevent);

        onEvent(document, 'show', cancelShowEventSpy);
        dialog.hide();
        dialog.show();
        offEvent(document, 'show', cancelShowEventSpy);

        expect(cancelShowEventSpy).to.have.been.calledOnce;
        expect(layerWidget($el).isVisible()).to.be.false;
    });

    it('does not remove on hide when removeOnHide is false', function () {
        var $el = createContentEl();

        var dialog = dialog2Widget($el);
        dialog.show();
        dialog.hide();

        expect($el.parent().get(0)).to.exist;
    });

    it('removes on hide when removeOnHide is true', function () {
        var $el = createContentEl();
        $el.attr('data-aui-remove-on-hide', 'true');

        var dialog = dialog2Widget($el);
        dialog.show();
        dialog.hide();

        expect($el.parent().get(0)).to.not.exist;
    });

    it('does not remove when removeOnHide is false with ESC key', function () {
        var $el = createContentEl();

        var dialog = dialog2Widget($el);
        dialog.show();

        pressKey(keyCode.ESCAPE);

        expect($el.parent().get(0)).to.exist;
    });

    it('does remove when removeOnHide is true with ESC key', function () {
        var $el = createContentEl();
        $el.attr('data-aui-remove-on-hide', 'true');

        var dialog = dialog2Widget($el);
        dialog.show();

        pressKey(keyCode.ESCAPE);

        expect($el.parent().get(0)).to.not.exist;
    });

    it('does not remove on blanket on click when removeOnHide is false', function () {
        var $el = createContentEl();

        var dialog = dialog2Widget($el);
        dialog.show();

        clickBlanket();

        expect($el.parent().get(0)).to.exist;
    });

    it('does remove on blanket on click when removeOnHide is true', function () {
        var $el = createContentEl();
        $el.attr('data-aui-remove-on-hide', 'true');

        var dialog = dialog2Widget($el);
        dialog.show();

        clickBlanket();
        expect($el.parent().get(0)).to.not.exist;
    });

    describe('multiple dialogs', function () {
        let dialog1;
        let dialog2;

        beforeEach(function () {
            dialog1 = createDialog({ id: 'dialog1' });
            dialog2 = createDialog({ id: 'dialog2' });
        });

        describe('instance events', function () {
            it('can register same event for multiple dialogs', function () {
                const spy = sinon.spy();
                dialog1.on('show', spy);
                dialog2.on('show', spy);

                dialog1.show();
                dialog2.show();
                expect(spy.callCount).to.equal(2);
            });

            it('unregistering does not affect other dialogs', function () {
                const spy = sinon.spy();
                dialog1.on('show', spy);
                dialog2.on('show', spy);
                dialog2.off('show', spy);

                dialog1.show();
                expect(spy.callCount).to.equal(1);
                expect(spy.args[0][0].target.id).to.equal('dialog1');
            });
        });

        describe('global events', function () {
            it('can register same event for multiple dialogs', function () {
                const spy = sinon.spy();
                dialog2Widget.on('show', spy);

                dialog1.show();
                dialog2.show();
                expect(spy.callCount).to.equal(2);
            });
        });
    });
});
