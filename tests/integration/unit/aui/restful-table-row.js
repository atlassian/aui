import RestfulTableRow from '@atlassian/aui/src/js/aui/restful-table/row';
import $ from '@atlassian/aui/src/js/aui/jquery';

describe('restuful table row', function () {
    describe('deleting rows', function () {
        var view;
        var options;
        const VALIDATION_ERROR_MESSAGE = 'deleteConfirmationCallback needs to return a Promise';

        beforeEach(() => {
            options = {
                deleteConfirmationCallback() {},
                model: {
                    toJSON() {
                        return {
                            id: 1,
                        };
                    },
                    destroy() {},
                },
            };
        });

        describe('callback validation', function () {
            it('throws error when provided callback is returning falsy value', function () {
                sinon.stub(options, 'deleteConfirmationCallback').returns(null);
                view = new RestfulTableRow(options);

                expect(() => view.destroy()).to.throw(VALIDATION_ERROR_MESSAGE);
            });

            it('throws error when provided callback is returning object without then method', function () {
                sinon.stub(options, 'deleteConfirmationCallback').returns({});
                view = new RestfulTableRow(options);

                expect(() => view.destroy()).to.throw(VALIDATION_ERROR_MESSAGE);
            });

            if ('Promise' in window) {
                it('should not throw error when provided callback is returning ES6 Promise', function () {
                    sinon.stub(options, 'deleteConfirmationCallback').returns(Promise.resolve());
                    view = new RestfulTableRow(options);

                    expect(() => view.destroy()).to.not.throw(VALIDATION_ERROR_MESSAGE);
                });
            }

            it('should not throw error when provided callback is returning $.deffered', function () {
                sinon.stub(options, 'deleteConfirmationCallback').returns($.Deferred());
                view = new RestfulTableRow(options);

                expect(() => view.destroy()).to.not.throw(VALIDATION_ERROR_MESSAGE);
            });
        });

        it('calls delete confirmation callback before deleting row', function () {
            sinon.stub(options, 'deleteConfirmationCallback').returns($.Deferred().resolve());
            view = new RestfulTableRow(options);

            view.destroy();

            expect(options.deleteConfirmationCallback.callCount).to.equal(1);
        });

        it('removes row if promise was resolved', function (done) {
            let promise = $.Deferred().resolve();
            sinon.stub(options, 'deleteConfirmationCallback').returns(promise);
            sinon.stub(options.model, 'destroy');
            view = new RestfulTableRow(options);

            view.destroy();

            promise.then(() => {
                expect(options.model.destroy.callCount).to.equal(1);
                done();
            });
        });

        it('prevents removal of row if promise was rejected', function (done) {
            let promise = $.Deferred().reject();
            sinon.stub(options, 'deleteConfirmationCallback').returns(promise);
            sinon.stub(options.model, 'destroy');
            view = new RestfulTableRow(options);

            view.destroy();

            promise.then(
                () => {},
                () => {
                    expect(options.model.destroy.callCount).to.equal(0);
                    done();
                }
            );
        });

        it('deletes row without when callback is not provided', function () {
            sinon.stub(options.model, 'destroy');
            view = new RestfulTableRow($.extend({}, options, { deleteConfirmationCallback: null }));

            view.destroy();

            expect(options.model.destroy.callCount).to.equal(1);
        });
    });
});
