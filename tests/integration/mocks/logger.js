export let error = sinon.stub();
export let log = sinon.stub();
export let warn = sinon.stub();

beforeEach(function () {
    error.reset();
    log.reset();
    warn.reset();
});
