export let needsLayeringShim = sinon.stub();
export let supportsVoiceOver = sinon.stub();
export let supportsDateField = sinon.stub();
export let supportsFocusWithin = sinon.stub();

function reset() {
    [needsLayeringShim, supportsVoiceOver, supportsDateField, supportsFocusWithin].forEach((stub) =>
        stub.reset()
    );
    needsLayeringShim.returns(false);
    supportsVoiceOver.returns(false);
    supportsDateField.returns(true);
    supportsFocusWithin.returns(false);
}

beforeEach(reset);
reset();
