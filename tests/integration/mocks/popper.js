import { createPopper as popperCreare } from '@popperjs/core';

/**
 * Popper2-like synced-enabled injectable dependency.
 *
 * Creates a Popper2-like object with factory that's meant to be injected as a dependency,
 * producing popper instances which may enforce sync operations on demand
 *
 * @returns {{createPopper: (function(): Instance)}} returns Popper factory that produces popper instances
 *              with sync-able behaviour
 */

let sync = false;

/**
 * Activates on-demand-sync behaviour and allows control over timers
 *
 * Upon calling - any Popper instance that was created by this mock will switch from "async" to "sync on demand" updates
 * The updates are delegated to and controlled by Sinon's fake timers.
 *
 * This allows to run the queue of updates only when it's expected (but still in same process queue).
 *
 * @param clock - sinon Fake Timer object to control timers, if any used in test.
 *            FakedTimers are global and need to be controlled by one Fake Timer.
 * @returns {Object} [controlMethods]
 * @returns {Function} [controlMethods.tick()] - progresses the Popper update queue
 * @returns {Function} [controlMethods.restore()] - Restores async behaviour
 */
function control(clock) {
    let ownClock = false;

    if (!clock) {
        ownClock = true;
        clock = sinon.useFakeTimers();
    }

    sync = true;

    return {
        tick() {
            clock.tick(100);
        },
        restore() {
            // we only restore clock if we were the ones that created one
            if (ownClock) {
                clock.restore();
            }
            clock = null;
            sync = false;
        },
    };
}

function isSync() {
    return sync;
}

/**
 * Creates new Popper instances with sync-able behaviour.
 *
 * Popper instance has 2 internal methods to trigger updates
 * async update() and sync forceUpdate()
 * Internally - async update() de-facto wraps the sync forceUpdate() in a immediately-resolved Promise
 * To avoid that - we'll redirect the async method calls the to sync one.
 *
 * This behaviour will only be activated if test suit demanded taking control of "mocked" Popper instance
 *
 * @param opt pass all the options to Popper
 * @returns {Instance} PopperInstance
 */
function createPopper(...opt) {
    const onNextTick = (fn) => setTimeout(fn, 0);
    const pop = popperCreare(...opt);
    const origUpdate = pop.update;

    sinon.stub(pop, 'update').callsFake(function () {
        if (isSync()) {
            onNextTick(pop.forceUpdate);
        } else {
            return origUpdate();
        }
    });

    /**
     *  If we already took over control over the mock / activated sync behaviour before the mocked instance was created
     *  we need to manually push certain behaviour that's "locked" by Popper's original async nature
     *  that means - anything that "acts" on first tick needs to be re-created and anything that "skips"  first tick
     *  needs to be blocked.
     */
    if (isSync()) {
        // "onUpdate" modifier skips first tick. We need to manually block it for this one,
        if (pop.state.modifiersData['onUpdate#persistent']) {
            pop.state.modifiersData['onUpdate#persistent'].enabled = false;
        }
        // we already initialised Popper, thus there's a async update pending.
        // Let's make use of our proxied forceUpdate to re-run all actions on demand
        pop.update();

        // re-activate "onUpdate" modifier for next calls
        if (pop.state.modifiersData['onUpdate#persistent']) {
            onNextTick(() => (pop.state.modifiersData['onUpdate#persistent'].enabled = true));
        }

        // OnFirstUpdate is stuck on unresolved promise. Let's run that manually.
        if ('function' === typeof pop.state.options.onFirstUpdate) {
            onNextTick(() => pop.state.options.onFirstUpdate(pop.state));
        }
    }

    return pop;
}

export { createPopper, control };
