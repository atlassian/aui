const webpackParts = require('@atlassian/aui-webpack-config/webpack.parts');
const { libraryExternals } = require('@atlassian/aui-webpack-config/webpack.skeleton');
const { merge } = require('webpack-merge');
const path = require('path');

module.exports = function (config) {
    const reporters = ['progress', 'junit'];
    config.watch || reporters.push('coverage-istanbul');

    const jsTranspileStrategy = config.watch
        ? webpackParts.transpileJs
        : webpackParts.transpileJsWithCodeCoverage;

    const webpackConfig = merge([
        webpackParts.setAuiVersion(),
        webpackParts.loadSoy(),
        webpackParts.resolveSoyDeps(),
        webpackParts.inlineCss(),
        jsTranspileStrategy({
            exclude: /(tether|soy-template-plugin-js|node_modules|mocks)/,
        }),
        {
            mode: 'development',
            devtool: false,
            externals: [libraryExternals.jqueryExternal],
            module: {
                rules: [
                    /**
                     *  Allows for mocking dependencies in any JS.
                     **/
                    {
                        test: /\.js$/i,
                        exclude: /(tether|soy-template-plugin-js|node_modules|mocks)/,
                        resolve: {
                            /**
                             * We use aliases to mock some low-level module dependencies in AUI.
                             * As at 2021-03-01, AUI didn't have need for a general-purpose module mocking or dependency-injection solution.
                             */
                            alias: {
                                /**
                                 *  As in our tests:
                                 *  - we are testing the Alignment (our abstraction layer),
                                 *  - not the technical / practical implementation that sits inside,
                                 *  - do not explicitly contract any async behaviour
                                 *  - want to run tests fast && sync
                                 *  we will mock internal dependency (Popper) in a way that allows for sync and async
                                 *  behaviour on demand, easily controllable in tests.
                                 *
                                 *  We use @popperjs/core$ (with $ sign at the end) to enforce exact match
                                 */
                                '@popperjs/core$': path.resolve(__dirname, 'mocks/popper.js'),
                                /**
                                 * Some tests want to check if anything got logged, so we mock our logger proxy module
                                 */
                                './log': path.resolve(__dirname, 'mocks/logger.js'),
                                './internal/log': path.resolve(__dirname, 'mocks/logger.js'),
                                /**
                                 * Some tests expect different capabilities to exist in some browsers
                                 */
                                './browser': path.resolve(__dirname, 'mocks/browser.js'),
                                './internal/browser': path.resolve(__dirname, 'mocks/browser.js'),
                                'wrmI18n$': path.resolve(__dirname, 'mocks/i18n.js'),
                            },
                        },
                    },
                    {
                        test: /\.(png|jpg|gif|svg|ttf|eot|woff|woff2)$/,
                        use: 'ignore-loader',
                    },
                ],
            },
        },
    ]);

    const testPattern = './entry/all-tests.js';
    const bootstrapPattern = './*.js';

    var webdriverConfig = {
        hostname: 'hub.lambdatest.com',
        port: 80,
    };
    config.set({
        hostname: 'localhost', // hostname, where karma web server will run
        autoWatch: config.watch,
        singleRun: !config.watch,
        captureTimeout: 10 * 60 * 1000, // 10 minutes
        retryLimit: 1,
        browserDisconnectTimeout: 90 * 1000, // 90 s
        browserDisconnectTolerance: 1,
        browserNoActivityTimeout: 90 * 1000, // 90 s
        reporters: reporters,
        concurrency: 1,
        logLevel: config.LOG_DEBUG,
        browsers: config.watch
            ? ['Chrome_dev']
            : ['Chrome_Headless_1024x768', 'Firefox_Headless_1024x768'],

        frameworks: ['mocha', 'chai', 'sinon-chai', 'webpack'],

        plugins: [
            require('karma-chai'),
            require('karma-coverage'),
            require('karma-coverage-istanbul-reporter'),
            require('karma-junit-reporter'),
            require('karma-mocha'),
            require('karma-sinon-chai'),
            require('karma-spec-reporter'),
            require('karma-webpack'),
            require('karma-chrome-launcher'),
            require('karma-firefox-launcher'),
            require('karma-webdriver-launcher'),
        ],
        client: {
            useIframe: true,
            chai: {
                includeStack: true,
            },
            mocha: {
                timeout: 12345,
                slow: 1,
                ui: 'bdd',
            },
        },
        customLaunchers: {
            Chrome_dev: {
                base: 'Chrome',
                flags: ['--window-size=1624,768', '--auto-open-devtools-for-tabs'],
            },
            Chrome_1024x768: {
                base: 'Chrome',
                flags: ['--window-size=1024,768'],
            },
            Chrome_Headless_1024x768: {
                base: 'ChromeHeadless',
                flags: [
                    '--window-size=1024,768',
                    '--disable-translate',
                    '--disable-extensions',
                    '--no-sandbox',
                ],
            },
            Firefox_1024x768: {
                base: 'Firefox',
                flags: ['-foreground', '-width', '1024', '-height', '768'],
            },
            Firefox_Headless_1024x768: {
                base: 'Firefox',
                flags: ['-foreground', '-width', '1024', '-height', '768', '-headless'],
                prefs: {
                    'network.proxy.type': 0,
                },
            },
            Edge_Win: {
                build: process.env.LT_BUILD || 'AUI',
                base: 'WebDriver',
                config: webdriverConfig,
                browserName: 'MicrosoftEdge',
                platform: 'windows 11',
                version: '124',
                resolution: '1024x768',
                name: process.env.LT_NAME || 'Unit tests on Edge Win10',
                user: process.env.LT_USERNAME,
                accessKey: process.env.LT_ACCESS_KEY,
                tunnel: true,
                tunnelName: process.env.LT_TUNNEL_NAME,
                pseudoActivityInterval: 5000, // 5000 ms heartbeat
            },
            Chrome_macOS: {
                build: process.env.LT_BUILD || 'AUI',
                base: 'WebDriver',
                config: webdriverConfig,
                browserName: 'Chrome',
                platform: 'macOS Ventura',
                version: '124',
                resolution: '1024x768',
                name: process.env.LT_NAME || 'Unit tests on Chrome macOS',
                user: process.env.LT_USERNAME,
                accessKey: process.env.LT_ACCESS_KEY,
                tunnel: true,
                tunnelName: process.env.LT_TUNNEL_NAME,
                pseudoActivityInterval: 5000, // 5000 ms heartbeat
            },
            Safari_macOS: {
                build: process.env.LT_BUILD || 'AUI',
                base: 'WebDriver',
                config: webdriverConfig,
                browserName: 'Safari',
                platform: 'macOS Ventura',
                version: '16.5',
                resolution: '1024x768',
                name: process.env.LT_NAME || 'Unit tests on Safari macOS',
                user: process.env.LT_USERNAME,
                accessKey: process.env.LT_ACCESS_KEY,
                tunnel: true,
                tunnelName: process.env.LT_TUNNEL_NAME,
                pseudoActivityInterval: 5000, // 5000 ms heartbeat
            },
        },
        coverageIstanbulReporter: {
            'reports': ['text-summary', 'lcov'],
            'projectRoot': '../../',
            'fixWebpackSourcePaths': true,
            'combineBrowserReports': true,
            'skipFilesWithNoCoverage': true,
            'report-config': {
                lcov: { projectRoot: '../../' },
            },
        },
        junitReporter: {
            outputDir: 'test-reports',
            suite: 'AUI',
        },
        preprocessors: {
            [testPattern]: 'webpack',
            [bootstrapPattern]: 'webpack',
        },
        webpack: webpackConfig,
        files: [
            require.resolve('jquery'),
            require.resolve('jquery-migrate'),
            {
                pattern: testPattern,
                watched: false,
            },
        ],
    });
};
