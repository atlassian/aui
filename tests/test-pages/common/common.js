/**
 * @fileOverview
 * These behaviours are meant to affect most test pages.
 */

require(['jquery'], function () {
    // Add your behaviour if needed
});
