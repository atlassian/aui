AJS.$(function ($) {
    const $log = $('#event-log');
    let counter = 1;

    function addLog(e) {
        const now = Date.now();
        console.debug(now, e);

        $(`<details aria-atomic="true">
            <summary><time datetime="${now}">Event ${counter++}</time></summary>
            <span></span>
        </details>`)
            .find('span')
            .text(JSON.stringify(parseData(e)))
            .parent()
            .appendTo($log);
    }

    function parseData(e) {
        return {
            name: e.type,
            target: friendlyDOM(e.target),
            relatedTarget: friendlyDOM(e.relatedTarget),
        };
    }

    function friendlyDOM(node) {
        return String((node && node.outerHTML) || 'N/A');
    }

    $(document).on('aui-layer-hide', addLog);
    $(document).on('aui-layer-show', addLog);
});
