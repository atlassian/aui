require(['aui/banner'], function (banner) {
    const BUTTON_TYPES = ['announcement', 'warning', 'error'];
    const DURATION = 5000;

    const buttonConfigs = BUTTON_TYPES.map((curr) => ({
        text: `Show ${curr} banner`,
        bannerConfig: {
            body: `This is an ${curr} banner - <a href='https://example.com/'>Example link</a>`,
            type: curr,
        },
    }));

    buttonConfigs.forEach((config) => {
        const { body, type } = config.bannerConfig;

        const buttonContainer = document.getElementById('button-container');
        const button = document.createElement('button');
        button.classList.add('aui-button');
        button.id = `show-${type}-banner`;

        button.textContent = config.text;

        button.addEventListener('click', function () {
            banner({ body, type });
        });

        buttonContainer.appendChild(button);
    });

    const startTimerButton = document.getElementById('timer-start');
    const timeLeftContainer = document.getElementById('time-left');

    timeLeftContainer.textContent = `${DURATION / 1000}s`;

    startTimerButton.addEventListener('click', () => {
        startTimerButton.disabled = true;
        setTimeoutWithTimeOnElement(
            () => {
                banner({
                    body: `
            Banner body with some
            <a href="http://example.com">focusable links to other websites</a>
            `,
                });
                startTimerButton.disabled = false;
            },
            DURATION,
            timeLeftContainer
        );
    });
});

function setTimeoutWithTimeOnElement(callback, duration, element) {
    const getTimeLeft = (startTime, duration) =>
        Math.ceil((startTime + duration - Date.now()) / 1000);

    const startTime = Date.now();
    setTimeout(callback, duration);

    function update() {
        const timeLeft = getTimeLeft(startTime, duration);

        if (timeLeft >= 0) {
            element.textContent = `${timeLeft}s`;
            window.requestAnimationFrame(update);
        }
    }

    window.requestAnimationFrame(update);
}
