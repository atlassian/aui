/* global soy, $, Select2 */

const noopInput = function (s) {
    return s;
};

const escapeMarkup = function (text) {
    /**
     * soy.$$escapeHtml(text) only works in JIRA 6 - this is a bridge so this code works in JIRA 6 and 7
     */
    const escapedHtml = soy.$$escapeHtml(text);
    return escapedHtml.toString ? escapedHtml.toString() : escapedHtml;
};

const ajaxOptions = {
    ajax: {
        type: 'GET',
        url: 'https://api.github.com/search/repositories',
        dataType: 'json',
        quietMillis: 500,
        data: function (term, page) {
            return {
                q: term ? term : 'user:github',
                page: page,
            };
        },
        results: function (data, page) {
            const more = page * 30 < data.total_count;
            return { results: data.items, more: more };
        },
    },
    formatResult(user) {
        return user.name;
    },
    formatSelection(user) {
        return user.name;
    },
};

const setupExample = (selector, setup) => {
    const context = $(selector);
    const $el = setup(context);

    $('code', context).first().text(setup.toString());
    if ($el) {
        $el.on('change', (event) => {
            $('#select-value', context).text(`Value: ${event.val}`);
        });
    }
};

const setupProfileLanguageSelect = (context) => {
    $('select', context).each(function () {
        $(this).auiSelect2({
            formatResult(match, container, query) {
                const result = [];
                // Loaded as part of soy utils by `/aui-soy.js`
                Select2.util.markMatch(
                    escapeMarkup(match.text),
                    escapeMarkup(query.term),
                    result,
                    noopInput
                );
                return result.join('');
            },
            formatSelection(match) {
                return escapeMarkup(match.text);
            },
            escapeMarkup: noopInput,
        });
    });
};

const setupUserPicker = (context) => {
    const selectWrapper = $('#select-wrapper', context);
    const $input = $('<input>').appendTo(selectWrapper);

    const options = {
        fieldId: 'user-picker',
        value: 'gitignore',
        default: {
            id: 'initSelection',
            name: 'Init Selection',
        },
    };
    const defaultSeparator = '|';

    const pickerDefaultOptions = {
        containerCssClass: `${options.fieldId}-container`,
        separator: defaultSeparator,
        tokenSeparators: [defaultSeparator],
        placeholder: 'Select user',
        createSearchChoicePosition: 'top',
        createSearchChoice(term) {
            return {
                id: term,
                name: term,
            };
        },
        minimumInputLength: 1,
        escapeMarkup: noopInput,
        width() {
            return '100%';
        },
        formatInputTooShort() {
            return 'Start typing';
        },
        formatNoMatches() {
            return 'No results';
        },
        formatSearching() {
            return 'Searching...';
        },
        ...ajaxOptions,
        initSelection(element, callback) {
            callback(options.default);
        },
    };

    $input
        .auiSelect2(pickerDefaultOptions)
        .auiSelect2(
            'val',
            options.value ? options.value.toString().split(defaultSeparator) : undefined
        );

    return $input;
};

const setupCustomStyling = (context) => {
    return $('select', context).auiSelect2({
        containerCssClass: 'custom-styling-container',
        dropdownCssClass: 'custom-styling-dropdown',
    });
};

const setupSeparators = (context) => {
    const selectWrapper = $('#select-wrapper', context);
    const $input = $('<input>').appendTo(selectWrapper);

    return $input.auiSelect2({
        tags: ['red', 'blue', 'green'],
        width: 200,
        separator: ',',
        tokenSeparators: [' '],
    });
};

const setupInputRestrictions = (context) => {
    return $('select', context).auiSelect2({
        minimumInputLength: 2,
        maximumInputLength: 5,
    });
};

const setupSearchChoice = (context) => {
    const selectWrapper = $('#select-wrapper', context);
    const $input = $('<input>').appendTo(selectWrapper);
    return $input.auiSelect2({
        query: ({ term, callback }) =>
            callback({
                results: [{ id: term, text: term.toUpperCase() }],
            }),
        width: 200,
        createSearchChoicePosition: 'bottom',
        createSearchChoice: () => ({
            id: 'pink',
            text: 'Pink (Dynamic)',
        }),
    });
};

const setupDynamicActions = (context) => {
    const $select = $('select', context).auiSelect2();

    $('#open', context).on('click', () => {
        $select.auiSelect2('open');
    });
    $('#close', context).on('click', () => {
        $select.auiSelect2('close');
    });
    $('#enable', context).on('click', () => {
        $select.auiSelect2('enable', true);
    });
    $('#disable', context).on('click', () => {
        $select.auiSelect2('enable', false);
    });
    $('#set-value', context).on('input', (event) => {
        console.log(event.target.value);
        $select.auiSelect2('val', event.target.value);
    });

    return $select;
};

const setupPage = () => {
    setupExample('#test-case-profile-language-wrapper', setupProfileLanguageSelect);
    setupExample('#test-case-user-picker-wrapper', setupUserPicker);

    setupExample('#test-case-custom-styling-wrapper', setupCustomStyling);
    setupExample('#test-case-separators-wrapper', setupSeparators);
    setupExample('#test-case-input-restrictions-wrapper', setupInputRestrictions);
    setupExample('#test-case-search-choice-wrapper', setupSearchChoice);
    setupExample('#test-case-dynamic-actions-wrapper', setupDynamicActions);
};

// for Cypress light reloads
if (
    document.readyState === 'complete' ||
    document.readyState === 'loaded' ||
    document.readyState === 'interactive'
) {
    console.log('ready state');
    setupPage();
}

window.addEventListener('DOMContentLoaded', () => {
    console.log('dcl');
    setupPage();
});
