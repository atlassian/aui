/* global $ */
import { Autocomplete } from './bamboo-autocomplete.js';

const providedData = [
    { id: 1, text: 'master' },
    { id: 2, text: 'release/1.0.x' },
    { id: 3, text: 'release/2.0.x' },
    { id: 4, text: 'release/2.1.x' },
    { id: 5, text: 'issue/2.1.x/TICK-123_some-issue' },
    { id: 6, text: 'issue/2.1.x/TICK-321_feature' },
];

$(() => {
    const autocompleteInstance = new Autocomplete('#bamboo-autocomplete', {
        data: [...providedData],
        minimumInputLength: 1,
    });

    const resultContainer = document.querySelector('#bamboo-autocomplete-results');

    document.querySelector('#bamboo-autocomplete-hint').innerText = JSON.stringify(
        providedData,
        null,
        2
    );

    document.querySelector('#bamboo-autocomplete-actions').addEventListener('click', (event) => {
        const { target } = event;
        const { textContent: action } = target;

        switch (action.toLowerCase()) {
            case 'disable':
                autocompleteInstance.disable();
                target.textContent = 'Enable';
                break;
            case 'enable':
                autocompleteInstance.enable();
                target.textContent = 'Disable';
            case 'get selected data':
                resultContainer.textContent = JSON.stringify(
                    autocompleteInstance.getSelectedData()
                );
                break;
            case 'get container':
                resultContainer.textContent = `container: #${autocompleteInstance.getContainer()[0].id}`;
                break;
            case 'select first':
                autocompleteInstance.$el.auiSelect2('data', providedData[0]);
                break;
            case 'select random':
                const selected = autocompleteInstance.getSelectedData();

                let newValue;
                do {
                    newValue = providedData[Math.floor(Math.random() * providedData.length)];
                } while (newValue === selected);

                autocompleteInstance.$el.auiSelect2('data', newValue);
                break;
            case 'clear results':
                resultContainer.textContent = '';
                autocompleteInstance.$el.auiSelect2('data', null);
                break;
            default:
                console.error('Unknown action:', action);
        }
    });
});
