/* global $ */
import { simpleSelect2 } from './bamboo-simple-select2.js';

$(() => {
    simpleSelect2('#bamboo-simple-select2-container select');
});
