/* global $ */
/**
 * This is a copy of Bamboo's simple-select2.js
 * https://stash.atlassian.com/projects/BAM/repos/bamboo/browse/components/bamboo-plugins2/bamboo-plugin-web-resources/src/main/resources/widget/simple-select2/simple-select2.js
 *
 * Changes for Test Pages purposes:
 * - converted from AMD to ES module
 * - removed `bamboo.widget` references
 * - replaced the `bamboo.widget` SOY template return with a simple string template
 */

function format(item) {
    const originalOptionTitle = $(item.element).attr('title');

    return originalOptionTitle
        ? `<em><strong>${originalOptionTitle}:</strong> ${item.text}</em>`
        : item.text;
}

export function simpleSelect2(selector) {
    const $select2 = $(selector);

    $select2.removeClass('widget-simple-select2--multiple');
    $select2.auiSelect2({
        dropdownCssClass: 'widget-simple-select2__dropdown',
        formatResult: format,
    });

    return $select2;
}
