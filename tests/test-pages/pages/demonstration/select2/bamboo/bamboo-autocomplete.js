/* global $ */
/**
 * This is a copy of Bamboo's Autocomplete component
 * For simplicity purposes, we've removed some business logic
 * and dependencies (`underscore`, `widget` and `backbone-brace`)
 */
export class Autocomplete {
    constructor(el, options) {
        options = options || {};

        this.$el = $(el);

        options.initialValue = JSON.parse(options.initialValue || null);

        this.settings = Object.assign(
            {},
            {
                escapeMarkup: (m) => m,
                placeholder: ' ',
            },
            options
        );

        if (!this.$el.val()) {
            this.$el.val('empty initial value');
        }

        if (options.data) {
            this.data = options.data;
        }

        this.$el.auiSelect2(this.settings);

        this.$el.on('change', this.onChange.bind(this));
    }

    onChange() {
        $('#select2-drop-mask').hide();

        this.$el.auiSelect2('dropdown').hide();
        this.getContainer()
            .removeClass('select2-dropdown-open')
            .removeClass('select2-container-active');
    }

    getSelectedData(callback) {
        let selected = this.$el.auiSelect2('data');

        if (!selected) {
            const _filterFn =
                typeof callback === 'function' ? callback : (item) => item.id === this.$el.val();

            selected = this.data.find(_filterFn);
        }

        return selected || null;
    }

    getContainer() {
        return this.$el.auiSelect2('container');
    }

    disable() {
        this.$el.auiSelect2('enable', false);
        return this;
    }

    enable() {
        this.$el.auiSelect2('enable', true);
        return this;
    }
}
