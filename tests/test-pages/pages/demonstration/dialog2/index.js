AJS.$(function () {
    // Content dialog behaviours
    AJS.$(document).on('click', '#dialog-with-lots-of-content-button', function (e) {
        AJS.dialog2('#dialog-with-lots-of-content').show();
        e.preventDefault();
    });
    AJS.$(document).on('click', '#dialog-with-lots-of-content-close-button', function (e) {
        AJS.dialog2('#dialog-with-lots-of-content').hide();
        e.preventDefault();
    });
    var sizeOptions = ['small', 'medium', 'large', 'xlarge', '\u26C4\uFE0F'];
    sizeOptions.forEach(function (size) {
        var option =
            '<div class="field-group"><label><input type="radio" name="content-dialog-size" value="' +
            size +
            '" />' +
            size +
            '</label></div>';
        AJS.$('#change-content-dialog-size').append(option);
    });
    AJS.$(document).on('submit', '#change-content-dialog-size', function (e) {
        e.preventDefault();
    });
    AJS.$(document).on('change', '#change-content-dialog-size input', function (e) {
        var value = e.target.value;
        var $dialog = AJS.$('#dialog-with-lots-of-content');
        $dialog.removeClass(
            sizeOptions
                .map(function (size) {
                    return 'aui-dialog2-' + size;
                })
                .join(' ')
        );
        $dialog.addClass('aui-dialog2-' + value);
    });

    // Dialog with changed primary button order
    AJS.$(document).on('click', '#dialog-with-changed-focus-order-open-button', function () {
        AJS.dialog2('#dialog-with-changed-focus-order').show();
    });
    AJS.$(document).on(
        'click',
        '#dialog-with-changed-focus-order-cancel-button, #dialog-with-changed-focus-order-confirm-button',
        function () {
            AJS.dialog2('#dialog-with-changed-focus-order').hide();
        }
    );

    AJS.$(document).on('click', '#dialogWithRadioButtonsOpen', function (e) {
        AJS.dialog2('#dialogWithRadioButtons').show();
        e.preventDefault();
    });
    AJS.$(document).on('click', '#dialogWithRadioButtonsClose', function (e) {
        AJS.dialog2('#dialogWithRadioButtons').hide();
        e.preventDefault();
    });
});
