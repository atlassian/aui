require([
    'jquery',
    'aui-test/single-select-server',
    'aui/flag',
    'aui/form-validation',
    'aui/form-validation/validator-register',
    'aui/select',
], function ($, server, flag, validator) {
    validator.register(['correctbeatle'], function (field) {
        var answer = $(field.el).val();
        if (answer !== 'Ringo Starr') {
            field.invalidate('Ringo Starr is the best Beatle');
        } else {
            field.validate();
        }
    });

    let focusedElement;

    $(document).on('aui-flag-close', () => {
        if (focusedElement) {
            focusedElement.focus();
            focusedElement = undefined;
        }
    });

    $(document).on('submit', 'form', function (e) {
        e.preventDefault();
        // save previous focused element
        focusedElement = document.activeElement;

        const flagContainer = flag({
            type: 'success',
            title: 'Form submitted',
            body: $(e.target).serialize(),
        });

        // move focus on popup container
        flagContainer.setAttribute('tabindex', -1);
        flagContainer.focus();
    });
});
