require([
    'require',
    'jquery',
    'aui-test/dropdown-server',
    'aui-test/dropdown-fixtures',
    'aui/flag',
], function (require) {
    const $ = require('jquery');
    const server = require('aui-test/dropdown-server');
    const {
        serverResponse,
        noSectionLabelResponse,
        opensSubmenuResponse,
        customAsyncTemplate,
    } = require('aui-test/dropdown-fixtures');
    const flag = require('aui/flag');

    AJS.$('#lazy-trigger-button').on('click', function () {
        const $trigger = AJS.$('#lazy-trigger');
        $trigger.addClass('aui-dropdown2-trigger');
        $trigger.attr('href', '#lazy-dropdown');
        $trigger.attr('aria-owns', 'lazy-dropdown');
        $trigger.attr('data-aui-trigger', 'toggle');
    });

    $(document).on('click', '#main a:not(#docs-link)', function (event) {
        showLinkData(event);
    });

    function showLinkData(event) {
        if ($(event.target).parent().data().noOverride) {
            return;
        }
        const link = event.target.href;
        const body = `This would take you to <b>${link}</b>`;
        if (!event.isDefaultPrevented()) {
            event.preventDefault();
            flag({ body, close: 'auto' });
        }
    }

    buildRows(
        document.getElementById('repeated-trigger-table'),
        document.getElementById('repeated-trigger-details')
    );

    const removeServerResponse = function () {
        server.responses.forEach(function (response) {
            if (response.url.source === 'custom-async-dropdown') {
                const index = server.responses.indexOf(response);
                server.responses.splice(index, 1);
            }
        });
    };

    $(function () {
        var responseCodeInput = document.getElementById('response-code');
        var responseCodeDelay = document.getElementById('response-delay');
        var responseDataInput = document.getElementById('response-data');

        var resetButton = document.getElementById('async-reset');
        var form = document.getElementById('dd-custom-form');

        var toggleContainer = document.getElementById('dropdown-container');

        var applyChanges = function () {
            toggleContainer.innerHTML = customAsyncTemplate;
            server.respondWith(/custom-async-dropdown/, [
                parseInt(responseCodeInput.value),
                { 'Content-Type': 'application/json' },
                responseDataInput.value,
            ]);
            server.autoRespondAfter = responseCodeDelay.value * 1000;
        };

        var handleChange = function () {
            removeServerResponse();
            applyChanges();
        };

        responseCodeInput.addEventListener('change', function () {
            document.querySelector('input[name="custom-response"][value="custom"]').checked = true;
            handleChange();
        });
        responseDataInput.addEventListener('change', handleChange);
        responseCodeDelay.addEventListener('change', handleChange);

        form.addEventListener('submit', function (e) {
            e.preventDefault();
            applyChanges();
        });

        var sampleResponseMap = {
            1: function () {
                return JSON.stringify(serverResponse);
            },
            2: function () {
                return JSON.stringify(noSectionLabelResponse);
            },
            3: function () {
                return JSON.stringify(opensSubmenuResponse);
            },
            custom: function () {
                return '{}';
            },
        };
        var defaultResponse = function () {
            responseCodeInput.value = 200;
            responseCodeDelay.value = 10;
            document.querySelector('input[name="custom-response"]').click();
            handleChange();
        };

        $('#dd-custom-form').on('click', 'input[name="custom-response"]', function (e) {
            var selected = e.target.value;
            var action = sampleResponseMap[selected] || defaultResponse;
            responseDataInput.value = action();
            handleChange();
        });

        resetButton.addEventListener('click', defaultResponse);

        defaultResponse();
        applyChanges();

        AJS.$('#dtoadItem').on('click', function (e) {
            e.preventDefault();
            AJS.dialog2('#dtoadDialog').show();
        });

        AJS.$('#dtoadSubmit').on('click', function (e) {
            e.preventDefault();
            AJS.dialog2('#dtoadDialog').hide();
        });
    });
});

function genRows(id, times) {
    let html = [];
    for (let i = 0; i < times; i++) {
        html.push(
            '<td><button class="aui-dropdown2-trigger" aria-controls="' +
                id +
                '">row ' +
                (i + 1) +
                '</button></td>'
        );
    }
    return html.join('');
}

function buildRows(tableEl, dialogEl) {
    tableEl.querySelectorAll('tr').forEach(function (row) {
        row.innerHTML = genRows(dialogEl.id, 15);
    });
}
