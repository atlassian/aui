define('aui-test/dropdown-fixtures', function () {
    var serverResponse = [
        {
            type: 'section',
            label: 'Projects',
            items: [
                { type: 'link', href: '#', content: "\"><img src=x onmouseover=alert('pwned!')>" },
                { type: 'link', href: '#', content: 'Design Platform' },
                { type: 'link', href: '#', content: 'Children' },
            ],
        },
        {
            type: 'section',
            label: 'Issues',
            items: [
                { type: 'link', href: '#', content: 'AUI-111' },
                { type: 'link', href: '#', disabled: true, content: 'AUI-222' },
                { type: 'link', href: '#', hidden: true, content: 'AUI-333' },
            ],
        },
        {
            type: 'section',
            label: 'Checkboxes',
            items: [
                { type: 'checkbox', href: '#', interactive: true, content: 'checkbox' },
                {
                    type: 'checkbox',
                    href: '#',
                    interactive: true,
                    checked: true,
                    content: 'checkbox checked',
                },
            ],
        },
        {
            type: 'section',
            label: 'Radio',
            items: [
                { type: 'radio', href: '#', interactive: true, content: 'radio' },
                {
                    type: 'radio',
                    href: '#',
                    interactive: true,
                    checked: true,
                    content: 'radio checked',
                },
            ],
        },
    ];

    var noSectionLabelResponse = [
        {
            type: 'section',
            items: [
                { type: 'link', href: '#', content: 'Shiba Inu' },
                { type: 'link', href: '#', content: 'Pomeranian' },
                { type: 'link', href: '#', content: 'Cavalier' },
            ],
        },
    ];

    var opensSubmenuResponse = [
        {
            type: 'section',
            items: [
                {
                    type: 'link',
                    href: '#',
                    for: 'dd-web-component-static-submenu',
                    content: 'Open submenu',
                },
            ],
        },
    ];

    var customAsyncTemplate = `
        <button aria-owns='custom-async-dd' aria-haspopup='true' class='aui-button aui-dropdown2-trigger' type='button'>Custom async</button>
        <aui-dropdown-menu src='/custom-async-dropdown' id='custom-async-dd'></aui-dropdown-menu>`;

    return {
        serverResponse,
        noSectionLabelResponse,
        opensSubmenuResponse,
        customAsyncTemplate,
    };
});
