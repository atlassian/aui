window.colorPickers = {
    headerBgColor: '--atl-theme-header-bg-color',
    headerItemTextColor: '--atl-theme-header-item-text-color',
    headerPrimaryButtonBgColor: '--atl-theme-header-primary-button-bg-color',
    headerPrimaryButtonTextColor: '--atl-theme-header-primary-button-text-color',
};

window.initWrmResourcesInRefapp = function () {
    if ('WRM' in window) {
        console.info(
            'WRM is defined: running in the Refapp context; load aui-design-tokens-themes'
        );

        window.WRM.require('wr!com.atlassian.auiplugin:aui-design-tokens-themes', () => {
            AJS.DesignTokens.setGlobalTheme({ colorMode: 'light' });
        });
    }
};
