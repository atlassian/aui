// In the ideal world, we'd wrap this in DOMContentLoaded, but for some reason that doesn't work when run inside Cypress.

const urlParams = new URLSearchParams(window.location.search);

Object.entries(window.colorPickers).forEach(([name, varName]) => {
    const colorValue = urlParams.get(name);

    const colorPreview = document.querySelector(`[data-color-name="${name}"]`);

    if (colorValue && colorValue !== 'null') {
        colorPreview.style.backgroundColor = colorValue;

        document.documentElement.style.setProperty(varName, colorValue);
    } else {
        colorPreview.classList.add('uncustomized');
    }
});
document.querySelector('.preset').classList.remove('hide');

window.initWrmResourcesInRefapp();
