function convertToBase64(imageFile) {
    return new Promise((resolve) => {
        const reader = new FileReader();
        reader.onloadend = (e) => {
            resolve(e.target.result);
        };
        reader.readAsDataURL(imageFile);
    });
}

function applySliderColors() {
    Object.entries(window.colorPickers).forEach(([name, varName]) => {
        const picker = document.querySelector(`[data-color-picker-name="${name}"]`);

        const isThemable = picker.querySelector(`#${name}-themable`).checked;
        const red = picker.querySelector(`.${name}-red`).value;
        const green = picker.querySelector(`.${name}-green`).value;
        const blue = picker.querySelector(`.${name}-blue`).value;
        const colorString = `rgb(${red}, ${green}, ${blue})`;

        const colorPreview = picker.querySelector(`.${name}-colorPreview`);
        colorPreview.style.backgroundColor = colorString;

        if (isThemable) {
            document.documentElement.style.setProperty(varName, colorString);
            colorPreview.classList.remove('uncustomized');
        } else {
            document.documentElement.style.removeProperty(varName);
            colorPreview.classList.add('uncustomized');
        }
    });
}

document.addEventListener('DOMContentLoaded', function () {
    window.initWrmResourcesInRefapp();

    document.querySelectorAll('[data-color-picker-name] input').forEach((input) => {
        input.addEventListener('input', applySliderColors);
    });

    applySliderColors();

    const logoInputElement = document.querySelector('input#logo-themeable');
    logoInputElement.addEventListener('change', async (event) => {
        const [file] = event.target.files;
        const base64 = await convertToBase64(file);

        document
            .querySelector('#dynamic')
            .style.setProperty('--atl-theme-header-logo-image', `url(${base64})`);
    });
});
