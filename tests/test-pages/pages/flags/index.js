require(['aui/flag'], function (flag) {
    function showActionFlag({ closeType, type, duration, ariaLive }) {
        const options = {
            type: type || 'info',
            title: 'Flag with some action',
            body: `
                <p>Auto close type will not work here: ${closeType}</p>
                <ul class="aui-nav-actions-list">
                    <li><a href="#">Action 1</a></li>
                    <li><a href="#">Action 2</a></li>
                </ul>
            `,
            duration,
            ariaLive,
        };
        if (closeType !== '') {
            options.close = closeType;
        }
        flag(options);
    }

    function showPlainFlag({ closeType, type, duration, ariaLive }) {
        const options = {
            type: type || 'info',
            title: 'Nothing to do here',
            body: `
                <p>Close: ${closeType}</p>
            `,
            duration,
            ariaLive,
        };
        if (closeType !== '') {
            options.close = closeType;
        }
        flag(options);
    }

    showActionFlag({ closeType: 'manual' });
    showActionFlag({ closeType: 'manual', type: 'success' });
    showPlainFlag({ closeType: 'manual', type: 'error', closeType: 'auto' });
    showPlainFlag({ closeType: 'manual', type: 'warning', closeType: 'auto' });

    AJS.$('#show-flag').on('click', function () {
        const closeType = AJS.$('#flag-close-type').val();
        const appearanceType = AJS.$('#flag-appearance-type').val();
        const duration = Number(AJS.$('#duration').val());
        const ariaLive = AJS.$('#flag-aria-live').val();
        const alertType = AJS.$('#flag-alert-type').val();

        if (alertType === 'alert') {
            showPlainFlag({ closeType, type: appearanceType, duration, ariaLive });
        }

        if (alertType === 'alertdialog') {
            showActionFlag({ closeType, type: appearanceType, duration, ariaLive });
        }
    });

    AJS.$(document).on('aui-flag-close', function () {
        document.getElementById('event-log').innerHTML += '<p>Flag close event</p>';
    });

    AJS.$('#flag-close-type').on('change', function () {
        const value = AJS.$('#flag-close-type').val();
        const input = AJS.$('#duration');

        switch (value) {
            case 'auto':
                input.prop('disabled', false);
                break;
            default:
                input.prop('disabled', true);
                break;
        }
    });
});
