define('auitest/restfultable-server', function (require) {
    const sinon = require('sinon');
    const asJson = { 'Content-Type': 'application/json' };

    let counter = 1;
    const newId = (seed) => String(seed || '') + counter++;

    const contacts = [];

    const byId = (id) => contacts.filter((c) => c.id === id)[0];
    const byName = (name) => contacts.filter((c) => c.name === name)[0];

    const validate = (contact) => {
        const errors = {};
        const existing = byName(contact.name);
        if (existing && existing.id !== contact.id) {
            errors.name = 'You have another contact with the same name.';
        }
        if (contact.number && isNaN(Number(contact.number))) {
            errors.number = 'Not a valid number';
        }
        return Object.keys(errors).length ? errors : false;
    };

    const url = '/rest/contacts/1.0/contacts';
    const itemUrl = new RegExp(`${url}/(\\d+)`);
    const server = sinon.fakeServer.create();
    server.autoRespond = true;
    server.autoRespondAfter = 100;

    server.respondWith('GET', url, [200, asJson, JSON.stringify(contacts)]);

    server.respondWith('POST', url, function (xhr) {
        const newContact = JSON.parse(xhr.requestBody);
        const errors = validate(newContact);
        if (errors) {
            xhr.respond(400, asJson, JSON.stringify({ errors }));
        } else {
            newContact.id = newId('1100');
            contacts.push(newContact);
            xhr.respond(200, asJson, JSON.stringify(newContact));
        }
    });

    server.respondWith('GET', itemUrl, function (xhr, id) {
        let contact = byId(id);
        if (contact) {
            xhr.respond(200, asJson, JSON.stringify(contact));
        } else {
            xhr.respond(404, asJson, '{"errors":["Not found"]}');
        }
    });

    server.respondWith('PUT', itemUrl, function (xhr, id) {
        const contact = byId(id);
        if (contact) {
            const newContact = Object.assign({}, contact, JSON.parse(xhr.requestBody));
            const errors = validate(newContact);
            if (errors) {
                xhr.respond(400, asJson, JSON.stringify({ errors }));
            } else {
                xhr.respond(200, asJson, JSON.stringify(newContact));
            }
        } else {
            xhr.respond(404, asJson, '{"errors":["Not found"]}');
        }
    });

    server.respondWith('DELETE', itemUrl, function (xhr, id) {
        let contact = byId(id);
        if (contact) {
            let idx = contacts.indexOf(contact);
            contacts.splice(idx, 1);
            xhr.respond(200, asJson, JSON.stringify(contact));
        } else {
            xhr.respond(404, asJson, '{"errors":["Not found"]}');
        }
    });

    return {
        server,
        url,
        itemUrl,
    };
});
