(function (jQuery) {
    const legacyClassName = 'aui-badge';
    const trigger = document.getElementById('badge-converter');
    trigger.addEventListener('change', function () {
        if (trigger.checked) {
            jQuery('.aui-badge').each(function () {
                let classList = Array.from(this.classList).filter(
                    (className) => className !== legacyClassName
                );

                let $el = jQuery('<aui-badge></aui-badge>', { class: classList.join(' ') }).text(
                    this.textContent
                );
                jQuery(this).replaceWith($el);
            });
        } else {
            jQuery('aui-badge').each(function () {
                let classList = Array.from(this.classList);
                classList.push(legacyClassName);

                let $el = jQuery('<span></span>', { class: classList.join(' ') }).text(
                    this.textContent
                );
                jQuery(this).replaceWith($el);
            });
        }
    });
})(window.jQuery);
