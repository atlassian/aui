/* eslint-env node */
const del = require('del');
const gulp = require('gulp');
const gulpWebserver = require('gulp-webserver');
const path = require('path');

const serverOpts = require('./build/flatapp.opts');
const buildPages = require('./build/flatapp.pages');
const copyFrontendAssets = require('./build/flatapp.assets');

const outDir = path.resolve(__dirname, 'dist');

const runWebserver = (opts) =>
    function server() {
        opts = Object.assign(serverOpts, opts);
        return gulp.src('dist', { encoding: false }).pipe(gulpWebserver(opts));
    };

let frontendOpts = {
    outDir,
    isDarkMode: process.argv.indexOf('--darkMode') !== -1,
};
let backendOpts = {
    outDir,
    isDarkMode: process.argv.indexOf('--darkMode') !== -1,
};

const dev = (isDev) =>
    function setDevmode(done) {
        if (isDev) {
            frontendOpts.watch = true;
            backendOpts.watch = true;
        }
        done();
    };

const clean = (done) => del(['.tmp', 'dist']).then(() => done());
const build = gulp.series(
    clean,
    gulp.parallel(
        function buildFrontend(done) {
            copyFrontendAssets(frontendOpts)(done);
        },
        function buildBackend(done) {
            buildPages(backendOpts)(done);
        }
    )
);
const run = gulp.series(dev(false), build, runWebserver({ livereload: false }));
const watch = gulp.series(dev(true), build, runWebserver({ livereload: false }));

module.exports = {
    clean,
    build,
    run,
    watch,
};
