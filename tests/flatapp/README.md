# Flatapp

This document is needed to describe Flatapp and how you can contribute to it.

## Structure

This folder only contains basis for Flatapp. Tests pages are also referenced from Refapp,
so they are located in separate folder (`../test-pages/`). Test pages folder structure reflects
URL they will be available under. If you want to know more, you can check `./build/flatapp.pages.js`.

File `./src/soy/dependencies/wrapper.soy` contains Flatapp layout.
File `./test-pages/pages/index.soy` contains main page (dashboard) for Flatapp.

## Demonstration pages vs Integration pages

**Demonstration pages** are needed to create clear testing environment in which only one
component is present.

Idea is that whenever tests fail on demonstration page - problem is either
in page itself, or in component, which allows to easily detect regressions when component is changed.

**Integration pages** can have several components integrated together which can be good for
showcasing components or uncover more potential issues. Though, it's not that good to keep track
of state for given component.

This separation was introduced in 2022 to allow more transparent a11y testing of components.

## Contributing

### Adding test page

1. Go to `./test-pages/pages/`
2. Decide if your page should sit under `./demonstration` or `./integrationExamples` section
3. Create folder with `index.soy` file under selected section
4. Go to `./test-pages/pages/index.soy` and add link to your page
5. Go to `./tests/a11y/aui-page-list.json` and connect your page to a11y testing
6. Go to `p2/p2-harness/src/main/resources/atlassian-plugin.xml` and add your page to
   resource list for components
7. Go to `cypress/integration/visreg/` and add visreg tests for your page

### Moving page

When moving page to different folder make sure to update places mentioned in previous section
as 4, 5, 6, 7.
Additionally, you need to update namespace name in `index.soy` and all references to represent new folder structure.
