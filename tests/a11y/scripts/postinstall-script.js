(async function () {
    const hasUserAccess = require('./check-user-access');
    if (!(await hasUserAccess())) {
        const config = require('./config');
        const fs = require('fs');
        const path = require('path');
        const targetFilePath = path.resolve(config.targetFilePath);
        const tempFilePath = path.resolve(config.tempFilePath);
        const packageJson = require(targetFilePath);
        const { EOL } = require('os');

        if (fs.existsSync(tempFilePath)) {
            const optionalDependencies = require(tempFilePath);
            packageJson.optionalDependencies = optionalDependencies;

            fs.writeFileSync(targetFilePath, JSON.stringify(packageJson, 0, 4), 'utf8');
            fs.appendFileSync(targetFilePath, EOL, 'utf8');
            console.info(config.colors.info, config.messages.optionalDependenciesRestored);

            if (optionalDependencies) {
                fs.unlinkSync(tempFilePath);
            }
        }
    }
})();
