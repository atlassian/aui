const config = require('./config');

(async function () {
    const hasUserAccess = require('./check-user-access');

    console.info(config.colors.info, config.messages.checkAccess);
    if (!(await hasUserAccess())) {
        console.warn(config.colors.warning, config.messages.noAccess);

        const fs = require('fs');
        const path = require('path');
        const targetFilePath = path.resolve(config.targetFilePath);
        const tempFilePath = path.resolve(config.tempFilePath);
        const packageJson = require(targetFilePath);
        const { EOL } = require('os');

        const { optionalDependencies } = packageJson;
        packageJson.optionalDependencies = undefined;

        console.info(config.colors.info, config.messages.optionalDependenciesRemoved);
        fs.writeFileSync(targetFilePath, JSON.stringify(packageJson, 0, 4), 'utf8');
        fs.appendFileSync(targetFilePath, EOL, 'utf8');

        if (optionalDependencies) {
            fs.writeFileSync(tempFilePath, JSON.stringify(optionalDependencies, 0, 4), 'utf8');
        }
    } else {
        console.info(config.colors.success, config.messages.hasAccess);
    }
})();
