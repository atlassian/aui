module.exports = {
    artifactoryUrl: '',
    tempFilePath: './optional_dependencies.json',
    targetFilePath: './package.json',
    colors: {
        warning: '\x1b[31m%s\x1b[0m',
        success: '\x1b[32m%s\x1b[0m',
        info: '\x1b[33m%s\x1b[0m',
    },
    messages: {
        checkAccess: 'Checking if you have access to Atlassian internal tools...',
        hasAccess: 'Good news! You have access.',
        noAccess:
            'Warning:\tLooks like you do not have access to Atlassian internal tools.\n\t\tThe optional dependencies in package.json are for internal usage only.\n\t\tThey will be skipped during installation.\n',
        optionalDependenciesRemoved: 'Optional dependencies are temporary removed',
        optionalDependenciesRestored: 'Optional dependencies are restored',
    },
};
