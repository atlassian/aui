'use strict';

const fs = require('fs');
const path = require('path');

const BASELINE_PATH = path.join(__dirname, '..', 'baselines/light', 'WCAG_AA');
const BASELINE_FILENAME = 'baseline.json';

function readJsonFromFile(fileName) {
    if (!fs.existsSync(fileName)) {
        console.error('File not found', fileName);
        process.exit(1);
    }
    const contents = fs.readFileSync(fileName);

    return JSON.parse(contents);
}

/**
 * Sorts pages by totalIssue number or, if equal, by name
 * @param pageObj1
 * @param pageObj2
 * @return {number|number}
 */
function sortComparator(pageObj1, pageObj2) {
    if (pageObj1.totalIssues !== pageObj2.totalIssues) {
        return pageObj1.totalIssues < pageObj2.totalIssues ? -1 : 1;
    }

    if (pageObj1.name === pageObj2.name) {
        return 0;
    }

    return pageObj1.name < pageObj2.name ? -1 : 1;
}

function padEnd(input, targetLength, padString) {
    let result = String(input);
    while (result.length < targetLength) {
        result = result + padString;
    }
    return result;
}

function printSection(header, entries, columnLabels, convertEntryToParts) {
    console.log(`${header} (${entries.length}):`);

    const columnWidths = columnLabels.map((label, index) => {
        const maxLength = Math.max(
            label.length,
            ...entries.map((entry) => {
                const parts = convertEntryToParts(entry);

                return String(parts[index]).length;
            }),
        );

        return maxLength;
    });

    function formatLine(parts) {
        const paddedParts = parts.map((part, index) => {
            const width = columnWidths[index];
            return padEnd(part, width, ' ');
        });
        return '    ' + paddedParts.join(' \t');
    }

    console.log(formatLine(columnLabels));

    entries.forEach((entry) => {
        const parts = convertEntryToParts(entry);
        console.log(formatLine(parts));
    });

    console.log(''); // new line to separate sections
}

const suffix = '_WCAG2AA';
const convertedComponentDefinitions = [
    { componentName: 'Button', pageName: 'Component Button' },
    { componentName: 'Single select', pageName: 'Component Single Select' },
    { componentName: 'Dropdown', pageName: 'Component Dropdown 2' },
    { componentName: 'Checkbox', pageName: 'Checkbox' },
    { componentName: 'Inline Dialog', pageName: 'Component Inline Dialog 2' },
    { componentName: 'Textarea', pageName: 'Textarea' },
    { componentName: 'Icon', pageName: 'Icon' },
    { componentName: 'Avatar', pageName: 'Component Avatar' },
    { componentName: 'Lozenge', pageName: 'Lozenge' },
    { componentName: 'Banner', pageName: 'Banner' },
];
const skippedIssueCodes = ['color-contrast'];

/**
 * type BenchmarkPage = {
 *   componentName: string; // how we call the component
 *   pageName: string; // same as a11y-runner page name
 *   nSkippedIssues: number; // issues which we are allowing now e.g. color contrast
 *   nRelevantIssues: number; // number of issues that are not skipped
 * }
 *
 * @param baselineObj: representation of baselines/WCAG_AA/baseline.json
 * @return BenchmarkPage[]
 */
function collectBenchmarkPageStats(baselineObj) {
    const notFoundPages = [];

    const result = convertedComponentDefinitions.map((definition) => {
        const { componentName, pageName } = definition;

        const page = baselineObj.pages.find((thatPage) => thatPage.name === `${pageName}${suffix}`);
        if (!page) {
            notFoundPages.push(pageName);
            return null;
        }

        const pageDetails = readJsonFromFile(
            path.join(BASELINE_PATH, 'details', `${pageName}${suffix}.json`),
        );

        const nSkippedIssues = pageDetails.issues.filter((issue) => {
            return skippedIssueCodes.includes(issue.code);
        }).length;
        const nRelevantIssues = page.totalIssues - nSkippedIssues;

        return {
            componentName,
            pageName: page.name,
            nSkippedIssues,
            nRelevantIssues,
        };
    });

    if (notFoundPages.length > 0) {
        throw new Error(`Benchmark pages not found in baseline: ${JSON.stringify(notFoundPages)}`);
    }

    return result;
}

function main() {
    const baselineObj = readJsonFromFile(`${BASELINE_PATH}/${BASELINE_FILENAME}`);

    const pagesWithoutIssues = baselineObj.pages
        .filter((page) => page.totalIssues === 0)
        .sort(sortComparator);

    printSection('Pages without issues', pagesWithoutIssues, ['Name'], (page) => {
        return [page.name];
    });

    const pagesWithIssues = baselineObj.pages
        .filter((page) => page.totalIssues !== 0)
        .sort(sortComparator);

    printSection('Pages with issues', pagesWithIssues, ['Issues', 'Name'], (page) => {
        return [page.totalIssues, page.name];
    });

    const benchmarkPages = collectBenchmarkPageStats(baselineObj);
    printSection(
        'Benchmark pages',
        benchmarkPages,
        ['Relevant', 'Skipped', 'Component', 'Page'],
        (benchmarkPage) => {
            return [
                benchmarkPage.nRelevantIssues,
                benchmarkPage.nSkippedIssues,
                benchmarkPage.componentName,
                benchmarkPage.pageName,
            ];
        },
    );
}

main();
