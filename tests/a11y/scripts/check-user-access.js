const fetch = require('node-fetch');
const artifactoryUrl = 'https://packages.atlassian.com/artifactory/atlassian-npm';

function hasUserAccess() {
    return new Promise((resolve) => {
        return fetch(artifactoryUrl, { method: 'HEAD' }).then((res) => {
            resolve(res.statusCode === 200);
        });
    });
}

module.exports = hasUserAccess;
