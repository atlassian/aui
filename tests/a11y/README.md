# a11y-runner tests

This folder contains a set of scripts to integrate a11y-runner with AUI's
Flatapp and to analyze its output.

## generate-page-report

A script that reads the latest baseline produced by a11y-runner and outputs
three sections to the standard output:

- List of pages without issues
- List of pages with issues, along with amount of issues
- List of benchmark pages for selected components

Selected components are those that underwent accessibility improvements. Each
of those components has a benchmark page (usually the Flatapp's demonstration
page) that represents only that component. If a11y-runner reports zero issues
for that page, we consider it an indicator that the component itself follows
the accessibility guidelines.

Certain issues are marked as "skipped": this means that we are aware of them
and have decided to address these issues in a more holistic way rather than the
per-component basis; this need arose specifically for the color contrast: the
approach to palettes has to be done with the view of many components at once.
