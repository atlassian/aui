import globals from 'globals';
import path from 'node:path';
import { fileURLToPath } from 'node:url';
import js from '@eslint/js';
import { FlatCompat } from '@eslint/eslintrc';
import mocha from 'eslint-plugin-mocha';
import cypress from 'eslint-plugin-cypress';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const compat = new FlatCompat({
    baseDirectory: __dirname,
    recommendedConfig: js.configs.recommended,
    allConfig: js.configs.all,
});

export default [
    {
        ignores: [
            '**/*.min.*',
            '**/.tmp/**/*',
            '**/dist/**/*',
            '**/js/jquery/jquery.*.js',
            '**/js-vendor/**/*.js',
            '**/lib/**/*',
            '**/node_modules/**/*',
            '**/target/**/*',
            '**/tests/reports/**/*',
            '.git/**/*',
            '.idea/**/*',
            '.vscode/**/*',
            'packages/core/entry/token-themes-generated/',
            'eslint.config.mjs',
            'lint-staged.config.js',
            'tests/integration/coverage/**',
        ],
    },
    ...compat.extends('@atlassian/atlassian-fecq', 'prettier'),
    {
        files: ['**/*'],
        languageOptions: {
            globals: {
                ...Object.fromEntries(Object.entries(globals.browser).map(([key]) => [key, 'off'])),
                ...Object.fromEntries(Object.entries(globals.amd).map(([key]) => [key, 'off'])),
                ...Object.fromEntries(Object.entries(globals.jquery).map(([key]) => [key, 'off'])),
                ...globals.node,
                window: true,
                document: true,
            },

            ecmaVersion: 2018,
            sourceType: 'module',

            parserOptions: {
                ecmaFeatures: {
                    jsx: true,
                    modules: true,
                    experimentalObjectRestSpread: true,
                },
            },
        },
        rules: {
            'camelcase': [
                1,
                {
                    properties: 'never',
                },
            ],

            'curly': [1, 'all'],
            'dot-notation': 1,

            'no-debugger': 2,

            'no-empty': [
                2,
                {
                    allowEmptyCatch: true,
                },
            ],

            'no-multi-str': 1,

            'no-unused-vars': [
                'error',
                {
                    argsIgnorePattern: '^_',
                    args: 'after-used',
                },
            ],

            'quotes': [
                1,
                'single',
                {
                    avoidEscape: true,
                },
            ],
        },
    },
    {
        files: ['tests/test-pages/**/*'],
        languageOptions: {
            globals: {
                ...globals.browser,
                ...globals.amd,
                // We occasionally use and test soy templates.
                aui: 'readonly',
                // Because we occasionally test the exported namespace.
                AJS: 'readonly',
                // We use sinon as a test server in a number of places.
                sinon: 'readonly',
            },
        },
    },
    {
        files: ['tests/integration/**/*'],
        plugins: {
            mocha,
        },
        languageOptions: {
            globals: {
                ...globals.node,
                ...globals.amd,
                ...globals.mocha,
                aui: false,
                AJS: false,
                sinon: false,
                amdRequire: true,
                chai: false,
                assert: false,
                expect: false,
            },
        },
        rules: {
            'mocha/no-exclusive-tests': 2,
        },
    },
    {
        files: ['tests/integration/unit/aui/**/*'],
        languageOptions: {
            globals: {
                ...globals.browser,
            },
        },
    },
    {
        files: ['tests/a11y-unit-tests/**/*'],
        plugins: {
            mocha,
        },
        languageOptions: {
            globals: {
                ...globals.node,
                ...globals.amd,
                ...globals.mocha,
                aui: false,
                AJS: false,
                sinon: false,
                amdRequire: true,
                chai: false,
                expect: false,
            },
        },
        rules: {
            'mocha/no-exclusive-tests': 2,
        },
    },
    {
        files: ['tests/a11y-unit-tests/tests/**/*'],
        languageOptions: {
            globals: {
                ...globals.browser,
            },
        },
    },
    {
        files: ['packages/deprecated/template/test/**/*'],
        languageOptions: {
            globals: {
                ...globals.node,
                ...globals.mocha,
                ...globals.jquery,
                sinon: true,
            },
        },
    },
    {
        files: ['packages/deprecated/dropdown1/**/*'],
        languageOptions: {
            globals: {
                ...globals.browser,
                jQuery: 'readonly',
            },

            ecmaVersion: 2015,
            sourceType: 'module',

            parserOptions: {
                ecmaFeatures: {
                    modules: true,
                },
            },
        },
    },
    {
        files: ['packages/deprecated/dropdown1/test/**/*'],
        languageOptions: {
            globals: {
                ...globals.node,
                ...globals.mocha,
                expect: false,
            },
        },
    },
    {
        files: ['packages/core/src/**/*'],
        languageOptions: {
            globals: {
                ...Object.fromEntries(Object.entries(globals.node).map(([key]) => [key, 'off'])),
                ...globals.amd,
                ...globals.browser,
                ...globals.jquery,
                AJS: false,
                Modernizr: false,
                skate: false,
                aui: false,
            },
        },
        rules: {
            strict: [0, 'global'],
        },
    },
    {
        files: ['packages/core/src/**/*'],
        languageOptions: {
            globals: {
                ...Object.fromEntries(Object.entries(globals.node).map(([key]) => [key, 'off'])),
                ...globals.amd,
                ...globals.browser,
                ...globals.jquery,
                AJS: false,
                Modernizr: false,
                skate: false,
                aui: false,
            },
        },
        rules: {
            strict: [0, 'global'],
        },
    },
    {
        files: ['packages/core/src/js/aui/internal/i18n/**/*'],
        rules: {
            quotes: 0,
        },
    },
    {
        files: ['p2/**/*'],
        languageOptions: {
            globals: {
                ...globals.browser,
                ...globals.amd,
                // We occasionally use and test soy templates.
                aui: false,
                // Because we occasionally test the exported namespace.
                AJS: false,
                // We use sinon as a test server in a number of places.
                sinon: false,
            },
        },
    },
    {
        files: ['cypress/**/*'],
        plugins: {
            cypress,
        },
        languageOptions: {
            globals: {
                ...globals.node,
                ...cypress.environments.globals.globals,
            },
        },
    },
];
