// ***********************************************************
// This example support/e2e.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands';

// Alternatively you can use CommonJS syntax:
// require('./commands')

// Hide all carets when text fields are focussed
Cypress.on('window:before:load', (window) => {
    const { document } = window;
    const el = document.createElement('style');
    el.innerHTML = 'html { caret-color: transparent !important; }';
    document.head.appendChild(el);
});
