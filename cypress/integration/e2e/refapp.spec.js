import layeringScenarios from '../layering-e2e.spec';
import i18nScenarios from './scenarios/i18n.scenario';

describe('e2e - p2 plugin', function () {
    it('renders a test page index', function () {
        cy.visit('/', { failOnStatusCode: true, timeout: 40000 });
    });

    i18nScenarios();
    layeringScenarios();
});
