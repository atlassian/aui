export default () => {
    it('should do internal AUI translations', function () {
        cy.visit('keyboardshortcuts/', { failOnStatusCode: true, timeout: 20000 });
        // if the format function isn't exported then this should throw an error and fail the test
        cy.window().then((win) => {
            // Testing on the assumption that AJS#whenIType is something in AUI that requires format
            win.AJS.whenIType('?').goTo('http://www.example.com');
            win.AJS.whenIType('gghf').execute(() => alert('good luck, have fun!'));
        });
    });
};
