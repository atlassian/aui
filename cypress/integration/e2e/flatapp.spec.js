import layeringScenarios from '../layering-e2e.spec';
import i18nScenarios from './scenarios/i18n.scenario';

describe('e2e - flatapp', function () {
    it('renders a test page index', function () {
        cy.visit('/', { failOnStatusCode: true, timeout: 20000 });
    });

    i18nScenarios();
    layeringScenarios();
});
