/// <reference types="Cypress" />

const variants = {
    'experimental/avatar/sizes/': ['#content'],
    'auiBadge/': ['#content'],
    'demonstration/avatars/': [
        {
            capture: '#content',
            setup: () => cy.get('#avatar-group-test button').click(),
        },
    ],
    'demonstration/banners/': [
        {
            capture: '#page',
            setup: () =>
                ['announcement', 'warning', 'error'].forEach((type) =>
                    cy.get(`#show-${type}-banner`).click()
                ),
        },
    ],
    'demonstration/buttons/': ['#content'],
    'integrationExamples/buttons/': ['#content'],
    'demonstration/datePicker/': [
        {
            capture: '#date-picker10',
            suffix: () => '.aui-datepicker-dialog[aria-hidden=false]',
            setup: () => cy.get('#test-default-always').click(),
        },
    ],
    'i18n/fontStacks/': ['#content'],
    'demonstration/icons/': ['#content'],
    'demonstration/inlineDialog2/': [
        {
            capture: 'body',
            suffix: (selector) => selector + '_onload',
        },
        {
            capture: 'body',
            setup: () => {
                // persistent dialog
                cy.get('[aria-controls="inline-dialog2-18"]').click();
                // inside large text
                cy.get('[aria-controls="aui-5144-dialog"]').click();
            },
            suffix: (selector) => selector + '_variants',
        },
    ],
    'demonstration/lozenges/': ['#content'],
    'experimental/pageLayout/layouts/navigation/default/': ['#content'],
    'experimental/pageLayout/layouts/groups/': ['#content'],
    'experimental/pageLayout/header/auiHeader/': ['#nav1', '#nav2', '#nav3', '#nav4'],
    'experimental/pageLayout/header/pageHeaderVariations/': ['#content'],
    'tables/': ['#basic', '#nested'],
    'demonstration/toolbar2/': [
        {
            capture: '#content',
            setup: () => {
                // make sure buttons are loaded
                cy.get('#toolbar2-test-container').should('not.be.empty');
            },
        },
    ],
};

Object.entries(variants).forEach(([path, selectors]) => {
    context(path, () => {
        const pathPrefix = path.replace(/\//g, '_');

        beforeEach(() => {
            cy.visit(path);
        });

        selectors.forEach((val) => {
            if (typeof val === 'string') {
                val = { capture: val };
            }
            const { capture, setup = () => true, suffix = (selector) => selector } = val;

            it(capture, () => {
                setup();
                cy.get(capture).matchImageSnapshot(`${pathPrefix}_${suffix(capture)}`);
            });
        });
    });
});
