/// <reference types="Cypress" />

const variants = {
    'docs/typography.html': ['.aui-page-panel-nav', '.aui-page-panel-content'],
    'docs/app-header.html': ['.aui-flatpack-example'],
    'docs/banner-example-iframe.html': [
        {
            capture: '.aui-flatpack-example',
            setup: () => cy.contains('.aui-banner', 'license has expired'),
        },
    ],
    'docs/dropdown.html': [
        {
            capture: '#simple-example',
            setup: () => {
                cy.get('#simple-example .aui-dropdown2-trigger[aria-controls=example-dropdown]')
                    .focus()
                    .type('{enter}');
                cy.focused().type('{downArrow}');
                cy.focused().type('{downArrow}');
                cy.focused().type('{downArrow}');
                cy.focused().type('{downArrow}');
            },
        },
        {
            capture: '#submenu-example',
            setup: () => {
                cy.get('#submenu-example .aui-dropdown2-trigger[aria-controls=has-submenu]')
                    .focus()
                    .type('{enter}');
                cy.focused().type('{downArrow}');
                cy.focused().type('{downArrow}');
                cy.focused().type('{downArrow}');
                cy.focused().type('{downArrow}');
                cy.focused().type('{enter}');
            },
        },
    ],
    'docs/flag.html': [
        {
            capture: '#aui-flag-container',
            setup: () => {
                cy.viewport(1000, 1000);
                cy.get('#next-flag-show-button').click().click().click().click().click();
            },
        },
    ],
    'docs/forms.html': ['.aui-flatpack-example'],
    'docs/icons.html': [
        {
            capture: '#icons-list',
            setup: () => cy.get('#icons-list .aui-icon-small'),
        },
    ],
    'docs/labels.html': ['.aui-flatpack-example'],
    'docs/lozenges.html': ['.aui-flatpack-example'],
    'docs/messages.html': ['.aui-flatpack-example'],
    'docs/navigation.html': [
        '#horizontal-nav-example',
        '#vertical-nav-example',
        '#pagination-example',
        '#actions-list-example',
    ],
    'docs/page-header.html': ['#pageheader-example'],
    'docs/progress-tracker.html': [
        '#default-progress-tracker-example',
        '#inverted-progress-tracker-example',
    ],
    'docs/tabs.html': ['.aui-flatpack-example'],
    'docs/toolbar2.html': ['.aui-flatpack-example'],
    'docs/dialog2.html': ['#static-dialog-example'],
    'docs/progress-indicator.html': ['#progress-example-attrs-and-props'],
    'docs/toggle-button.html': [
        {
            capture: '#default-toggle-example',
            suffix: () => 'aui-docs-example',
        },
    ],
};

Object.entries(variants).forEach(([path, selectors]) => {
    context(path, () => {
        const pathPrefix = path.replace(/\//g, '_');

        beforeEach(() => {
            cy.visit(path);
        });

        selectors.forEach((val) => {
            if (typeof val === 'string') {
                val = { capture: val };
            }
            const { capture, setup = () => true, suffix = (selector) => selector } = val;

            it(capture, () => {
                setup();
                cy.get(capture).matchImageSnapshot(`${pathPrefix}_${suffix(capture)}`);
            });
        });
    });
});
