export function getSnapshotPrefix(path) {
    return path.replace(/\//g, '_');
}

export function createSnapshotMatcherFn(basePath, snapshotOptions) {
    return (selector, detail = '') => {
        cy.get(selector).matchImageSnapshot(
            `${getSnapshotPrefix(basePath)}_${selector}${detail}`,
            snapshotOptions
        );
    };
}
