import { snapshotOptions } from '../_config.js';
import { getSnapshotPrefix, createSnapshotMatcherFn } from '../_utils';

const basePath = 'demonstration/select2/jsm';
const shouldMatchImageSnapshot = createSnapshotMatcherFn(basePath, snapshotOptions);

describe('demonstration/select2/jsm', () => {
    it('should highlight query', () => {
        const dropdownSelector = '#select2-drop';
        const inputSelector = '#s2id_autogen1_search';
        const selectSelector = '#s2id_test-case-profile-language';
        cy.visit(basePath);

        cy.get(selectSelector).first().click();
        cy.get(inputSelector).type('spa'); // beginning of Spanish
        shouldMatchImageSnapshot(
            dropdownSelector,
            `${getSnapshotPrefix(basePath)}_${selectSelector}`
        );
    });

    it('should load data from api', () => {
        const dropdownSelector = '#select2-drop';
        const inputSelector = '#s2id_autogen3_search';
        const selectSelector = '#test-case-user-picker-wrapper #s2id_autogen2';
        cy.visit(basePath);

        cy.get(selectSelector).first().click();
        shouldMatchImageSnapshot(
            selectSelector,
            `${getSnapshotPrefix(basePath)}_${selectSelector}`
        );
        shouldMatchImageSnapshot(dropdownSelector, 'empty_search');

        cy.get(inputSelector).type('git');
        cy.get('#select2-result-label-13').should('be.visible');
        shouldMatchImageSnapshot(dropdownSelector, 'searched');

        cy.get(inputSelector).type('adjfhaviauhfiaodfh');
        cy.get('#select2-result-label-42').should('be.visible');
        shouldMatchImageSnapshot(dropdownSelector, 'no_results');
    });

    it('should customise select and dropdown', () => {
        const dropdownSelector = '#select2-drop';
        const selectSelector = '#s2id_test-case-custom-styling';
        const containerSelector = '#test-case-custom-styling-wrapper .section-content';

        cy.visit(basePath);

        shouldMatchImageSnapshot(containerSelector);
        cy.get(selectSelector).first().click();
        shouldMatchImageSnapshot(dropdownSelector, 'dropdown');
    });

    it('should use separator', () => {
        const dropdownSelector = '#select2-drop';
        const inputSelector = '#s2id_autogen5 input';
        const selectSelector = '#test-case-separators-wrapper #s2id_autogen5';
        const contentSelector = '#test-case-separators-wrapper .section-content';
        cy.visit(basePath);

        cy.get(selectSelector).first().click();
        shouldMatchImageSnapshot(dropdownSelector, 'tags');

        cy.get(inputSelector).type('yell ow ');

        cy.focused().blur();
        shouldMatchImageSnapshot(contentSelector, 'values');
    });

    it('should restrict input characters', () => {
        const dropdownSelector = '#select2-drop';
        const inputSelector = '#s2id_autogen7_search';
        const selectSelector = '#s2id_test-case-input-restrictions';

        cy.visit(basePath);

        cy.get(selectSelector).first().click();
        shouldMatchImageSnapshot(dropdownSelector);

        cy.get(inputSelector).type('polish'); // should cut at 5 `polis`
        shouldMatchImageSnapshot(dropdownSelector, 'searched');
    });

    it('should create dynamic option', () => {
        const dropdownSelector = '#select2-drop';
        const inputSelector = `${dropdownSelector} input`;
        const selectSelector = '#test-case-search-choice-wrapper .select2-container';

        cy.visit(basePath);

        cy.get(selectSelector).first().click();
        cy.get(inputSelector).type('pin'); // should show pink option
        shouldMatchImageSnapshot(dropdownSelector);
    });

    it('should interact programmatically', () => {
        const dropdownSelector = '#select2-drop';
        const selectSelector = '#s2id_test-case-dynamic-actions';
        const containerSelector = '#test-case-dynamic-actions-wrapper';
        const openButtonSelector = `${containerSelector} button#open`;
        const closeButtonSelector = `${containerSelector} button#close`;
        const enableButtonSelector = `${containerSelector} button#enable`;
        const disableButtonSelector = `${containerSelector} button#disable`;

        cy.visit(basePath);

        cy.get(openButtonSelector).click();
        cy.get(dropdownSelector).should('be.visible');
        cy.get(closeButtonSelector).click({ force: true }); // can be hidden under
        cy.get(dropdownSelector).should('not.exist');

        cy.get(disableButtonSelector).click();
        cy.get(openButtonSelector).click();
        cy.get(dropdownSelector).should('not.exist');

        cy.get(enableButtonSelector).click();
        cy.get(openButtonSelector).click();
        cy.get(dropdownSelector).should('be.visible');

        cy.get(closeButtonSelector).click({ force: true });
        cy.get('input#set-value').type('de');

        cy.get(selectSelector)
            .parent()
            .matchImageSnapshot(
                `${getSnapshotPrefix(basePath)}_${selectSelector}`,
                snapshotOptions
            );
    });
});
