import { snapshotOptions } from '../_config.js';
import { createSnapshotMatcherFn } from '../_utils';

const basePath = 'demonstration/select2/bamboo';
const shouldMatchImageSnapshot = createSnapshotMatcherFn(basePath, snapshotOptions);

const select2ContainerSelector = '.select2-container';

describe(basePath, () => {
    it('should render the page', () => {
        const selector = '#pagecontent';

        cy.visit(basePath);

        shouldMatchImageSnapshot(selector);
    });

    describe('autocomplete', () => {
        const parentSelector = '#bamboo-autocomplete-container';
        const formSelector = '#bamboo-autocomplete-label';
        const resultSelector = '#bamboo-autocomplete-results';
        const actionsSelector = '#bamboo-autocomplete-actions';
        const selectSelector = `${parentSelector} ${select2ContainerSelector}`;

        it('"Select first" should fill the first item into select box', () => {
            cy.visit(basePath);

            cy.get(actionsSelector).contains('Select first').click();

            shouldMatchImageSnapshot(formSelector, '_after-select-first');
        });

        it('"Get selected data" should fill the resultBox with object data', () => {
            cy.visit(basePath);

            cy.get(actionsSelector).contains('Select first').click();
            cy.get(actionsSelector).contains('Get selected data').click();

            shouldMatchImageSnapshot(resultSelector, '_show-selected-data');
        });

        it('"Clear results" should force the select and result to be empty', () => {
            cy.visit(basePath);

            cy.get(actionsSelector).contains('Select random').click();
            cy.get(actionsSelector).contains('Clear results').click();

            shouldMatchImageSnapshot(parentSelector, '_after-clear-results');
        });

        it('should open the select after clicking on it', () => {
            cy.visit(basePath);

            cy.get(selectSelector).click();

            shouldMatchImageSnapshot(parentSelector, '_select-click');
        });

        it('should not allow to open the select if it was first disabled', () => {
            cy.visit(basePath);

            cy.get(actionsSelector).contains('Disable').click();
            cy.get(selectSelector).click();

            shouldMatchImageSnapshot(parentSelector, '_select-click_disabled');
        });
    });

    describe('simple-select2', () => {
        const parentSelector = '#bamboo-simple-select2';

        it('should open first select with some items marked with titles', () => {
            cy.visit(basePath);

            cy.get(`${parentSelector} [data-test="single"] ${select2ContainerSelector}`).click();

            shouldMatchImageSnapshot(parentSelector, '_first-select-opened');
        });

        it('should open second select with some items marked with titles', () => {
            cy.visit(basePath);

            cy.get(`${parentSelector} [data-test="multiple"] ${select2ContainerSelector}`).click();

            shouldMatchImageSnapshot(parentSelector, '_second-select-opened');
        });
    });
});
