module.exports = {
    headless: true,
    video: false,
    e2e: {
        setupNodeEvents(on, config) {
            return require('../plugins/index.js')(on, config);
        },
    },
};
