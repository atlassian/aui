const commonConfig = require('./base-visreg.config');

module.exports = {
    ...commonConfig,
    reporterOptions: {
        mochaFile: 'results-docs.xml',
        toConsole: false,
    },
    e2e: {
        ...commonConfig.e2e,
        baseUrl: 'http://localhost:8000',
        specPattern: ['cypress/integration/visreg/docs.spec.js'],
    },
};
