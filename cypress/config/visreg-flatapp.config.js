const commonConfig = require('./base-visreg.config');

module.exports = {
    ...commonConfig,
    reporterOptions: {
        mochaFile: 'results-flatapp.xml',
        toConsole: false,
    },
    e2e: {
        ...commonConfig.e2e,
        baseUrl: 'http://localhost:7000',
        specPattern: ['cypress/integration/visreg/flatapp/**/*.spec.js'],
    },
};
