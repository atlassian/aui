const commonConfig = require('./base.config');

module.exports = {
    ...commonConfig,
    e2e: {
        ...commonConfig.e2e,
        baseUrl: 'http://localhost:9999/ajs/plugins/servlet/ajstest/test-pages/pages',
        specPattern: ['cypress/integration/e2e/refapp.spec.js'],
    },
    reporter: 'junit',
    reporterOptions: {
        mochaFile: 'results-e2e-refapp.xml',
        toConsole: false,
    },
};
