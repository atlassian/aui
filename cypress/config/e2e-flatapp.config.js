const commonConfig = require('./base.config');

module.exports = {
    ...commonConfig,
    e2e: {
        ...commonConfig.e2e,
        baseUrl: 'http://localhost:7000',
        specPattern: ['cypress/integration/e2e/flatapp.spec.js'],
    },
};
