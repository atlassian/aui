const commonConfig = require('./base-visreg.config');

module.exports = {
    ...commonConfig,
    reporterOptions: {
        mochaFile: 'results-refapp.xml',
        toConsole: false,
    },
    e2e: {
        ...commonConfig.e2e,
        baseUrl: 'http://localhost:9999/ajs/plugins/servlet/ajstest/test-pages/pages',
        specPattern: ['cypress/integration/visreg/refapp.spec.js'],
    },
};
