const baseConfig = require('./base.config');

module.exports = {
    ...baseConfig,
    reporter: 'junit',
    env: {
        updateSnapshots: true,
    },
};
