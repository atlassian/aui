const fs = require('fs');
const { DOMParser, XMLSerializer } = require('xmldom');
const xpath = require('xpath');

const xmlFilePath = process.argv[2];
if (!xmlFilePath) {
    console.error('Please provide the path to the webpacked-modules.xml file as a parameter.');
    process.exit(1);
}

const lookandfeelStylesWebResource = 'com.atlassian.lookandfeel.lookandfeel-plugin:styles';
const webResourcesToInject = [
    'aui-reset',
    'aui-design-tokens-base-themes-css',
    'aui-design-tokens-base-themes',
];

console.log(
    `Injecting WebResource Look and Feel dependencies into ${xmlFilePath} / ${webResourcesToInject} ...`
);
const xmlContent = fs.readFileSync(xmlFilePath, 'utf-8');
const doc = new DOMParser().parseFromString(xmlContent, 'application/xml');

const getWebResourceNode = (webResourceKey) => {
    const nodes = xpath.select(`//*[@key="${webResourceKey}"]`, doc);
    if (nodes.length !== 1) {
        throw new Error(`Look and Feel injection error: Failed to locate ${webResourceKey}`);
    }

    return nodes[0];
};

const injectLnfDependency = (targetNode) => {
    console.log(`Injecting into ${targetNode.getAttribute('key')} ...`);

    const lnfDependencyNode = doc.createElement('dependency');
    lnfDependencyNode.appendChild(doc.createTextNode(lookandfeelStylesWebResource));

    const firstStandardChild = targetNode.firstChild;
    targetNode.insertBefore(doc.createTextNode('\n'), firstStandardChild);
    targetNode.insertBefore(lnfDependencyNode, firstStandardChild);
};

webResourcesToInject.forEach((webResourceKey) => {
    console.log(`Handling ${webResourceKey} ...`);

    const webResourceNode = getWebResourceNode(webResourceKey);

    injectLnfDependency(webResourceNode);

    for (let i = 0; i < webResourceNode.childNodes.length; i++) {
        const childNode = webResourceNode.childNodes[i];

        if (childNode.nodeName === 'dependency' && childNode.textContent.includes('splitchunk')) {
            const shortWebResourceName = childNode.textContent.split(':')[1];
            const actualSplitchunkNode = getWebResourceNode(shortWebResourceName);

            injectLnfDependency(actualSplitchunkNode);
        }
    }
});

const updatedXmlContent = new XMLSerializer().serializeToString(doc);
fs.writeFileSync(xmlFilePath, updatedXmlContent, 'utf-8');

console.log('Injection completed.');
