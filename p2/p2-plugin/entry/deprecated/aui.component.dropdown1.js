import '@atlassian/aui-legacy-dropdown/src/drop-down.less';
import { default as dropDown } from '@atlassian/aui-legacy-dropdown/src/drop-down.js';
import globalize from '@atlassian/aui/src/js/aui/internal/globalize';

// to make the globals keep working
globalize('dropDown', dropDown);

export { dropDown };
