/* global console */
import _ from 'our/underscore';
export default _;

console.warn &&
    console.warn('Use of `window._` through AUI is deprecated and will be removed in AUI 10.0.0');

let ourUnderscore = _;
window._ = ourUnderscore;
