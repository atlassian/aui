package com.atlassian.test.spring;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.test.lookandfeel.P2LookAndFeelVariablesProvider;
import com.atlassian.webresource.api.WebResourceManager;
import com.atlassian.webresource.api.WebResourceUrlProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

import com.atlassian.lookandfeel.spi.internal.LookAndFeelVariablesProvider;
import org.osgi.framework.ServiceRegistration;
import org.springframework.beans.factory.FactoryBean;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;

@Configuration
public class OsgiImports {

    @Bean
    public PluginAccessor pluginAccessor() {
        return importOsgiService(PluginAccessor.class);
    }

    @Bean
    public SoyTemplateRenderer soyTemplateRenderer() {
        return importOsgiService(SoyTemplateRenderer.class);
    }

    @Bean
    public WebResourceManager webResourceManager() {
        return importOsgiService(WebResourceManager.class);
    }

    @Bean
    public WebResourceUrlProvider webResourceUrlProvider() {
        return importOsgiService(WebResourceUrlProvider.class);
    }

    @Bean
    public LookAndFeelVariablesProvider lookAndFeelVariablesProvider(WebResourceUrlProvider webResourceUrlProvider) {
        return new P2LookAndFeelVariablesProvider(webResourceUrlProvider);
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportLookAndFeelVariablesProvider(LookAndFeelVariablesProvider lookAndFeelVariablesProvider) {
        return exportOsgiService(lookAndFeelVariablesProvider, as(LookAndFeelVariablesProvider.class));
    }

}
