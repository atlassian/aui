package com.atlassian.test.lookandfeel;

import com.atlassian.lookandfeel.spi.internal.LookAndFeelVariablesProvider;
import com.atlassian.webresource.api.WebResourceUrlProvider;

import java.util.HashMap;
import java.util.Map;

import static com.atlassian.webresource.api.UrlMode.AUTO;

public class P2LookAndFeelVariablesProvider implements LookAndFeelVariablesProvider {
    public P2LookAndFeelVariablesProvider(WebResourceUrlProvider webResourceUrlProvider) {
        this.webResourceUrlProvider = webResourceUrlProvider;
    }

    private final WebResourceUrlProvider webResourceUrlProvider;
    private static final String LOGO_WIDTH = "50px";

    @Override
    public Map<String, String> getLightModeVariables() {
        Map<String, String> result = new HashMap<>();
        result.put("--atl-theme-header-logo-image", getLogoUrl());
        result.put("--atl-theme-header-logo-width", LOGO_WIDTH);
        return result;
    }

    private String getLogoUrl() {
        String logoUrl = webResourceUrlProvider.getStaticPluginResourceUrl("com.atlassian.auinext.p2-harness:test-page-images", "header-img-test.jpg", AUTO);
        return "url('" + logoUrl + "')";
    }

    @Override
    public Map<String, String> getDarkModeVariables() {
        Map<String, String> result = new HashMap<>();
        result.put("--atl-theme-header-logo-image", getLogoUrl());
        result.put("--atl-theme-header-logo-width", LOGO_WIDTH);
        return result;
    }
}
