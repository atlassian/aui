package com.atlassian.test;

public class PluginInfo {
    // It would be nice for this to come in dynamically from the pom file or plugin xml.
    public static final String PLUGIN_KEY = "com.atlassian.auinext.p2-harness";
}
