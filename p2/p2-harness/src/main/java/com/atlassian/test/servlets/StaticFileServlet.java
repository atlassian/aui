package com.atlassian.test.servlets;


import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.atlassian.test.PluginInfo.PLUGIN_KEY;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;

@UnrestrictedAccess
public class StaticFileServlet extends HttpServlet {
    private final Plugin plugin;
    private static final Logger log = LoggerFactory.getLogger(StaticFileServlet.class);

    public StaticFileServlet(PluginAccessor pluginAccessor) {
        this.plugin = pluginAccessor.getPlugin(PLUGIN_KEY);
        requireNonNull(plugin, "plugin");
    }

    private String httpPathToResourcePath(String httpPath) {
        return httpPath.replaceFirst("/testPages/", "").replaceFirst("/test-pages/", "");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final String pathInfo = req.getPathInfo();
        final String path = httpPathToResourcePath(pathInfo);

        log.info("Loading '{}', taken from '{}'", path, pathInfo);

        String type = Files.probeContentType(Paths.get(path));
        if (isNull(type) || type.isBlank()) {
            if (path.endsWith(".html")) {
                type = "text/html";
            } else if (path.endsWith(".js")) {
                type = "text/javascript";
            } else if (path.endsWith(".css")) {
                type = "text/css";
            } else if (path.endsWith(".png")) {
                type = "image/png";
            } else if (path.endsWith(".jpg")) {
                type = "image/jpg";
            } else if (path.endsWith(".svg")) {
                type = "image/svg+xml";
            }
        }

        resp.setContentType(type);

        InputStream in = plugin.getResourceAsStream(path);
        OutputStream out = resp.getOutputStream();
        copyStreams(in, out);
        out.close();
    }

    /**
     * Prevents an unnecessary dependency for the sake of testing. Can be replaced by #transferTo() once Java 9 or
     * higher is the minimum supported
     */
    private static void copyStreams(final InputStream inputStream, final OutputStream outputStream) throws IOException {
        final int EOF = -1;
        final byte[] buffer = new byte[8192];

        int n;
        while (EOF != (n = inputStream.read(buffer))) {
            outputStream.write(buffer, 0, n);
        }
    }
}
