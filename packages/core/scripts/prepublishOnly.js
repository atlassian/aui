const fs = require('fs');
const path = require('path');
const { copySync } = require('fs-extra');

// Path to package.json
const projectDir = path.resolve(__dirname, '..');
const packageJsonPath = path.join(projectDir, 'package.json');

// Read package.json
const packageJson = JSON.parse(fs.readFileSync(packageJsonPath, 'utf8'));

console.log(
    ':: Cleaning `package.json` dependencies from those being linked with the `file:` protocol'
);

let fileDependencies = [];

// Filter out dependencies with "file://" protocol
Object.keys(packageJson.dependencies).forEach((dep) => {
    if (packageJson.dependencies[dep].startsWith('file:')) {
        fileDependencies.push(dep);
        delete packageJson.dependencies[dep];
    }
});

if (fileDependencies.length > 0) {
    // Backup package.json
    fs.copyFileSync(packageJsonPath, `${packageJsonPath}.backup`);
    // Write the modified package.json

    fs.writeFileSync(packageJsonPath, JSON.stringify(packageJson, null, 4), 'utf8');

    console.log(`:: Removed ${fileDependencies.length} dependencies.`);

    // Copy effective file dependencies to the src/js-vendor-effective directory (for transparency and testing purposes)
    const nodeModulesDir = path.resolve(projectDir, '..', '..', 'node_modules');
    fileDependencies.forEach((dep) => {
        console.log(`Including js-vendor-effective dependency ${dep} ...`);
        const sourceDir = path.resolve(nodeModulesDir, dep);
        const targetDir = path.resolve(projectDir, 'src', 'js-vendor-effective', dep);
        copySync(sourceDir, targetDir, (src) => {
            return path.basename(src) !== 'node_modules';
        });
    });
} else {
    console.log(':: No such dependencies found.');
}
