const fs = require('fs');
const path = require('path');

// Path to package.json and its backup
const projectDir = path.resolve(__dirname, '..');
const packageJsonPath = path.join(projectDir, 'package.json');
const backupPath = `${packageJsonPath}.backup`;

// If the backup file exists
if (fs.existsSync(backupPath)) {
    // Restore the original package.json and remove the backup
    fs.copyFileSync(backupPath, packageJsonPath);
    fs.unlinkSync(backupPath);
    console.info(':: Successfully restored `package.json` from `package.json.backup` file.');

    // Remove the js-vendor-effective directory
    fs.rmdirSync(path.resolve(projectDir, 'src', 'js-vendor-effective'), { recursive: true });
    console.info(':: Successfully removed the js-vendor-effective directory.');
} else {
    console.warn(
        ':: The `package.json.backup` file does not exist! Skipping `postpublish` script.'
    );
}
