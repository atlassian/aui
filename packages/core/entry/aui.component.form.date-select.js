import './styles/aui.pattern.forms';
import './aui.component.inline-dialog2'; // so that the inline dialog styles get placed before date picker.
import '@atlassian/aui/src/less/aui-date-picker.less';
export { default as DatePicker, CalendarWidget } from '@atlassian/aui/src/js/aui/date-picker';
