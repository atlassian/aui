import './styles/aui.page.reset';
import './styles/aui.pattern.basic';
import '@atlassian/aui/src/less/layer.less';
// todo AUI-4814: undo the weird relationships between layer, layerManager, and LayerManager.global
export { default as layer } from '@atlassian/aui/src/js/aui/layer';
