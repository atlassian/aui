import './styles/aui.pattern.avatar';
import '@atlassian/aui/src/less/aui-avatars.less';
import { AvatarBadged } from '@atlassian/aui/src/js/aui/avatar-badged';
import { AvatarEl } from '@atlassian/aui/src/js/aui/avatar';
export { AvatarEl, AvatarBadged };
