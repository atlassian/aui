import './aui.component.static-header';
import './aui.component.trigger';
import './aui.component.dropdown2';
import '@atlassian/aui/src/less/aui-header-responsive.less';
import '@atlassian/aui/src/js/aui/header.js';
export { default as Header } from '@atlassian/aui/src/js/aui/header-async';
