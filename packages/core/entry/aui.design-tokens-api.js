// Enables Design Token theme with colorMode='auto' on page load
// Only exposes setGlobalTheme function to save bundle size
export * from '@atlassian/aui/src/js/aui/design-tokens/design-tokens';
