/*
 * This config is used in build/webpack/webpack.parts.js (exports.extractThemesIntoJSFile)
 * It defines which themes from Design tokens library will be bundled into additional resource.
 *
 * This config influences both NPM and P2 plugin builds.
 */

module.exports = ['light', 'dark'];
