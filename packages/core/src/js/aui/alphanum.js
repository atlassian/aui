import globalize from './internal/globalize';

/**
 * Compare two strings in alphanumeric way
 * @method alphanum
 * @param {String} a first string to compare
 * @param {String} b second string to compare
 * @return {Number(-1|0|1)} -1 if a < b, 0 if a = b, 1 if a > b
 */
function alphanum(a, b) {
    a = (a + '').toLowerCase();
    b = (b + '').toLowerCase();

    var chunks = /(\d+|\D+)/g;
    var am = a.match(chunks);
    var bm = b.match(chunks);
    var len = Math.max(am.length, bm.length);

    for (var i = 0; i < len; i++) {
        if (i === am.length) {
            return -1;
        }

        if (i === bm.length) {
            return 1;
        }

        var ad = parseInt(am[i], 10) + '';
        var bd = parseInt(bm[i], 10) + '';

        if (ad === am[i] && bd === bm[i] && ad !== bd) {
            return (ad - bd) / Math.abs(ad - bd);
        }

        if ((ad !== am[i] || bd !== bm[i]) && am[i] !== bm[i]) {
            return am[i] < bm[i] ? -1 : 1;
        }
    }

    return 0;
}

globalize('alphanum', alphanum);

export default alphanum;
