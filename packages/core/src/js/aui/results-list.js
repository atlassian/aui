import { wrap } from 'underscore';
import Backbone from 'backbone';
import globalize from './internal/globalize';
import ResultSet from './result-set';

var ResultsList = Backbone.View.extend({
    events: {
        'click [data-id]': 'setSelection',
    },

    initialize: function (options) {
        if (!this.model) {
            this.model = new ResultSet({ source: options.source });
        }

        if (!(this.model instanceof ResultSet)) {
            throw new Error('model must be set to a ResultSet');
        }

        this.model.bind('update', this.process, this);

        this.render = wrap(this.render, function (func) {
            this.trigger('rendering');
            func.apply(this, arguments);
            this.trigger('rendered');
        });
    },

    process: function () {
        if (!this._shouldShow(this.model.get('query'))) {
            return;
        }
        this.show();
    },

    render: function () {
        var ul = Backbone.$('<ul/>');
        this.model.each(function (model) {
            Backbone.$('<li/>').attr('data-id', model.id).html(this.renderItem(model)).appendTo(ul);
        }, this);
        this.$el.html(ul);
        return this;
    },

    renderItem: function () {
        return;
    },

    setSelection: function (event) {
        var id = event.target.getAttribute('data-id');
        var selected = this.model.setActive(id);
        this.trigger('selected', selected);
    },

    show: function () {
        this.lastQuery = this.model.get('query');
        this._hiddenQuery = null;
        this.render();
        this.$el.show();
    },

    hide: function () {
        this.$el.hide();
        this._hiddenQuery = this.lastQuery;
    },

    size: function () {
        return this.model.get('length');
    },

    _shouldShow: function (query) {
        return query === '' || !(this._hiddenQuery && this._hiddenQuery === query);
    },
});

globalize('ResultsList', ResultsList);

export default ResultsList;
