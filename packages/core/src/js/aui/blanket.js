import $ from './jquery';
import { getMessageLogger } from './internal/deprecation';
import { recomputeStyle } from './internal/animation';
import globalize from './internal/globalize';

var overflowEl;

/**
 * Blanketing is pattern for hiding all page content from user when overlapping layer is open.
 *
 * This pattern is old and should be removed, but it has a lot of dependants:
 * - Layer, LayerManager
 * - Dialog2, Dialog1
 * - Dropdown2
 * - InlineDialog2
 *
 * Removal should be done as separate process with usage research and go through deprecation.
 */

/**
 * Dims the screen using a blanket div
 * @param useShim deprecated, it is calculated by dim() now
 */
function dim(useShim, zIndex) {
    //if we're blanketing the page it means we want to hide the whatever is under the blanket from the screen readers as well
    // eslint-disable-next-line no-unused-vars
    function hasAriaHidden(element) {
        return element.hasAttribute('aria-hidden');
    }

    // eslint-disable-next-line no-unused-vars
    function isAuiLayer(element) {
        return element.classList && element.classList.contains('aui-layer');
    }

    if (!overflowEl) {
        overflowEl = document.body;
    }

    if (useShim === true) {
        useShimDeprecationLogger();
    }

    const isBlanketShowing = !!dim.$dim && !dim.$dim[0].hasAttribute('hidden');

    if (!!dim.$dim) {
        dim.$dim.remove();
        dim.$dim = null;
    }

    dim.$dim = $('<div aria-hidden="true"></div>').addClass('aui-blanket');
    dim.$dim.appendTo(document.body);

    if (!isBlanketShowing) {
        //recompute after insertion and before setting aria-hidden=false to ensure we calculate a difference in
        //computed styles
        recomputeStyle(dim.$dim);

        dim.cachedOverflow = {
            overflow: overflowEl.style.overflow,
            overflowX: overflowEl.style.overflowX,
            overflowY: overflowEl.style.overflowY,
        };

        overflowEl.style.overflowX = 'hidden';
        overflowEl.style.overflowY = 'hidden';
        overflowEl.style.overflow = 'hidden';
    }

    dim.$dim.removeAttr('hidden');

    if (zIndex) {
        dim.$dim.css({ zIndex: zIndex });
    }

    return dim.$dim;
}

/**
 * Removes semitransparent DIV
 * @see dim
 */
function undim() {
    if (dim.$dim) {
        dim.$dim[0].setAttribute('hidden', '');

        if (overflowEl) {
            overflowEl.style.overflow = dim.cachedOverflow.overflow;
            overflowEl.style.overflowX = dim.cachedOverflow.overflowX;
            overflowEl.style.overflowY = dim.cachedOverflow.overflowY;
        }
    }
}

const useShimDeprecationLogger = getMessageLogger('useShim', {
    extraInfo: 'useShim has no alternative as it is now calculated by dim().',
});

globalize('dim', dim);
globalize('undim', undim);

export { dim, undim };
