import globalize from './internal/globalize';

/**
 * Similar to Javascript's in-built escape() function, but where the built-in escape()
 * might encode unicode charaters as %uHHHH, this function will leave them as-is.
 *
 * NOTE: this function does not do html-escaping, see escapeHtml().
 */
const jsEscape = window.escape;

function escape(string) {
    return jsEscape(string).replace(/%u\w{4}/gi, function (w) {
        return unescape(w);
    });
}

globalize('escape', escape);

export default escape;
