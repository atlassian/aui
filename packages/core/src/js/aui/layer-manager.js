import globalize from './internal/globalize';
import layer from './layer';

globalize('LayerManager', layer.Manager);

export default layer.Manager;
