import { setGlobalTheme } from '@atlaskit/tokens';

if (document.documentElement.hasAttribute('data-color-mode-auto')) {
    setGlobalTheme({ colorMode: 'auto' });
}
