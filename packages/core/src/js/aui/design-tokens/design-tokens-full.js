import * as Tokens from '@atlaskit/tokens';
import * as TestingTheme from './design-tokens-testing-theme';
import globalize from '../internal/globalize';

import './design-tokens-init';

// Export for NPM bundle, for which Webpack automatically puts those functions under AJS.DesignTokens
export const { setGlobalTheme, token, getTokenValue, ThemeMutationObserver } = Tokens;

export const { setTestingThemeColor, enableTestingTheme, disableTestingTheme, toggleTestingTheme } =
    TestingTheme;

// Export for p2-plugin bundle, where we need to do put those functions under AJS.DesignTokens manually
globalize('DesignTokens', {
    setGlobalTheme,
    token,
    getTokenValue,
    ThemeMutationObserver,

    toggleTestingTheme,
    enableTestingTheme,
    disableTestingTheme,
    setTestingThemeColor,
});
