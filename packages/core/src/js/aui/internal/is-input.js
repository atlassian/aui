export default function (el) {
    return 'value' in el || el.isContentEditable;
}
