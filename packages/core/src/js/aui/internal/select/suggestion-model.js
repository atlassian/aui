import Backbone from 'backbone';

export default Backbone.Model.extend({
    idAttribute: 'value',
    getLabel: function () {
        return this.get('label') || this.get('value');
    },
});
