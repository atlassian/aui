var DocumentTouch = window.DocumentTouch;
var hasTouch = 'ontouchstart' in window || (DocumentTouch && document instanceof DocumentTouch);
export default hasTouch;
