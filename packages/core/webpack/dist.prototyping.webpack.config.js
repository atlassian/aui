/* eslint-env node */
const tokens = require('@atlaskit/tokens');
const { BannerPlugin, IgnorePlugin } = require('webpack');
const { merge } = require('webpack-merge');
const {
    librarySkeleton,
    libraryExternals,
} = require('@atlassian/aui-webpack-config/webpack.skeleton');
const parts = require('@atlassian/aui-webpack-config/webpack.parts');
const { sourceDir, outputDir } = require('./dist.config');
const { name: packageName } = require('../package.json');

const IgnoreEmitPlugin = require('ignore-emit-webpack-plugin');
const path = require('path');

const bannerPlugin = new BannerPlugin({
    banner: require('./util/banner'),
    raw: true,
    entryOnly: true,
});

const mainLibraryConfig = merge([
    librarySkeleton,

    {
        context: sourceDir,
        output: {
            globalObject: 'window',
            libraryTarget: 'umd',
            library: {
                type: 'window',
                name: {
                    amd: packageName,
                    commonjs: packageName,
                    root: 'AJS',
                },
                umdNamedDefine: true,
            },
            path: outputDir,
            publicPath: '/',
            uniqueName: '__auiPrototyping',
        },

        resolve: {
            alias: {
                underscore$: require.resolve('underscore'),
                backbone$: require.resolve('backbone'),
                wrmI18n$: './i18n-stub.js',
            },
        },

        optimization: {
            splitChunks: {
                chunks: 'initial',
                maxAsyncRequests: Infinity,
                maxInitialRequests: Infinity,
                minSize: Infinity,
                cacheGroups: {
                    vendors: false,
                },
            },
        },

        plugins: [bannerPlugin],
    },
]);

const libraryWithInternalisedDeps = merge([
    mainLibraryConfig,
    {
        entry: {
            'aui-prototyping': './aui.batch.prototyping.js',
        },
        externals: [libraryExternals.jqueryExternal],
    },
]);

const libraryWithExternalisedDeps = merge([
    mainLibraryConfig,
    {
        entry: {
            'aui-prototyping.nodeps': './aui.batch.prototyping.js',
        },
        externals: Object.values(libraryExternals),
    },
]);

const deprecationsConfig = merge([
    librarySkeleton,
    {
        entry: {
            'aui-css-deprecations': require.resolve(
                '@atlassian/aui/src/js/aui-css-deprecations.js'
            ),
        },
        output: {
            path: outputDir,
        },
        externals: [libraryExternals.jqueryExternal],
    },
]);

const generatedThemesPath = path.join(sourceDir, 'token-themes-generated', 'npm');

const extractDesignTokenThemes = (async () =>
    merge([
        mainLibraryConfig,
        {
            entry: {},
            output: {
                path: outputDir,
            },
        },
        await parts.extractThemesIntoJSAndCSSFiles({
            outputDir: generatedThemesPath,
            importMapName: 'aui-prototyping-design-tokens-theme-import-map.js',
            themesJsBundleFileName: 'aui-prototyping-design-tokens-base-themes.js',
            themesCssBundleFileName: 'aui-prototyping-design-tokens-base-themes.css',
            getThemeStyles: tokens.getThemeStyles,
        }),
    ]))();

const designTokens = merge([
    mainLibraryConfig,
    {
        entry: {
            'aui-prototyping-design-tokens-base-themes': path.join(
                generatedThemesPath,
                'aui-prototyping-design-tokens-base-themes.js'
            ),
            'aui-prototyping-design-tokens-base-themes-css': path.join(
                generatedThemesPath,
                'aui-prototyping-design-tokens-base-themes.css'
            ),
            'aui-prototyping-design-tokens-api': './aui.design-tokens-api.js',
            'aui-prototyping-design-tokens-api-full': './aui.design-tokens-api-full.js',
        },
        resolve: {
            alias: {
                '../artifacts/theme-import-map': path.join(
                    generatedThemesPath,
                    'aui-prototyping-design-tokens-theme-import-map.js'
                ),
                './use-theme-observer': false, // This part of Design Tokens library uses React, but we don't need it
            },
        },
        output: {
            library: {
                name: {
                    root: ['AJS', 'DesignTokens'],
                },
            },
        },
        plugins: [
            new IgnorePlugin({
                checkResource: (resource, context) =>
                    context.match(/.*@atlaskit\/tokens.*/gi) &&
                    resource.match(/\.\/custom-theme/gi),
            }),
        ],
    },
]);

const cssOnlyEntryPoints = merge([
    parts.extractCss(),
    parts.production(),
    {
        context: sourceDir,
        output: {
            path: outputDir,
        },
        entry: {
            'aui-prototyping-darkmode': './styles/aui.page.dark-mode.js',
            'aui-prototyping-browserfocus': './styles/aui.page.focus.js',
            'aui-prototyping-design-tokens-compatibility':
                './styles/aui-design-tokens-compatibility.less',
            // XXX: this shouldn't be a separate resource, but rather included
            // in the main artifact.
            'aui-prototyping-lf-extracted-color-channels': path.join(
                generatedThemesPath,
                'aui-prototyping-lf-extracted-color-channels.css'
            ),
        },
        plugins: [
            //we do not want to output any js
            new IgnoreEmitPlugin(/\.js/),
        ],
    },
]);

module.exports = [
    libraryWithInternalisedDeps,
    libraryWithExternalisedDeps,
    deprecationsConfig,
    extractDesignTokenThemes,
    designTokens,
    cssOnlyEntryPoints,
];
