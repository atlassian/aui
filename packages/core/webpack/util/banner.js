const pkg = require('../../package.json');

const banner = `/*!!
 * ${pkg.name} - ${pkg.description}
 * @version v${pkg.version}
 * @link ${pkg.homepage}
 * @license ${pkg.license}
 * @author ${pkg.author}
 */
`;

module.exports = banner;
