const { merge } = require('webpack-merge');
const { librarySkeleton } = require('@atlassian/aui-webpack-config/webpack.skeleton');

module.exports = merge([
    librarySkeleton,
    {
        entry: {
            'aui-soy': './entry/aui-soy.js',
        },
        optimization: {
            minimize: process.env.NODE_ENV === 'production',
            splitChunks: {
                minSize: Infinity,
                chunks: 'all',
                maxAsyncRequests: Infinity,
                maxInitialRequests: Infinity,
                name: false,
            },
        },
    },
]);
