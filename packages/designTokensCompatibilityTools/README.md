# Tools for AUI design tokens compatibility theme

This is an internal tool that was created to generate a particular file -
`aui/packages/core/entry/styles/aui-design-tokens-compatibility.less`.

The tool is stored in the repo because there might come a time we need to
return to it: such time will come if we update the version of
`@atlaskit/tokens` to a version that has renamed or removed some design token
variables.

## Context

AUI re-exports themes from `@atlaskit/tokens`.

(Here, a "theme" is a piece of CSS that has set of mappings from `--ds-abc`
variables to hex color values, wrapped in a selector that matches the current
theme as per that library's implementation.)

That library evolves; over time some mappings have been renamed or removed; in
other words, some variables became obsolete.

In a use case where a vendor app uses an older version of `@atlaskit/tokens`,
uses an obsolete variable there, and runs in a context that has AUI with a
newer version of `@atlaskit/tokens`, the variable definition will be missing,
and the app will look wrong.

We want to support this use case through AUI. Therefore, we provide a
compatibility theme that products can load; this theme will define all obsolete
variables.

## This package helps prepare the compatibility themes' file content

Since the library itself doesn't have metadata on removed/renamed variables,
it's up to us to come up with this list. The process looks like this:

- Go through all released versions of `@atlaskit/tokens` from newest to oldest
- Extract the theme files as of that version
- Compare pairwise: if variable is absent in `N` and present in `N-1`, it's
  obsolete and last seen in `N-1`
- Collect such variables
- Write out file contents: definitions wrapped in selectors, for light and dark
  theme

## How to use

There are several entry points to handle the steps listed above.

### Get all released versions of the library

Strictly speaking, this doesn't use the current package, but this step prepares
for the next step which does.

`$ npm view @atlaskit/tokens versions --json > all.json`

### Get contents of theme files at versions

```sh
$ mkdir -p output
$ node src/getThemeContentsAtVersions.js [startingVersion]

```

Must have `all.json` at the root level of this package.

If `startingVersion` is supplied, start from it; otherwise start from the
bottom of the list (latest version).

Goes bottom to top, or latest version to earliest!

Will fill the `output` folder with CSS files like `1.25.0.css`, one per
version. The file contains the concatenated themes at that version.

Relies on `./outputThemesFromJs.sh` and `./src/outputThemes.js`. Copies the
themes files to the `src` folder! This is not clean, but done for reasons of
expediency: this way the imports work easily.

### Analyze and generate

The main command to generate the definitions file contents:

```sh
$ node src/generateDefinitions.js generate > aui-design-tokens-compatibility.less

```

Must have all versions in the `output` folder as per the previous
command.

#### Other, testing-only subcommands of `generateDefinitions`

```sh
$ node src/generateDefinitions.js parseOne [versionCssFile]
```

Will output the parsed structure of a css file: a mapping from a CSS variable
to all encountered values.

```sh
$ node src/generateDefinitions.js parseTwo [newer] [older]
```

Will output the values for obsolete variables: i.e. those absent in `newer` and
present in `older`.

## scripts

```sh
yarn generate
```

Alias to generate the output file. Must have prerequisites in place, see
"Analyze and generate" above.

```sh
yarn copy
```

Copy the output file to the proper place in the AUI codebase.
