set -ex

version=$1
yarn add @atlaskit/tokens@$version

cp \
  node_modules/@atlaskit/tokens/dist/esm/artifacts/themes/atlassian-light.js \
  node_modules/@atlaskit/tokens/dist/esm/artifacts/themes/atlassian-dark.js \
  src

node src/outputThemes.js  > output/$version.css
