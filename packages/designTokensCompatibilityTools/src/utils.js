export function reverseObj(obj) {
    const result = {};
    Object.entries(obj).forEach(([key, value]) => {
        result[value] = key;
    });
    return result;
}
