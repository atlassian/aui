import fs from 'fs';
import { spawnSync, execSync } from 'child_process';

function outputThemesFromJs(version) {
    const subprocess = spawnSync('sh', ['./outputThemesFromJs.sh', version], {
        stdio: ['inherit', fs.openSync(`output/${version}.css`, 'w'), 'inherit'],
    });

    if (subprocess.error) {
        console.error(`Failed to start subprocess: ${subprocess.error}`);
        process.exit(1);
    }

    if (subprocess.status !== 0) {
        console.error(`Subprocess exited with code: ${subprocess.status}`);
        process.exit(subprocess.status);
    }
}

function copyThemesFromCss(version) {
    execSync(`yarn add @atlaskit/tokens@${version}`, { stdio: 'inherit' });
    const content = [
        'node_modules/@atlaskit/tokens/css/atlassian-light.css',
        'node_modules/@atlaskit/tokens/css/atlassian-dark.css',
    ]
        .map((fileName) => {
            return fs.readFileSync(fileName, 'utf-8');
        })
        .join('\n');
    fs.writeFileSync(`output/${version}.css`, content, 'utf-8');
}

const [, , startingVersion] = process.argv;

const versions = JSON.parse(fs.readFileSync('./all.json', 'utf8')).reverse();

let startingIndex = startingVersion ? versions.indexOf(startingVersion) : 0;

if (startingIndex === -1) {
    startingIndex = 0;
}

for (let i = startingIndex; i < versions.length; ++i) {
    const currentVersion = versions[i];

    const [majorVersion] = currentVersion.split('.');
    if (majorVersion === '0') {
        copyThemesFromCss(currentVersion);
    } else {
        outputThemesFromJs(currentVersion);
    }
}
