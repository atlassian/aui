const React = require('react');
const ReactDOMServer = require('react-dom/server');
const Provider = require('../MetalsmithContext').Provider;

const Page = require('../../src/demo/patterns/Page');
const makeHref = require('../../src/demo/helpers/makeHref');

module.exports = ({ lang, content, i18n, files, metalsmith, auiVersion, homePage }) => {
    const translate = i18n(lang);
    const tree = content.tree;
    const menus = content.menus;

    return (page, loggedIn) => {
        const pageUrl = makeHref(page.id, loggedIn, lang);
        const pageFilePath = pageUrl.slice(1);

        files[pageFilePath] = {
            rootPath: './',
            mode: '0644',
            contents:
                '<!doctype html>' +
                ReactDOMServer.renderToStaticMarkup(
                    <Provider
                        value={Object.assign({}, metalsmith, {
                            auiVersion,
                            menus,
                            page,
                            pageUrl,
                            loggedIn,
                            lang,
                            i18n,
                            translate,
                            homePage,
                            space: page.spaceLabel,
                            sidebarItems: tree.find((node) => node.id === page.spaceId).children,
                        })}
                    >
                        <Page />
                    </Provider>
                ),
        };
    };
};
