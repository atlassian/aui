const fs = require('fs');
const path = require('path');

/**
Convert aui/packages/core/src/less/imports/aui-theme/core/colors.less to a data
structure ColorsLessRep.

type ColorsLessRep = {
    key: string;
    themes: RTheme[];
}

type RTheme = {
    themeHeaderKey: string;
    imports: string[];
    sections: RSection[];
}

type RSection = {
    sectionComment: string;
    definitions: RDefinition[];
}

type RDefinition = {
    varName: string;
    varValue: string;
    comment?: string;
}
*/
function parseColorsLess(inputFileName = null) {
    if (!inputFileName) {
        inputFileName = path.resolve(
            __dirname,
            '../../../../packages/core/src/less/imports/aui-theme/core/colors.less'
        );
    }

    let state = 'beginning';
    let lineNumber = 0;
    let currentTheme;
    let currentSection;

    const output = {
        key: null,
        themes: [],
    };

    function initTheme(themeHeaderKey) {
        currentTheme = {
            themeHeaderKey,
            imports: [],
            sections: [],
        };
    }

    function commitTheme() {
        output.themes.push(currentTheme);
    }

    function commitSection() {
        currentTheme.sections.push(currentSection);
    }

    function initSection(sectionComment) {
        currentSection = {
            sectionComment,
            definitions: [],
        };
    }

    const nonSpaces = '\\S+';
    const sp = '\\s*';
    const doubleSlash = '\\/\\/';

    /* eslint-disable complexity */
    function processLine(line) {
        ++lineNumber;
        line = line.trim();

        function fail(message) {
            throw new Error(`error at ${lineNumber}: ${message}: '${line}'`);
        }

        if (state === 'beginning') {
            let key;
            if (parseStandaloneComment(line)) {
                // ignore
            } else if ((key = parsePreamble(line))) {
                output.key = key;
                state = 'contents';
            } else {
                fail('not a preamble');
            }
        } else if (state === 'contents') {
            let themeHeaderKey;
            if (isClosingBrace(line)) {
                state = 'end';
            } else if ((themeHeaderKey = parseThemeHeader(line))) {
                state = 'inTheme';
                initTheme(themeHeaderKey);
            } else if (isEmpty(line)) {
                // ignore
            } else {
                fail('expected either closing brace or theme header');
            }
        } else if (state === 'inTheme') {
            if (isClosingBrace(line)) {
                commitTheme();
                state = 'contents';
            } else if (isEmpty(line)) {
                state = 'themeSections';
            } else {
                currentTheme.imports.push(line);
            }
        } else if (state === 'themeSections') {
            let sectionComment;
            if (isEmpty(line)) {
                // ignore
            } else if ((sectionComment = parseStandaloneComment(line))) {
                initSection(sectionComment);
                state = 'themeDefinitions';
            } else if (isClosingBrace(line)) {
                commitTheme();
                state = 'contents';
            } else {
                fail('expected theme section comment or closing brace');
            }
        } else if (state === 'themeDefinitions') {
            let definition;
            if (isEmpty(line)) {
                commitSection();
                state = 'themeSections';
            } else if (isClosingBrace(line)) {
                commitSection();
                commitTheme();
                state = 'contents';
            } else if ((definition = parseDefinition(line))) {
                currentSection.definitions.push(definition);
            } else {
                fail('expected definition or blank or closing brace');
            }
        } else if (state === 'end') {
            if (isEmpty(line)) {
                // do nothing
            } else {
                fail('stuff after postamble');
            }
        }
    }

    function parsePreamble(line) {
        const regexp = new RegExp(`^(${nonSpaces})${sp}\\{$`);
        const match = line.match(regexp);
        return match ? match[1] : null;
    }

    function parseThemeHeader(line) {
        const regexp = new RegExp(`^${sp}(${nonSpaces})${sp}\\{$`);
        const match = line.match(regexp);
        return match ? match[1] : null;
    }

    function parseStandaloneComment(line) {
        const regexp = new RegExp(`^${sp}${doubleSlash}${sp}(.+)$`);
        const match = line.match(regexp);
        return match ? match[1] : null;
    }

    function isClosingBrace(line) {
        return line === '}';
    }

    function parseDefinition(line) {
        const regexp = new RegExp(
            `^${sp}(${nonSpaces})${sp}:${sp}([^;]+);${sp}(?:${doubleSlash}${sp}(.+))?$`
        );
        const match = line.match(regexp);

        if (match) {
            return {
                varName: match[1],
                varValue: match[2],
                ...(match[3]
                    ? {
                          comment: match[3],
                      }
                    : {}),
            };
        }

        return null;
    }

    function isEmpty(line) {
        return line === '';
    }

    const input = fs.readFileSync(inputFileName, 'utf-8');

    input.split('\n').forEach((line) => {
        processLine(line);
    });
    return output;
}

function indented(line, indent) {
    const spaces = ' '.repeat(indent * 4);
    return spaces + line;
}

// Testing function: the invariant `repToLess(parseColorsLess(fileName)) ===
// fileContents` should be kept.
// eslint-disable-next-line no-unused-vars
function repToLess(rep) {
    let indent = 0;
    let result = [];
    function add(line) {
        if (line === '') {
            result.push(line);
        } else {
            result.push(indented(line, indent));
        }
    }

    add(`${rep.key} {`);

    ++indent;
    rep.themes.forEach((theme) => {
        const { themeHeaderKey, imports, sections } = theme;
        add(`${themeHeaderKey} {`);
        ++indent;

        imports.forEach((oneImport) => {
            add(oneImport);
        });
        add('');

        sections.forEach((section, sectionIndex) => {
            const { sectionComment, definitions } = section;

            const isLastSection = sectionIndex === sections.length - 1;

            add(`// ${sectionComment}`);
            definitions.forEach((definition) => {
                const { varName, varValue, comment } = definition;

                if (comment) {
                    add(`${varName}: ${varValue}; // ${comment}`);
                } else {
                    add(`${varName}: ${varValue};`);
                }
            });
            if (!isLastSection) {
                add('');
            }
        });

        --indent;
        add('}');
        add('');
    });

    --indent;
    add('}');
    add('');

    return result.join('\n');
}

// Testing function to find differences between theme definitions.
// eslint-disable-next-line no-unused-vars
function compareThemes(themeA, themeB) {
    if (themeA.sections.length !== themeB.sections.length) {
        return {
            eq: false,
            reason: `number of sections differ: ${themeA.sections.length} vs ${themeB.sections.length}`,
        };
    }

    const reasons = [];

    themeA.sections.forEach((sectionA, sectionIndex) => {
        const sectionB = themeB.sections[sectionIndex];

        if (sectionA.sectionComment !== sectionB.sectionComment) {
            reasons.push(
                `Section ${sectionIndex}: comments differ: '${sectionA.sectionComment}' vs '${sectionB.sectionComment}'`
            );
        }

        if (sectionA.definitions.length !== sectionB.definitions.length) {
            reasons.push(
                `Section ${sectionIndex} - ${sectionA.sectionComment}: definitions differ: ${sectionA.definitions.length} vs ${sectionB.definitions.length}`
            );
            return;
        }

        sectionA.definitions.forEach((definitionA, definitionIndex) => {
            const definitionB = sectionB.definitions[definitionIndex];
            if (definitionA.varName !== definitionB.varName) {
                reasons.push(`varName differs: ${definitionA.varName} vs ${definitionB.varName}`);
            }
            if (definitionA.comment !== definitionB.comment) {
                reasons.push(
                    `var ${definitionA.varName} comment differs: '${definitionA.comment}' vs '${definitionB.comment}'`
                );
            }
        });
    });

    return {
        eq: reasons.length === 0,
        reasons,
    };
}

const classNamePerColumn = ['oldLight', 'oldDark', 'designTokens'];

function getClassName(varName) {
    return `${varName.replace(/^--/, 'colorvar-')}`;
}

function getFullClassName(varName, themeIndex) {
    const base = getClassName(varName);
    const additional = classNamePerColumn[themeIndex];
    return `${base} ${additional}`;
}

/**
   Combine the three themes in ColorsLessRep to a representation that includes
   every variable name only once.

type Combined = {
    key: string,
    themeHeads: { themeHeaderKey: any, imports: any }[], // like in RTheme
    sections: CSection[],
};

type CSection = {
    sectionComment: string,
    // single varName, one CVariable per original theme.
    combinedDefinitions: Record<VarName, CVariable[]>,
};

type CVariable = {
    varValue: string,
    comment?: string,
    className: string, // for rendering a table cell
    previewType: 'notAColor' | 'internalReference' | 'normal' | 'designToken',
    hasPreview: boolean,
    isInternalReference: boolean,
    internalReferenceTarget?: string,
    isDesignToken: boolean,
    designTokenValue?: string,
};
 */
function convertToCombinedFormat(rep) {
    const result = {
        key: rep.key,
        themeHeads: rep.themes.map((theme) => {
            const { themeHeaderKey, imports } = theme;
            // the sections are removed, because the vars will be in a combined sections list
            return { themeHeaderKey, imports };
        }),

        sections: rep.themes[0].sections.map((section, sectionIndex) => {
            const { sectionComment, definitions } = section;
            const combinedDefinitions = {};

            definitions.forEach((definition, definitionIndex) => {
                const { varName } = definition;

                const combinedVariables = rep.themes.map((thatTheme, themeIndex) => {
                    const thatDefinition =
                        thatTheme.sections[sectionIndex].definitions[definitionIndex];
                    const { varValue, comment } = thatDefinition;
                    const className = getFullClassName(varName, themeIndex);
                    const notAColor = varValue === 'none' || varValue === 'underline';

                    let previewType;
                    const internalReferenceMatch = varValue.match(/var\((--aui.*)\)/);
                    let internalReferenceTarget;
                    const designTokenMatch = varValue.match(/var\((--ds-.*?)(, .*)?\)/);
                    let designTokenValue;
                    if (notAColor) {
                        previewType = 'notAColor';
                    } else if (internalReferenceMatch) {
                        previewType = 'internalReference';
                        internalReferenceTarget = internalReferenceMatch[1];
                    } else if (designTokenMatch) {
                        previewType = 'designToken';
                        designTokenValue = designTokenMatch[1];
                    } else {
                        previewType = 'normal';
                    }
                    return {
                        varValue,
                        comment,
                        className,
                        previewType,
                        hasPreview: previewType === 'normal' || previewType === 'designToken',
                        isInternalReference: previewType === 'internalReference',
                        internalReferenceTarget,
                        isDesignToken: previewType === 'designToken',
                        designTokenValue,
                    };
                });

                combinedDefinitions[varName] = combinedVariables;
            });

            return { sectionComment, combinedDefinitions };
        }),
    };
    return result;
}

/**
   Generate contents for aui/packages/docs/src/styles/aui-colors-table.less.

   The original colors.less file has less variables e.g. @ak-color-N600; less
   formulas on those; and external references like var(--ds-text); we want to
   resolve these names using less. So we copy whatever the variable definition
   is into the value for preview's background; and import this generated file
   from index.less; so all the colors are up to date.

   One limitation with this approach is we cannot show the ds variables
   previews in both dark and light modes at the same time (the values for those
   variables depend on html attribute);
   https://atlassian.design/components/tokens/all-tokens resolves this somehow
   but I didn't investigate it.
 */
function generateTableLess(combined) {
    const lines = [];
    function add(line) {
        lines.push(line);
    }

    add('// This file is auto-generated by colorsLessTools. Do not modify!');
    add("@import (reference) '~@atlassian/aui/src/less/imports/aui-theme/adg/adg-colors';");

    function addColumnClasses(columnIndex) {
        add(`.${classNamePerColumn[columnIndex]} {`);
        add(
            columnIndex === 1
                ? "  @import (reference) '~@atlassian/aui/src/less/imports/aui-theme/adg/adg-neutral-dark';"
                : "  @import (reference) '~@atlassian/aui/src/less/imports/aui-theme/adg/adg-neutral-light';"
        );
        combined.sections.forEach((section) => {
            Object.entries(section.combinedDefinitions).forEach(([varName, combinedVariables]) => {
                const className = getClassName(varName);
                const { varValue, previewType } = combinedVariables[columnIndex];

                if (previewType === 'normal') {
                    add(`  &.${className} { background: ${varValue} }`);
                } else if (previewType === 'designToken') {
                    add(`  &.${className} { background: ${varValue} }`);
                } else {
                    // ignore for now
                }
            });
        });
        add('}');
    }

    classNamePerColumn.forEach((_, columnIndex) => {
        addColumnClasses(columnIndex);
    });

    return lines.join('\n');
}

module.exports = {
    parseColorsLess,
    convertToCombinedFormat,
    generateTableLess,
};
