const path = require('path');
const fs = require('fs');
const {
    parseColorsLess,
    convertToCombinedFormat,
    generateTableLess,
} = require('./utils/colorsLessTools');

function generateColorsTableLess(done) {
    const rep = parseColorsLess();
    const auiColorsData = convertToCombinedFormat(rep);
    const tableLess = generateTableLess(auiColorsData);
    const auiColorsTablePath = path.resolve(__dirname, '../src/styles/aui-colors-table.less');
    fs.writeFileSync(auiColorsTablePath, tableLess, 'utf-8');
    console.info(`written ${auiColorsTablePath}`);
    done();
}

module.exports = {
    generateColorsTableLess,
};
