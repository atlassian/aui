const fetch = require('node-fetch');
const { name: auiPkgName } = require('../../core/package.json');

const STABLE_REG_EXP = /^\d+\.\d+\.\d+$/;
const MINOR_REG_EXP = /^\d+\.\d+/;

const matchStableVersion = (version) => version.match(STABLE_REG_EXP);
const getMinorVersionPart = (version) => version.match(MINOR_REG_EXP).pop();

function versionComparator(a, b) {
    let [majorA, minorA, patchA] = a.split('.');
    let [majorB, minorB, patchB] = b.split('.');

    if (majorA !== majorB) {
        return majorA - majorB;
    }

    if (minorA !== minorB) {
        return minorA - minorB;
    }

    return patchA - patchB;
}

function getStableVersions(versions) {
    return versions.filter(matchStableVersion);
}

function getLatestStableVersions(versions) {
    return versions.filter((version, i) => {
        const nextVersion = versions[i + 1];

        if (nextVersion) {
            return getMinorVersionPart(version) !== getMinorVersionPart(nextVersion);
        }

        return true;
    });
}

function libGetAuiVersions() {
    return new Promise((resolve, reject) => {
        fetch(`https://registry.npmjs.com/${auiPkgName.replace('/', '%2F')}`, {
            headers: { 'Content-Type': 'application/json' },
        })
            .then((resp) => resp.json())
            .then((json) => {
                const allVersions = Object.keys(json.versions).sort(versionComparator);
                const versionReleaseDates = allVersions.reduce((map, version) => {
                    map[version] = json.time[version];
                    return map;
                }, {});
                const stableVersions = getStableVersions(allVersions).reverse();
                const latestVersions = getLatestStableVersions(stableVersions);

                resolve({
                    all: versionReleaseDates,
                    latest: latestVersions,
                    stable: stableVersions,
                });
            })
            .catch((err) => {
                return reject(err);
            });
    });
}

/**
 * Filters versions object by minorVersion to get latest stable (with patch number).
 * eg. from [9.0.2, 9.0.1, 9.0.0] by 9.0 => 9.0.2
 *
 * @param {string} minorVersion
 * @param {{ stable: string[], latest: string[], all: {Object.<string, string>}}} versions
 *
 * @returns {string} found stable version or given minorVersion as a fallback.
 */
function findStableVersionByMinor(minorVersion, versions) {
    return (
        versions.stable.find((singleStableVersion) =>
            singleStableVersion.startsWith(minorVersion)
        ) || minorVersion
    );
}

/**
 * Adapts version number from opts and package.json.
 *
 * @param {{ docsRawVersion: string, docsVersion: string, versions: {Object} }} docsOpts
 * @param {{ version: string }} packageJson
 *
 * @returns {{ patchVersion: string, minorVersion: string }}
 */
function adaptVersion({ docsRawVersion, docsVersion, versions }, { version }) {
    // eg. 9.2.1, 9.2.1-SNAPSHOT, 9.2.1-alpha-1 etc.
    const fullVersion = String(docsRawVersion || docsVersion || version);
    const minorVersion = getMinorVersionPart(fullVersion);

    return {
        // eg. 9.2.1
        patchVersion: findStableVersionByMinor(minorVersion, versions),
        // eg. 9.2
        minorVersion,
    };
}

module.exports = {
    libGetAuiVersions,
    adaptVersion,
    getMinorVersionPart,
};
