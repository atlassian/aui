const webpack = require('webpack');
const webpackConfig = require('./docs.webpack.config');

const pkg = require('@atlassian/aui/package.json');
const { getMinorVersionPart } = require('./get-aui-versions');

module.exports = (opts) =>
    function buildFrontend(done) {
        const minorVersion = getMinorVersionPart(pkg.version);

        const { replacePublicPathForProd, watch } = opts;

        const finalConfig = Object.assign(webpackConfig, { watch });

        // When running single version locally or on the CI machine, the server
        // hosts the files at the root; but when building to be hosted at the docs
        // site, the particular version will be under a subfolder; in order to load
        // the design tokens theme via dynamic import, set the proper publicPath.
        if (replacePublicPathForProd && process.env.NODE_ENV === 'production') {
            finalConfig.output.publicPath = `/aui/${minorVersion}/`;
        }

        webpack(finalConfig, function (err, stats) {
            let errors = [].concat(err);
            if (stats) {
                const json = stats.toJson();
                errors = errors.concat(json.errors);
            }
            errors = errors.filter(Boolean);

            if (errors.length) {
                console.error('webpack compilation produced errors', errors);
                done(errors);
            } else {
                // if (stats.hasWarnings()) {
                //     console.warn('webpack compilation produced warnings', info.warnings);
                // }
                done();
            }
        });
    };
