/* eslint-env node */
const fs = require('fs');
const del = require('del');
const gulp = require('gulp');
const less = require('gulp-less');
const gulpWebserver = require('gulp-webserver');
const argv = require('yargs').argv;

const docsOpts = require('./build/docs.opts');

const buildWebpack = require('./build/docs.webpacker');
const { generateColorsTableLess } = require('./build/generateColorsTableLess');
const buildMetalsmith = require('./build/docs.metalsmith');
const { libGetAuiVersions } = require('./build/get-aui-versions');

let frontendOpts = {};
let backendOpts = {
    /**
     * Versions from NPM registry
     * @see processVersions
     */
    versions: null,
    // For Backward Compatibility.
    docsVersion: argv.docsVersion,
    // Raw version number passed from console (has precedence over docsVersion)
    docsRawVersion: argv.docsRawVersion,
};

//
// Helper functions and data used when building
//

const processVersions = (opts) =>
    function processAuiVersions(done) {
        const directoryPath = opts;
        libGetAuiVersions()
            .then((versions) => {
                backendOpts.versions = versions;

                return JSON.stringify(versions);
            })
            .then((data) => {
                fs.mkdirSync(directoryPath, { recursive: true });
                fs.writeFileSync(directoryPath + '/versions.json', data, {
                    encoding: 'utf8',
                    flags: 'w',
                });
            })
            .then(() => done());
    };

const runWebserver = (opts, distSrc = 'dist') =>
    function docsServer() {
        opts = Object.assign(
            {
                host: argv.host || docsOpts.host,
                port: argv.port || docsOpts.port,
            },
            opts
        );

        return gulp.src(distSrc, { encoding: false }).pipe(gulpWebserver(opts));
    };

const dev = (isDev) =>
    function setDevmode(done) {
        if (isDev) {
            frontendOpts.watch = true;
            backendOpts.watch = true;
        }
        done();
    };

//
// Actual Gulp tasks
//

const clean = (done) => del(['.tmp', 'dist']).then(() => done());
const doBuild = (options) => {
    const { replacePublicPathForProd } = options;

    return gulp.series(
        clean,
        gulp.series(
            processVersions('./dist/aui'),
            function (done) {
                generateColorsTableLess(done);
            },
            function buildFrontend(done) {
                buildWebpack({ ...frontendOpts, replacePublicPathForProd })(done);
            },
            function buildBackend(done) {
                buildMetalsmith.docs(backendOpts)(done);
            }
        )
    );
};

const build = gulp.series(doBuild({ replacePublicPathForProd: true }));

const run = gulp.series(
    dev(false),
    doBuild({ replacePublicPathForProd: false }),
    runWebserver({ livereload: false })
);

// livereload saves a lot of time when you work on something from scratch
const watch = gulp.series(
    dev(true),
    doBuild({ replacePublicPathForProd: false }),
    runWebserver({ livereload: true })
);

const copyImages = (done) => {
    gulp.src('./index-page/images/**', { encoding: false }).pipe(
        gulp.dest('./dist-index-page/assets/images')
    );
    done();
};
const createHTML = (done) => buildMetalsmith.indexPage(backendOpts)(done);
const processStyles = (done) => {
    gulp.src('./index-page/src/styles/*.less', { encoding: false })
        .pipe(less())
        .pipe(gulp.dest('./dist-index-page/styles'));
    done();
};

const buildIndex = gulp.series(
    clean,
    copyImages,
    processVersions('./dist-index-page/aui'),
    createHTML,
    processStyles
);

const runIndex = gulp.series(
    dev(false),
    buildIndex,
    runWebserver({ livereload: false }, 'dist-index-page')
);

module.exports = {
    clean,
    build,
    run,
    watch,
    buildIndex,
    runIndex,
};
