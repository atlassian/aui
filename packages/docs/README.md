# AUI documentation microsite

Documentation on how to use various patterns and components from the AUI library.

This documentation is hosted at [https://aui.atlassian.com/].

# Some commands

`yarn build`: creates a build for the current version in `dist`; can be viewed
by `http-server dist`.

`NODE_ENV=production yarn build`: creates a build for the current version in
`dist` for hosting in production; must be served from
`http://<server>/aui/<major.minor>/`.

`yarn run`: runs the current version docs server

`NODE_ENV=production yarn run`: like before. This command is used by the visreg
tests.
