// {{compare unicorns ponies operator="<"}}
// 	I knew it, unicorns are just low-quality ponies!
// {{/compare}}
//
// (defaults to == if operator omitted)
//
// {{equal unicorns ponies }}
// 	That's amazing, unicorns are actually undercover ponies
// {{/equal}}
// (from http://doginthehat.com.au/2012/02/comparison-block-helper-for-handlebars-templates/)

/* eslint-env node */
module.exports = function (lvalue, rvalue, options) {
    if (arguments.length < 3) {
        throw new Error("Handlerbars Helper 'compare' needs 2 parameters");
    }

    const operator = options.hash.operator || '==';

    const operators = {
        // eslint-disable-next-line eqeqeq
        '==': (l, r) => l == r,
        '===': (l, r) => l === r,
        // eslint-disable-next-line eqeqeq
        '!=': (l, r) => l != r,
        '<': (l, r) => l < r,
        '>': (l, r) => l > r,
        '<=': (l, r) => l <= r,
        '>=': (l, r) => l >= r,
        'typeof': (l, r) => typeof l === r,
    };

    if (!operators[operator]) {
        throw new Error("Handlerbars Helper 'compare' doesn't know the operator " + operator);
    }

    const result = operators[operator](lvalue, rvalue);

    if (result) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
};
