// Append the specified suffix to the given string.
//
//     Params
//
// str {String}
// suffix {String}
// returns {String}
// Example
//
// <!-- given that "item.stem" is "foo" -->
// {{append item.stem ".html"}}
// <!-- results in:  'foo.html' -->
// (from https://github.com/helpers/handlebars-helpers#string)
/* eslint-env node */
module.exports = function (str, suffix) {
    if (typeof str === 'string' && typeof suffix === 'string') {
        return str + suffix;
    }
    return str;
};
