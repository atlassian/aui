const pkg = require('@atlassian/aui/package.json');
const { execSync } = require('child_process');
const { getMinorVersionPart } = require('../build/get-aui-versions');

// Build both the index page and the current version of the docs and place them
// in a directory structure that mimics the prod deployment. Useful for
// checking path resolution issues.

function cmd(line) {
    console.log(`$ ${line}`);
    const output = execSync(line);
    console.log(output.toString());
}

function prepareProdLikeBuild() {
    const minorVersion = getMinorVersionPart(pkg.version);

    cmd('rm -rf release');
    cmd('NODE_ENV=production yarn build-index');
    cmd('mv dist-index-page release');

    cmd('NODE_ENV=production yarn build');
    cmd('cp -R dist release/aui/latest');
    cmd(`mv dist release/aui/${minorVersion}`);
}

prepareProdLikeBuild();
