---
component: Buttons
analytics:
  pageCategory: component
  component: button
design: https://design.atlassian.com/latest/product/components/buttons/
status:
  api: general
  core: true
  amd: aui/button
  wrm: com.atlassian.auiplugin:aui-button
  experimentalSince: 4.2
  generalSince: 5.1
---

<h2>Summary</h2>

<p>Use buttons as triggers for actions that are used in forms, toolbars, dialog footers and as stand-alone action triggers. Try to avoid the usage of buttons for navigation. The main difference between actions and navigation is that <strong>Actions</strong> are operations performed on objects, while <strong>Navigation</strong> refers to elements on the screen or view that take you to another context in the application.

<h2>Status</h2>
{{> status }}

<h2 id="button-examples">Examples</h2>
<h3 id="button-types">Button types</h3>

<style type="text/css">
    .examples-in-rows {
        display: grid;
        gap: 10px;
    }
    .examples-in-rows figure {
        margin: 0;
        padding: 0;
    }
</style>

<article class="aui-flatpack-example examples-in-rows">
    <figure>
        <button class="aui-button">Standard button</button>
        <button class="aui-button" disabled>Disabled</button>
    </figure>

    <figure>
        <button class="aui-button aui-button-primary">Primary Button</button>
        <button class="aui-button aui-button-primary" disabled>Disabled</button>
    </figure>

    <figure>
        <button class="aui-button aui-button-warning">Warning Button</button>
        <button class="aui-button aui-button-warning" disabled>Disabled</button>
    </figure>

    <figure>
        <button class="aui-button aui-button-danger">Danger Button</button>
        <button class="aui-button aui-button-danger" disabled>Disabled</button>
    </figure>

    <figure>
        <button class="aui-button aui-button-link">Link button</button>
        <button class="aui-button aui-button-link" disabled>Disabled</button>
    </figure>

    <figure>
        <button class="aui-button aui-dropdown2-trigger" aria-owns="dropdown2-more" aria-haspopup="true">
            Dropdown button
        </button>

        <button class="aui-button aui-dropdown2-trigger" aria-owns="dropdown2-more" aria-haspopup="true" disabled>
            Disabled
        </button>

        <div id="dropdown2-more" class="aui-dropdown2">
            <ul class="aui-list-truncate">
                <li><a href="https://example.com/">Menu item 1</a></li>
                <li><a href="https://example.com/">Menu item 2</a></li>
                <li><a href="https://example.com/">Menu item 3</a></li>
            </ul>
        </div>
    </figure>
</article>

<h3 id="button-variations">Button variations</h3>

<p>These variations can be used against all button types. For button groups on your page, only choose one type of variation, do not mix them.</p>

<article class="aui-flatpack-example examples-in-rows" id="button-variations-example">
    <figure>
        <button class="aui-button">Label only</button>
    </figure>

    <figure>
        <button class="aui-button">
            <span class="aui-icon aui-icon-small aui-iconfont-configure" aria-hidden="true"></span> Icon and label
        </button>
    </figure>

    <figure>
        <button class="aui-button" aria-label="Configure">
            <span class="aui-icon aui-icon-small aui-iconfont-configure" aria-hidden="true"></span>
        </button>
    </figure>

    <figure>
        <button class="aui-button aui-button-subtle">
            <span class="aui-icon aui-icon-small aui-iconfont-configure" aria-hidden="true"></span> Subtle button
        </button>
    </figure>

    <figure>
        <button class="aui-button" disabled>Disabled button</button>
        <a class="aui-button" role="button" aria-disabled="true">Disabled link button</a>
    </figure>

    <figure>
        <button class="aui-button aui-button-link aui-button-link-icon-text">
            <span class="aui-icon aui-icon-small aui-iconfont-configure" aria-hidden="true"></span> Link button with icon and text
        </button>

        <button class="aui-button aui-button-link aui-button-link-icon-text" disabled>
            <span class="aui-icon aui-icon-small aui-iconfont-configure" aria-hidden="true"></span> Disabled link button with icon and text
        </button>
    </figure>

    <figure>
        <div class="aui-buttons">
            <button class="aui-button">Tool</button>
            <button class="aui-button">Bar</button>
        </div>
    </figure>

    <figure>
        <div id="split-container" class="aui-buttons">
            <button class="aui-button aui-button-split-main">Split button</button>
            <button class="aui-button aui-dropdown2-trigger aui-button-split-more"
                    aria-owns="dropdown-button-split">Split More</button>
        </div>
        <div id="dropdown-button-split" class="aui-dropdown2"
             data-aui-alignment-container="#split-container">
            <ul>
                <li><a href="#">Menu item 1</a></li>
                <li><a href="#">Menu item 2</a></li>
                <li><a href="#">Menu item 3</a></li>
            </ul>
        </div>
    </figure>

    <figure>
        <button class="aui-button aui-button-compact">Compact</button>
        <button aria-owns="compact-button-dropdown"
                aria-label="More"
                class="aui-button aui-button-compact aui-button-subtle aui-dropdown2-trigger aui-dropdown2-trigger-arrowless"
        >
            <span class="aui-icon aui-icon-small aui-iconfont-more" aria-hidden="true"></span>
        </button>
        <div id="compact-button-dropdown" class="aui-dropdown2">
            <ul>
                <li><a href="#" class="active">Menu item 1</a></li>
                <li><a href="#" class="">Menu item 2</a></li>
                <li><a href="#" class="">Menu item 3</a></li>
            </ul>
        </div>
    </figure>
</article>
<p><span class="aui-lozenge aui-lozenge-error">deprecated</span></p>
<article class="aui-flatpack-example examples-in-rows" id="deprecated-button-variations-example">
    <figure>
        <button class="aui-button aui-button-light">Light button</button>
    </figure>
</article>

<h3 id="button-spinners">Busy buttons</h3>
{{> example-spinner-button }}

<h2 id="button-code">Code</h2>
<h3 id="button-code-html">HTML</h3>

<p>The base button code is:</p>
<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <button class="aui-button">Button</button>
    </noscript>
</aui-docs-example>

<p>You can then apply a button type by adding the appropriate class, for example <code>aui-button-primary</code>:</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <button class="aui-button aui-button-primary">Button</button>
    </noscript>
</aui-docs-example>

<h3 id="button-code-types">Button types and classes</h3>

<ul>
    <li>Standard/default - (no extra class)</li>
    <li>Primary - <code>aui-button-primary</code></li>
    <li>Link-style (used for "cancel" actions) - <code>aui-button-link</code></li>
    <ul>
        <li>with icon and text - <code>aui-button-link</code> <code>aui-button-link-icon-text</code></li>
    </ul>
    <li>Subtle (looks like a link while inactive, looks like a button when hovered/focused) - <code>aui-button-subtle</code></li>
</ul>

<h3>Customising the state</h3>
<p>The AUI button uses three CSS variables to control the colours used in a given state:</p>
<dl>
    <dt><var>--aui-btn-bg</var></dt>
    <dd>The button's background colour</dd>

    <dt><var>--aui-btn-border</var></dt>
    <dd>The button's border colour</dd>

    <dt><var>--aui-btn-text</var></dt>
    <dd>The button's text colour</dd>
</dl>

<p>
    In addition, the AUI theme defines CSS variables on the <var>:root</var> element for each button type's
    base and pseudo-states.
</p>

<aui-docs-example label="You can affect the way an existing button type renders by adjusting its theme CSS variable">
    <noscript is="aui-docs-code" type="text/css">
        /* let's make the subtle button very un-subtle... */
        .my-custom-theme {
            --aui-button-subtle-bg-color: #f0f;
            --aui-button-subtle-text-color: #000;
        }
    </noscript>
</aui-docs-example>

<aui-docs-example live-demo label="You can create your own button type by changing the button's internal CSS variables">
    <noscript is="aui-docs-code" type="text/css">
        /* these will only affect the button "at rest". */
        .green-button {
            --aui-btn-bg: #36B37E;
            --aui-btn-text: #FFF;
            --aui-btn-border: #006644;
        }
        /* override hover styles in the same way if you want to ;) */
        .green-button:hover {
            --aui-btn-bg: #FFF;
            --aui-btn-text: #006644;
        }
    </noscript>
    <noscript is="aui-docs-code" type="text/html">
        <button class="aui-button green-button">
            The forest is lovely this time of year!
        </button>
    </noscript>
</aui-docs-example>

<h3 id="button-code-states">Button states</h3>
<p>Button states are applied using boolean ARIA attributes:</p>
<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <button class="aui-button" aria-disabled="true" disabled>Button</button>
        <button class="aui-button" aria-pressed="true">Button</button>
    </noscript>
</aui-docs-example>

<p>States:</p>

<ul>
    <li>Disabled: Buttons provides the disabled <em>style</em> but you still need to disable the events - <code>aria-disabled="true"</code>.</li>
    <li>Pressed: A pressed/enabled style for toggle buttons - <code>aria-pressed="true"</code></li>
</ul>

<p>Note: The style applies when the attribute is present and set to true.</p>
<p><strong>Button groups</strong></p>
<p>Create a button group by wrapping buttons in an <code>aui-buttons</code> (note plural) DIV element:</p>
<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <div class="aui-buttons">
            <button class="aui-button">Button</button>
            <button class="aui-button">Button</button>
            <button class="aui-button">Button</button>
        </div>
    </noscript>
</aui-docs-example>

<p><strong>Split buttons</strong></p>
<p>Require a wrapper and extra modifier classes; the second button should always be a Dropdown2 trigger:</p>
<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <div class="aui-buttons">
            <button class="aui-button aui-button-split-main">Split main</button>
            <button class="aui-button aui-dropdown2-trigger aui-button-split-more" aria-controls="split-container-dropdown">Split more</button>
        </div>
        <!-- Be sure to put your dropdown markup outside the button group...
             otherwise the buttons will get jaggy edges! -->
        <aui-dropdown-menu id="split-container-dropdown">
            <aui-item-link>Menu item 1</aui-item-link>
            <aui-item-link>Menu item 2</aui-item-link>
            <aui-item-link>Menu item 3</aui-item-link>
        </aui-dropdown-menu>
    </noscript>
</aui-docs-example>

<p>
    Read the <a href="{{rootPath}}docs/dropdown.html">Dropdown menu component documentation</a> for more details on how to
    control the rendering and behaviour of the dropdown menu.
</p>

<h3>Soy</h3>

<h4>Single button</h4>
<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.buttons.button}
        {param text: 'Button'/}
    {/call}

    {call aui.buttons.button}
        {param text: 'Primary Button'/}
        {param type: 'primary'/}
    {/call}
</noscript>

<h4>Dropdown 2 button</h4>
<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.buttons.button}
        {param text: 'Dropdown button'/}
        {param type: 'link'/}
        {param dropdown2Target: 'dropdown2id'/}
    {/call}
</noscript>

<h4>Icon button</h4>
<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.buttons.button}
        {param text: ' Icon Button' /}
        {param iconType: 'aui' /}
        {param iconClass: 'aui-icon-small aui-iconfont-view' /}
        {param iconText: 'View' /}
    {/call}
</noscript>

<h4>Grouped buttons</h4>
<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.buttons.buttons}
        {param content}
            {call aui.buttons.button}{param text: 'Button'/}{/call}
            {call aui.buttons.button}{param text: 'Button'/}{/call}
            {call aui.buttons.button}{param text: 'Button'/}{/call}
        {/param}
    {/call}
</noscript>

<h4>Split buttons</h4>
<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.buttons.buttons}
        {param content}
            {call aui.buttons.splitButton}
                {param splitButtonMain: [
                    'text': 'Split main'
                ] /}
                {param splitButtonMore: [
                    'text': 'Split more',
                    'dropdown2Target': 'split-container-dropdown'
                ] /}
            {/call}
        {/param}
    {/call}
</noscript>

<h4>Disabled buttons</h4>

<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.buttons.button}
        {param text: 'Button'/}
        {param isDisabled: 'true'/}
    {/call}
</noscript>

<h4>Pressed buttons</h4>
<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.buttons.button}
        {param text: 'Button'/}
        {param isPressed: 'true'/}
    {/call}
</noscript>

<h4>Link buttons with icon and text</h4>
<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.buttons.button}
        {param text: 'Go back' /}
        {param iconType: 'aui' /}
        {param extraClasses: 'aui-button-link-icon-text' /}
        {param iconClass: 'aui-icon-small aui-iconfont-chevron-left' /}
        {param iconText: 'Go back' /}
        {param type: 'link' /}
    {/call}
</noscript>


<h2>A11Y guidelines</h2>

{{> a11y-side-note }}

<ul>
    <li>Avoid using other HTML tags than <code>button</code> or <code>input</code> if possible.
        <ul>
            <li>If you're creating a custom button (again! not advised!) ensure <code>role="button"</code>, <code>tabindex="0"</code>, and all expected keyboard support is present.</li>
        </ul>
    </li>
    <li>When using icons inside buttons
        <ul>
            <li>Make sure the icon has <code>aria-hidden="true"</code> applied to it.</li>
            <li>Don't add additional text into the icon's <code>span</code>.</li>
            <li>If this is an icon-only button, please add <code>aria-label</code> attribute to the button, provide descriptive and unique text.</li>
        </ul>
    </li>
    <li>
        In case of multiple buttons with similar purpose on a page (e.g. "edit" button on each item in a list), specify descriptive text (e.g. "Edit Profile 1”) via the <code>aria-label</code> or <code>aria-labelledby</code> attributes to make the detailed information available for screen reader users.
        <br>
        <strong>Examples:</strong>
        <noscript is="aui-docs-code" type="text/html">
            <button class="aui-button aui-button-primary" aria-label="Edit Profile 1">Edit</button>
        </noscript>
        <noscript is="aui-docs-code" type="text/html">
            <span id="prof_1">Profile 1</span>
            <span>/* ... some ... */</span>
            <span>/* ... other ... */</span>
            <span>/* ... elements ... */</span>
            <button class="aui-button" id="edit_1" aria-labelledby="edit_1 prof_1">
                <span class="aui-icon aui-icon-small aui-iconfont-configure"></span> Configure
            </button>
        </noscript>
    </li>
</ul>
