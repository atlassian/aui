---
component: Application header
analytics:
  pageCategory: component
  component: header
design: https://design.atlassian.com/latest/product/components/navigation/
status:
  api: general
  core: false
  wrm:
    - com.atlassian.auiplugin:aui-header
  experimentalSince: 5.0
  generalSince: 5.1
---

<h2>Summary</h2>
<p>Atlassian is standardising the header pattern across its suite of apps. To have consistent experiences with multiple products we're making the common areas of usage look and work the same every time.</p>
<p>Each app will have its own logo and set of specific navigation. The right side of the header is for search, administration tasks, help and the user menu.</p>

<h2>Status</h2>
{{> status }}

<h2>Examples</h2>

<div class="aui-flatpack-example header-example">
    <header id="header" role="banner">
        <!-- App Header goes inside #header -->
        {{> example-appheader-skiplinks }}
        {{> example-appheader }}
    </header>
</div>

<h2>Code</h2>

<h3>Soy</h3>
<p>This example includes the optional Quicksearch pattern:</p>
<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.page.header}
      {param skipLinks: [['href': '#main', 'label': 'Skip to main content']] /}
      {param logo: 'jira' /}
      {param headerLink: '#' /}
      {param headerLogoText: 'Jira' /}
      {param primaryNavContent}
        <ul class="aui-nav">
          <li><a href="https://example.com/">Navigation item or dropdown trigger</a></li>
          <li><a href="https://example.com/" class="aui-button aui-button-primary">Primary button</a></li>
        </ul>
      {/param}
      {param secondaryNavContent}
        <ul class="aui-nav">
          <li>
            {{> example-appheader-quicksearch }}
          </li>
          <li><a href="https://example.com/">Often an icon-only dropdown</a></li>
        </ul>
      {/param}
    {/call}
</noscript>

<h2>Options</h2>
<ul>
    <li>The header <strong>must</strong> contain skip links <strong>as the first couple child elements</strong> to make it easier for keyboard-only users to jump to the crucial page content</li>
    <li>The header may contain banners with system-level messages</li>
    <li>The logo can be set to a product logo or simple text</li>
    <li>The header navigation can be simple links, dropdown2 triggers, a primary button for the hero action</li>
    <li>The header contains a quick search pattern</li>
    <li>The header navigation items can contain an icon or avatar (aui-avatar-small) and also be dropdowns. If you include these, they must contain the <code>aui-dropdown2-trigger-arrowless</code> so that no right caret is displayed.</li>
    <li>The header navigation items (<code>&lt;a&gt;</code>) that are icon buttons shall contain <code>aui-round-button</code> class to make their background rounded.</li>
</ul>

<h3>Initialisation</h3>

<p>
    The header is initialised automatically. The timing depends on which of these two triggers occurs first:
</p>
<ol>
    <li>when the document has loaded, or</li>
    <li>asynchronously after the header is inserted into the DOM.
        <p><em>a note:</em> <code>&lt;aui-header-end&gt;&lt;/aui-header-end&gt;</code> element needs to be present
            as the last child of <code>&lt;nav class="aui-header"...&gt;</code> to enable asynchronous initialisation.
            The element is already there when using <code>aui.page.header</code> soy template.</p>
    </li>
</ol>
<p>
    This is to avoid reliance on all DOM content being loaded before the header is navigable.
</p>

<h3>Rendering a logo</h3>

<p>
    AUI supports adding a logo to the application header pattern. It is assumed that this is the
    desired default behaviour, so plain-text is hidden in the basic markup pattern.
</p>

<p>
    To add a logo to the header, there are two common approaches.
</p>

<h4>Use CSS</h4>

<p>
    The variables make use of the <a href="{{rootPath}}docs/look-and-feel.html">Look and
    feel</a> feature even when your app doesn&rsquo;t allow to customize the image.
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <nav class="aui-header" role="navigation">
            <div class="aui-header-primary">
                <span id="logo" class="aui-header-logo aui-header-logo-PRODUCTNAME">
                    <!-- Keep in mind that labels should be translated -->
                    <a href="https://example.com/" aria-label="Go to home page">
                        <span class="aui-header-logo-device">My application name</span>
                    </a>
                </span>
            </div>
        </nav>
    </noscript>
    <noscript is="aui-docs-code" type="text/css">
        .aui-header .aui-header-logo-PRODUCTNAME .aui-header-logo-device {
            --atl-theme-header-logo-image: url('images/aui-sandbox.png'); /* place your image here */
            --atl-theme-header-logo-width: 32px; /* add your logo's width here */
        }
    </noscript>
</aui-docs-example>

<p>
    To set the product logo, replace <var>PRODUCTNAME</var> with your product's name, then add your
    logo to a CSS rule.
</p>

<h4>Use an image element</h4>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <nav class="aui-header" role="navigation">
            <div class="aui-header-primary">
                <span id="logo" class="aui-header-logo aui-header-logo-custom">
                    <!-- Keep in mind that labels should be translated -->
                    <a href="https://example.com/" aria-label="Go to home page"><img src="images/aui-sandbox.png" alt="My application name"></a>
                </span>
            </div>
        </nav>
    </noscript>
</aui-docs-example>

<h4>Use plain text</h4>

<p>
    To render plain-text in the place of the logo, you can add the <code>aui-header-logo-textonly</code> CSS class
    to the <code>aui-header-logo</code> element.
    You <strong>must</strong> include the this class to ensure proper rendering for plain text.
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <nav class="aui-header" role="navigation">
            <div class="aui-header-primary">
                <span id="logo" class="aui-header-logo aui-header-logo-textonly">
                    <!-- Keep in mind that labels should be translated -->
                    <a href="https://example.com/" aria-label="Go to home page">My application name</a>
                </span>
            </div>
        </nav>
    </noscript>
</aui-docs-example>

<h3>Rendering a logo and text</h3>

<p>
    If you want to render both an image <em>and</em> plain-text in the application header,
    add a second <var>span</var> element with the <code>aui-header-logo-text</code> CSS class.
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <nav class="aui-header" role="navigation">
            <div class="aui-header-primary">
                <span id="logo" class="aui-header-logo aui-header-logo-custom">
                    <!-- Keep in mind that labels should be translated -->
                    <a href="https://example.com/" aria-label="Go to home page">
                        <img src="images/aui-sandbox.png" alt="My application name">
                        <span class="aui-header-logo-text">Some extra text</span>
                    </a>
                </span>
            </div>
        </nav>
    </noscript>
</aui-docs-example>
