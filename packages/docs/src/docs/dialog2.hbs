---
component: Dialog2
analytics:
  pageCategory: component
  component: dialog2
design: https://design.atlassian.com/latest/product/components/modal-dialog/
status:
  api: general
  core: false
  wrm: com.atlassian.auiplugin:aui-dialog2
  amd: aui/dialog2
  experimentalSince: 5.3
  generalSince: 5.8
---

<h2>Summary</h2>

<p>Modal dialogs are used to get a response from a user or reveal extra information related to a given scenario.</p>

<h2 id="usage">Usage</h2>

<p>
For the best practices on usage please refer to Atlas Kit
<a href="https://atlassian.design/components/modal-dialog/usage" target="_blank">Dialog usage documentation</a>.
</p>

<p>
For the best practices on accessibility please refer to
<a href="https://www.w3.org/WAI/ARIA/apg/patterns/dialog-modal/" target="_blank">WAI Guidelines on Dialog</a>.
</p>

<p>
There are also
<a href="https://hello.atlassian.net/wiki/spaces/A11YDC/pages/2242773781/Modal+dialog+-+AUI+accessibility+spec"
   target="_blank"
>
    Dialog A11y specifications for AUI
</a>
available inside of Atlassian.
</p>

<h2>Status</h2>
{{> status }}

<aui-docs-contents></aui-docs-contents>

<h2 id="examples">Examples</h2>

<h3 id="anatomy">Anatomy of a dialog</h3>

<p>Below you can see an overview of the dialog HTML pattern.</p>

<aui-docs-example id="static-dialog-example" live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <!--
            Renders a static dialog.
            To ensure the dialog is not rendered inline in the page, add:
             * class="aui-layer"
             * hidden
            to this element.
        -->
        <section
            id="static-dialog"
            class="aui-dialog2 aui-dialog2-medium"
            role="dialog"
            aria-modal="true"
            tabindex="-1"
            aria-labelledby="static-dialog--heading"
            aria-describedby="static-dialog--description"
        >
            <!-- Dialog header -->
            <header class="aui-dialog2-header">
                <!-- The dialog's title -->
                <h1 class="aui-dialog2-header-main" id="static-dialog--heading">The modal dialog title</h1>
            </header>
            <!-- Main dialog content -->
            <div class="aui-dialog2-content" id="static-dialog--description">
                <p>Content for the modal dialog.</p>
            </div>
            <!-- Dialog footer -->
            <footer class="aui-dialog2-footer">
                <!-- Actions to render on the right of the footer -->
                <div class="aui-dialog2-footer-actions">
                    <button class="aui-button aui-button-primary">Okay</button>
                    <button class="aui-button" autofocus>Next</button>
                    <button class="aui-button aui-button-link">Close</button>
                </div>
                <!-- Hint text is rendered on the left of the footer -->
                <div class="aui-dialog2-footer-hint">This is a hint.</div>
            </footer>
        </section>
    </noscript>
    <noscript type="text/js">
        AJS.$("#static-dialog").on("click submit", function(e) {
            e.preventDefault();
            e.stopImmediatePropagation();
        });
    </noscript>
    <noscript type="text/css">
        #static-dialog-example .aui-live-demo {
            position: relative;
            background-color: #999;
        }
        #static-dialog {
            margin-top: 100px; /* to push it inside the example box. */
        }
    </noscript>
</aui-docs-example>

<p>
    On its own, the dialog HTML pattern itself is static &mdash; the contents are added to the page inline.
    To ensure the dialog does not get rendered to the page, you should add <code>aui-layer</code> class
    and <code>hidden</code> attribute to the dialog element.
</p>

<p>
    Note that in the footer, the hint text (<code>class="aui-dialog2-footer-hint"</code>) should be placed
    in the DOM below the footer actions (<code>class="aui-dialog2-footer-actions"</code>) even though the hint text
    appears to the left of the footer actions.
</p>


<h3 id="initial-focus-management">Initial focus management</h3>

<p>
    When working with Dialogs initial focus is one of the most important things impacting user experience.
</p>
<p>
    This is how Dialog2 handles the focus:
    <ol>
        <li> find <b>autofocus</b> attributes in Dialog and focus the first one if found </li>
        <li> if there are no <b>autofocus</b> attributes, find all focusable controls and focus the first one </li>
        <li> if still nothing was focused, focus Dialog container element </li>
    </ol>
</p>
<p>
    When adding Dialog to the page, you need to think about what would be the best initial focus element in this specific Dialog.
    If it's not focused by default, you should consider adding <b>autofocus</b> attribute to it, or restructuring Dialog contents.
</p>
<p>
    If you are not sure what is the best approach in your case, please refer to guides from <a href="#usage"> Usage section </a> of the page.
</p>


<h3 id="open-close">Opening and closing a dialog</h3>

<p>
    By default the dialog closes after the close button or the blanket is clicked.
</p>

<p>
    To make the dialog open and close in other cases you can use <code>.show</code> and <code>.hide</code> methods.
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <!-- Create a trigger which will be used by the JavaScript -->
        <button id="dialog-show-button" class="aui-button">Show dialog</button>

        <section
            id="demo-dialog"
            class="aui-dialog2 aui-dialog2-small aui-layer"
            role="dialog"
            tabindex="-1"
            aria-modal="true"
            aria-labelledby="dialog-show-button--heading"
            aria-describedby="dialog-show-button--description"
            hidden
        >
            <header class="aui-dialog2-header">
                <h1 class="aui-dialog2-header-main" id="dialog-show-button--heading">Captain...</h1>
            </header>
            <div class="aui-dialog2-content" id="dialog-show-button--description">
                <p>We've detected debris of some sort in a loose orbit.</p>
                <p>I suggest we beam a section aboard for analysis...</p>
            </div>
            <footer class="aui-dialog2-footer">
                <div class="aui-dialog2-footer-actions">
                    <button id="dialog-submit-button" class="aui-button aui-button-primary">Make it so</button>
                </div>
            </footer>
        </section>
    </noscript>
    <noscript is="aui-docs-code" type="text/js">
        // Shows the dialog when the "Show dialog" button is clicked
        AJS.$("#dialog-show-button").on('click', function(e) {
            e.preventDefault();
            AJS.dialog2("#demo-dialog").show();
        });

        // Hides the dialog
        AJS.$("#dialog-submit-button").on('click', function (e) {
            e.preventDefault();
            AJS.dialog2("#demo-dialog").hide();
        });
    </noscript>
</aui-docs-example>

<h3>Focus management after dialog is closed</h3>

<p>
    By default after the dialog is closed the focus returns to the element that was focused before it was opened.
</p>

<p>
    However there are cases in which you have to explicitly set the focus after the dialog is closed,
    e.g. the element focused before dialog opening no longer exists in the document.
    While deciding which element should be focused keep in mind the natural flow of operation and user journey.
    Subscribe to <code>hide</code> event to achieve the required behaviour.
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/js">
        // Listen for hide event of #demo-dialog and focus target element
        AJS.dialog2("#demo-dialog").on('hide', function (e) {
            $("#new-active-element").focus()
        });
    </noscript>
</aui-docs-example>

<h3 id="warning">A warning dialog</h3>

<p>
    Use this dialog type when you're representing a destructive action, and want the end-user to think more carefully
    about how they proceed.
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <button id="warning-dialog-show-button" class="aui-button">Show warning dialog</button>

        <section
            id="demo-warning-dialog"
            class="aui-dialog2 aui-dialog2-medium aui-dialog2-warning aui-layer"
            role="dialog"
            tabindex="-1"
            aria-modal="true"
            aria-labelledby="demo-warning-dialog--heading"
            aria-describedby="demo-warning-dialog--description"
            hidden
        >
            <header class="aui-dialog2-header">
                <h1 class="aui-dialog2-header-main" id="demo-warning-dialog--heading">Confirm you want to delete this thing</h1>
            </header>
            <div class="aui-dialog2-content" id="demo-warning-dialog--description">
                <p>If you do this, there's no going back. Are you certain that you want this thing to be gone forever?</p>
            </div>
            <footer class="aui-dialog2-footer">
                <div class="aui-dialog2-footer-actions">
                    <button id="warning-dialog-confirm" class="aui-button aui-button-primary">Delete the thing</button>
                    <button id="warning-dialog-cancel" class="aui-button aui-button-link">Cancel</button>
                </div>
            </footer>
        </section>
    </noscript>
    <noscript is="aui-docs-code" type="text/js">
        // Shows the warning dialog when the "Show warning dialog" button is clicked
        AJS.$("#warning-dialog-show-button").on('click', function (e) {
            e.preventDefault();
            AJS.dialog2("#demo-warning-dialog").show();
        });

        AJS.$(document).on("click", "#demo-warning-dialog button", function (e) {
            e.preventDefault();
            AJS.dialog2("#demo-warning-dialog").hide();
        });
    </noscript>
</aui-docs-example>


<h2 id="options">Options</h2>

<h3 id="html-attributes">HTML attributes</h3>
<p>Dialog2 configuration options are expressed through markup.</p>
<table class="aui">
    <tbody>
    <tr>
        <th scope="col">Class</th>
        <th scope="col">Description</th>
        <th scope="col">Example Usage</th>
    </tr>
    <tr>
        <td><code>aui-dialog2-small | aui-dialog2-medium | aui-dialog2-large | aui-dialog2-xlarge</code></td>
        <td>Controls the size of the dialog according to ADG size specifications.</td>
        <td>
            <noscript is="aui-docs-code" type="text/html">
                <section class="aui-dialog2 aui-dialog2-small"
                         role="dialog"
                         hidden>
                    <!-- inner content -->
                </section>
            </noscript>
        </td>
    </tr>
    <tr>
        <td><code>.aui-dialog2-warning</code></td>
        <td>Gives the dialog's header a red background color.</td>
        <td>
            <noscript is="aui-docs-code" type="text/html">
                <section class="aui-dialog2 aui-dialog2-warning"
                         role="dialog"
                         hidden>
                    <!-- inner content -->
                </section>
            </noscript>
        </td>
    </tr>
    </tbody>
</table>

<table class="aui" id="dialog-attributes">
    <thead>
    <tr>
        <th>Attribute</th>
        <th>Values</th>
        <th class="description">Description</th>
        <th>Example Usage</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><code>data-aui-modal</code></td>
        <td><code>true</code></td>
        <td>Specifies that the dialog is modal. Modal dialogs have no close icon in the top right corner, and cannot be
            closed by clicking on the blanket behind it.
        </td>
        <td>
            <noscript is="aui-docs-code" type="text/html">
                <section class="aui-dialog2"
                         data-aui-modal="true"
                         role="dialog"
                         hidden>
                    <!-- inner content -->
                </section>
            </noscript>
        </td>
    </tr>
    <tr>
        <td><code>data-aui-remove-on-hide</code></td>
        <td><code>true</code></td>
        <td>Specifies that the dialog element should be removed from the DOM when it is hidden, either by clicking on
            the close icon, clicking on the blanket behind the dialog, or calling the hide() method.
        </td>
        <td>
            <noscript is="aui-docs-code" type="text/html">
                <section class="aui-dialog2"
                         data-aui-remove-on-hide="true"
                         role="dialog"
                         hidden>
                    <!-- inner content -->
                </section>
            </noscript>
        </td>
    </tr>
    <tr>
        <td><code>data-aui-focus-selector</code><span class="aui-lozenge aui-lozenge-error">Deprecated</span></td>
        <td><code>selector</code></td>
        <td>
            <p>
                Controls the element that is focussed when the dialog is opened.
                By default the focus goes to the dialog element itself to meet
                the accessibility expectations in most common cases.
            </p>
        </td>
        <td>
            <noscript is="aui-docs-code" type="text/html">
                <section class="aui-dialog2"
                         data-aui-focus-selector=".aui-dialog2-content :input:visible:enabled"
                         role="dialog"
                         hidden>
                    <!-- inner content -->
                </section>
            </noscript>
        </td>
    </tr>
    </tbody>
</table>


<h3 id="javascript">JavaScript API</h3>
<p>To get a reference to the API for a dialog2 instance, call AJS.dialog2(selector), where selector
    can be a selector string, DOM node, or jQuery element.</p>
<noscript is="aui-docs-code" type="text/js">var demoDialog = AJS.dialog2("#demo-dialog");</noscript>

<h4 id="javascript-methods">Methods</h4>
<table class="aui" id="dialog-methods">
    <thead>
    <tr>
        <th>Method</th>
        <th class="description">Description</th>
        <th>Example Usage</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><code>show</code></td>
        <td>Shows a dialog.</td>
        <td>
            <noscript is="aui-docs-code" type="text/js">AJS.dialog2("#demo-dialog").show();</noscript>
        </td>
    </tr>
    <tr>
        <td><code>hide</code></td>
        <td>Hides a dialog.</td>
        <td>
            <noscript is="aui-docs-code" type="text/js">AJS.dialog2("#demo-dialog").hide();</noscript>
        </td>
    </tr>
    <tr>
        <td><code>remove</code></td>
        <td>Removes the dialog from the DOM.</td>
        <td>
            <noscript is="aui-docs-code" type="text/js">AJS.dialog2("#demo-dialog").remove();</noscript>
        </td>
    </tr>
    </tbody>
</table>


<h4 id="javascript-events">Events</h4>
<p>Events are triggered when dialogs are shown or closed. These can be listened to for a single
    dialog instance, or for all dialogs.</p>
<table class="aui" id="dialog-events">
    <thead>
    <tr>
        <th>Event</th>
        <th class="description">Description</th>
        <th>Example Usage</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><code>show</code></td>
        <td>Triggered when a dialog instance is shown.</td>
        <td><noscript is="aui-docs-code" type="text/js">
            AJS.dialog2("#demo-dialog").on("show", function() {
                console.log("demo-dialog was shown");
            });
        </noscript>
        </td>
    </tr>
    <tr>
        <td><code>hide</code></td>
        <td>Triggered when a dialog instance is hidden.</td>
        <td><noscript is="aui-docs-code" type="text/js">
            AJS.dialog2("#demo-dialog").on("hide", function() {
                console.log("demo-dialog was hidden");
            });
        </noscript>
        </td>
    </tr>
    <tr>
        <td><code>global show</code></td>
        <td>Triggered when any dialog is shown.</td>
        <td><noscript is="aui-docs-code" type="text/js">
            AJS.dialog2.on("show", function() {
                console.log("a dialog was shown");
            });
        </noscript>
        </td>
    </tr>
    <tr>
        <td><code>global hide</code></td>
        <td>Triggered when any dialog is hidden.</td>
        <td><noscript is="aui-docs-code" type="text/js">
            AJS.dialog2.on("hide", function() {
                console.log("a dialog was hidden");
            });
        </noscript>
        </td>
    </tr>
    </tbody>
</table>
