---
component: Look and Feel
status:
  api: experimental
  core: true
  wrm:
    - com.atlassian.auiplugin:aui-design-tokens-themes
  experimentalSince: 9.13
---
<h2>Summary</h2>

<p>
    AUI components can be configured with dynamic CSS variables following the pattern
    <code>--atl-theme-&lt;name&gt;</code>.
</p>

<h2>Status</h2>
<p>
    {{> status }}
</p>

<h2>Import</h2>

<div class="aui-message aui-message-warning">
    <p>

        The feature works <strong>only when design token themes are present on the page</strong>,
        see <a href="/docs/dark-mode.html">Dark mode</a>.

    </p>
    <p>
        The Original theme is <strong>not supported</strong>, make sure you don't set the
        <code>--atl-theme&lt;name&gt;</code> variables globally if your application allows the Original
        theme.
    </p>
</div>

<p>

    If you use AUI from WRM, make sure you are including
    <code>com.atlassian.auiplugin:aui-design-tokens-themes</code>. Most likely, you've done this
    already when setting up the design tokens themes.

</p>
<p>

    If you use NPM, there is a separate resource: include the
    <code>aui-prototyping-lf-extracted-color-channels.css</code> file on the page.

</p>


<h2>Usage</h2>
<p>
    Set <code>--atl-theme&lt;name&gt;</code> CSS variables to the page <code>:root</code>. (More
    fine-grained contexts can work too.) It will affect the look and feel of certain AUI
    components.
</p>
<p>
    These variables are intended to be customized by the app admin, and until introduction of this
    AUI feature applications handled the use case differently.
</p>
<p>
    If your application allows customization of a single set of colors, include those variables
    under a CSS selector that catches that a design-tokens-based theme (Light or Dark; not
    Original) is loaded.
</p>

<aui-docs-example live-demo>
<noscript is="aui-docs-code" type="text/less">
    @design-tokens-enabled() {
        --atl-theme-header-bg-color: @customizedPrimaryColor;
    }

    </noscript>
</aui-docs-example>

<p>
    If your application allows customization of pairs of colors&mdash;separately for the Light and
    for the Dark theme,&mdash;include two blocks: the first customized value under the light-theme
    selector and the second customized value under the dark-theme selector.
</p>

<aui-docs-example>
    <noscript is="aui-docs-code" type="text/css">
    @design-tokens-light-theme-enabled() {
        --atl-theme-header-bg-color: @customizedPrimaryColorLight;
    }

    @design-tokens-dark-theme-enabled() {
        --atl-theme-header-bg-color: @customizedPrimaryColorDark;
    }

    </noscript>
</aui-docs-example>

<table class="aui">
    <thead>
    <tr>
        <th>Variable</th>
        <th>Effect</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><code>--atl-theme-header-bg-color</code></td>
        <td>
            Sets app header background. Sets item text color to contrast with the background unless
            the text color is customized.
        </td>
    </tr>
    <tr>
        <td><code>--atl-theme-header-item-text-color</code></td>
        <td>
            Sets text color for regular items in the header. If not set, the appropriate value of
            <code>--ds-text</code> will be used to contrast with the background.
        </td>
    </tr>
    <tr>
        <td><code>--atl-theme-header-primary-button-bg-color</code></td>
        <td>
            Sets background for the primary button in the header. Sets its text color to contrast
            with the background unless the text color is customized.
        </td>
    </tr>
    <tr>
        <td><code>--atl-theme-header-primary-button-text-color</code></td>
        <td>
            Sets text color for the primary button in the header. If not set, the appropriate value
            of <code>--ds-text-inverse</code> will be used to contrast with the background.
        </td>
    </tr>
    <tr>
        <td><code>--atl-theme-header-logo-image</code></td>
        <td>
            For the CSS mode of logo rendering, applies the <code>background-image</code> property
            on the <code>.aui-header-logo-device</code> element. Use in conjunction with the
            <code>--atl-theme-header-logo-width</code> (see below), otherwise the logo won't be
            visible.
        </td>
    </tr>
    <tr>
        <td><code>--atl-theme-header-logo-width</code></td>
        <td>
            For the CSS mode of logo rendering, applies <code>width</code> on the
            <code>.aui-header-logo-device</code> element.
        </td>
    </tr>
    </tbody>
</table>

<h3>Output variables</h3>

<p>
    AUI exports derived variables whose values depend on the input variables.
</p>
<table class="aui">
    <thead>
    <tr>
        <th>Variable</th>
        <th>Explanation</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><code>--atl-theme-header-bg-color-tinted-contrasting</code></td>
        <td>
            A value derived from <code>--atl-theme-header-bg-color</code>: darkened if it is
            perceptually light; lightened if it is perceptually dark. Can be used, for example, to
            show the "selected" state of header items.
        </td>
    </tr>
    </tbody>
</table>

<h2>Accessibility note</h2>

<div class="aui-message aui-message-info">
    <p>
        This feature allows the user to set the customized colors to arbitrary values, which can
        lead to the contrast between elements&mdash;e.g., between the logo and the header, or
        between the item text color and the header background color&mdash;be below the WCAG AA
        recommended values.
    </p>
    <p>
        As such, consider adding a notice for the person who'd be configuring the values (in case
        of an Atlassian product, an admin) that it is their responsibility to check the contrast of
        the customized values.
    </p>
    <p>
        <a href="https://developer.mozilla.org/en-US/docs/Web/Accessibility/Understanding_WCAG/Perceivable/Color_contrast">
            Reference: MDN
        </a>
    </p>
</div>
