---
component: Page layout
analytics:
  pageCategory: component
  component: page
design: https://design.atlassian.com/latest/product/foundations/layout/
status:
  api: general
  core: true
  wrm: com.atlassian.auiplugin:aui-page-layout
  amd: false
  experimentalSince: 3.6
  generalSince: 5.1
---

<h2>Summary</h2>
<p>Common markup to produce a standard page layout and base design.</p>
<p>
    This is an "outside-in" type of component aiming to provide a standardised page, intended for use with the full set
    of application header, navigation, page header, footer, etc. The content area has a preset layout system for common
    layout variations.
    There are several overall page layout options (full width, fixed width, etc).
</p>

<h2>Status</h2>
{{> status }}

<aui-docs-contents></aui-docs-contents>

<h2 id="anatomy">Anatomy of a page</h2>
<h3 id="anatomy-doc">HTML document structure</h3>

<p>AUI requires the following document structure:</p>

<noscript is="aui-docs-code" type="text/html">
    <html lang="en">
        <head>
            <meta charset="utf-8"/>
            <meta http-equiv="X-UA-Compatible" content="IE=EDGE">
            <title><!-- Your page's title --></title>
        </head>

        <!-- All page variations are added to <body> -->
        <body class="...">
            <div id="page">
                <!-- The page's structure is within this container -->
            </div>
        </body>
    </html>
</noscript>

<p>
    The <var>#page</var> container element is important to include.
    Some of AUI's components and behaviours will attempt to place content before or after this element.
</p>

<h3 id="anatomy-containers">Primary containers</h3>
<p>The page is constructed of the following containers, each with a known ID:</p>

<noscript is="aui-docs-code" type="text/html">
    <div id="page">
        <header id="header" role="banner">
            <!--
             The #header container houses the product's application header.
             It may optionally include any banner notifications. -->
        </header>
        <div id="content">
            <!-- The #content container houses various page layout, navigation, and content patterns. -->
        </div>
        <footer id="footer" role="contentinfo">
            <!-- The #footer should include content like the product name, version, support links, etc. -->
        </footer>
    </div>
</noscript>

<p>
    The <var>#content</var> container is required, while the <var>#header</var> and <var>#footer</var> containers
    are optional.
</p>

<h2 id="accessibility">Accessibility</h2>
<p>
    Your page will consist of several landmarks. Users of assistive technologies, such as screen readers,
    can make use of these landmarks to quickly jump to different parts of your page, skipping across
    repeated, familiar, or less relevant content.
</p>

<h3 id="accessibility-screenreader-support">HTML5 element support</h3>
<p>
    The HTML5 <var>&lt;header&gt;</var>, <var>&lt;footer&gt;</var>, and <var>&lt;main&gt;</var> elements have an implied role
    that screen readers should capture. As at <time datetime="2020-04-14">April 14, 2020</time>, our testing
    indicates that some screen readers do not correctly expose these landmarks when using only the HTML5 element.
    <br />
    We recommend adding a "redundant" <var>role</var> attribute to each element that mirrors the semantic
    of the HTML5 element. This provides the widest screen reader support.
</p>

<h3 id="accessibility-main-container">Guarantee a <var>&lt;main&gt;</var> container</h3>
<p>
    The most important landmark is the <var>&lt;main&gt;</var> element.
    There should only be one <var>&lt;main&gt;</var> element per page.
</p>

<p>
    This landmark should be placed around what users would consider the page's "most important"
    or "most unique" content.
    <br/>
    A simple test for determining this would be to ask: "does this content appear on any other pages,
    or only this one?" If it appears on only this page, it should be wrapped in <var>&lt;main&gt;</var>.
    <br />
    Otherwise, it may still be important enough to wrap in <var>&lt;main&gt;</var> &mdash;
    ask "in most cases, would users want to quickly navigate <em>to this content</em>, or would they
    want to <em>skip past it</em>?" If they would skip past it more times than not,
    it's less likely the content should be wrapped in <var>&lt;main&gt;</var>.
</p>

<p>
    Refer to <a href="#examples">the examples</a> on this page to see where <var>&lt;main&gt;</var>
    is placed in common page layouts.
</p>

<h3 id="accessibility-name-nav">Name your landmark elements</h3>
<p>
    Your page may have multiple tiers or hierarchies of landmark elements, such as multiple
    <var>&lt;nav&gt;</var>, <var>&lt;header&gt;</var> or <var>&lt;section&gt;</var> elements.
    Just as the purpose of these elements are represented in a visual hierarchy,
    their purposes need to be perceivable in non-visual contexts, too.
</p>
<p>
    Screen readers use an <code>aria-label</code> value placed on landmark elements to describe
    the purpose of an element. Its value should be a translated, recognisable,
    short name that gives context cues to screen reader users.
</p>

<p>
    We recommend using the following names at specific hierarchy levels on your page:
</p>
<dl>
    <dt>site</dt>
    <dd>
        For the page's top-level <var>#header</var> container or the main navigational element
        inside it (such as the <a href="{{rootPath}}docs/app-header.html">application header</a>).
        <br />
        Screen readers should announce this as <q>site banner</q> if placed on <var>&lt;header&gt;</var>
        or <q>site navigation</q> if placed on <var>&lt;nav&gt;</var>.
    </dd>

    <dt>sidebar</dt>
    <dd>
        If your page includes a sidebar, use this for the <var>&lt;section&gt;</var> that houses it.
        <br />
        Screen readers should announce this as <q>sidebar region</q>.
    </dd>

    <dt>page</dt>
    <dd>
        For any <var>&lt;nav&gt;</var> element that either appears anywhere within the
        <var>.aui-page-panel</var> container, or would change the content rendered inside
        the <var>&lt;main&gt;</var> element.
        <br />
        Screen readers should announce these as <q>page navigation</q>.
    </dd>
</dl>
<p>
    You are encouraged to use domain-specific names for your navigation elements.
    For example:
</p>
<ul>
    <li>
        In Confluence, the sidebar pattern's <var>&lt;nav&gt;</var> can be labelled "space",
        as it only contains navigation items and actions related to Confluence's Spaces concept.
        <br />
        Screen readers should announce this landmark as "space navigation".
    </li>
    <li>
        In Bamboo, a build results page is visually divided in to three regions: the page's header;
        a coloured bar containing the build result; and a page panel with fine-grained build details.
        Each region could be given a more friendly name, such as "build name", "build result", "build details".
    </li>
</ul>

<h3 id="accessibility-combining-nav-and-main">Combining navigation patterns and main container</h3>
<p>
    If your page includes navigation patterns, such as a sidebar or in-page vertical navigation,
    you must ensure the <var>&lt;main&gt;</var> element <strong>does not wrap these navigation patterns</strong>.
    You must place navigation patterns &mdash; such as <a href="{{rootPath}}docs/sidebar.html">sidebar</a>
    or <a href="{{rootPath}}docs/navigation.html#horizontal">horizontal navigation</a> &mdash;
    outside of, or adjacent to, the <var>&lt;main&gt;</var> element.
</p>
<!-- TODO: wrong vs. right examples -->

<h2 id="variations">Variations</h2>
<h3 id="variations-pagetypes">Page types</h3>
<p>The page "type" affects how some sub-parts of the page's common structure are presented in a visual context.</p>
<p>The valid types are as follows:</p>

<table class="aui">
    <thead>
    <tr>
        <th>Page type</th>
        <th>CSS class (add to <code>&lt;body&gt;</code>)</th>
        <th>Intended usage</th>
        <th>Illustration of the effect</th>
    </tr>
    </thead>
    <tbody>
    <tr id="pagetype-fluid">
        <th scope="row">Fluid (full-width) <span class="aui-lozenge aui-lozenge-current">Default</span></th>
        <td><abbr title="Not Applicable">N/A</abbr></td>
        <td>Use this layout for most pages.</td>
        <td>
            <figure>
                <img src="images/example-layout-fluid.png" alt="" width="150" height="130"/>
                <figcaption>
                    The application header and content both stretch to fill the full width of the browser.
                </figcaption>
            </figure>
        </td>
    </tr>
    <tr id="pagetype-fixed">
        <th scope="row">Fixed width</th>
        <td><code>aui-page-fixed</code></td>
        <td>
            This layout reduces the amount of effort required to scan and read textual content.
            Best used on one-off pages with long passages of text, such as legal disclaimers, terms and conditions, or
            other documents.
        </td>
        <td>
            <figure>
                <img src="images/example-layout-fixed.png" alt="" width="150" height="130"/>
                <figcaption>
                    The application header contents are horizontally centered in the middle of the
                    header bar. The page's content width is constrained and is horizontally centered in the browser.
                    The application header, page header, and page contents are aligned vertically.
                </figcaption>
            </figure>
        </td>
    </tr>
    <tr id="pagetype-hybrid">
        <th scope="row">Hybrid width</th>
        <td><code>aui-page-hybrid</code></td>
        <td>
            Use this layout to get the benefit of "Fixed width" legibility for long passages of text, but
            keep the application header's content positions consistent across other pages.
        </td>
        <td>
            <figure>
                <img src="images/example-layout-hybrid.png" alt="" width="150" height="130"/>
                <figcaption>
                    The application header contents and header bar both span the full width of the browser, while
                    the page's content width is constrained. The content is horizontally centered in the browser.
                </figcaption>
            </figure>
        </td>
    </tr>
    <tr id="pagetype-focused">
        <th scope="row">Focused task page</th>
        <td><code>aui-page-focused</code></td>
        <td>
            Use this layout for every step in a logical flow the user should go through from start to finish,
            such as product setup pages, bulk changes to data, imports or exports, and other "wizard"-style processes.
        </td>
        <td>
            <figure>
                <img src="images/example-layout-focusedtask.png" alt="" width="150" height="130"/>
                <figcaption>
                    The application header contents and header bar both span the full width of the browser.
                    The page's main content is narrow, with significant whitespace between it and the application header.
                    Forms in this layout have alterations to encourage easy flow between stages in a process.
                </figcaption>
            </figure>
        </td>
    </tr>
    <tr id="pagetype-notification">
        <th scope="row">Notification task</th>
        <td><code>aui-page-notification</code></td>
        <td>
            The layout is similar to "Focused task", but has purpose-built extensions for rendering textual content.
            <br />
            Use this layout when there is exceptional information to present to the user, such as when
            a server error occurs, a page cannot be found, a system is down for maintenance, etc.
        </td>
        <td>
            <figure>
                <img src="images/example-layout-notification.png" alt="" width="150" height="130"/>
                <figcaption>
                    The application header contents and header bar both span the full width of the browser.
                    The page's main content is narrow, with significant whitespace between it and the application header.
                    Textual content in this layout have alterations to improve legibility of important information
                    and support progressive disclosure of additional information.
                </figcaption>
            </figure>
        </td>
    </tr>
    </tbody>
</table>

<h3 id="variation-sizes">Page sizes</h3>
<p>
    The focused and notification pages allow for adjusting the width of content by applying one of the following CSS
    classes to the <code>&lt;body&gt;</code> tag:
</p>
<ul>
    <li><code>aui-page-size-small</code></li>
    <li><code>aui-page-size-medium</code></li>
    <li><code>aui-page-size-large</code></li>
    <li><code>aui-page-size-xlarge</code> <span class="aui-lozenge aui-lozenge-current">Default</span></li>
</ul>


<h3 id="variation-content">Page content patterns</h3>

<p>
    Within the <var>#content</var> container, you may place any of the following:
</p>

<table class="aui">
    <thead>
        <tr>
            <th>Pattern</th>
            <th>Required markup structure inside <var>#content</var></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><a href="{{rootPath}}docs/sidebar.html">Sidebar</a></td>
            <td>
                <noscript is="aui-docs-code" type="text/html">
                    <div id="content">
                        <div class="aui-sidebar">
                            <!-- The sidebar pattern and content goes here -->
                        </div>
                        <div class="aui-page-panel">
                            <div class="aui-page-panel-inner">
                                <main id="main" role="main" class="aui-page-panel-content">
                                    <!-- Your page heading and content goes here -->
                                </main>
                            </div>
                        </div>
                    </div>
                </noscript>
            </td>
        </tr>
        <tr>
            <td>
                <a href="{{rootPath}}docs/navigation.html#horizontal">Horizontal navigation</a>
                <em>above</em> the standard <a href="{{rootPath}}docs/page-content-layout.html">page content layout</a>.
            </td>
            <td>
                <noscript is="aui-docs-code" type="text/html">
                    <div id="content">
                        <nav class="aui-navgroup aui-navgroup-horizontal">
                            <!-- The horizontal nav pattern -->
                        </nav>
                        <div class="aui-page-panel">
                            <div class="aui-page-panel-inner">
                                <main id="main" role="main" class="aui-page-panel-content">
                                    <!-- Your page heading and content goes here -->
                                </main>
                            </div>
                        </div>
                    </div>
                </noscript>
            </td>
        </tr>
        <tr>
            <td>
                <a href="{{rootPath}}docs/navigation.html#vertical">Vertical navigation</a>
                within the standard <a href="{{rootPath}}docs/page-content-layout.html">page content layout</a>.
            </td>
            <td>
                <noscript is="aui-docs-code" type="text/html">
                    <div id="content">
                        <div class="aui-page-panel">
                            <div class="aui-page-panel-inner">
                                <nav class="aui-page-panel-nav">
                                    <div class="aui-navgroup aui-navgroup-vertical">
                                        <!-- The vertical nav pattern -->
                                    </div>
                                </nav>
                                <main id="main" role="main" class="aui-page-panel-content">
                                    <!-- Your page heading and content goes here -->
                                </main>
                            </div>
                        </div>
                    </div>
                </noscript>
            </td>
        </tr>
    </tbody>
</table>


<h2 id="examples">Examples</h2>
<h3 id="example-focuspage">Large focused page with horizontal navigation</h3>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
    <html lang="en">
        <head>
            <meta charset="utf-8"/>
            <meta http-equiv="X-UA-Compatible" content="IE=EDGE">
            <title>AUI - Default Page</title>
        </head>

        <!-- For a normal page, omit all 'aui-page-' classes on body -->
        <body class="aui-page-focused aui-page-size-large">

        <div id="page">
            <header id="header" role="banner">

                {{> example-appheader-skiplinks }}
                {{> example-appheader-tiny }}
            </header>

            <div id="content">
                <div class="aui-page-header">
                    <div class="aui-page-header-inner">
                        <div class="aui-page-header-main">
                            <h1>Default page layout</h1>
                        </div>
                    </div>
                </div>

                <!-- Remember to translate all `aria-label` values!
                     Use the `aui.landmarks.main` i18n key here. -->
                <nav class="aui-navgroup aui-navgroup-horizontal" aria-label="main">
                    <div class="aui-navgroup-inner">
                        <div class="aui-navgroup-primary">
                            <ul class="aui-nav">
                                <li><a href="https://example.com">Nav item</a></li>
                            </ul>
                        </div>

                        <div class="aui-navgroup-secondary">
                            <ul class="aui-nav">
                                <li><a href="https://example.com/">Nav item</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div class="aui-page-panel">
                    <div class="aui-page-panel-inner">
                        <!-- The redundant `role` attribute is required for the VoiceOver + Safari combo -->
                        <main id="main" role="main" class="aui-page-panel-content">
                            <h2>Page content heading</h2>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Phasellus vitae diam in arcu ultricies gravida sed sed nisl.
                                Curabitur nibh nulla, semper non pharetra eu, suscipit vitae eros.
                                Donec eget lectus elit. Etiam metus diam, adipiscing convallis
                                blandit sit amet, sollicitudin sit amet felis. Phasellus justo elit,
                                rhoncus sed tincidunt a, auctor sit amet turpis. Praesent turpis lectus,
                                aliquet vitae sollicitudin ac, convallis vitae urna. Donec consectetur
                                lacus a lacus tincidunt at varius felis venenatis. Pellentesque dapibus
                                mattis arcu, a vestibulum lacus congue at.
                            </p>
                        </main>
                    </div>
                </div>
            </div>

            <footer id="footer" role="contentinfo">
                <section class="footer-body">
                    <ul>
                        <li>I &hearts; AUI</li>
                    </ul>

                    <div id="footer-logo"><a href="https://www.atlassian.com/" target="_blank">Atlassian</a></div>
                </section>
            </footer>
        </div>
        </body>
    </html>
    </noscript>
</aui-docs-example>

<h3 id="example-withsidebar">A page with sidebar</h3>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <html lang="en">
        <head>
            <meta charset="utf-8"/>
            <meta http-equiv="X-UA-Compatible" content="IE=EDGE">
            <title>AUI - Default Page</title>
        </head>
        <body>

        <div id="page">
            <header id="header" role="banner">

                {{> example-appheader-skiplinks }}
                {{> example-appheader-tiny }}
            </header>

            <div id="content">
                <!-- Remember to translate all `aria-label` values!
                     Use the `aui.landmarks.sidebar` i18n key here. -->
                <section class="aui-sidebar" aria-label="sidebar">
                    <!-- Your sidebar markup goes here ;) -->
                </section>
                <!-- The redundant `role` attribute is required for the VoiceOver + Safari combo -->
                <main role="main" id="main">
                    <div class="aui-page-header">
                        <div class="aui-page-header-inner">
                            <div class="aui-page-header-main">
                                <h1>A page with a sidebar</h1>
                            </div>
                        </div>
                    </div>

                    <div class="aui-page-panel">
                        <div class="aui-page-panel-inner">
                            <section class="aui-page-panel-content">
                                <h2>Page content heading</h2>

                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Phasellus vitae diam in arcu ultricies gravida sed sed nisl.
                                    Curabitur nibh nulla, semper non pharetra eu, suscipit vitae eros.
                                    Donec eget lectus elit. Etiam metus diam, adipiscing convallis
                                    blandit sit amet, sollicitudin sit amet felis. Phasellus justo elit,
                                    rhoncus sed tincidunt a, auctor sit amet turpis. Praesent turpis lectus,
                                    aliquet vitae sollicitudin ac, convallis vitae urna. Donec consectetur
                                    lacus a lacus tincidunt at varius felis venenatis. Pellentesque dapibus
                                    mattis arcu, a vestibulum lacus congue at.
                                </p>
                            </section>
                        </div>
                    </div>
                </main>
            </div>

            <footer id="footer" role="contentinfo">
                <section class="footer-body">
                    <ul>
                        <li>I &hearts; AUI</li>
                    </ul>

                    <div id="footer-logo"><a href="https://www.atlassian.com/" target="_blank">Atlassian</a></div>
                </section>
            </footer>
        </div>
        </body>
        </html>
    </noscript>
</aui-docs-example>

<h3 id="example-multinav">An admin page (multiple tiers of navigation)</h3>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <html lang="en">
        <head>
            <meta charset="utf-8"/>
            <meta http-equiv="X-UA-Compatible" content="IE=EDGE">
            <title>AUI - Default Page</title>
        </head>
        <body>

        <div id="page">
            <header id="header" role="banner">

                {{> example-appheader-skiplinks }}
                {{> example-appheader-tiny }}
            </header>

            <div id="content">
                <!-- Remember to translate all `aria-label` values!
                     Use the `aui.landmarks.sidebar` i18n key here. -->
                <section class="aui-sidebar" aria-label="sidebar">
                    <!-- Your sidebar markup goes here ;) -->
                </section>
                <!-- You may choose to label this element according to your information hierarchy. -->
                <section>
                    <div class="aui-page-header">
                        <div class="aui-page-header-inner">
                            <div class="aui-page-header-main">
                                <h1>Admin section</h1>
                            </div>
                            <div class="aui-page-header-actions">
                                <div class="aui-buttons">
                                    <button type="button" class="aui-button">
                                        Do a thing here
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- You may add a horizontal navigation pattern here ;)
                         Be sure to label it with the same label as the parent section element has. -->

                    <div class="aui-page-panel">
                        <div class="aui-page-panel-inner">
                            <div class="aui-page-panel-nav">
                                <!-- Remember to translate all `aria-label` values!
                                     Consider using the `aui.landmarks.page` i18n key here. -->
                                <nav class="aui-navgroup aui-navgroup-vertical" aria-label="page">
                                    <!-- Your in-page navigation content goes here. -->
                                </nav>
                            </div>
                            <!-- The redundant `role` attribute is required for the VoiceOver + Safari combo -->
                            <main id="main" role="main" class="aui-page-panel-content">
                                <h2>Page content heading</h2>

                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Phasellus vitae diam in arcu ultricies gravida sed sed nisl.
                                    Curabitur nibh nulla, semper non pharetra eu, suscipit vitae eros.
                                    Donec eget lectus elit. Etiam metus diam, adipiscing convallis
                                    blandit sit amet, sollicitudin sit amet felis. Phasellus justo elit,
                                    rhoncus sed tincidunt a, auctor sit amet turpis. Praesent turpis lectus,
                                    aliquet vitae sollicitudin ac, convallis vitae urna. Donec consectetur
                                    lacus a lacus tincidunt at varius felis venenatis. Pellentesque dapibus
                                    mattis arcu, a vestibulum lacus congue at.
                                </p>
                            </main>
                            <aside class="aui-page-panel-sidebar">
                                <h2>Explanation of the page</h2>
                                <p>
                                    Docs, tips, tutorials, steps, or other helpful text can go here.
                                </p>
                            </aside>
                        </div>
                    </div>
                </section>
            </div>

            <footer id="footer" role="contentinfo">
                <section class="footer-body">
                    <ul>
                        <li>I &hearts; AUI</li>
                    </ul>

                    <div id="footer-logo"><a href="https://www.atlassian.com/" target="_blank">Atlassian</a></div>
                </section>
            </footer>
        </div>
        </body>
        </html>
    </noscript>
</aui-docs-example>

<h3 id="example-systemnotify">Notification page for system status</h3>
<p>
    See <a href="{{rootPath}}docs/system-notifications.html">the System notification page</a> for this example.
</p>


<h2 id="code">Code</h2>
<h3 id="code-soy">Soy</h3>

<p>
    To construct a full document with soy, you need to call <code>document</code> and <code>page</code> templates.
</p>

<noscript is="aui-docs-code" type="text/handlebars">
    {template .index}
        {call aui.page.document}
            {param windowTitle: 'Window title text' /}
            {param headContent}
                <!-- HEAD content such as CSS and JS resources -->
            {/param}
            {param content}
                {call aui.page.page}
                    {param headerContent}
                        <!-- call aui.page.header here -->
                    {/param}
                    {param contentContent}
                        <!-- call aui.page.pagePanel here -->
                    {/param}
                    {param footerContent}
                        <!-- literal content here -->
                    {/param}
                {/call}
            {/param}
        {/call}
    {/template}
</noscript>

<p>To set the page layout in Soy, use the pageType param:</p>
<noscript is="aui-docs-code" type="text/handlebars">
    @param? pageType Default: default (full width).
      Options: default, focused, notification, fixed, hybrid, generic (no class applied).
</noscript>

<p>If you need a small focused page, you can set the size with pageSize. focusedPageSize as been deprecated:</p>
<noscript is="aui-docs-code" type="text/handlebars">
    @param? pageSize Default: xlarge.
      Options: small, medium, large, xlarge.
</noscript>
