---
component: Date pickers
analytics:
  pageCategory: component
  component: date-picker
status:
  api: general
  core: false
  wrm: com.atlassian.auiplugin:aui-date-picker
  amd: false
  experimentalSince: 4.0
  generalSince: 5.1
---

<h2>Summary</h2>
<p>A simple date picker polyfill for browsers that don't support date fields. Based on the jQuery UI Date Picker.</p>

<h2>Status</h2>
{{> status }}

<h2>Code</h2>

<h3>HTML &amp; JavaScript</h3>

<p>The AUI Date Picker is set up with an HTML5 date input, plus JavaScript to set the desired options. In most implementations Date Picker is set to override the native implementation at all times. This gives a consistent experience in all browsers.</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <input class="aui-date-picker" id="demo-range-1" type="date" max="2012-01-25" min="2011-12-25" />
    </noscript>
    <noscript is="aui-docs-code" type="text/js">
        const demo1 = document.getElementById('demo-range-1');
        const controller = new AJS.DatePicker(demo1, {'overrideBrowserDefault': true});
    </noscript>
</aui-docs-example>

<h2 id="options">Options</h2>

<p>Options are set in the JavaScript options object:</p>

<table class="aui">
    <thead>
        <tr>
            <th scope="col">Option</th>
            <th scope="col">Type</th>
            <th scope="col">Default</th>
            <th scope="col">Effect</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>overrideBrowserDefault</code></td>
            <td>Boolean</td>
            <td>false</td>
            <td>Provides the polyfill regardless of whether or not the browser supports date input fields. If the browser supports date input fields, the field type is changed totext&nbsp;to suppress the browser's native date picker implementation.&nbsp;<br><br>Use this option to maintain a consistent look across browsers, especially when native implementations are aesthetically unpleasant (as with Opera) or incomplete (as with Safari's spinner-only implementation).</td>
        </tr>
        <tr>
            <td><code>firstDay</code></td>
            <td>Number</td>
            <td>-1</td>
            <td>Sets the starting day of the calendar week. Zero to six represents Sunday to Saturday, and -1 represents the localisation's default setting.</td>
        </tr>
        <tr>
            <td><code>languageCode</code></td>
            <td>String</td>
            <td>The&nbsp;<code>html</code> element's <code>lang</code> attribute if available, otherwise <code>en-AU</code>.</td>
            <td>
               <p>Localises various aspects of the date picker, including day names, the first day of the week, RTL text, month names, whether the year precedes the month, and whether the year requires a suffix.&nbsp;</p>
               <p>The following locales are supported: '' (used when an unavailable language code is specified), 'af', 'ar-DZ', 'ar', 'az', 'bg', 'bs', 'ca', 'cs', 'da', 'de', 'el', 'en-AU', 'en-GB', 'en-NZ', 'eo', 'es', 'et', 'eu', 'fa', 'fi', 'fo', 'fr-CH', 'fr', 'gl', 'he', 'hr', 'hu', 'hy', 'id', 'is', 'it', 'ja', 'ko', 'kz', 'lt', 'lv', 'ml', 'ms', 'nl', 'no', 'pl', 'pt-BR', 'pt', 'rm', 'ro', 'ru', 'sk', 'sl', 'sq', 'sr-SR', 'sr', 'sv', 'ta', 'th', 'tj', 'tr', 'uk', 'vi', 'zh-CN', 'zh-HK', and 'zh-TW'.</p>
            </td>
        </tr>
        <tr>
            <td><code>dateFormat</code></td>
            <td>String</td>
            <td><var>yy-mm-dd</var> (outputs an ISO 8601 standard date)</td>
            <td>Sets the format to use when outputting the user's chosen date. See the <a href="#date-format">date format</a> section for details.</td>
        </tr>
        <tr>
            <td><code>hint</code></td>
            <td>String</td>
            <td>undefined</td>
            <td>Displays hint message below the calendar</td>
        </tr>
        <tr>
            <td><code>placeholder</code></td>
            <td>String</td>
            <td>undefined</td>
            <td>Sets the placeholder for input. Works only if the <code>overrideBrowserDefault</code> is set to <code>true</code></td>
        </tr>
        <tr>
            <td><code>minDate</code></td>
            <td>String</td>
            <td>undefined</td>
            <td>Sets the minimum date. Overrides <code>min</code> attrubute from the input field.</td>
        </tr>
        <tr>
            <td><code>maxDate</code></td>
            <td>String</td>
            <td>undefined</td>
            <td>Sets the maximum date. Overrides <code>max</code> attrubute from the input field.</td>
        </tr>
    </tbody>
</table>

<h2>Using calendar widget alone</h2>

<p>There is an option to use the calendar widget without dialog and related input field. </p>
<p>Apart from the options from the listing above you can use any options acceptable by jQueryUI <code>datepicker</code> here.</p>
<p><strong>Exception!</strong> The&nbsp;<code>overrideBrowserDefault</code> is not supported here.</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <div id="demo-calendar-container"></div>
    </noscript>
    <noscript is="aui-docs-code" type="text/js">
        const demo2 = document.getElementById('demo-calendar-container');
        const controller = new AJS.CalendarWidget(demo2, {
            minDate: '24 Jun 2000',
            maxDate: '28 Jun 2000',
            dateFormat: 'd M yy',
            defaultDate: '26 Jun 2000',
            hint: 'This will be a hint message below the calendar.'
        });
    </noscript>
</aui-docs-example>

<h2 id="date-format">Date format</h2>

<p>The format can be combinations of the following:</p>

<ul>
    <li><b><code>d</code></b> - day of month (no leading zero)
    <li><b><code>dd</code></b> - day of month (two digit)
    <li><b><code>o</code></b> - day of the year (no leading zeros)
    <li><b><code>oo</code></b> - day of the year (three digit)
    <li><b><code>D</code></b> - day name short
    <li><b><code>DD</code></b> - day name long
    <li><b><code>m</code></b> - month of year (no leading zero)
    <li><b><code>mm</code></b> - month of year (two digit)
    <li><b><code>M</code></b> - month name short
    <li><b><code>MM</code></b> - month name long
    <li><b><code>y</code></b> - year (two digit)
    <li><b><code>yy</code></b> - year (four digit)
    <li><b><code>@</code></b> - Unix timestamp (ms since 01/01/1970)
    <li><b><code>!</code></b> - Windows ticks (100ns since 01/01/0001)
    <li><b><code>'...'</code></b> - literal text
    <li><b><code>''</code></b> - single quote
    <li><b><code>anything else</code></b> - literal text
</ul>


<p>
    The default format used by the date picker is <code>yy-mm-dd</code>, which is an ISO 8601 standard format.
    This will output <code>2020-01-29</code>.
</p>

<p>
    More date format examples can be found
    <a href="https://jqueryui.com/datepicker/#date-formats" target="_blank">
        in the jQuery UI documentation
    </a>.
</p>

<h2 id="api-reference">API reference</h2>
<h3 id="api-construction">Construction</h3>

<p>
    There are two ways to construct a date picker: via the global constructor, or via a jQuery helper. Either approach will return
    a date picker controller object.
</p>

<aui-docs-example>
    <noscript is="aui-docs-code" type="text/js">
        // "el" should be a non-null HTMLElement
        const el = document.querySelector('.my-field');
        // "options" are listed further up this page
        const options = { ... };
    </noscript>
    <noscript is="aui-docs-code" type="text/js">
        let controller;
        // attach a date picker to an HTML input field via global
        controller = new AJS.DatePicker(el, options);

        // attach a date picker to an HTML input field via jQuery helper
        controller = AJS.$(el).datePicker(options);

        // render a calendar widget in to el via global
        controller = new AJS.CalendarWidget(el, options);

        // render a calendar widget in to el via jQuery helper
        controller = AJS.$(el).calendarWidget(options);
    </noscript>
</aui-docs-example>

<p>
    Note: Using the jQuery helper will avoid re-constructing a date picker for the same element multiple times.
</p>

<h3 id="methods">Date picker controller methods</h3>

<table class="aui" id="methods-description-table">
    <thead>
    <tr>
        <th>Function</th>
        <th>Arguments</th>
        <th class="description">Description</th>
        <th>Example Usage</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>reconfigure</td>
        <td><a href="#options"><var>DatePickerOptions</var> object</a></td>
        <td>Replaces all configuration options with values. Any unset options will be set to their <strong>default values</strong>.</td>
        <td>
            <noscript is="aui-docs-code" type="text/js">controller.reconfigure({ firstDay: 5 });</noscript>
        </td>
    </tr>
    <tr>
        <td>destroy</td>
        <td>&mdash;</td>
        <td>
            <p>Removes the date picker or calendar widget from the element, as well as all behaviours and event handlers.</p>
            <p>Some notes:</p>
            <ul>
                <li>
                    In the case of the date picker, the field's <var>type</var> will return to its original value. However, whatever <var>value</var> has been
                    set to by the user will remain.
                </li>
                <li>
                    In the case of the calendar widget, all the element's inner HTML will be removed.
                </li>
                <li>
                    You can also check for the existence of a <var>destroyPolyfill</var> function. If it is set,
                    calling it will also destroy the date picker widget. This is not recommended, but it is
                    useful when working with previous versions of AUI.
                </li>
            </ul>
        </td>
        <td>
            <noscript is="aui-docs-code" type="text/js">controller.destroy();</noscript>
        </td>
    </tr>
    </tbody>
</table>
