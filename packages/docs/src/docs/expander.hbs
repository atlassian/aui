---
component: Expanders
analytics:
  pageCategory: component
  component: expander
design: https://design.atlassian.com/latest/product/patterns/reveal-text/
status:
  api: general
  core: false
  wrm: com.atlassian.auiplugin:aui-expander
  amd: false
  experimentalSince: 5.1
  generalSince: 5.8
---

<h2>Summary</h2>

<p>Use this when you need to show users one or more small snippets of a larger body of text in order to keep the
    user interface lightweight and scannable.</p>

<p>This pattern includes both revealing the rest of the text on the same screen, and linking to another screen for
    the full text.</p>

<h2>Status</h2>
{{> status }}

<aui-docs-contents></aui-docs-contents>

<h2 id="examples">Examples</h2>

<article class="aui-flatpack-example">

    <h2>Content title</h2>

    <div id="reveal-text-content-example" class="aui-expander-content">
        What happens when you add a shiny new browser to a stack of already-disagreeing citizens? You’ll inevitably
        find some bugs. This is the story of how we found a rendering quirk and how the Atlassian frontend team
        found and refined the fix. The Problem The Atlassian User Interface (AUI) library has just finished an IE10
        sprint to get our library prepped and ready for the newest member of the browser family. While IE10 seems
        generally quite good, we found a couple of problems due to IE10 dropping conditional comments; plus some
        undesirable behaviours.
        <button id="reveal-text-trigger-example" data-replace-text="Show less"
            class="aui-expander-trigger aui-expander-reveal-text" aria-controls="reveal-text-content-example">
            Show more
        </button>
    </div>

    <style>
        #reveal-text-content-example {
            min-height: 1.5em;
        }
    </style>
</article>

<h2 id="code">Code</h2>
<h3 id="code-html">HTML</h3>
<h4>Regular expander</h4>
<p>To use regular expander through HTML you need to create:</p>
<ul>
    <li>A content container, typically a <code>div</code> element with a <code>aui-expander-content</code> class
        and a unique id.
    </li>
    <li>An expander trigger, ideally a <code>button</code> for accessibility reasons, with a
        <code>aui-expander-trigger</code> class and an <code>aria-controls</code> attribute set to the unique
        id of the content container.
    </li>
</ul>
<p>If you wish to have the text inside the trigger replaced, you must supply a <code>data-replace-text</code> attribute.
    This text will serve as the replacement for the expander's next state. The expander will find the most deeply
    nested tag inside and substitute the text within it.</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <div class="aui-expander-content" id="example-regular-expander-html">
        What happens when you add a shiny new browser to a stack of already-disagreeing citizens? You’ll inevitably find some bugs. This is the story of how we found a rendering quirk and how the Atlassian frontend team found and refined the fix. The problem The Atlassian User Interface (AUI) library has just finished an IE10 sprint to get our library prepped and ready for the newest member of the browser family. While IE10 seems generally quite good, we found a couple of problems due to IE10 dropping conditional comments; plus some undesirable behaviours.
        </div>
        <button class="aui-expander-trigger" aria-controls="example-regular-expander-html" data-replace-text="Read less">Read more</button>
    </noscript>
</aui-docs-example>

<h4>Initially opened expander</h4>
<p>To create an expander that is initially opened via HTML, you need to apply the <code>expanded</code> class to the content
    container.</p>
<p>
    Keep in mind that the button's content corresponds to the initial state of the expander. Therefore, in this example,
    it's for the expanded state of the expander.<br />
</p>
<p>The <code>data-replace-text</code> will be used for the collapsed state.</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <div class="aui-expander-content expanded" id="example-expanded-expander-html">
            What happens when you add a shiny new browser to a stack of already-disagreeing citizens? You’ll inevitably find some bugs. This is the story of how we found a rendering quirk and how the Atlassian frontend team found and refined the fix. The problem The Atlassian User Interface (AUI) library has just finished an IE10 sprint to get our library prepped and ready for the newest member of the browser family. While IE10 seems generally quite good, we found a couple of problems due to IE10 dropping conditional comments; plus some undesirable behaviours.
        </div>
        <button class="aui-expander-trigger" aria-controls="example-expanded-expander-html" data-replace-text="Read more">Read less</button>
    </noscript>
</aui-docs-example>

<h4>Reveal text</h4>
<p>Expander also allows us to display a portion of the text initially. The remaining text can be
    revealed by interacting with the expander. This is particularly useful when dealing with long
    blocks of text where you want to provide the user with the option to view more information if they choose.
</p>
<p>
    To create this, you need to follow these steps:
    <ul>
        <li>Enclose the expander's trigger within the content.</li>
        <li>Add the <code>aui-expander-reveal-text</code> class to the trigger.</li>
        <li>Use CSS to set a minimum height for the expander content. This will serve as the limit for the text.</li>
    </ul>
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <div id="container">
            <div class="aui-expander-content" id="example-reveal-text-html">
                What happens when you add a shiny new browser to a stack of already-disagreeing citizens? You’ll inevitably
                find some bugs. This is the story of how we found a rendering quirk and how the Atlassian frontend team
                found and refined the fix. The problem The Atlassian User Interface (AUI) library has just finished an IE10
                sprint to get our library prepped and ready for the newest member of the browser family. While IE10 seems
                generally quite good, we found a couple of problems due to IE10 dropping conditional comments; plus some
                undesirable behaviours
                <button class="aui-expander-trigger aui-expander-reveal-text" aria-controls="example-reveal-text-html" data-replace-text="Show less">
                    Show more
                </button>
            </div>
        </div>
    </noscript>
    <noscript is="aui-docs-code" type="text/css">
        #example-reveal-text-html {
            min-height: 1.5em;
        }
    </noscript>
</aui-docs-example>

<h3 id="code-soy">Soy</h3>

<h3>Regular expander</h3>
<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.expander.trigger}
        {param id: 'replace-text-trigger'/}
        {param contentId: 'expander-with-replace-text-content'/}
        {param content: 'Read More'/}
        {param replaceText: 'Read Less'/}
    {/call}
    {call aui.expander.content}
        {param id: 'expander-with-replace-text-content'/}
        {param content}
        What happens when you add a shiny new browser to a stack of already-disagreeing citizens? You’ll inevitably find some bugs. This is the story of how we found a rendering quirk and how the Atlassian frontend team found and refined the fix. The problem The Atlassian User Interface (AUI) library has just finished an IE10 sprint to get our library prepped and ready for the newest member of the browser family. While IE10 seems generally quite good, we found a couple of problems due to IE10 dropping conditional comments; plus some undesirable behaviours
        {/param}
    {/call}
</noscript>
<h3>Reveal text</h3>
<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.expander.revealText}
        {param triggerId: 'reveal-text-trigger'/}
        {param contentId: 'reveal-text-content'/}
        {param contentContent}
            What happens when you add a shiny new browser to a stack of already-disagreeing citizens? You’ll inevitably find some bugs. This is the story of how we found a rendering quirk and how the Atlassian frontend team found and refined the fix.
            The problem The Atlassian User Interface (AUI) library has just finished an IE10 sprint to get our library prepped and ready for the newest member of the browser family. While IE10 seems generally quite good, we found a couple of problems due to IE10 dropping conditional comments; plus some undesirable behaviours.
        {/param}
    {/call}
</noscript>

<h2 id="options">Options</h2>

<div class="aui-tabs horizontal-tabs">
    <ul class="tabs-menu">
        <li class="menu-item active-tab">
            <a href="#options-html">HTML</a>
        </li>
        <li class="menu-item">
            <a href="#options-soy">Soy</a>
        </li>
        <li class="menu-item">
            <a href="#options-css">CSS</a>
        </li>
        <li class="menu-item">
            <a href="#options-events">Events</a>
        </li>
    </ul>
    <div class="tabs-pane active-pane" id="options-html">
        <h4>Trigger</h4>
        <table class="aui">
            <thead>
                <tr>
                    <th>Class</th>
                    <th>Description</th>
                    <th>Required</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><code>aui-expander-trigger</code></td>
                    <td>This is the necessary class to apply when using a trigger</td>
                    <td><span class="aui-icon aui-icon-small aui-iconfont-check" role="img" aria-label="Required" />
                    </td>
                </tr>
                <tr>
                    <td><code>aui-expander-reveal-text</code></td>
                    <td>Necessary class when implementing the expander reveal text pattern</td>
                    <td><span class="aui-icon aui-icon-small aui-iconfont-cross" role="img" aria-label="Not required" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="aui">
            <thead>
                <tr>
                    <th>Attribute</th>
                    <th>Description</th>
                    <td>Required</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><code>aria-controls</code></td>
                    <td>The id of the content that this trigger expands/hides</td>
                    <td><span class="aui-icon aui-icon-small aui-iconfont-check" role="img" aria-label="Required" />
                    </td>
                </tr>
                <tr>
                    <td><code>data-replace-text</code></td>
                    <td>The text that replaces the trigger's content after the expanding/collapsing the component</td>
                    <td><span class="aui-icon aui-icon-small aui-iconfont-cross" role="img" aria-label="Not required" />
                    </td>
                </tr>
                <tr>
                    <td><code>data-replace-selector</code> <span
                            class="aui-lozenge aui-lozenge-removed">deprecated</span></td>
                    <td>Selector to the element that will get get the text from <code>data-replace-text</code> replaced
                    </td>
                    <td><span class="aui-icon aui-icon-small aui-iconfont-cross" role="img" aria-label="Not required" />
                    </td>
                </tr>
                <tr>
                    <td><code>data-collapsible</code></td>
                    <td>Defines whether the component can be collapsed or not</td>
                    <td><span class="aui-icon aui-icon-small aui-iconfont-cross" role="img" aria-label="Not required" /></td>
                </tr>
            </tbody>
        </table>

        <h4>Content</h4>
        <table class="aui">
            <thead>
                <tr>
                    <th>Class</th>
                    <th>Description</th>
                    <th>Required</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><code>aui-expander-content</code></td>
                    <td>This is the necessary class to apply when using a expander's content</td>
                    <td><span class="aui-icon aui-icon-small aui-iconfont-check" role="img" aria-label="Required" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="aui">
            <thead>
                <tr>
                    <th>Attribute</th>
                    <th>Description</th>
                    <th>Required</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><code>id</code></td>
                    <td>Unique id that triggers must match to work properly</td>
                    <td><span class="aui-icon aui-icon-small aui-iconfont-check" role="img" aria-label="Required" />
                    </td>
                </tr>
                <tr>
                    <td><code>aria-expanded</code> <span class="aui-lozenge aui-lozenge-removed">deprecated</span></td>
                    <td>Old version of setting the content to be initially expanded</td>
                    <td><span class="aui-icon aui-icon-small aui-iconfont-cross" role="img" aria-label="Not required" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="tabs-pane" id="options-soy">
        <h4>Trigger</h4>
        <table class="aui">
            <thead>
                <tr>
                    <th>Param</th>
                    <th>Description</th>
                    <th>Required</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><code>content</code></td>
                    <td>The content of the trigger</td>
                    <td><span class="aui-icon aui-icon-small aui-iconfont-check" role="img" aria-label="Required" />
                    </td>
                </tr>
                <tr>
                    <td><code>contentId</code></td>
                    <td>The id of the content that this trigger expands/hides</td>
                    <td><span class="aui-icon aui-icon-small aui-iconfont-check" role="img" aria-label="Required" />
                    </td>
                </tr>
                <tr>
                    <td><code>id</code></td>
                    <td>Unique id of the trigger</td>
                    <td><span class="aui-icon aui-icon-small aui-iconfont-cross" role="img" aria-label="Not required" />
                    </td>
                </tr>
                <tr>
                    <td><code>replaceText</code></td>
                    <td>Text replacement for trigger button after invokation</td>
                    <td><span class="aui-icon aui-icon-small aui-iconfont-cross" role="img" aria-label="Not required" />
                    </td>
                </tr>
                <tr>
                    <td><code>collapsible</code></td>
                    <td>Whether or not the trigger will collapse the expander after it is expanded</td>
                    <td><span class="aui-icon aui-icon-small aui-iconfont-cross" role="img" aria-label="Not required" />
                    </td>
                </tr>
                <tr>
                    <td><code>replaceSelector</code> <span class="aui-lozenge aui-lozenge-removed">deprecated</span>
                    </td>
                    <td>Selector for element which text will be replaced with the initial trigger text when it is
                        invoked</td>
                    <td><span class="aui-icon aui-icon-small aui-iconfont-cross" role="img" aria-label="Not required" />
                    </td>
                </tr>
                <tr>
                    <td><code>tag</code></td>
                    <td>This tag is used for the trigger, with the default being a button. Refer to the <a
                            href="#accessibility-reference">A11Y guidelines</a> for more information</td>
                    <td><span class="aui-icon aui-icon-small aui-iconfont-cross" role="img" aria-label="Not required" />
                    </td>
                </tr>
            </tbody>
        </table>
        <h4>Content</h4>
        <table class="aui">
            <thead>
                <tr>
                    <th>Param</th>
                    <th>Description</th>
                    <th>Required</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><code>id</code></td>
                    <td>Unique id that triggers must match to work properly</td>
                    <td><span class="aui-icon aui-icon-small aui-iconfont-check" role="img" aria-label="Required" />
                    </td>
                </tr>
                <tr>
                    <td><code>content</code></td>
                    <td>Content of the expander</td>
                    <td><span class="aui-icon aui-icon-small aui-iconfont-cross" role="img" aria-label="Not required" />
                    </td>
                </tr>
                <tr>
                    <td><code>initiallyExpanded</code></td>
                    <td>Whether or not the expander is expanded by default</td>
                    <td><span class="aui-icon aui-icon-small aui-iconfont-cross" role="img" aria-label="Not required" />
                    </td>
                </tr>
            </tbody>
        </table>
        <h4>Reveal text pattern</h4>
        <table class="aui">
            <thead>
                <tr>
                    <th>Param</th>
                    <th>Description</th>
                    <th>Required</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><code>triggerId</code></td>
                    <td>Unique id of the trigger, must match content's one</td>
                    <td><span class="aui-icon aui-icon-small aui-iconfont-check" role="img" aria-label="Required" />
                    </td>
                </tr>
                <tr>
                    <td><code>contentId</code></td>
                    <td>Unique id of the trigger, must match trigger's one</td>
                    <td><span class="aui-icon aui-icon-small aui-iconfont-check" role="img" aria-label="Required" />
                    </td>
                </tr>
                <tr>
                    <td><code>contentContent</code></td>
                    <td>The content of the contents</td>
                    <td><span class="aui-icon aui-icon-small aui-iconfont-check" role="img" aria-label="Required" />
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
    <div class="tabs-pane" id="options-css">
        <p>The expander uses css to determine a collapsed state. If you apply a <code>min-height</code> attribute to the
            <code> aui-expander-content </code> element it will collapse to this height. If no min-height is specified the
            collapsed state is completely hidden.
        </p>
        <p>An element containing the short version of the content to be shown when the content is hidden, must be placed as
            the child of a trigger.</p>
        <h4>.aui-expander-short-content <span class="aui-lozenge aui-lozenge-removed">deprecated</span></h4>
        <p>An element containing the short version of the content to be shown when the content is hidden, must be placed as the
            child of a trigger.</p>
    </div>
    <div class="tabs-pane" id="options-events">
        <p> The AUI Expander throws the following events, you can hook into these events to add your own functionality to
            the expander: </p>
        <table class="aui">
            <thead>
                <tr>
                    <th>Event</th>
                    <th>Element</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><code>aui-expander-expanded</code></td>
                    <td>aui-expander-trigger</td>
                    <td>Triggers when the content is expanded.</td>
                </tr>
                <tr>
                    <td><code>aui-expander-collapsed</code></td>
                    <td>aui-expander-trigger</td>
                    <td>Triggers when the content is hidden/partially hidden.</td>
                </tr>
            </tbody>
        </table>

        <p> You can also trigger the following events to manipulate the expander: </p>
        <table class="aui">
            <thead>
                <tr>
                    <th>Event</th>
                    <th>Element</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><code>aui-expander-invoke</code></td>
                    <td>aui-expander-trigger</td>
                    <td>invokes the trigger (collapses or expands it depending on the current state of content)</td>
                </tr>
                <tr>
                    <td><code>aui-expander-expand</code></td>
                    <td>aui-expander-trigger</td>
                    <td>expands the content</td>
                </tr>
                <tr>
                    <td><code>aui-expander-collapse</code></td>
                    <td>aui-expander-trigger</td>
                    <td>collapses the content</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<h2 id="accessibility-reference">A11Y guidelines</h2>
{{> a11y-side-note }}

<h3>Accessible trigger</h3>
<p>Use button tag for the Expander trigger. If there is a special case to change this default tag, ensure that it
remains operable with a keyboard and accessible for screen-reader users.</p>
<ul>
    <li>add <code>role="button"</code>. This attribute indicates to assistive technologies that
        the element should be treated like a button.</li>
    <li>set <code>tabindex="0"</code> on the tag. This makes the element focusable and allows it to
        be navigated to via the keyboard.</li>
</ul>

<div class="aui-message aui-message-info">
    <p class="title">
        <strong>Remember!</strong>
    </p>
    <p>The <code>tabindex="0"</code> attribute allows the custom button to be focused using
        the <code>Tab</code> key. It's important to note that you must implement functionality
        for the <code>Enter</code> and <code>Space</code> keys in your JavaScript.</p>
</div>

<div class="aui-group">
    <div class="aui-item">
        <h3>HTML</h3>
        <aui-docs-example live-demo>
            <noscript is="aui-docs-code" type="text/html">
                <a role="button" tabindex="0" data-replace-text="Read less" class="aui-expander-trigger"
                    aria-controls="regular-a11y-expander-anchor-html" id="regular-a11y-anchor-trigger">Read more</a>
                <div id="regular-a11y-expander-anchor-html" class="aui-expander-content">
                    Unearth the secrets of gourmet cuisine. From exotic recipes
                    to innovative cooking techniques, we've got it all covered.
                </div>
            </noscript>
            <noscript is="aui-docs-code" type="text/js">
                const trigger = document.querySelector('#regular-a11y-anchor-trigger')
                trigger.addEventListener('keydown', function(event) {
                    if (event.key === 'Enter' || event.key === ' ') {
                        event.preventDefault();
                        trigger.click();
                    }
                });
            </noscript>
        </aui-docs-example>
    </div>
    <div class="aui-item">
        <h3>Soy</h3>
        These modifications can be made using the <code>extraAttributes</code> parameter in the Soy template.
        <aui-docs-example>
            <noscript is="aui-docs-code" type="text/html">
                {call aui.expander.trigger}
                    {param id: 'normal-expander-trigger'/}
                    {param contentId: 'normal-expander-content'/}
                    {param content: 'Read More'/}
                    {param tag: 'a' /}
                    {param extraAttributes: [ 'tabindex': '0', 'role': 'button' ] /}
                    {param ariaLabel: 'Read More About Digital Art' /}
                {/call}

                {call aui.expander.content}
                    {param id: 'normal-expander-content'/}
                    {param content: 'Immerse yourself in the dynamic world of digital art.
                        Explore stunning creations, learn about emerging artists, and discover
                        the latest trends.'/}
                {/call}
            </noscript>
        </aui-docs-example>
    </div>
</div>

<h3>Descriptive links</h3>
<p>Non-descriptive and repetitive link texts, such as 'Read more', may lead to confusion for screen reader users.</p>
<p>There are two solutions:</p>
<ul>
    <li>Substitute 'Read more' with context-specific phrases, like 'Discover more about Earth's surface'.</li>
    <li>Use the <code>aria-label</code> attribute to assign unique and descriptive text to each button.</li>
</ul>

<div class="aui-group">
    <div class="aui-item">
        <aui-docs-example live-demo>
            <noscript is="aui-docs-code" type="text/html">
                <button data-replace-text="Read less" class="aui-expander-trigger"
                    aria-label="Read More About Technology Updates"
                    aria-controls="regular-a11y-expander-html">Read more</button>
                <div id="regular-a11y-expander-html" class="aui-expander-content">
                    Explore the fascinating world of cutting-edge technology.
                    From AI advancements to the latest in cybersecurity, stay
                    updated with our insightful articles.
                </div>
            </noscript>
        </aui-docs-example>
    </div>
    <div class="aui-item">
        <aui-docs-example>
            <noscript is="aui-docs-code" type="text/hbs">
                {call aui.expander.trigger}
                    {param contentId: 'regular-a11y-expander-soy'/}
                    {param content: 'Read more'/}
                    {param extraClasses: 'aui-button aui-button-subtle'/}
                    {param replaceText: 'Read less'/}
                    {param ariaLabel: 'Read more about modern literature' /}
                {/call}

                {call aui.expander.content}
                    {param id: 'regular-a11y-expander-soy'/}
                    {param content: 'Dive into the captivating realm of modern
                        literature. Uncover new authors, trending genres, and
                        groundbreaking narratives in our comprehensive collection.'/}
                {/call}
            </noscript>
        </aui-docs-example>
    </div>
</div>
<h3>Recommendation</h3>
{{> a11y-recommendation }}
