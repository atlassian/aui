---
component: Dark mode
status:
  api: experimental
  core: false
  wrm: com.atlassian.auiplugin:aui-design-tokens-api
  amd: false
  experimentalSince: 9.7
---
<h2 id="summary">Summary</h2>

<p>There is a library for colors in all Atlassian products - <a href="https://atlassian.design/tokens/design-tokens" target="_blank">Design Tokens</a>.
   It's bundled into AUI and components can be switched to use color tokens provided from there.</p>

<div class="aui-message aui-message-info">
    <p class="title">
        <strong>This API is experimental</strong>
    </p>
    <p>
        This API is available in AUI for testing purposes now, so you can try it and see how it works, and later it's
        going to replace old theming which will be removed from AUI completely. Design Tokens theme will be the main way
        of accessing and switching AUI color themes.

        Functions still might change, so don't rely on this exact implementation.
    </p>
</div>

<h2 id="vision">Our vision</h2>

<p> Our plan is that Design Tokens library will evolve and provide more themes and maintain colors (including contrast).
    We are going to update version of library that is bundled with AUI and make sure all AUI variables are correctly mapped to Design Tokens.</p>

<p> We expect that AUI users will use both AUI variables and Design Tokens in their code. </p>

<p> We recommend using AUI variables in UI parts that are tightly related to AUI components or custom extensions over AUI components.</p>

<p> Our approach is that we don't wrap or change Design Tokens library API in any way, since:</p>
<ul>
    <li>Library already has maintained documentation, we don't want to complicate it by creating additional source of truth</li>
    <li>Usage should also be concentrated in single location for easier maintenance and not be accessed in many places,
        so complexity is less important.</li>
</ul>
<p> At the same time, we bundle Design Tokens library in AUI to simplify and unify consumption for products,
    additionally allowing ourselves to provide extensions on top (like testing theme or automation).</p>


<h2 id="status">Status</h2>
{{> status }}


<h2 id="setup">Setup (for product developers)</h2>

<h3 id="enable-tokens">1. Enable Design Tokens API in AUI</h3>

<p>To enable Design Tokens in AUI, you should include separate entry point which provides Design tokens API. </p>

<p>
    There are two entry points available: one for minimal API that should be sufficient to enable wanted theme, and one
    with full API that might be needed in some cases (for example, to track when theme is switch using ThemeObserver).
</p>

<p> You need to chose only one - full entry includes minimal as well. </p>

<p> To enable minimal API, you should include one of those entry points in your frontend:</p>
<ul>
    <li>if you use AUI from WRM - include <b>com.atlassian.auiplugin:design-tokens-api</b> resource
    <li>if you use AUI from NPM - include <b>aui-prototyping-design-tokens-api.js</b> entry
</ul>

<p> If you would like to include full API instead, </p>
<ul>
    <li>if you use AUI from WRM - include <b>com.atlassian.auiplugin:design-tokens-api-full</b> resource
    <li>if you use AUI from NPM - include <b>aui-prototyping-design-tokens-api-full.js</b> entry
</ul>

<p>
    After resources are loaded, you should be able to access Design Tokens API functions under <code>AJS.DesignTokens</code>.
</p>
<p>
    Currently, we re-expose the following API:
</p>
<ul>
    <li>in minimal API - <i>setGlobalTheme()</i>
    <li><i>token()</i>
    <li><i>getTokenValue()</i>
    <li><i>ThemeMutationObserver</i>
</ul>
<p>
    You can find description of how they should be used in <a href="https://atlassian.design/components/tokens/code" target="_blank">Design Tokens API doc</a>.
</p>

<h3 id="activate-tokens">2. Activate Design Tokens theme</h3>

<p> This is done by simply calling <i>setGlobalTheme()</i> on client-side. This function will check if theme is loaded,
    load it if not, then activate chosen theme in application. For more details, please refer to Design Tokens
    documentation linked in step 1.</p>

<p> <i>setGlobalTheme()</i> function is the tool to control which theme is active at the moment, and it's up to you decided
    when it will be executed. However, we recommend it to use it together with ahead-of-time loading from step 2 and
    call function for the first time before DOMContentLoaded to avoid white flash. </p>

<p> Also it's up to product to implement UI for switching themes on demand and store currently selected option.
    Please, consider the case of plugin developers using this function to switch theme in application. Maybe you'll
    need to wrap it additionally to track theme switch attempts.</p>

<p> Congratulations! Now you should have tokens available on your page.</p>

<h3 id="preload-themes">3. Preload Design Token themes packed in AUI on initial page load </h3>

<p> By default, Design Tokens library is loading tokens asynchronously after <code>setGlobalTheme()</code> was called.
    This can lead to white flash and delays when the theme is enabled.</p>

<p> To avoid these problems, you can preload themes during initial page load or overall before <code>setGlobalTheme()</code> was called.</p>

<p> How to do it: </p>
<ul>
    <li>if you use AUI from WRM - include <code>com.atlassian.auiplugin:aui-design-tokens-themes</code> web-resource together with <code>com.atlassian.auiplugin:design-tokens-api</code>.</li>
    <li>
        if you use AUI from NPM - load <code>aui-prototyping-design-tokens-themes.js</code> together with <code>aui-prototyping-design-tokens-api.js</code>.
        <ul>
            <li>consider also including <code>aui-prototyping-design-tokens-compatibility.css</code>.</li>
        </ul>
    </li>
</ul>

<p> This will load themes in advance and make Design Tokens library to pick them up when API is used.</p>

<h4 id="compatibility-themes">Compatibility themes</h4>

<p>
    In <a href="https://atlassian.design/components/tokens/changelog#100">version 1.0.0 of the Design Tokens
    library</a>, several token variables have reached their end of life and were deleted. However, AUI strives for
    maximal compatibility, so it still provides definitions for the obsolete variables.
</p>

<h5>For app developers</h5>

<p>
    If your app uses <code>@atlaskit/tokens</code> below version 0.3.0, we strongly suggest you upgrade: the naming
    convention for tokens changed at this point to include the <code>--ds-</code> prefix, and the compatibility themes
    support only this naming convention, not earlier.
</p>

<p>
    If your app uses <code>@atlaskit/tokens</code> between 0.3.0 and 0.13.5, you should still consider
    upgrading. However, if your app runs in the context of a product that includes AUI, then the obsolete variables will
    resolve to their modern equivalents.
</p>

<h5>For product developers</h5>

<p>
    If you use AUI from WRM and include the <code>com.atlassian.auiplugin:aui-design-tokens-themes</code> web resource for
    theme definitions, there's no further action required: the compatibility themes are rolled into that resource.
</p>

<p>
    But if you use AUI from NPM, you should include the <code>aui-prototyping-design-tokens-compatibility.css</code>
    file on the page separately. This file contains definitions for the obsolete variables.
</p>


<h2 id="using-tokens"> Using tokens (for product and plugin developers) </h2>

<p> The goal is that all colors in your application are switched when <code>setGlobalTheme()</code> is called. </p>

<p> If some colors are hardcoded in your code, you should replace them with semantically appropriate Design Tokens.
    You can see all available tokens on
    <a href="https://atlassian.design/components/tokens/all-tokens" target="_blank">Design Token list</a>.</p>

<p> Keep in mind edge cases:
    <ul>
        <li>images</li>
        <li>canvases</li>
        <li>colors received from server</li>
    </ul>
</p>

<h3 id="tools">Tools that can accelerate you</h3>

<h4 id="codemods"> Design Token codemods </h4>
<p> Design Tokens provide <a href="https://atlassian.design/tokens/migrate-to-tokens/" target="_blank">automated
    codemods script</a> which you can execute to automatically replace most of the colors.
    Those replacements still can be not precise and require manual overview, but can speed you up a lot.</p>

<h4 id="testing-theme"> Testing theme </h4>
<p> Even after manual overview you can find yourself not sure if all colors 100% on a page use Design Tokens. </p>
<p> For that we introduced testing theme that is incorporated into AUI and can help you visually find inconsistencies on the page. </p>
<p> The idea is simple - all Design tokens will be switched to use one color, and anything that doesn't use it will be easy to identify. You can also configure color yourself through API. </p>
<p> You can access it through these functions: </p>
<ul>
    <li><code>enableTestingTheme()</code>
    <li><code>disableTestingTheme()</code>
    <li><code>toggleTestingTheme(stateBoolean)</code> - you can pass boolean to force specific state
    <li><code>setTestingThemeColor(cssColorString)</code> - you can override default color by passing custom color value. Will reset to default if nothing is passed.
</ul>
<p> Here is an example of how it looks like in Jira Data Center dashboard before migrating any Jira colors
    (only AUI Design Tokens theme enabled):</p>
<p>
    <img width="952" height="500" src="images/jira-design-tokens-testing-theme.png" alt="Showcase of design tokens testing theme in Jira"/>
</p>
<p>
    Things highlighted with pink are using Design tokens correctly, everything else should is using custom / hardcoded
    colors and should be replaced.
</p>
