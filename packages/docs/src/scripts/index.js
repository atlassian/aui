import './icons';
import './components/component';
import './components/aui-docs-code/aui-docs-code';
import './components/aui-docs-example/aui-docs-example';
import './components/aui-docs-contents/aui-docs-contents';
import './versions/versions';
import $ from 'jquery';
import auiFlag from '@atlassian/aui/src/js/aui/flag';
import auiVersion from '@atlassian/aui/src/js/aui/version';

try {
    /*global ace,dataLayer*/
    ace.analytics.Initializer.initWithPageAnalytics('bNSPpYAL9OiAkamBRvfRJoRu9YANW14I', {
        auiVersion: dataLayer[0].auiVersion,
    });
    // eslint-disable-next-line no-unused-vars
} catch (e) {}

function makeBitbucketChangelogUrl(version) {
    var baseUrl = 'https://bitbucket.org/atlassian/aui/src/master/CHANGELOG.md#markdown-header-';
    var strippedVersionNumber = version.replace(/\./g, '');
    return baseUrl + strippedVersionNumber;
}

$(function () {
    // Highlight the selected nav item.
    var url = window.location.pathname;
    var currentPage = url.substring(url.lastIndexOf('/') + 1);
    var leftPanel = document.querySelector('#left-navigation');

    if (leftPanel) {
        var pageLink = leftPanel.querySelector(`a[href$="${currentPage}"]`);
        if (pageLink) {
            pageLink.parentNode.classList.add('aui-nav-selected');
        }
    }

    console.log("Like great design and digging into the code? We're hiring! http://bit.ly/Y9xoQu");

    if (window.location.hash === '#sandboxRedirect') {
        auiFlag({
            type: 'info',
            title: "We're now using JS Bin",
            close: 'manual',
            body: 'Find code samples for each component in the documentation. If you want to hack on a component, click the "Edit in JS Bin" link next to the code sample.',
        });
    }

    // Changelog links.
    $('.aui-docs-changelog-link').attr('href', makeBitbucketChangelogUrl(auiVersion));
});
