import $ from 'jquery';
import auiFlag from '@atlassian/aui/src/js/aui/flag';

/* global Clipboard, document */
$(function () {
    if (!document.getElementById('icons-list')) {
        return;
    }

    const iconDimmedClassName = 'icon-list-dimmed';

    function renderIconList(iconNames, container) {
        container.innerHTML = '';

        iconNames.forEach(function (icon) {
            const iconSpan = document.createElement('span');
            const classes = `aui-icon aui-icon-small aui-iconfont-${icon}`;

            iconSpan.className = classes;
            iconSpan.role = 'img';
            iconSpan.setAttribute('aria-label', `${icon} icon`);

            const iconSpanApiHtml = `<span class="${classes}" role="img" aria-label="Insert meaningful text here for accessibility" />`;

            iconSpan.setAttribute('data-clipboard-text', iconSpanApiHtml);
            iconSpan.title = icon; // used for tooltip
            iconSpan.dataset.title = icon; // used for search
            container.appendChild(iconSpan);

            $(iconSpan).tooltip();
        });
    }

    $.ajax('../assets/icons-list.json').then(function (icons) {
        renderIconList(icons, document.getElementById('icons-list'));

        const allIconElements = Array.prototype.slice.call(
            document.getElementById('icons-list').querySelectorAll('.aui-icon')
        );

        function iconSearchHandler(e) {
            const query = e.target.value.toLowerCase();

            allIconElements.forEach(function (icon) {
                const iconName = icon.dataset.title;
                const shouldHighlightIcon = query === '' || iconName.includes(query);

                if (shouldHighlightIcon) {
                    icon.classList.remove(iconDimmedClassName);
                } else {
                    icon.classList.add(iconDimmedClassName);
                }
            });
        }

        document.getElementById('search-icons').addEventListener('input', iconSearchHandler);

        const clipboard = new Clipboard('[data-clipboard-text]');

        clipboard.on('success', function (e) {
            auiFlag({
                type: 'success',
                title: 'Copied to clipboard.',
                body: e.text + ' has been copied to the clipboard.',
                close: 'auto',
            });
        });

        clipboard.on('error', function () {
            auiFlag({
                type: 'warning',
                title: 'Icon copying failed to load.',
                body: 'There was a problem copying the icon to the clipboard',
                close: 'auto',
            });
        });
    });
});
