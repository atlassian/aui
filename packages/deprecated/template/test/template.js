const template = require('../src/template');
const $ = require('jquery');
const { expect } = require('chai');
const sinon = require('sinon');

describe('aui/template', function () {
    it('handles html escaping correctly', function () {
        const tmp = template('Hello, {name}. Welcome to {application}.<br>');

        expect(
            tmp
                .fill({
                    name: '"O\'Foo"',
                    application: '<JIRA & Confluence>',
                })
                .toString()
        ).to.equal('Hello, &quot;O&#39;Foo&quot;. Welcome to &lt;JIRA &amp; Confluence&gt;.<br>');
    });

    if (typeof window !== 'undefined') {
        describe('browser security', function () {
            let alerter;
            beforeEach(function () {
                alerter = sinon.stub(window, 'alert');
            });
            afterEach(function () {
                alerter.restore();
            });

            it('does not execute scripts', function () {
                const tpl = '<div class="a-legit-element">{body}</div>';
                const badValues = {
                    'body:html': 'one <script>alert(2)</script> three',
                };
                const parsedHtml = template(tpl).fill(badValues).toString();
                $(parsedHtml).appendTo('#test-fixture');
                expect(alerter.callCount).to.equal(0);
            });
        });
    }
});
