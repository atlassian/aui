const { cacheIdentity } = require('./build/lib/cacher');

/* eslint-env node */
module.exports = function (api) {
    api.cache.invalidate(() => cacheIdentity());

    return {
        presets: [['@babel/env', { debug: true, bugfixes: true }]],
    };
};
