# 7.10.2

* [Documentation](https://docs.atlassian.com/aui/7.10/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.10.2)

This version contains all changes from up to 7.9.11 that were skipped in 7.10.1

## Fixed

* RestfulTable autoFocus on CreateRow event not turning off (AUI-5048)
* AUI Toggle component rendering in IE11 (AUI-5074)

## Security

* XSS vulnerability in dropdown component (AUI-5275)
* All XSS vulnerabilities fixed up to 7.9.11

# 7.10.1

* [Documentation](https://docs.atlassian.com/aui/7.10/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.10.1)

## Fixed

* Fixed CVE-2010-5312 in jquery-ui dialog.
* Can now update min or max date for datepicker dynamically.

# 7.10.0

* [Documentation](https://aui.atlassian.com/aui/7.10/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.10.0)

## Upgrade notes

* If you use `AJS.template`, `AJS.messages`, `AJS.flag`, or `AJS.banner`, and are passing `<script>` tags in to their
  html properties, you should convert the scripts to separate discrete files and load them in an alternate way.

## Added

* The "app-switcher" icon glyph was redesigned.

## Changed

* The `AJS.template` function will strip `<script>` tags from any html property it fills in.
* `AJS.messages`, `AJS.flag`, and `AJS.banner` will strip `<script>` tags from their respective body html attributes.
* A disabled AUI button has an alpha-transparent background instead of a solid colour.
* The `aui-buttons` web-resource will now pull in the code to
  make [spinners in buttons](https://aui.atlassian.com/aui/7.10/docs/buttons.html#button-spinners) work.
* The icon glyphs for "approve", "check-circle-filled", "link", "link-filled", "search", and "unlink" were revised.
* Removed deprecation warning for the `aui-iconfont-appswitcher` and `aui-iconfont-app-switcher` CSS classes.

## Fixed

* Spinner elements no longer appears over the top of other layered elements, such as a flag or dialog.
* Toggle elements are more resilient to mutation events.
* Closing an AUI message no longer fires the close event multiple times.
* The close icon in an AUI Select2 now renders correctly.

# 7.9.9

* [Documentation](https://aui.atlassian.com/aui/7.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.9.9)

## Fixed

* Calling `jQuery.fn.spin` now works in IE 11.

# 7.9.8

## Upgrade notes

* This release is the exact same as 7.9.7. It pays to push changes before releasing :P

# 7.9.7

* [Documentation](https://aui.atlassian.com/aui/7.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.9.7)

## Fixed

* Tooltips on sidebar links correctly hide and will not prevent clicking content underneath them.
    * Tooltips on sidebar links will once again flicker when hovering over the link's badge. The bug will be re-opened.

# 7.9.6

* [Documentation](https://aui.atlassian.com/aui/7.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.9.6)

## Fixed

* Tooltips on sidebar links will not flicker when hovering over the link's badge.
* The resources are minified again in the P2 plugin.

# 7.9.5

* [Documentation](https://aui.atlassian.com/aui/7.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.9.5)

## Fixed

* When the (deprecated) `jQuery.fn.spinStop()` function is called, it also cleans up its cached data.

# 7.9.4

* [Documentation](https://aui.atlassian.com/aui/7.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.9.4)

## Added

* The spinner documentation page now has more usage examples of `<aui-spinner>`.
* The button documentation page demonstrates use of its `busy` state.

## Changed

* `<aui-spinner>` is used directly in the `busy` state of the button component.
* `<aui-spinner>` is used directly in AUI toggle's asynchronous variant.

## Deprecated

* The `filled` attribute of `<aui-spinner>` is deprecated. The element should be positioned using CSS.

# 7.9.3

* [Documentation](https://aui.atlassian.com/aui/7.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.9.3)

## Changed

* The (deprecated) `jQuery.fn.spin()` function now creates the `<aui-spinner>` element synchronously.
    * This means the element's attributes and prototype can be inspected and affected immediately after calling the
      function.

## Fixed

* An `<aui-spinner>` element will correctly position itself after multiple renders or when re-attached to the DOM.
* Spinners with no (or an invalid) size attribute will render at the default `medium` size.
* Calling the (deprecated) `jQuery.fn.spin(true)` will create a spinner with a `small` size.

# 7.9.2

* [Documentation](https://aui.atlassian.com/aui/7.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.9.2)

## Changed

* The `<aui-spinner>` element's colour can be changed by changing the CSS `color` property of the element.
* The `<aui-progressbar>` element now caches valid numeric values for its `value` property even if they exceed the `max`
  . This cached value will be used if `max` is updated to accommodate it. This is in line with the HTML5 `<progress>`
  element behaviour.
* The (deprecated) `jQuery.fn.spin` method can now change the spinner's `size` to `small`, `medium`, or `large`.

## Fixed

* `RestfulTable`'s `comparator` function (for sorting rows) now runs with the correct `this` context.
* `RestfulTable.EntryModel` no longer appends `undefined` to the URL for deleting a row.
* Setting the `value` property before `max` on an `<aui-progressbar>` now works.
* Calling `jQuery.fn.spin('small|medium|large')` correctly sets the spinner's size.
* Calling `jQuery.fn.spin({ size: 'small|medium|large' })` correctly sets the spinner's size.

# 7.9.1

* [Documentation](https://aui.atlassian.com/aui/7.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.9.1)

## Changed

* The (deprecated) `AJS.progressBars.update` method:
    * Now accepts an HTML element or bare ID as per the documentation examples.
    * Will trigger a visual refresh when updating to a value that was already set in markup.

## Fixed

* Documentation fixes:
    * The Buttons page lists the correct CSS class to add to a cancel button.
    * Typing in to the search bar on the iconography page once again visually dims non-matching icons.

# 7.9.0

* [Documentation](https://aui.atlassian.com/aui/7.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.9.0)

## Highlights

* AUI's various drop down menus have been updated to reflect the latest [ADG Server](https://atlassian.design/server)
  guidelines.

## Added

* The `<aui-item-link>`, `<aui-item-checkbox>` and `<aui-item-radio>` elements now allow setting an `item-id` attribute
  to place an `id` on the nested interactive element.

## Changed

* Items in AUI's various drop down menus are now a minimum of 30px tall.
* Sub-menu triggers are represented by a chevron glyph instead of a faux arrow.
* Sections in `<aui-dropdown-menu>` elements now have whitespace between them.

## Fixed

* Shadows of AUI's various drop down menus and dialogs will render more visibly in IE 11.
* Single selects with placeholder values will show their dropdown lists when clicked in IE 11.
* AUI's buttons now correctly compute their line-height value to be an effective 30px.
* Re-introduced ability to call `jQuery(element).spin(false)` to hide a spinner. This is equivalent to
  calling `jQuery(element).spinStop()`.
* Spinners inside an asynchronous drop down are now positioned correctly.
* Spinner and progress bar animations work correctly when the minified distribution files are used.

# 7.8.3

* [Documentation](https://aui.atlassian.com/aui/7.8/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.8.3)

## Fixed

* Calling `jQuery.fn.spin` now works in IE 11.

# 7.8.2

* [Documentation](https://aui.atlassian.com/aui/7.8/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.8.2)

## Changed

* The `<aui-spinner>` element's colour can be changed by changing the CSS `color` property of the element.
* The `<aui-progressbar>` element now caches valid numeric values for its `value` property even if they exceed the `max`
  . This cached value will be used if `max` is updated to accommodate it. This is in line with the HTML5 `<progress>`
  element behaviour.
* The (deprecated) `jQuery.fn.spin` method can now change the spinner's `size` to `small`, `medium`, or `large`.
* The (deprecated) `AJS.progressBars.update` method:
    * Now accepts an HTML element or bare ID as per the documentation examples.
    * Will trigger a visual refresh when updating to a value that was already set in markup.

## Fixed

* Setting the `value` property before `max` on an `<aui-progressbar>` now works.
* Calling `jQuery.fn.spin('small|medium|large')` correctly sets the spinner's size.
* Calling `jQuery.fn.spin({ size: 'small|medium|large' })` correctly sets the spinner's size.

# 7.8.1

* [Documentation](https://aui.atlassian.com/aui/7.8/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.8.1)

## Fixed

* Spinner and progress bar animations work correctly when the minified distribution files are used.

# 7.8.0

* [Documentation](https://aui.atlassian.com/aui/7.8/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.8.0)

## Highlights

* AUI's use of colour and typography have been updated to reflect the
  latest [ADG Server](https://atlassian.design/server) guidelines.
* AUI's tables patterns have been updated to match the [ADG Server](https://atlassian.design/server) guidelines.

## Added

* A new `aui-table-list` class has been added for styling tables. See
  the [tables](https://aui.atlassian.com/aui/7.8/docs/tables.html) page for an example.
* There are now explicit web-resource keys for a few components and patterns:
    * Basic form field CSS: `com.atlassian.auiplugin:aui-forms`
    * Basic table CSS: `com.atlassian.auiplugin:table`
    * The Tabs pattern: `com.atlassian.auiplugin:tabs`

## Changed

* Table header styles now match the ADG Server guidelines.
* Avatar images now take up 100% of the element's available width and height, stretching non-square images as necessary.
* The AUI P2 plugin is
  now [transformerless](https://developer.atlassian.com/server/framework/atlassian-sdk/configuration-of-instructions-in-atlassian-plugins/)
  , so should install and start a bit faster.
* The AUI P2 plugin requires Spring Scanner `2.0.0` or higher in order to run.

## Fixed

* AUI's legacy toolbar applies the same height to `<a>`, `<input>`, and `<button>` elements with the `.toolbar-trigger`
  class applied to them.
* Avatars using SVG for their image content will now render in IE 11.

# 7.7.6

* [Documentation](https://aui.atlassian.com/aui/7.7/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.7.6)

## Fixed

* Calling `jQuery.fn.spin` now works in IE 11.

# 7.7.5

* [Documentation](https://aui.atlassian.com/aui/7.7/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.7.5)

## Changed

* The `<aui-spinner>` element's colour can be changed by changing the CSS `color` property of the element.
* The `<aui-progressbar>` element now caches valid numeric values for its `value` property even if they exceed the `max`
  . This cached value will be used if `max` is updated to accommodate it. This is in line with the HTML5 `<progress>`
  element behaviour.
* The (deprecated) `jQuery.fn.spin` method can now change the spinner's `size` to `small`, `medium`, or `large`.
* The (deprecated) `AJS.progressBars.update` method:
    * Now accepts an HTML element or bare ID as per the documentation examples.
    * Will trigger a visual refresh when updating to a value that was already set in markup.

## Fixed

* Setting the `value` property before `max` on an `<aui-progressbar>` now works.
* Calling `jQuery.fn.spin('small|medium|large')` correctly sets the spinner's size.
* Calling `jQuery.fn.spin({ size: 'small|medium|large' })` correctly sets the spinner's size.

# 7.7.4

## Upgrade notes

* This release is broken. Use 7.7.5 instead.

# 7.7.3

* [Documentation](https://aui.atlassian.com/aui/7.7/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.7.3)

## Fixed

* Spinner and progress bar animations work correctly when the minified distribution files are used.

# 7.7.2

* [Documentation](https://aui.atlassian.com/aui/7.7/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.7.2)

## Fixed

* Spinner now works in IE 11.
* Toggle button's `change` event fires in IE 11.
* Toggle button no longer fires a `change` event when it is first rendered.

# 7.7.1

* [Documentation](https://aui.atlassian.com/aui/7.7/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.7.1)

## Changed

* The colour usage in step names in the multi-step progress tracker have been tweaked.
* A light shade of grey was removed from the multi-step progress tracker's progress bar.

## Fixed

* AUI's Select2 version will now work in an environment with Content Security Policy (CSP) enabled.

# 7.7.0

* [Documentation](https://aui.atlassian.com/aui/7.7/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.7.0)

## Highlights

* Several of AUI's components have been updated to match the [ADG Server](https://atlassian.design/server) guidelines,
  including:
    * [Progress indicator](https://aui.atlassian.com/aui/7.7/docs/progress-indicator.html)
    * [Progress tracker](https://aui.atlassian.com/aui/7.7/docs/progress-tracker.html)
    * [Spinner](https://aui.atlassian.com/aui/7.7/docs/spinner.html)

## Added

* The AUI progress indicator is implemented as a web component: `<aui-progressbar>`.
* The AUI spinner is implemented as a web component: `<aui-spinner>`.

## Changed

* The sidebar now always renders inside a `requestAnimationFrame` callback, improving initial render time.
* The page footer now renders Atlassian's new brand logo.
* The Toggle Button element's `defaultValue` property now remains constant, which makes "dirty form" checking work.
* Styles for hyperlinks are now independently consumable from the page layout pattern:
    * (In CSS) via the `lib/css/aui-link.css` file; or
    * (In P2) via the `com.atlassian.auiplugin:aui-link` web-resource.
* Styles for `.aui-group` and `.aui-item` are now independently consumable from the page layout pattern:
    * (In CSS) via the `lib/css/aui-group.css` file; or
    * (In P2) via the `com.atlassian.auiplugin:aui-group` web-resource.

## Fixed

* AUI's [buttons](https://aui.atlassian.com/aui/7.7/docs/buttons.html) documentation was updated to reference how to set
  them as "busy".
* AUI's [toggle button](https://aui.atlassian.com/aui/7.7/docs/toggle-button.html) now fires a "change" event when its
  value changes.

## Deprecated

* Use of `AJS.progressBars.update` and `AJS.progressBars.setIndeterminate` are deprecated.
* Use of the `jquery.throbber` plugin via AUI is deprecated.
* (To reiterate a 6.1.0 deprecation notice): Consuming the Atlassian brand logos via AUI has been deprecated.
  Use [@atlassian/brand-logos](https://www.npmjs.com/package/@atlassian/brand-logos) instead.

## Removed

* The [spin.js](https://github.com/fgnass/spin.js) library is no longer used by AUI and has been removed.
    * As a consequence, a global `Spinner` function is no longer provided by AUI.
* The `jquery.spin` plugin is no longer used by AUI and has been removed.

## Upgrade notes

* Read the upgrade guide for a checklist of things to review in your code when updating to this version of AUI.

# 7.6.2

* [Documentation](https://aui.atlassian.com/aui/7.6/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.6.2)

## Fixed

* The order of buttons in sidebar footer and disappearing sidebar when scrolling.

# 7.6.0

* [Documentation](https://aui.atlassian.com/aui/7.6/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.6.0)

## Highlights

* AUI's sidebar, vertical navigation, and horizontal navigation patterns have been updated to match
  the [ADG Server](https://atlassian.design/server) guidelines.

## Upgrade notes

* Read the upgrade guide for a checklist of things to review in your own CSS when updating to this version of AUI.

# 7.5.1

* [Documentation](https://aui.atlassian.com/aui/7.5/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion%20IN%20(7.5.0%2C%207.5.1))

## Highlights

* AUI's iconography has been updated to match the [ADG Server](https://atlassian.design/server) guidelines.
* AUI's icon fonts declare their @font-face separately from their usage.

## Changed

* Chevrons in the application header's menu items have been updated.

## Deprecated

* Some icon class names were deprecated, as they are no longer used in Atlassian products. Check the upgrade guide for
  details.

## Upgrade notes

* Read the upgrade guide for information about how the iconography changes may affect you.

# 7.5.0

## Upgrade notes

This release is broken. Please use 7.5.1 instead.

# 7.4.0

* [Documentation](https://aui.atlassian.com/aui/7.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.4.0)

## Highlights

* AUI's avatar component has been updated to match the [ADG Server](https://atlassian.design/server) guidelines.
* AUI's tab component has been updated to match the [ADG Server](https://atlassian.design/server) guidelines.
* Primary buttons in the application header have been adjusted to match
  the [ADG Server](https://atlassian.design/server) guidelines.

## Fixed

* Badge components in the application header have an updated contrast so they can be seen.
* Non-primary buttons in the application header have an updated contrast so they can be seen.

## Removed

* Support for Internet Explorer versions 9 and 10 has been removed. IE 11 is now the oldest supported version.

## Upgrade notes

* Read the upgrade guide for a checklist of things to review in your own CSS when updating to this version of AUI.

# 7.3.4

* [Documentation](https://aui.atlassian.com/aui/7.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.2.4)

## Fixed

* A modal dialog's height will resize fluidly with the browser instead of jumping to explicit fixed heights.

# 7.3.3

* [Documentation](https://aui.atlassian.com/aui/7.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion%20IN%20(7.3.2%2C%207.3.3))

## Changed

* Links in an `.aui-nav-actions-list` now have a bullet between them.
* Modal dialog heading text size has shrunk slightly to accommodate the longer text we see in them. Y'all are more
  verbose than this changelog line!
* Modal dialog and most AUI components now have a consistent border-radius of 3px.

## Fixed

* Inline dialog components have seen the light; they cast a shadow once again.
* Hint text in modal dialog footers is more subtle so as not to distract from the dialog's contents.
* The `"browser"` field in AUI's Node package points to the right file path now, so things like webpack and [unpkg.com]
  will load the correct `aui.js` batched file by default.

# 7.3.2

## Upgrade notes

This release is broken. Please use 7.3.3 instead.

# 7.3.1

* [Documentation](https://aui.atlassian.com/aui/7.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project%3DAUI%20and%20fixVersion%20IN%20(7.3.0%2C%207.3.1))

## Highlights

* AUI's modal dialog patterns have been updated to match the [ADG Server](https://atlassian.design/server) guidelines.
* AUI's flag and message patterns have been updated to match the [ADG Server](https://atlassian.design/server)
  guidelines.
* The AUI Soy templates for use in JavaScript are now compiled via Webpack.

## Fixed

* The checksum for AUI 7.3.0's Node package was incorrect and preventing installation. This has been fixed.

# 7.3.0

## Upgrade notes

This release is broken. Please use 7.3.1 instead.

# 7.2.0

* [Documentation](https://aui.atlassian.com/aui/7.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.2.0)

## Highlights

* AUI's Button and Toolbar pattern have been updated to match the [ADG Server](https://atlassian.design/server)
  guidelines.
* The buttons in Dialog1 and file upload fields have also been updated to match AUI's Button pattern.

# 7.1.0

* [Documentation](https://aui.atlassian.com/aui/7.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.1.0)

## Highlights

* The [ADG Server](https://atlassian.design/server) colour and typography foundations have been applied to AUI.

## Added

* The `badge` component has a new `primary` sub-type.

## Upgrade notes

* Read the upgrade guide for a checklist of things to review in your own CSS when updating to this version of AUI.

# 7.0.0

* [Documentation](https://aui.atlassian.com/aui/7.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=7.0.0)

## Upgrade notes

* AUI has been re-licensed under
  the [Atlassian Developer Terms](https://developer.atlassian.com/platform/marketplace/atlassian-developer-terms/).

## Changed

* The AUI and AUI-ADG repositories have been merged in to a single repository.
* The build process for AUI has been drastically simplified.
