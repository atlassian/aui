const process = require('process');
const pkg = require('../../package.json');

function cacheIdentity(extra = {}) {
    const { NODE_ENV, BABEL_ENV, BROWSERSLIST_ENV, NO_CACHE } = process.env;

    if (NO_CACHE) {
        return '' + Date.now();
    }
    return JSON.stringify({
        NODE_ENV,
        BABEL_ENV,
        BROWSERSLIST_ENV,
        pkg,
        extra,
    });
}

module.exports = {
    cacheIdentity,
};
