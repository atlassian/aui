var soy = require('./soy');

function compileArgs(soyOpts) {
    var params = {
        basedir: soyOpts.basedir || soyOpts.baseDir,
        outdir: soyOpts.outdir || soyOpts.outDir,
        type: soyOpts.render || 'render',
        data: soyOpts.data,
        i18n: soyOpts.i18nBundle,
        extension: soyOpts.extension || soyOpts.outputExtension,
        rootnamespace: soyOpts.rootnamespace || soyOpts.rootNamespace,
    };

    if (soyOpts.dependencies) {
        params.dependencies = soyOpts.dependencies
            .map((dep) => `${dep.baseDir}:${dep.glob}`)
            .join(',');
    }

    return params;
}

module.exports = function soyCli(opts) {
    let cmdArgParts = compileArgs(opts);

    return {
        compile: function (glob) {
            return new Promise(function (resolve, reject) {
                try {
                    soy(
                        {
                            args: Object.assign({}, cmdArgParts, { glob }),
                        },
                        function (err) {
                            console.log('soy cli completed');

                            if (err) {
                                reject(err);
                            } else {
                                resolve();
                            }
                        }
                    );
                } catch (e) {
                    reject(e);
                }
            });
        },
    };
};
