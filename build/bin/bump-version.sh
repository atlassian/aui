#!/usr/bin/env bash
#set -x
#set -e

PACKAGES=( "@atlassian/aui" )
PACKAGES+=( "@atlassian/aui-docs" )
PACKAGES+=( "@atlassian/aui-p2-plugin" )
PACKAGES+=( "@atlassian/aui-p2-harness" )
PACKAGES+=( "@atlassian/aui-workspace" )
PACKAGES+=( "@atlassian/aui-flatapp" )
PACKAGES+=( "@atlassian/aui-integration-test-suite" )
PACKAGES+=( "@atlassian/aui-soy" )

self=${0##*/}
usage=" USAGE: ${self} [--dry] <dir> <version>
  <dir>     : relative path to location where the code is checked out
  <version> : version of the release

    A utility function for bumping the versions of our deliverables in our monorepo

  Options:
    --dry <dry_run> : {Boolean} if <dry_run> set to true - logs actions but does not perform them.
  "

#--[ Process Arguments ]----
while [ "${1:0:1}" == "-" ]; do
  case ${1:1} in
    -dry)
      dry_run="${2}"
      shift # past argument
    ;;
    h|-help)
      echo "$usage"
      exit 0
    ;;
    *)    # unknown option
      echo "Unknown argument: -${1}"
      echo "$usage"
      exit 0
    ;;
  esac
  shift # past argument or value
done

if [ -n "$dry_run" ]; then
  if [ "$dry_run" != "true" ]; then
    unset dry_run
  else
    echo "DRY run mode enabled."
  fi
fi

if [ ${#@} -lt 2 ]; then echo "$usage"; exit 1; fi

CHECKOUT_DIR="${1}"
version="${2}"

# Set and check the working directory
cd "$CHECKOUT_DIR"

CHECKOUT_DIR=$(pwd)
auidir="${CHECKOUT_DIR}"

if [ -z "${dry_run}" ]; then
  echo "Updating changelog for ${version}"
  npx --yes @atlassian/aui-ci-tools updateChangelogForVersion . ${version}
else
  echo "DRY: Updating changelog for ${version}"
  npx --yes @atlassian/aui-ci-tools updateChangelogForVersion . ${version} --dryRun=true
fi

for package in "${PACKAGES[@]}"
do
  if [ -z "${dry_run}" ]; then
    echo "Bumping ${package} to ${version}"
    yarn yarn-bump --dir "${auidir}" --new-version "${version}" --package "${package}"
  else
    echo "DRY: Bumping ${package} to ${version}"
    echo "yarn yarn-bump --dir ${auidir} --new-version ${version} --package ${package}"
  fi
done

cd "${auidir}/p2"
if [ -z "${dry_run}" ]; then
  echo "Bumping P2 plugin to ${version}"
  mvn versions:set --batch-mode -DnewVersion="${version}" -DgenerateBackupPoms=false
else
  echo "DRY: Bumping P2 plugin to ${version}"
  echo "mvn versions:set --batch-mode -DnewVersion=${version} -DgenerateBackupPoms=false"
fi
