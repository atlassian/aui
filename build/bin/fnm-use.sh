#!/bin/sh

attempts=5

success=0

for i in $(seq 1 $attempts); do
    fnm use --install-if-missing && success=1 && break
    echo "Install ($i) failed. Retrying in 5 seconds..."
    sleep 5
done

if [ $success -ne 1 ]; then
    echo "Failed to install fnm after $attempts attempts."
    exit 1
fi
