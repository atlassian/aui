#!/bin/sh

if ! [ -e test-reports/test-report.xml ]
then
    echo "There is no such file"
    exit 1;
elif grep -q "failures=\"[1-9]" "./test-reports/test-report.xml"
then
    echo "There are failed tests"
    exit 1;
else
    echo "Success"
fi
