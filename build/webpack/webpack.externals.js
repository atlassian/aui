const jqueryExternal = {
    jquery: {
        commonjs: 'jquery',
        commonjs2: 'jquery',
        amd: 'jquery',
        root: 'jQuery',
        assign: 'jQuery',
        global: 'jQuery',
        var: 'jQuery',
    },
};

const underscoreExternal = {
    underscore: {
        commonjs: 'underscore',
        commonjs2: 'underscore',
        amd: 'underscore',
        root: '_',
        assign: '_',
        global: '_',
        var: '_',
    },
};

const backboneExternal = {
    backbone: {
        commonjs: 'backbone',
        commonjs2: 'backbone',
        amd: 'backbone',
        root: 'Backbone',
        assign: 'Backbone',
        global: 'Backbone',
        var: 'Backbone',
    },
};

module.exports = {
    backboneExternal,
    jqueryExternal,
    underscoreExternal,
};
