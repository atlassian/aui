const { oklch } = require('culori');

// The switcher in look-and-feel relies on the perceptual lightness of the input color and picks #1
// for the lighter value, or #2 for the darker. The colors that are normally fit on the darker
// background in the light mode (e.g. --ds-text-inverse) will have their channels reversed for the
// correct visual results.
const LIGHT_THEN_DARK_ORDER = 'light-then-dark';
const DARK_THEN_LIGHT_ORDER = 'dark-then-light';

const tokensToExtract = {
    '--ds-text': LIGHT_THEN_DARK_ORDER,
    '--ds-background-neutral-hovered': LIGHT_THEN_DARK_ORDER,
    '--ds-background-neutral-subtle-pressed': LIGHT_THEN_DARK_ORDER,
    '--ds-text-inverse': DARK_THEN_LIGHT_ORDER,
    '--ds-icon': LIGHT_THEN_DARK_ORDER,
};

// @atlaskit/tokens doesn't have an API for giving us color values in a
// particular theme, so we parse them out of the css it gives us, relying
// on the predictable format of `--ds-<abc>: <value>;` declarations.
function extractCssVariables(str) {
    const varRecord = {};
    const regex = /(--[\w-]+):\s*(.+?);/gm;

    let match;

    while ((match = regex.exec(str)) !== null) {
        varRecord[`${match[1]}`] = match[2];
    }

    return varRecord;
}

function roundDecimals(num, decimals = 2) {
    const factor = Math.pow(10, decimals);
    return Math.round(num * factor) / factor;
}

function createExtractedColorChannelsFileContents(bundleThemeStyles) {
    // We assume that colorRecords is an array of two items: the first for the light theme, the
    // second for dark.
    const colorRecords = bundleThemeStyles.map((themeStyles) => {
        const { css } = themeStyles;
        return extractCssVariables(css);
    });

    const values = Object.entries(tokensToExtract).flatMap((entry) => {
        const [token, mode] = entry;

        let records = [...colorRecords];
        if (mode === DARK_THEN_LIGHT_ORDER) {
            records = records.reverse();
        }

        return records.map((colorRecord, themeIndex) => {
            const color = colorRecord[token];
            const culoriResult = oklch(color);
            const { l, c, h } = culoriResult;
            const a = 'alpha' in culoriResult ? culoriResult.alpha : 1;

            const number = themeIndex + 1;
            return {
                token,
                number,
                l: roundDecimals(l),
                c: roundDecimals(c),
                h: roundDecimals(h),
                a: roundDecimals(a),
            };
        });
    });

    const result = `
:root {
    ${values
        .flatMap((value) => {
            const { token, number } = value;

            const base = `--auix${token}`;
            return ['l', 'c', 'h', 'a'].map((component) => {
                return `${base}-${component}${number}: ${value[component]};`;
            });
        })
        .join('\n    ')}
}
    `;

    return result;
}

module.exports = {
    createExtractedColorChannelsFileContents,
};
