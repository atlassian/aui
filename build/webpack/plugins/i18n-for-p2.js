const i18n = require('../../lib/i18n');
const fs = require('fs');

class I18nForP2 {
    apply(compiler) {
        compiler.hooks.emit.tapAsync('I18nForP2', (compilation, callback) => {
            i18n({}, (results) => {
                results.map((file) => {
                    compilation.assets[`i18n/${file.filename}`] = {
                        source() {
                            return fs.readFileSync(file.filepath, 'utf8');
                        },
                        size() {
                            return fs.statSync(file.filepath).size;
                        },
                    };
                });
                callback();
            });
        });
    }
}

module.exports = I18nForP2;
