const path = require('path');
const { DefinePlugin } = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { cacheIdentity } = require('../lib/cacher');
const project = require('../../package.json');
const generateFile = require('generate-file-webpack-plugin');
const { createExtractedColorChannelsFileContents } = require('./extractColorChannels');

const { NODE_ENV } = process.env;

const namedFilesCfg = {
    filename: 'assets/[name][ext]',
};

exports.setAuiVersion = () => ({
    plugins: [
        new DefinePlugin({
            AUI_VERSION: JSON.stringify(project.version || 'UNKNOWN'),
        }),
    ],
});

exports.transpileJs = ({ include, exclude, options } = {}) => ({
    module: {
        rules: [
            {
                test: /\.js$/i,
                include,
                exclude,
                use: {
                    loader: 'babel-loader',
                    options: Object.assign(
                        {
                            cacheDirectory: true,
                            cacheIdentifier: cacheIdentity(options),
                            configFile: path.resolve(__dirname, '../../babel.config.js'),
                        },
                        options
                    ),
                },
            },
        ],
    },
});

exports.transpileJsWithCodeCoverage = ({ include, exclude, options } = {}) => ({
    module: {
        rules: [
            {
                test: /\.js$/i,
                include,
                exclude,
                use: {
                    loader: 'babel-loader',
                    options: Object.assign(
                        {
                            cacheDirectory: true,
                            cacheIdentifier: cacheIdentity(options),
                            configFile: path.resolve(__dirname, '../../babel.config.js'),
                        },
                        options
                    ),
                },
            },
            {
                test: /\.js$/i,
                include,
                exclude: [/(entry|node_modules|js-vendor|tests|\.spec\.ts$)/],
                enforce: 'post',
                use: {
                    loader: '@jsdevtools/coverage-istanbul-loader',
                    options: {
                        compact: true,
                        esModules: true,
                    },
                },
            },
        ],
    },
});

exports.extractCss = ({ include, exclude, options } = {}) => {
    const extractCSS = new MiniCssExtractPlugin(options);

    return {
        module: {
            rules: [
                {
                    test: /\.(?:less|css)$/i,
                    include,
                    exclude,

                    use: [
                        {
                            loader: MiniCssExtractPlugin.loader,
                            options: {
                                publicPath: '',
                            },
                        },
                        'css-loader',
                        {
                            loader: require.resolve('postcss-loader'),
                            options: {
                                postcssOptions: {
                                    plugins: ['autoprefixer'],
                                },
                            },
                        },
                        'less-loader',
                    ],
                },
            ],
        },

        plugins: [extractCSS],
    };
};

const lfExtractedColorChannelsFileName = 'aui-prototyping-lf-extracted-color-channels.css';

// Function to receive theme styles is provided as callback to avoid direct dependency on Design Tokens in this package
exports.extractThemesIntoJSAndCSSFiles = async ({
    outputDir,
    importMapName,
    themesCssBundleFileName,
    themesJsBundleFileName,
    getThemeStyles,
}) => {
    const designTokenThemeIdsToBundle = require('../../packages/core/configs/token-themes-to-bundle');

    const allThemeStyles = await getThemeStyles('all');
    const bundleThemeStyles = allThemeStyles.filter((themeObject) =>
        designTokenThemeIdsToBundle.includes(themeObject.id)
    );

    const getThemeRelativePath = (themeStyles, extension = true) =>
        `./themes/${themeStyles.id}${extension ? '.js' : ''}`;

    const createThemeFileContents = (themeStyles) => {
        return `export default \`${themeStyles.css}\``;
    };

    const createThemesJsBundleContents = (bundleThemeStyles) => {
        let content = '';

        content += 'let style;\n';
        content += bundleThemeStyles
            .map((theme) => {
                let content = '';

                content += `\n\n/* ------ THEME: ${theme.id} ------ */\n\n`;

                /*
                 * The code is taken from @atlaskit/tokens/src/utils/theme-loading.tsx:16
                 * For easier maintenance it can be extracted into separate function
                 */
                content += "style = document.createElement('style');\n";
                content += `style.dataset.theme = '${theme.id}';\n`;
                content += `style.textContent += require('${getThemeRelativePath(theme)}').default;\n`;
                content += 'document.head.appendChild(style);\n';

                return content;
            })
            .join('\n');

        content += '\n\n/* ------ THEME: original ------ */\n\n';
        content += "style = document.createElement('style');\n";
        content += "style.dataset.theme = 'original';\n";
        content += "style.textContent += '';\n";
        content += 'document.head.appendChild(style);\n';

        return content;
    };
    const createThemesCSSBundleContents = (bundleThemeStyles) => {
        let content = '';
        content += bundleThemeStyles
            .map((theme) => {
                let content = '';

                content += `\n\n/* ------ THEME: ${theme.id} ------ */\n\n`;

                content += theme.css;
                return content;
            })
            .join('\n');

        return content;
    };

    const createThemeImportMapContents = (allThemeStyles) => {
        let content = '';

        content += 'export default {\n';
        content += allThemeStyles
            .map((themeStyles) => {
                let content = '';

                content += `\t'${themeStyles.id}': () => import(\n`;
                content += `\t\t/* webpackChunkName: "${getThemeRelativePath(themeStyles, false)}" */\n`;
                content += `\t\t'${getThemeRelativePath(themeStyles)}'\n`;
                content += '\t),\n';

                return content;
            })
            .join('');
        content += "\t'original': () => Promise.resolve(''),\n";
        content += '};';

        return content;
    };

    return {
        plugins: [
            ...allThemeStyles.map((themeStyles) =>
                generateFile({
                    file: path.join(outputDir, `${getThemeRelativePath(themeStyles)}`),
                    content: createThemeFileContents(themeStyles),
                })
            ),
            generateFile({
                file: path.join(outputDir, importMapName),
                content: createThemeImportMapContents(allThemeStyles),
            }),
            generateFile({
                file: path.join(outputDir, themesCssBundleFileName),
                content: createThemesCSSBundleContents(bundleThemeStyles),
            }),
            generateFile({
                file: path.join(outputDir, themesJsBundleFileName),
                content: createThemesJsBundleContents(bundleThemeStyles),
            }),
            generateFile({
                file: path.join(outputDir, lfExtractedColorChannelsFileName),
                content: createExtractedColorChannelsFileContents(bundleThemeStyles),
            }),
        ],
    };
};

exports.inlineCss = () => {
    return {
        module: {
            rules: [
                {
                    test: /\.(?:less|css)$/i,
                    use: ['style-loader', 'css-loader', 'less-loader'],
                },
            ],
        },
    };
};

exports.loadFonts = ({ generator } = {}) => {
    const generatorOptions = Object.assign({}, namedFilesCfg, generator);
    return {
        module: {
            rules: [
                {
                    test: /\.(eot|otf|ttf|woff|woff2)$/,
                    type: 'asset/resource',
                    generator: generatorOptions,
                },
            ],
        },
    };
};

exports.loadImages = ({ generator, parser } = {}) => {
    const generatorOptions = Object.assign({}, namedFilesCfg, generator);
    let imageMatcher = /\.(png|jpg|gif|svg)$/;
    let brandAndIconMatcher = /[\\/](?:.*?-logos|logos|fonts)[\\/]/;

    return {
        module: {
            rules: [
                {
                    test: imageMatcher,
                    exclude: brandAndIconMatcher,
                    type: 'asset',
                    generator: generatorOptions,
                    parser,
                },
                {
                    test: imageMatcher,
                    include: brandAndIconMatcher,
                    type: 'asset/resource',
                    generator: generatorOptions,
                },
            ],
        },
    };
};

exports.loadSoy = ({ include, exclude, options } = {}) => ({
    module: {
        rules: [
            {
                test: /\.soy$/,
                include,
                exclude,
                use: {
                    loader: '@atlassian/soy-loader',
                    options,
                },
            },
        ],
    },
    plugins: [
        new DefinePlugin({
            'goog.DEBUG': false, // will allow dead code removal of debugging code from generated templates
        }),
    ],
});

exports.resolveSoyDeps = () => ({
    module: {
        rules: [
            {
                test: require.resolve(
                    '@atlassian/soy-template-plugin-js/dist/js/atlassian-deps.js'
                ),
                use: [
                    {
                        loader: require.resolve('./loaders/expose-exports-loader'),
                        options: 'atl_soy',
                    },
                ],
            },
            {
                test: require.resolve('@atlassian/soy-template-plugin-js/dist/js/soyutils.js'),
                use: [
                    {
                        loader: require.resolve('./loaders/expose-exports-loader'),
                        options: 'soy,soydata,goog',
                    },
                ],
            },
        ],
    },
});

exports.production = (mode = NODE_ENV) => {
    const definePlugin = new DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify(mode),
        },
    });

    if (mode !== 'production') {
        return {
            mode: 'development',
            devtool: 'cheap-module-source-map',

            plugins: [definePlugin],
        };
    }

    const TerserWebpackPlugin = require('terser-webpack-plugin');
    const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');

    return {
        mode: 'production',
        devtool: 'source-map',

        optimization: {
            minimize: true,
            usedExports: false,

            minimizer: [
                new TerserWebpackPlugin({
                    parallel: true,
                    extractComments: false,
                    terserOptions: {
                        mangle: {
                            properties: false,
                            reserved: ['I18n', 'getText'], // Don't mangle usage of I18n.getText so the transformer can detect it
                        },
                    },
                }),
                new CssMinimizerPlugin({
                    minimizerOptions: {
                        processorOptions: {
                            safe: true,
                            discardComments: {
                                removeAll: true,
                            },
                        },
                    },
                }),
            ],
        },

        plugins: [definePlugin],
    };
};
