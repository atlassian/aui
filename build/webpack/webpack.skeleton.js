const { merge } = require('webpack-merge');
const parts = require('./webpack.parts');
const libraryExternals = require('./webpack.externals');

const librarySkeleton = merge([
    parts.setAuiVersion(),

    parts.transpileJs({
        exclude: /(node_modules|bower_components|js-vendor|compiled-soy)/,
    }),

    parts.loadSoy(),
    parts.resolveSoyDeps(),

    parts.loadFonts({
        generator: {
            filename: 'fonts/[name][ext]',
        },
    }),

    parts.loadImages({
        generator: {
            filename: 'images/[name][ext]',
        },
        parser: {
            dataUrlCondition: {
                maxSize: 3 * 1024, // 3kb
            },
        },
    }),

    parts.extractCss(),

    parts.production(),
]);

module.exports = {
    librarySkeleton,
    libraryExternals,
};
